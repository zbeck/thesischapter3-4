using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using MyExtensions;

namespace AgentDraw
{
    [Serializable]
    class Distribution2D
    {
        Distribution1D[] cols;
        Distribution1D rowsampler;

        public double ExpectedVal { get { return rowsampler.ExpectedVal; } }

        public Distribution2D(Bitmap densityMap)
        {
            double[][] density = new double[densityMap.Size.Width][];
            for (int x = 0; x < densityMap.Size.Width; x++)
            {
                density[x] = new double[densityMap.Size.Height];
                for (int y = 0; y < densityMap.Size.Height; y++)
                {
                    density[x][y] = densityMap.GetPixel(x, y).GetBrightness();
                }
            }
            BuildDistributions(density);
        }

        public Distribution2D(double[][] density)
        {
            BuildDistributions(density);
        }

        void BuildDistributions(double[][] density)
        {
            cols = new Distribution1D[density.Length];
            for (int x = 0; x < density.Length; x++)
            {
                cols[x] = new Distribution1D(density[x]);
            }
            BuildRowSampler();
        }

        void BuildRowSampler()
        {
            double[] colDensity = new double[cols.Length];
            for (int x = 0; x < cols.Length; x++)
            {
                colDensity[x] = cols[x].ExpectedVal;
            }
            rowsampler = new Distribution1D(colDensity);
        }

        public double[][] GetDensity()
        {
            double[][] density = new double[cols.Length][];
            for (int i = 0; i < cols.Length; i++)
                density[i] = cols[i].GetDensity();
            return density;
        }

        /// <summary>
        /// Samples the 2D discrete distribution
        /// </summary>
        /// <param name="rnd">2 element random number array between 0 and 1</param>
        /// <returns>2 element index array of the sample {x, y}</returns>
        public int[] Sample(double[] rnd)
        {
            if (rnd.Length < 2)
                throw (new Exception("Distribution2D needs at least two random number input"));
            int[] sampleCoord = new int[2];
            sampleCoord[0] = rowsampler.Sample(rnd[0]);
            sampleCoord[1] = cols[sampleCoord[0]].Sample(rnd[1]);
            return sampleCoord;
        }

        internal int[] Mean()
        {
            int[] meanCoord = new int[2];
            meanCoord[0] = rowsampler.Mean();
            double sumMean = 0;
            foreach (Distribution1D col in cols)
            {
                sumMean += col.ExpectedVal * col.Mean();
            }
            meanCoord[1] = (int)Math.Round(sumMean / ExpectedVal);
            return meanCoord;
        }


        internal void Scale(double p)
        {
            foreach (Distribution1D c in cols)
            {
                c.Scale(p);
            }
            BuildRowSampler();
        }

        internal void ClearRectangle(Point center, int radius)
        {
            int xmin = (center.X - radius < 0) ? 0 : center.X - radius;
            int xmax = (center.X + radius > cols.Length) ? cols.Length : center.X + radius;
            for (int x = xmin; x < xmax; x++)
            {
                cols[x].ClearSegment(center.Y, radius);
            }
            BuildRowSampler();
        }

        internal void ClearOutsideRectangles(List<Rectangle> rectangles)
        {
            for (int x = 0; x < cols.Length; x++)
            {
                for (int y = 0; y < cols[x].Length; y++)
                {
                    if (!MyExtensionMethods.IsInsideRectangles(rectangles, x, y))
                        cols[x].ClearPoint(y);
                }
            }
            BuildRowSampler();
        }

        internal double ExpectedValueForIntersection(List<Rectangle> rectangles)
        {
            double E = 0;
            for (int x = 0; x < cols.Length; x++)
            {
                for (int y = 0; y < cols[x].Length; y++)
                {
                    if (MyExtensionMethods.IsInsideIntersection(rectangles, x, y))
                        E += cols[x].GetPointValue(y);
                }
            }
            return E;
        }

        internal double GetExpectedValueForRectangle(Rectangle rect)
        {
            double E = 0;
            for (int x = rect.Left; x < rect.Right; x++)
            {
                E += cols[x].GetSegmentValue(rect.Top, rect.Bottom);
            }
            return E;
        }

        internal double[] GetGradient(Point loc)
        {
            int index = Math.Max(0, loc.X - 1);
            int num2 = Math.Min(this.cols.Length, loc.X + 1);
            double[,] M = new double[3, 3];
            M[0, 0] = this.cols[index].GetPointValue(loc.Y + 1);
            M[0, 1] = this.cols[loc.X].GetPointValue(loc.Y + 1);
            M[0, 2] = this.cols[num2].GetPointValue(loc.Y + 1);
            M[1, 0] = this.cols[index].GetPointValue(loc.Y);
            M[1, 2] = this.cols[num2].GetPointValue(loc.Y);
            M[2, 0] = this.cols[index].GetPointValue(loc.Y - 1);
            M[2, 1] = this.cols[loc.X].GetPointValue(loc.Y - 1);
            M[2, 2] = this.cols[num2].GetPointValue(loc.Y - 1);
            return new double[] { (((M[0, 0] + (2.0 * M[1, 0])) + M[2, 0]) - ((M[0, 2] + (2.0 * M[1, 2])) + M[2, 2])), (((M[0, 0] + (2.0 * M[0, 1])) + M[0, 2]) - ((M[2, 0] + (2.0 * M[2, 1])) + M[2, 2])) };
        }
    }
}
