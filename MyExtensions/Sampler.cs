﻿using MyExtensions;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace AgentDraw
{
    [Serializable]
    public class Sampler : IDeserializationCallback
    {
        Distribution2D distribution;
        [NonSerialized]
        public Random rndSeed;

        public double ExpectedVal
        {
            get { return distribution.ExpectedVal; }
            set { distribution.Scale(value / distribution.ExpectedVal); }
        }

        public Sampler(Bitmap densityMap)
        {
            rndSeed = new Random();
            distribution = new Distribution2D(densityMap);
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="source">copy source</param>
        public Sampler(Sampler source)
        {
            rndSeed = new Random();
            distribution = new Distribution2D(source.distribution.GetDensity());
        }

        public void RefreshBitmap(Bitmap bmp)
        {
            double[][] density = distribution.GetDensity();
            double maxinv = 255 / density.Max<double[]>(e => e.Max());
            for (int x = 0; x < bmp.Size.Width; x++)
                for (int y = 0; y < bmp.Size.Height; y++)
                {
                    int den = (int)(density[x][y] * maxinv);
                    bmp.SetPixel(x, y, Color.FromArgb(den, den, den));
                }
        }

        /// <summary>
        /// Samples a single location in the distribution
        /// </summary>
        /// <returns>Point of location</returns>
        protected Point SampleLocation()
        {
            double[] rnd = new double[2] { rndSeed.NextDouble(), rndSeed.NextDouble() };
            int[] index = distribution.Sample(rnd);
            return new Point(index[0], index[1]);
        }

        /// <summary>
        /// Samples points according to the whole distribution.
        /// </summary>
        /// <returns>Point list: random sample of the distribution</returns>
        public List<Point> SampleLocations()
        {
            List<Point> plist = new List<Point>();
            int nofTasks = GetPoisson();
            for (int i = 0; i < nofTasks; i++)
                plist.Add(SampleLocation());
            return plist;
        }

        /// <summary>
        /// Samples points according to the whole distribution.
        /// </summary>
        /// <returns>Point list: random sample of the distribution</returns>
        public List<PointF> SampleLocationsF()
        {
            List<PointF> plist = new List<PointF>();
            int nofTasks = GetPoisson();
            for (int i = 0; i < nofTasks; i++)
                plist.Add(SampleLocation());
            return plist;
        }

        public Point GetMeanLocation()
        {
            int[] index = distribution.Mean();
            return new Point(index[0], index[1]);
        }

        public double GetGradientDirection(Point location)
        {
            double[] gradient = this.distribution.GetGradient(location);
            if ((gradient[0] == 0.0) && (gradient[1] == 0.0))
            {
                return MyExtensionMethods.StopAngle;
            }
            return Math.Atan2(gradient[1], -gradient[0]);
        }

        public void ClearRectangle(Point center, int radius)
        {
            distribution.ClearRectangle(center, radius);
        }

        public void ClearOutsideRectangles(List<Rectangle> rects)
        {
            distribution.ClearOutsideRectangles(rects);
        }

        public double GetExpectedValueForRectangle(Rectangle rect)
        {
            return distribution.GetExpectedValueForRectangle(rect);
        }

        public double GetExpectedValueInIntersection(List<Rectangle> rects)
        {
            return distribution.ExpectedValueForIntersection(rects);
        }

        public int GetPoisson()
        {
            return MyExtensions.MyExtensionMethods.GetPoisson(ExpectedVal, rndSeed);
        }

        public void OnDeserialization(object sender)
        {
            rndSeed = new Random();
        }
    }
}
