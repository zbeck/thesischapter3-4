﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AgentDraw
{
    [Serializable]
    class Distribution1D
    {
        double[] cdf;
        public double ExpectedVal { get { return cdf.Last<double>(); } }
        public int Length { get { return cdf.Length; } }

        public Distribution1D(double[] density)
        {
            cdf = new double[density.Length];
            double sumVal = 0.0;
            for (int i = 0; i < cdf.Length; i++)
            {
                sumVal += density[i];
                cdf[i] = sumVal;
            }
        }

        /// <summary>
        /// Samples the 1D discrete distribution
        /// </summary>
        /// <param name="rnd">random number between 0 and 1.0</param>
        /// <returns>resulting element's index</returns>
        public int Sample(double rnd)
        {
            return FindIndex(rnd * ExpectedVal);
        }

        protected int FindIndex(double val)
        {
            int index = Array.BinarySearch<double>(cdf, val);
            if (index < 0)
                index = Math.Abs(index + 1);
            if (index >= cdf.Length)
                index = cdf.Length - 1;
            return index;
        }

        internal void Scale(double p)
        {
            for (int i = 0; i < cdf.Length; i++)
                cdf[i] *= p;
        }

        internal void ClearSegment(int center, int radius)
        {
            int xmin = (center - radius < 0) ? 0 : center - radius;
            int xmax = (center + radius > cdf.Length) ? cdf.Length : center + radius;
            double valmin = (xmin == 0) ? 0.0 : cdf[xmin - 1];
            double valdiff = cdf[xmax] - valmin;
            for (int x = xmin; x < cdf.Length; x++)
            {
                if (x <= xmax)
                    cdf[x] = valmin;
                else //Had issues with negative densities resulting from rounding errors.
                    cdf[x] = Math.Max(valmin, cdf[x] - valdiff);
            }
        }

        internal void ClearPoint(int coord)
        {
            double valmin = (coord == 0) ? 0.0 : cdf[coord - 1];
            double valdiff = cdf[coord] - valmin;
            if (valdiff == 0)
                return;
            cdf[coord] = valmin;
            for (int x = coord + 1; x < cdf.Length; x++)
            {
                cdf[x] -= valdiff;
            }
        }

        internal double[] GetDensity()
        {
            double[] density = new double[cdf.Length];
            double prev = 0.0;
            for (int i = 0; i < cdf.Length; i++)
            {
                density[i] = cdf[i] - prev;
                prev = cdf[i];
            }
            return density;
        }

        internal double GetPointValue(int y)
        {
            if ((y < 0) || (y > Length))
                return 0;
            else if (y == 0)
                return cdf[0];
            else
                return cdf[y] - cdf[y - 1];
        }

        internal double GetSegmentValue(int y0, int y1)
        {
            if (y0 > y1)
            {
                int tmp = y1;
                y1 = y0;
                y0 = tmp;
            }
            y0 = (y0 < 0) ? 0 : y0;
            y0 = (y0 > Length) ? Length : y0;
            y1 = (y1 < 0) ? 0 : y1;
            y1 = (y1 > Length) ? Length : y1;
            double val0 = (y0 == 0) ? 0 : cdf[y0];
            double val1 = (y1 == 0) ? 0 : cdf[y1];
            return val1 - val0;
        }

        internal int Mean()
        {
            double meanVal = ExpectedVal / 2;
            return FindIndex(meanVal);
        }
    }
}
