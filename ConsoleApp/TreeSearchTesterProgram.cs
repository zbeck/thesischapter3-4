﻿using AgentDraw;
using csmatio.io;
using csmatio.types;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreeSearchSolver;

namespace ConsoleApp
{
    public static class TreeSearchTesterProgram
    {
        public static void Tester(string[] args)
        {
            //double[] factors = new double[] { 0.0, 2.0, 4.0, 6.0, 8.0, 10.0, 12.0, 14.0, 16.0, 18.0, 20.0, 22.0, 24.0 };
            //double[] factors = new double[] { 26.0, 28.0, 30.0, 32.0, 34.0, 36.0 };
            //double[] factors = new double[] { -1.0 };
            double[] factors = new double[] { -1.0, 0.0, 2.0, 4.0, 6.0, 8.0, 10.0, 15.0, 20.0, 25.0, 30.0, 40.0, 50.0, 60.0 };
            int nProblems = 100;
            int searchTasksRemaining = 20;
            int[] stasks = new int[] { searchTasksRemaining };
            int nSolutions;
            string filename;
            bool isHaiti = false;
            int mode = 6;
            int fileId = 28;
            if (args.Length > 1)
            {
                mode = Int32.Parse(args[0]);
                fileId = Int32.Parse(args[1]);
            }
            switch (mode)
            {
                case 0:
                    nProblems = 100;
                    nSolutions = 10000;
                    isHaiti = false;
                    filename = "factortestStat";
                    break;
                case 1:
                    nProblems = 100;
                    nSolutions = 10000;
                    isHaiti = true;
                    filename = "factortestStat";
                    break;
                case 2:
                    nProblems = 100;
                    nSolutions = 40;
                    isHaiti = false;
                    filename = "factortestExpandChance";
                    break;
                case 3:
                    nProblems = 100;
                    nSolutions = 40;
                    isHaiti = true;
                    filename = "factortestExpandChance";
                    break;
                case 4:
                    nProblems = 1000;
                    nSolutions = 10000;
                    isHaiti = false;
                    filename = "factortestTopStat";
                    break;
                case 5:
                    nProblems = 1000;
                    nSolutions = 10000;
                    isHaiti = true;
                    filename = "factortestTopStat";
                    break;
                case 6:
                    nSolutions = 50;
                    isHaiti = false;
                    stasks = new int[] { 60, 50, 40, 30, 20, 10, 5 };
                    filename = "factortestTopExpandChance";
                    break;
                case 7:
                    nSolutions = 50;
                    isHaiti = true;
                    stasks = new int[] { 60, 50, 40, 30, 20, 10, 5 };
                    filename = "factortestTopExpandChance";
                    break;
                case 8:
                    nProblems = 1000;
                    nSolutions = 10000;
                    searchTasksRemaining = 20;
                    isHaiti = false;
                    filename = "factortestProgressStat";
                    break;
                case 9:
                    nProblems = 1000;
                    nSolutions = 10000;
                    searchTasksRemaining = 20;
                    isHaiti = true;
                    filename = "factortestProgressStat";
                    break;
                default:
                    return;
            }
            filename += fileId.ToString();
            if (isHaiti)
                filename += "haiti";
            filename += ".mat";
            Console.WriteLine(filename);
            //if (args.Length > 3)
            //{
            //    factors = new double[args.Length - 3];
            //    for (int i = 0; i < args.Length - 3; i++)
            //        factors[i] = Double.Parse(args[i]);
            //    nProblems = Int32.Parse(args[args.Length - 3]);
            //    nSolutions = Int32.Parse(args[args.Length - 2]);
            //    filename = args.Last();
            //}
            //DebugDominantChanges(@"d:\VisualStudio\AgentDraw\IRIDIS\ajka\models07-19task\ajka7_model000.mus", 10);
            //BatchDominantEvaluation();
            //TestTreeSearch();
            //TestGuidedSearchFactor(new double[] { 7.0, 8.0, 9.0, 10.0}, "factortest3.mat");
            switch (mode)
            {
                case 0:
                case 1:
                    TestStatisticalGuidedSearchFactorBottomReusingProblems(factors, nProblems, nSolutions, filename, isHaiti);
                    break;
                case 2:
                case 3:
                    TestGuidedSearchFactorBottomNewSolutionChance(factors, nProblems, nSolutions, 10000, filename, isHaiti);
                    break;
                case 4:
                case 5:
                    TestStatisticalGuidedSearchFactorTop(factors, nProblems, nSolutions, filename, isHaiti);
                    break;
                case 6:
                case 7:
                    TestGuidedSearchFactorTopNewSolutionChance(stasks, factors, nSolutions, 10000, filename, isHaiti);
                    break;
                case 8:
                case 9:
                    TestStatisticalGuidedSearchFactorTopAfterProgress(searchTasksRemaining, factors, nProblems, nSolutions, filename, isHaiti);
                    break;
            }
            //TestMCSearch_compareSolutionPolicy(args);
            //TestMCSearch_increase_value(args, "increaseStats2.mat");
            //Console.ReadKey();
            Console.WriteLine("\nAll done.");
            //Console.ReadKey();
        }

        private static void TestMCSearch_increase_value(string[] args, string logfilename)
        {
            if (args.Length == 0)
                args = "20 10 100 10 300".Split(' ');
            List<int> cmd = new List<int>(args.Length);
            foreach (string arg in args)
                cmd.Add(Int32.Parse(arg));
            ConsoleProg program = new ConsoleProg("", "", false, "");
            Sampler sam = program.OpenBeliefSampler(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\ajkaPreSampler.sam", System.IO.FileMode.Open));
            SimModel model = program.OpenFile(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\ajkaModels07pre-20tasks\model000.mus", System.IO.FileMode.Open));
            Stopwatch sw = new Stopwatch();
            sw.Start();
            MCSearch tree = new MCSearch(model, sam, cmd.Take(3));
            sw.Stop();
            Console.WriteLine("Initial tree growth ({1} {2} {3}) took {0} s", sw.ElapsedMilliseconds / 1000.0, cmd[0], cmd[1], cmd[2]);
            List<double[][]> stats = tree.Test4(cmd.Skip(3));
            tree = null;
            List<MLArray> mlist = new List<MLArray>(3);
            mlist.Add(MatProcess(stats[0], "topexplog"));
            mlist.Add(MatProcess(stats[1], "midexplog"));
            mlist.Add(MatProcess(stats[2], "botexplog"));
            MatFileWriter mfw = new MatFileWriter(logfilename, mlist, true);
        }

        private static void TestMCSearch_normal(string[] args, string logfilename)
        {
            if (args.Length == 0)
                args = "20 5 20 10 100 2".Split(' ');
            List<int> cmd = new List<int>(args.Length);
            foreach (string arg in args)
                cmd.Add(Int32.Parse(arg));
            ConsoleProg program = new ConsoleProg("", "", false, "");
            Sampler sam = program.OpenBeliefSampler(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\ajkaPreSampler.sam", System.IO.FileMode.Open));
            SimModel model = program.OpenFile(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\ajkaModels07pre-20tasks\model000.mus", System.IO.FileMode.Open));
            Stopwatch sw = new Stopwatch();
            sw.Start();
            MCSearch tree = new MCSearch(model, sam, cmd.Take(3));
            sw.Stop();
            Console.WriteLine("Initial tree growth ({1} {2} {3}) took {0} s", sw.ElapsedMilliseconds / 1000.0, cmd[0], cmd[1], cmd[2]);
            tree.Test3(cmd.Skip(3), logfilename);
        }

        private static void TestMCSearch_compareSolutionPolicy(string[] args)
        {
            //GroupData.DefaultSolutionPolicy = GroupSoltion.NewSolutionPolicyType.FlatRandom;
            //TestMCSearch_normal(args, "logFlat.csv");
            GroupData.DefaultSolutionPolicy = GroupSoltion.NewSolutionPolicyType.GuidedRandom;
            TestMCSearch_normal(args, "logVoIUpdate.csv");
        }

        private static void TestMCSearch_preserveTree(string[] args)
        {
            if (args.Length == 0)
                args = "20 5 20 10 500 2".Split(' ');
            List<int> cmd = new List<int>(args.Length);
            foreach (string arg in args)
                cmd.Add(Int32.Parse(arg));
            ConsoleProg program = new ConsoleProg("", "", false, "");
            Sampler sam = program.OpenBeliefSampler(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\ajkaGTSampler.sam", System.IO.FileMode.Open));
            SimModel model = program.OpenFile(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\ajkaModels07pre-20tasks\model000.mus", System.IO.FileMode.Open));
            MCSearch tree = new MCSearch(model, sam, cmd.Take(3));
            tree.Test2(cmd.Skip(3));
        }

        private static void TestTreeSearch()
        {
            ConsoleProg program = new ConsoleProg("", "", false, "");
            List<Sampler> sList = new List<Sampler>(1);
            sList.Add(program.OpenBeliefSampler(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\ajkaPreSampler.sam", System.IO.FileMode.Open)));
            SimModel model = program.OpenFile(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\ajkaModels07pre-20tasks\model000.mus", System.IO.FileMode.Open));
            TreeStructure tree = new TreeStructure(model, sList);
            Stopwatch sw = new Stopwatch();
            sw.Start();
            double[][] datab = tree.TestBottomExpand(100000);
            sw.Stop();
            Console.WriteLine(String.Format("Bottom took {0}s", sw.ElapsedMilliseconds / 1000.0));
            sw.Restart();
            double[][] datam = tree.TestMidExpand(100000);
            sw.Stop();
            Console.WriteLine(String.Format("Middle took {0}s", sw.ElapsedMilliseconds / 1000.0));
            sw.Restart();
            double[][] datat = tree.TestTopExpand(100, 1000);
            sw.Stop();
            Console.WriteLine(String.Format("Top took {0}s", sw.ElapsedMilliseconds / 1000.0));
            ExportToMat(datab, "lowlevelStat10.mat", "sols");
            ExportToMat(datam, "midlevelStat10.mat", "sols");
            ExportToMat(datat, "hilevelStat10.mat", "sols");
        }

        private static void TestStatisticalGuidedSearchFactorBottom(double[] factors, int problemSamples, int solutions, string fname)
        {
            ConsoleProg program = new ConsoleProg("", "", false, "");
            List<Sampler> sList = new List<Sampler>(1);
            sList.Add(program.OpenBeliefSampler(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\ajkaPreSampler.sam", System.IO.FileMode.Open)));
            SimModel model = program.OpenFile(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\ajkaModels07pre-20tasks\model000.mus", System.IO.FileMode.Open));
            TreeStructure tree;
            double[][][] datab = new double[factors.Length][][];
            Stopwatch sw = new Stopwatch();
            int i = 0;
            foreach (double factor in factors)
            {
                GroupSoltion.SolverPowerFactor = factor;
                tree = new TreeStructure(model, sList);
                sw.Restart();
                datab[i] = tree.TestBottomExpandStatistics(solutions, problemSamples);
                sw.Stop();
                Console.WriteLine(String.Format("Bottom took {0}s", sw.ElapsedMilliseconds / 1000.0));
                i++;
            }
            tree = null;
            List<MLArray> mlist = new List<MLArray>(5);
            mlist.Add(MatProcess(factors, "factors"));
            mlist.Add(MatProcess(datab, "datab"));
            MatFileWriter mfw = new MatFileWriter(fname, mlist, true);
        }

        private static void TestStatisticalGuidedSearchFactorTopAfterProgress(int searchTasksRemaining, double[] factors, int problemSamples, int solutions, string fname, bool isHaiti)
        {
            ConsoleProg program = new ConsoleProg("", "", false, "");
            Sampler sampler;
            SimModel model;
            if (isHaiti)
            {
                sampler = program.OpenBeliefSampler(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\haiti10pcPreSampler.sam", System.IO.FileMode.Open));
                model = program.OpenFile(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\haitiModels11-61tasks\model000.mus", System.IO.FileMode.Open));
                SimController.timeStep = 10;
            }
            else
            {
                sampler = program.OpenBeliefSampler(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\ajkaPreSampler.sam", System.IO.FileMode.Open));
                model = program.OpenFile(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\ajkaModels07pre-20tasks\model000.mus", System.IO.FileMode.Open));
                SimController.timeStep = 30;
            }
            //Run decomposed HOP till search tasks are only searchTasksRemaining
            Debug.Assert(searchTasksRemaining > 0);
            int fixedExplorationSteps = 0;
            bool problemChanged = true;
            DualProblem lastProblem = null;
            int nofSteps = 0;
            while (model.SearchTaskCount > searchTasksRemaining)
            {
                List<List<double>> directions;
                List<List<int>> assignedIds;
                DualProblem problem;
                if (nofSteps > 0)
                    fixedExplorationSteps = 1;
                problem = new DualProblem(model as SimModel, sampler, lastProblem, problemChanged, fixedExplorationSteps);
                problem.Solve(0, 100, out assignedIds, out directions);
                problemChanged = program.Controller.SimulateOneStepWithExternalResults(directions, assignedIds);
                lastProblem = problem;
                nofSteps++;
            }
            //now it's searchTasksRemaining or less tasks left
            List<Sampler> sList = new List<Sampler>(1){sampler};
            TreeStructure tree = new TreeStructure(model, sList);
            double[][][] datab = tree.TestTopExpandMultipleFactors(ref factors, problemSamples, solutions);
            List<MLArray> mlist = new List<MLArray>(5);
            mlist.Add(MatProcess(factors, "factorst"));
            mlist.Add(MatProcess(datab, "datat"));
            MatFileWriter mfw = new MatFileWriter(fname, mlist, true);
        }

        private static void TestGuidedSearchFactorBottomNewSolutionChance(double[] factors, int problemSamples, int solutions, int tries, string fname, bool isHaiti)
        {
            ConsoleProg program = new ConsoleProg("", "", false, "");
            List<Sampler> sList = new List<Sampler>(1);
            SimModel model;
            if (isHaiti)
            {
                sList.Add(program.OpenBeliefSampler(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\haiti10pcPreSampler.sam", System.IO.FileMode.Open)));
                model = program.OpenFile(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\haitiModels11-61tasks\model000.mus", System.IO.FileMode.Open));
            }
            else
            {
                sList.Add(program.OpenBeliefSampler(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\ajkaPreSampler.sam", System.IO.FileMode.Open)));
                model = program.OpenFile(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\ajkaModels07pre-20tasks\model000.mus", System.IO.FileMode.Open));
            } 
            TreeStructure tree = new TreeStructure(model, sList);
            double[][][] datab = tree.TestBottomExpansionSuccess(factors, problemSamples, solutions, tries);
            List<MLArray> mlist = new List<MLArray>(5);
            mlist.Add(MatProcess(factors, "factors"));
            mlist.Add(MatProcess(datab, "datab"));
            MatFileWriter mfw = new MatFileWriter(fname, mlist, true);
        }

        private static void TestGuidedSearchFactorTopNewSolutionChance(int[] searchTasksRemaining, double[] factors, int solutions, int tries, string fname, bool isHaiti)
        {
            ConsoleProg program = new ConsoleProg("", "", false, "");
            Sampler sampler;
            SimModel model;
            if (isHaiti)
            {
                sampler = program.OpenBeliefSampler(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\haiti10pcPreSampler.sam", System.IO.FileMode.Open));
                model = program.OpenFile(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\haitiModels11-61tasks\model000.mus", System.IO.FileMode.Open));
                SimController.timeStep = 10;
            }
            else
            {
                sampler = program.OpenBeliefSampler(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\ajkaPreSampler.sam", System.IO.FileMode.Open));
                model = program.OpenFile(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\ajkaModels07pre-20tasks\model000.mus", System.IO.FileMode.Open));
                SimController.timeStep = 30;
            }
            double[][][] datat = new double[searchTasksRemaining.Length][][];
            int fixedExplorationSteps = 0;
            bool problemChanged = true;
            DualProblem lastProblem = null;
            int nofSteps = 0;
            for(int i=0;i<searchTasksRemaining.Length;i++)
            {
                int stasklimit = searchTasksRemaining[i];
                Console.WriteLine(stasklimit);
                //Run decomposed HOP till search tasks are only searchTasksRemaining
                Debug.Assert(stasklimit > 0);
                while (model.SearchTaskCount > stasklimit)
                {
                    List<List<double>> directions;
                    List<List<int>> assignedIds;
                    DualProblem problem;
                    if (nofSteps > 0)
                        fixedExplorationSteps = 1;
                    problem = new DualProblem(model as SimModel, sampler, lastProblem, problemChanged, fixedExplorationSteps);
                    problem.Solve(0, 100, out assignedIds, out directions);
                    problemChanged = program.Controller.SimulateOneStepWithExternalResults(directions, assignedIds);
                    lastProblem = problem;
                    nofSteps++;
                }
                //now it's searchTasksRemaining or less tasks left
                List<Sampler> sList = new List<Sampler>(1) { sampler };
                TreeStructure tree = new TreeStructure(model, sList);
                datat[i] = tree.TestTopExpansionSuccess(factors, solutions, tries);
            }
            List<MLArray> mlist = new List<MLArray>(5);
            mlist.Add(MatProcess(factors, "factors"));
            mlist.Add(MatProcess(searchTasksRemaining, "ntasks"));
            mlist.Add(MatProcess(datat, "datat"));
            MatFileWriter mfw = new MatFileWriter(fname, mlist, true);
        }

        private static void TestStatisticalGuidedSearchFactorBottomReusingProblems(double[] factors, int problemSamples, int solutions, string fname, bool isHaiti)
        {
            ConsoleProg program = new ConsoleProg("", "", false, "");
            List<Sampler> sList = new List<Sampler>(1);
            SimModel model;
            if (isHaiti)
            {
                sList.Add(program.OpenBeliefSampler(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\haiti10pcPreSampler.sam", System.IO.FileMode.Open)));
                model = program.OpenFile(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\haitiModels11-61tasks\model000.mus", System.IO.FileMode.Open));
            }
            else
            {
                sList.Add(program.OpenBeliefSampler(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\ajkaPreSampler.sam", System.IO.FileMode.Open)));
                model = program.OpenFile(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\ajkaModels07pre-20tasks\model000.mus", System.IO.FileMode.Open));
            }
            TreeStructure tree = new TreeStructure(model, sList);
            double[][][] datab = tree.TestBottomExpandMultipleFactors(factors, problemSamples, solutions);
            List<MLArray> mlist = new List<MLArray>(5);
            mlist.Add(MatProcess(factors, "factors"));
            mlist.Add(MatProcess(datab, "datab"));
            MatFileWriter mfw = new MatFileWriter(fname, mlist, true);
        }

        private static void TestStatisticalGuidedSearchFactorTop(double[] factors, int problemSamples, int solutions, string fname, bool isHaiti)
        {
            ConsoleProg program = new ConsoleProg("", "", false, "");
            List<Sampler> sList = new List<Sampler>(1);
            SimModel model;
            if (isHaiti)
            {
                sList.Add(program.OpenBeliefSampler(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\haiti10pcPreSampler.sam", System.IO.FileMode.Open)));
                model = program.OpenFile(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\haitiModels11-61tasks\model000.mus", System.IO.FileMode.Open));
            }
            else
            {
                sList.Add(program.OpenBeliefSampler(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\ajkaPreSampler.sam", System.IO.FileMode.Open)));
                model = program.OpenFile(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\ajkaModels07pre-20tasks\model000.mus", System.IO.FileMode.Open));
            }
            TreeStructure tree = new TreeStructure(model, sList);
            double[][][] datab = tree.TestTopExpandMultipleFactors(ref factors, problemSamples, solutions);
            List<MLArray> mlist = new List<MLArray>(5);
            mlist.Add(MatProcess(factors, "factorst"));
            mlist.Add(MatProcess(datab, "datat"));
            MatFileWriter mfw = new MatFileWriter(fname, mlist, true);
        }

        private static void TestGuidedSearchFactor(double[] factors, string fname)
        {
            ConsoleProg program = new ConsoleProg("", "", false, "");
            List<Sampler> sList = new List<Sampler>(1);
            sList.Add(program.OpenBeliefSampler(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\ajkaPreSampler.sam", System.IO.FileMode.Open)));
            SimModel model = program.OpenFile(System.IO.File.Open(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\ajkaModels07pre-20tasks\model000.mus", System.IO.FileMode.Open));
            TreeStructure tree;
            double[][][] datab = new double[factors.Length][][];
            double[][][] datat = new double[factors.Length][][];
            Stopwatch sw = new Stopwatch();
            int i = 0;
            foreach (double factor in factors)
            {
                GroupSoltion.SolverPowerFactor = factor;
                tree = new TreeStructure(model, sList);
                sw.Restart();
                datab[i] = tree.TestBottomExpand(50000);
                sw.Stop();
                tree = new TreeStructure(model, sList);
                Console.WriteLine(String.Format("Bottom took {0}s", sw.ElapsedMilliseconds / 1000.0));
                sw.Restart();
                datat[i] = tree.TestTopExpand(1000, 1000);
                sw.Stop();
                Console.WriteLine(String.Format("Top took {0}s", sw.ElapsedMilliseconds / 1000.0));
                i++;
            }
            tree = null;
            List<MLArray> mlist = new List<MLArray>(5);
            mlist.Add(MatProcess(factors, "factors"));
            mlist.Add(MatProcess(datab, "datab"));
            mlist.Add(MatProcess(datat, "datat"));
            MatFileWriter mfw = new MatFileWriter(fname, mlist, true);
        }

        private static void BatchDominantEvaluation()
        {
            int nFiles = 100;
            int nRuns = 100;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            var statistics = CountDominantChanges(@"d:\VisualStudio\AgentDraw\IRIDIS\ajka\models07-19task\ajka7_model", nFiles, nRuns);
            sw.Stop();
            Console.WriteLine(String.Format("Took {0:D}s", sw.ElapsedMilliseconds / 1000));
            ExportToMat(statistics, "ajka7_model_anyIter_CS.mat", "stat");
        }

        private static void ExportToMat(double[][] data, string fName, string varname)
        {
            List<MLArray> mlist = new List<MLArray>(1);
            MLDouble mstat = MatProcess(data, varname);
            mlist.Add(mstat);
            MatFileWriter mfw = new MatFileWriter(fName, mlist, true);
        }

        private static void ExportToMat(double[][][] data, string fName, string varname)
        {
            List<MLArray> mlist = new List<MLArray>(1);
            MLDouble mstat = MatProcess(data, varname);
            MatFileWriter mfw = new MatFileWriter(fName, mlist, true);
        }

        private static void ExportToMat(double[][][][] data, string fName, string varname)
        {
            List<MLArray> mlist = new List<MLArray>(1);
            MLDouble mstat = MatProcess(data, varname);
            MatFileWriter mfw = new MatFileWriter(fName, mlist, true);
        }

        private static MLDouble MatProcess(double[] data, string varname)
        {
            return new MLDouble(varname, data, data.Length);
        }

        private static MLInt32 MatProcess(int[] data, string varname)
        {
            return new MLInt32(varname, data, data.Length);
        }

        private static MLDouble MatProcess(double[][] data, string varname)
        {
            return new MLDouble(varname, data);
        }

        private static MLDouble MatProcess(double[][][] data, string varname)
        {
            int[] dims = { data.Length, data[0].Length, data[0][0].Length };
            MLDouble mstat = new MLDouble(varname, dims);
            double[][] data2 = DecreaseDim(data);
            for (int i = 0; i < data2.Length; i++)
            {
                for (int j = 0; j < data2[0].Length; j++)
                {
                    mstat.Set(data2[i][j], i, j);
                }
            }
            return mstat;
        }

        private static MLDouble MatProcess(double[][][][] data, string varname)
        {
            int[] dims = { data.Length, data[0].Length, data[0][0].Length, data[0][0][0].Length };
            MLDouble mstat = new MLDouble(varname, dims);
            double[][] data2 = DecreaseDim(data);
            for (int i = 0; i < data2.Length; i++)
            {
                for (int j = 0; j < data2[0].Length; j++)
                {
                    mstat.Set(data2[i][j], i, j);
                }
            }
            return mstat;
        }

        private static void DebugDominantChanges(string filepath, int nRuns)
        {
            int nLevels = 2;
            ConsoleProg program = new ConsoleProg("", "", false, "");
            program.OpenFile(System.IO.File.Open(filepath, System.IO.FileMode.Open));
            double[] batt;
            double[][,] dist;
            List<int>[][] C;
            program.GetDataForGT(out dist, out batt, out C);
            //python treeSearch.py "[[4, 6], [2, 1, 7]]" "[[11, 13, 4], [9], [6], [0, 1]]"
            //double[] activation;
            //GroupProblem p;
            //GroupSoltion s = null;
            //int l = 0;
            //activation = new double[dist[l].GetLength(1)];
            //p = new GroupProblem(dist[l], activation, batt[l]);
            //List<int>[] alloc = new List<int>[p.NAgents];
            //int[] a = { p.NTasks, 4, 6 };
            //int[] b = { p.NTasks + 1, 2, 1, 7 };
            //alloc[0] = new List<int>(a);
            //alloc[1] = new List<int>(b);
            //s = new GroupSoltion(p, alloc);

            //Console.WriteLine(s);

            //l = 1;
            //activation = s.ExtractActivation(C[l - 1]);
            //p = new GroupProblem(dist[l], activation, batt[l]);
            //alloc = new List<int>[p.NAgents];
            //int[] c = { p.NTasks, 11, 13, 4 };
            //int[] d = { p.NTasks+1, 9 };
            //int[] e = { p.NTasks+2, 6 };
            //int[] f = { p.NTasks + 3, 0, 1 };
            //alloc[0] = new List<int>(c);
            //alloc[1] = new List<int>(d);
            //alloc[2] = new List<int>(e);
            //alloc[3] = new List<int>(f);
            //s = new GroupSoltion(p, alloc);
            //Console.WriteLine(s);
            for (int i = 0; i < nRuns; i++)
            {
                double[] activation;
                GroupProblem p;
                GroupSoltion s = null;
                string[] solutions = new string[2 * nLevels];
                for (int l = 0; l < nLevels; l++)
                {
                    int iternum;
                    if (s != null)
                        activation = s.ExtractActivation(C[l - 1]);
                    else
                        activation = new double[dist[l].GetLength(1)];
                    p = new GroupProblem(l, dist[l], null, activation, batt[l]);
                    s = new GroupSoltion(p, out iternum, out solutions[l]);
                    solutions[l + nLevels] = s.ToString();
                }
                string pyarg = String.Format("\"{0}\" \"{1}\"", solutions[0], solutions[1]);
                bool matchFound = false;
                string[] lines = null;
                for (int j = 0; j < 100; j++)
                {
                    string returned = MyExtensions.MyExtensionMethods.RunScript("debugDominant.py", @"d:\Python\pygdl_2\", pyarg, isArgumentFilePath: false);
                    lines = returned.Split('\n');
                    bool match0 = (String.Compare(lines[0].Trim(), solutions[2]) == 0);
                    bool match1 = (String.Compare(lines[1].Trim(), solutions[3]) == 0);
                    if (match0 && match1)
                    {
                        matchFound = true;
                        break;
                    }
                }
                if (!matchFound)
                {
                    Console.WriteLine("Run" + i + ": python treeSearch.py " + pyarg);
                    Console.WriteLine("Recieved");
                    Console.WriteLine(lines[0].Trim());
                    Console.WriteLine(lines[1].Trim());
                    Console.WriteLine("Calculated");
                    Console.WriteLine(solutions[2]);
                    Console.WriteLine(solutions[3]);
                }
                else
                    Console.WriteLine("Run" + i + ": match found");
            }
        }


        private static double[][][][] CountDominantChanges(string path, int nFiles, int nRuns)
        {
            Stopwatch sw = new Stopwatch();
            int nLevels = 2;
            double[][][][] statistics = new double[nFiles][][][];
            ConsoleProg program = new ConsoleProg("", "", false, "");
            for (int f = 0; f < nFiles; f++)
            {
                statistics[f] = new double[nRuns][][];
                string fName = String.Format("{0}{1:D3}.mus", path, f);
                program.OpenFile(System.IO.File.Open(fName, System.IO.FileMode.Open));
                for (int i = 0; i < nRuns; i++)
                {
                    double[] activation;
                    GroupProblem p;
                    GroupSoltion s = null;
                    statistics[f][i] = new double[nLevels][];
                    double[] batt;
                    double[][,] dist;
                    List<int>[][] C;
                    program.GetDataForGT(out dist, out batt, out C);
                    for (int l = 0; l < nLevels; l++)
                    {
                        int iternum;
                        if (s != null)
                            activation = s.ExtractActivation(C[l - 1]);
                        else
                            activation = new double[dist[l].GetLength(1)];
                        p = new GroupProblem(l, dist[l], null, activation, batt[l]);
                        sw.Start();
                        s = new GroupSoltion(p, out iternum);
                        sw.Stop();
                        statistics[f][i][l] = new double[2];
                        statistics[f][i][l][0] = iternum;
                        statistics[f][i][l][1] = sw.Elapsed.TotalSeconds;
                        sw.Reset();
                    }
                }
            }
            return statistics;
        }

        static double[][] DecreaseDim(double[][][][] data)
        {
            int len = data.Length;
            double[][][] outData = new double[len][][];
            for (int i = 0; i < len; i++)
            {
                outData[i] = DecreaseDim(data[i]);
            }
            return DecreaseDim(outData);
        }

        static double[][] DecreaseDim(double[][][] data)
        {
            int len = data.Length;
            double[][] outData = new double[len][];
            for (int i = 0; i < len; i++)
            {
                outData[i] = DecreaseDim(data[i]);
            }
            return outData;
        }

        static double[] DecreaseDim(double[][] data)
        {
            double[] d = new double[data.Length * data[0].Length];
            for (int n = 0; n < data[0].Length; n++)
            {
                for (int m = 0; m < data.Length; m++)
                {
                    d[m + n * data.Length] = data[m][n];
                }
            }
            return d;
        }
    }
}
