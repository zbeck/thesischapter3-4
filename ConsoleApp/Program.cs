﻿using AgentDraw;
using AStarSolver;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Threading;

namespace ConsoleApp
{
    class Program
    {
        private static AStarSolver.TeamPath<NodeModel> EvaluateSolver(Stopwatch computationTime, List<NodeModel> uavs, List<NodeModel> tasks, Func<AStarSolver.TeamPath<NodeModel>, double> estimate)
        {
            //AStarSolver.TeamPath<NodeModel>.P = 3000;
            //AStarSolver.TeamEstimator<NodeModel>.EstimateCompare = 0;
            computationTime.Start();
            AStarSolver.TeamPath<NodeModel> best = AStarSolver.Program.FindPath<NodeModel>(uavs, 500 * 1.5, tasks, AgentModel.Distance, estimate);
            computationTime.Stop();
            foreach (AStarSolver.Agent a in best.Agents)
            {
                foreach (int i in a.Path)
                {
                    if (i >= tasks.Count)
                        Console.Write(String.Format("UAV{0} ", i - tasks.Count));
                    else
                    {
                        NodeModel n = tasks[i];
                        Console.Write(String.Format("({2})[{0}, {1}] ", n.Position.X, n.Position.Y, i));
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine(String.Format("Collected Utility: {0}", best.TotalUtil));
            Console.WriteLine(String.Format("Elapsed time[ms]: {0}", computationTime.Elapsed.TotalMilliseconds));
            Console.WriteLine(String.Format("Number of states visited: {0}", AStarSolver.TeamPath<NodeModel>.seenStates));
            //Console.WriteLine(String.Format("Number of cases when new estimate is lower: {0}", AStarSolver.TeamEstimator<NodeModel>.EstimateCompare));
            return best;
        }
        /// <summary>
        /// command line solver main function for IRIDIS
        /// </summary>
        /// <param name="args">[id] [distribution] [model] [work folder] [solver] [solver mode] [output] [sample/step] [steptime]
        /// 0   id:             parallel job id number                                      (int)
        /// 1   distribution:   Poisson sampler file (.sam)                                 (relative to workfolder)
        /// 2   model:          SimModel serialized                                         (absolute file path)
        /// 3   work folder:    folder containing the solver script                         (absolute path)
        /// 4   solver:         path to the solver script + modifiers                       (relative to workfolder)
        /// 5   solver mode:    0: sampled; 1: non-sampled; 2:optimal                       (integer)
        /// 6   output:         output directory                                            (absolute path)
        /// 7   sample/step:    number of random samples per timestep                       (positive integer)
        /// 8   steptime:       length of a timestep                                        (positive double)
        /// </param>
        static void Main(string[] args)
        {
            if (args.Length == 2)
            {
                TreeSearchTesterProgram.Tester(args);
                //string scriptPath = null;
                //if (args.Length > 0)
                //    scriptPath = args[0];
                //else
                //    scriptPath = @"d:\VisualStudio\AgentDraw\TestResults\consoleRun\testScript.txt";
                //int maxthreads = 8;
                //ThreadPool.SetMaxThreads(maxthreads, maxthreads);
                ////int worker, iothread;
                ////ThreadPool.GetMinThreads(out worker, out iothread);
                ////Console.WriteLine(worker.ToString() + " " + iothread.ToString());
                //string app = System.Windows.Forms.Application.ExecutablePath;
                //if (scriptPath != null)
                //{
                //    string[] scriptlines = System.IO.File.ReadAllLines(scriptPath);
                //    CountdownEvent poolCount = new CountdownEvent(scriptlines.Length);
                //    foreach (string pArgs in scriptlines)
                //    {
                //        MyProcessArgs mpargs = new MyProcessArgs(app, pArgs, poolCount);
                //        if (!ThreadPool.QueueUserWorkItem(new WaitCallback(run_cmd), mpargs))
                //            Console.WriteLine("Process didn't queue properly!");
                //        Thread.Sleep(1000);
                //    }
                //    poolCount.Wait();
                //    Console.WriteLine("Job pool finished.");
                //}
                //Console.ReadKey();
                return;
            }
            if (args.Length == 0)
            {
                args = new string[] {
                "105",
                "relationTest/haitiBlankSampler.sam",
                "d:/VisualStudio/AgentDraw/IRIDIS/relationTest/haitiModels11-61tasks/model105.mus",
                "d:/VisualStudio/AgentDraw/IRIDIS/",
                "MCTSImprovement",
                "40",  //1 minute initial growth
                "d:/VisualStudio/AgentDraw/TestResults/consoleTest/",
                "d:/VisualStudio/AgentDraw/TestResults/consoleTest/tmp/",
                "5",
                "10" };
                ConsoleProg.Debug = true;
            }
            if (args.Length != 10)
            {
                Console.WriteLine("Bad arguments. Please follow the following pattern:/n[id] [distribution] [model] [work folder] [solver] [solver mode] [output] [tmp path] [sample/step] [steptime]");
                return;
            }

            int id = Int32.Parse(args[0]);

            string workFolder = AddSlash(args[3]);
            string outputFolder = AddSlash(args[6]);
            string tmpFolder = AddSlash(args[7]);

            string samPath = String.Concat(workFolder, args[1]);
            string modelPath = args[2];
            string solver = args[4];
            int solverMode = Int32.Parse(args[5]);
            int samplePerStep = Int32.Parse(args[8]);
            double stepTime = Double.Parse(args[9]);

            ConsoleProg program = new ConsoleProg(workFolder, tmpFolder, true, outputFolder);
            using (var fs = System.IO.File.Open(samPath, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read))
                program.OpenBeliefSampler(fs);
            using (var fs = System.IO.File.Open(modelPath, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read))
                program.OpenFile(fs);
            string gradpath = samPath.Split(new char[] { '.' })[0] + "_grad.sam";
            if (System.IO.File.Exists(gradpath))
            {
                using (var fs = System.IO.File.Open(gradpath, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read))
                    program.OpenGradientSampler(fs);
            }
            else
            {
                Console.WriteLine("Cannot open gradient sampler: " + gradpath);
            }
            //program.Test(System.IO.File.OpenText(@"c:\Users\Zoli\Desktop\VisualStudio\AgentDraw\IRIDIS\output_sample1.txt").ReadToEnd());
            if (solver == "solvertime")
                program.TestDualRunTime(solver, samplePerStep, stepTime, id, solverMode);
            else if ((solver == "UCBImprovement") || (solver == "MCTSImprovement"))
                //Console.WriteLine(
                program.TestMCTSImprovement(solver, samplePerStep, stepTime, id, solverMode);
                //);
            else
                Console.WriteLine(program.RunSimulationNoUI(solver, samplePerStep, stepTime, id, solverMode));
        }

        public class MyProcessArgs
        {
            public string cmd;
            public string args;
            public CountdownEvent cnt;
            public MyProcessArgs(string cmd, string args, CountdownEvent cnt)
            {
                this.cmd = cmd;
                this.args = args;
                this.cnt = cnt;
            }
        }
        
        static void run_cmd(Object args)
        {
            MyProcessArgs Args = (MyProcessArgs)args;
            ProcessStartInfo start = new ProcessStartInfo();
            start.FileName = Args.cmd;
            start.Arguments = Args.args;
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;
            Console.WriteLine(String.Format("Starting proc {0}", Args.args.Substring(0, 3)));
            using (Process process = Process.Start(start))
            {
                using (System.IO.StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                    Console.Write(result);
                }
            }
            Args.cnt.Signal();
        }

        static string AddSlash(string path)
        {
            if (path.Last<char>() != '/')
                path = String.Concat(path, "/");
            return path;
        }

        static void TestAStar()
        {
            Stopwatch computationTime = new Stopwatch();
            NodeModel[] uavs = { new NodeModel(new Point(365, 246)), new NodeModel(new Point(430, 63)) };
            List<NodeModel> uavList = new List<NodeModel>(uavs);
            NodeModel[] allTasks = { new NodeModel(new Point(205, 107)), new NodeModel(new Point(257, 81)), new NodeModel(new Point(315, 71)),
                                    new NodeModel(new Point(321, 73)), new NodeModel(new Point(360, 90)), new NodeModel(new Point(370, 119)),
                                    new NodeModel(new Point(370, 187)), new NodeModel(new Point(271, 217)), new NodeModel(new Point(228, 208)),
                                    new NodeModel(new Point(226, 186)), new NodeModel(new Point(232, 144)), new NodeModel(new Point(282, 143)) };
            int nTasks = 9;
            List<NodeModel> tasks = new List<NodeModel>(nTasks);
            for (int i = 0; i < nTasks; i++)
                tasks.Add(allTasks[i]);
            //AStarSolver.TeamPath<NodeModel>  best = EvaluateSolver(computationTime, uavs, tasks, AStarSolver.TeamEstimator<NodeModel>.DumbOverestimate);
            //ValidateEstimate(best, AStarSolver.TeamEstimator<NodeModel>.EnhancedOverestimate);
            AStarSolver.TeamPath<NodeModel> best = EvaluateSolver(computationTime, uavList, tasks, AStarSolver.TeamEstimator<NodeModel>.BestEstimate);
            //EvaluateSolver(computationTime, uavs, tasks, AStarSolver.TeamEstimator<NodeModel>.MediumOverestimate);
            //EvaluateSolver(computationTime, uavs, tasks, AStarSolver.TeamEstimator<NodeModel>.ParallelOverestimate);
            //MessageBox.Show(best.TotalUtil.ToString());
        }
    }
}
