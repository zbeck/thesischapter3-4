﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace DiscoverySolver
{
    class AgentEnumerable : IEnumerable
    {
        int[] all;
        int start;
        int end;
        double[,] travelTTAT;
        int agent;

        public AgentEnumerable(int[] data, int start, int end, double[,] ttat, int agent)
        {
            this.start = start;
            this.end = end;
            all = data;
            travelTTAT = ttat;
            this.agent = agent + travelTTAT.GetLength(1);
        }

        //public AgentEnumerable() { }

        public AgentEnumerator GetEnumerator()
        {
            return new AgentEnumerator(all, start, end, travelTTAT, agent);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }
    }


    class AgentEnumerator : IEnumerator
    {
        int[] all;
        int start;
        int end;
        int current;
        double[,] travelTTAT;
        double time = 0;
        int position;

        public AgentEnumerator(int[] data, int start, int end, double[,] tt, int startPos)
        {
            this.start = start;
            this.end = end;
            all = data;
            current = start - 1;
            travelTTAT = tt;
            position = startPos;
        }

        public double NextTime
        {
            get
            {
                if (isAtEnd())
                    return Double.PositiveInfinity;
                else
                    return time + travelTTAT[position,all[current+1]];
            }
        }

        public double Time
        {
            get { return time; }
        }

        public int CurrentSlot { get { return current - start; } }

        public int Current
        {
            get
            {
                try
                {
                    return all[current];
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        bool isAtEnd()
        {
            return (current >= (end - 1));
        }

        public bool MoveNext()
        {
            if (isAtEnd())
                return false;
            time = NextTime;
            current++;
            position = Current;
            return true;
        }

        public void Reset()
        {
            current = start - 1;
            time = 0;
        }
    }

}
