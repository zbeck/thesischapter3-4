﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyExtensions;

namespace DiscoverySolver
{
    public class Solver
    {
        int nAgents;
        int nTasks;
        double[,] travelTTAT;
        double[,] EDiscovery;
        double battery;
        double P;
        double lam;
        double epsilon;

        public Solver(int nA, int nT, double[,] TT, double[,] AT, double[,] ED, double bat, double P, double lam, double eps)
        {
            nAgents = nA;
            nTasks = nT;
            if ((TT.GetLength(0) != nTasks) || (TT.GetLength(1) != nTasks) ||
                (AT.GetLength(0) != nAgents) || (AT.GetLength(1) != nTasks) ||
                (ED.GetLength(0) != nTasks) || (ED.GetLength(1) != nTasks))
                throw new Exception("Solver parameter length error!");
            travelTTAT = new double[nTasks + nAgents, nTasks];
            Array.Copy(TT, travelTTAT, TT.Length);
            Array.ConstrainedCopy(AT, 0, travelTTAT, TT.Length, AT.Length);
            EDiscovery = ED;
            battery = bat;
            this.P = P;
            this.lam = lam;
            epsilon = eps;
        }

        public List<List<int>> Solve()
        {
            List<List<int>> solution = new List<List<int>>(nAgents);
            int[] per = new int[nTasks];
            for(int i=0; i<nTasks; i++)
                per[i] = i;
            int[] bestPer = new int[nTasks];
            int[] bestSep = new int[nAgents];
            double maxUtil = 0;

            IteratePerm(per, ref bestPer, ref bestSep, 0, ref maxUtil);

            for (int i = 0; i < nAgents; i++)
            {
                int end = (i + 1 == nAgents) ? nTasks : bestSep[i + 1];
                List<int> currentList = new List<int>();
                for (int j = bestSep[i]; j < end; j++)
                    currentList.Add(bestPer[j]);
                solution.Add(currentList);
            }

            return solution;
        }

        static void swap(int[] array, int i, int j)
        {
            int tmp = array[i];
            array[i] = array[j];
            array[j] = tmp;
        }

        static int[] SubArray(int[] data, int begin, int end)
        {
            int[] result = new int[end - begin + 1];
            Array.Copy(data, begin, result, 0, end - begin + 1);
            return result;
        }

        double CalculateUtilityForSolution(int[] per, int[] separators)
        {
            double util = 0;

            bool[] done = new bool[nTasks];
            for (int i = 0; i < nTasks; i++)
                done[i] = false;
            AgentEnumerable[] allocation = new AgentEnumerable[separators.Length];
            AgentEnumerator[] enumerators = new AgentEnumerator[separators.Length];
            for (int i = 0; i < separators.Length; i++)
            {
                int end = ((i+1) == separators.Length) ? (per.Length) : separators[i + 1];
                allocation[i] = new AgentEnumerable(per, separators[i], end, travelTTAT, i);
                enumerators[i] = allocation[i].GetEnumerator();
            }
            bool exit = false;
            while (exit == false)
            {
                double minTime = Double.PositiveInfinity;
                int minindex = -1;
                for (int i = 0; i < enumerators.Length; i++)
                {
                    double nextTime = enumerators[i].NextTime;
                    if (nextTime < minTime)
                    {
                        minTime = nextTime;
                        minindex = i;
                    }
                }
                if (minindex == -1)
                    exit = true;
                else
                {
                    if (!enumerators[minindex].MoveNext())
                        throw new Exception("Error: AgentEnumerator.Nextime not infinity");
                    else
                    {
                        if (enumerators[minindex].Time > battery)   //battery exceeded
                            return -1;
                        double ECurrent = EDiscovery[enumerators[minindex].Current, enumerators[minindex].Current];
                        for (int i = 0; i < nTasks; i++)
                            if (done[i])
                                ECurrent -= EDiscovery[enumerators[minindex].Current, i];
                        done[enumerators[minindex].Current] = true;
                        util += ECurrent * (P - lam * enumerators[minindex].Time) * Math.Pow(epsilon, enumerators[minindex].CurrentSlot);
                    }
                }
            }
            return util;
        }

        /// <summary>
        /// Iterates over free separator positions, and returns the highest utility found
        /// </summary>
        /// <param name="per">current permutation of the task's execution</param>
        /// <param name="separators">Separator positions (ascending) between the tasks in per. The section before the first separator remains undone, and the remaining sections are done by agent1, agent2...</param>
        /// <param name="set">number of separators currently fixed</param>
        /// <param name="max">current max utility</param>
        /// <returns>max utility</returns>
        void MaxUtilForPerm(int[] per, int[] separators, ref int[] bestSeparator, int set, ref double max)
        {
            double utility = max;
            if (set >= separators.Length)
            {
                utility = CalculateUtilityForSolution(per, separators);
                if (utility > max)
                {
                    max = utility;
                    Array.Copy(separators, bestSeparator, separators.Length);
                }
            }
            else
            {
                int minpos = 0;
                if (set > 0)
                    minpos = separators[set - 1];
                int[] gotSeparators = new int[separators.Length];
                for (int pos = minpos; pos <= per.Length; pos++)
                {
                    separators[set] = pos;
                    MaxUtilForPerm(per, separators, ref gotSeparators, set + 1, ref utility);
                    if (utility > max)
                    {
                        max = utility;
                        bestSeparator = gotSeparators;
                    }
                }
            }
        }

        void IteratePerm(int[] per, ref int[] bestPer, ref int[] bestSep, int i, ref double max)
        {
            double utility = max;
            int[] gotSep = new int[nAgents];
            if (i >= per.Length - 1)
            {
                MaxUtilForPerm(per, new int[nAgents], ref gotSep, 0, ref utility);
                if (utility > max)
                {
                    max = utility;
                    Array.Copy(per, bestPer, per.Length);
                    bestSep = gotSep;
                }
            }
            else
            {
                int[] gotPer = new int[per.Length];
                IteratePerm(per, ref gotPer, ref gotSep, i + 1, ref utility);
                if (utility > max)
                {
                    max = utility;
                    bestPer = gotPer;
                    bestSep = gotSep;
                }
                for (int j = i + 1; j < per.Length; j++)
                {
                    swap(per, i, j);
                    IteratePerm(per, ref gotPer, ref gotSep, i + 1, ref utility);
                    if (utility > max)
                    {
                        max = utility;
                        bestPer = gotPer;
                        bestSep = gotSep;
                    }
                    swap(per, i, j);
                }
            }
        }
    }
}
