﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyExtensions;

namespace DiscoverySolver
{
    class Program
    {

        static void Main(string[] args)
        {
            /*int[] myPerm = { 0, 1, 2, 3, 4, 5 };
            //IteratePerm(myPerm, 0);
            double bestUtil = 0;
            int[] bestSep = { 0, 0, 0 };
            MaxUtilForPerm(myPerm, new int[3] { 0, 0, 0 }, ref bestSep, 0, ref bestUtil);
            Console.WriteLine(String.Format("Best utility: {0}", bestUtil));
            Console.WriteLine(MyExtensionMethods.ToDelimitedString<int>(bestSep, " "));*/
            int nAgents = 2, nTasks = 7;
            double[,] tt = { { 0, 8202, 4067, 7307, 6138, 9952, 11724 }, { 8202, 0, 4135, 5866, 9339, 11575, 9845 }, { 4067, 4135, 0, 5243, 6769, 10009, 10059 }, { 7307, 5866, 5243, 0, 4288, 5720, 4818 }, { 6138, 9339, 6769, 4288, 0, 3818, 6603 }, { 9952, 11575, 10009, 5720, 3818, 0, 4600 }, { 11724, 9845, 10059, 4818, 6603, 4600, 0 } };
            double[,] at = { { 4201, 9365, 5940, 10742, 10315, 14115, 15454 }, { 8360, 4802, 5436, 9858, 12189, 15231, 14352 } };
            double[,] ed = { { 5.04708031953294, 0, 1.84589844297554, 6.03555333512085E-05, 0, 0, 0 }, { 0, 4.79793269522611, 1.83606048984422, 0.00395328735029437, 0, 0, 0 }, { 1.84589844297554, 1.83606048984422, 8.00272079268402, 0.00294233219712049, 0, 0, 0 }, { 6.03555333512085E-05, 0.00395328735029437, 0.00294233219712049, 4.03429944729505, 1.01326373100139, 0.0403627618249336, 0.0341914086150235 }, { 0, 0, 0, 1.01326373100139, 3.87758632781981, 0.266379138635537, 0 }, { 0, 0, 0, 0.0403627618249336, 0.266379138635537, 2.95736064595369, 0.0137007057267181 }, { 0, 0, 0, 0.0341914086150235, 0, 0.0137007057267181, 1.30170281806404 } };
            Solver testSolver = new Solver(nAgents, nTasks, tt, at, ed, 164670, 100000, 1, 0.9);
            var l = testSolver.Solve();
            foreach (var e in l)
            {
                Console.WriteLine(MyExtensionMethods.ToDelimitedString<int>(e, " "));
            }
            //Console.ReadKey();
        }

        static void swap(int[] array, int i, int j)
        {
            int tmp = array[i];
            array[i] = array[j];
            array[j] = tmp;
        }

        static double IteratePerm(int[] per, int i)
        {
            if (i >= per.Length - 1)
                Console.WriteLine(MyExtensionMethods.ToDelimitedString<int>(per));
            else
            {
                IteratePerm(per, i + 1);
                for (int j = i + 1; j < per.Length; j++)
                {
                    swap(per, i, j);
                    IteratePerm(per, i + 1);
                    swap(per, i, j);
                }
            }
            return 0;
        }

        static void MaxUtilForPerm(int[] per, int[] separators, ref int[] bestSeparator, int set, ref double max)
        {
            double utility = max;
            int[] gotSeparators = new int[separators.Length];
            if (set >= separators.Length)
            {
                Console.WriteLine(MyExtensionMethods.ToDelimitedString<int>(separators, " "));
                utility = separators[0] * (separators[1] - separators[2]) * (separators[1] - separators[2]);
                if (utility > max)
                {
                    Console.WriteLine(String.Format("-- Best: {0}", utility));
                    max = utility;
                    Array.Copy(separators, bestSeparator, separators.Length);
                }
            }
            else
            {
                int minpos = 0;
                if (set > 0)
                    minpos = separators[set - 1];
                for (int pos = minpos; pos <= per.Length; pos++)
                {
                    separators[set] = pos;
                    MaxUtilForPerm(per, separators, ref gotSeparators, set + 1, ref utility);
                    if (utility > max)
                    {
                        Console.WriteLine(String.Format("-- Got: {0}", utility));
                        Console.WriteLine(MyExtensionMethods.ToDelimitedString<int>(gotSeparators));
                        max = utility;
                        bestSeparator = gotSeparators;
                    }
                }
            }
        }
    }
}
