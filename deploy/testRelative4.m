%% setup
evalPath = 'd:\VisualStudio\AgentDraw\TestResults';
simIds = 0:127;

% runName = 'relationMCTSHaiti19';
% runName = 'relationMCTSHaiti30';
% runName = 'relationMCTSAjka28';
% runName = 'relationMCTSAjka29';
% runName = 'relationCollabHaiti30';
% runName = 'relationCollabAjka29';
% runName = 'relationMCTSAjka40';
runName = 'relationMCTSHaiti31';

modes_renamed = false;
separate_bars = false;

if strcmp(runName, 'relationMCTSHaiti19')
    simPath = 'd:\VisualStudio\AgentDraw\IRIDIS\relationTest\current_thesis_version\run19';
    modes = {'B0', 'B1', 'B2', 'B3', 'B4', 'B6', 'B7', 'B6x20', 'B7x20',...
        'P0', 'P1', 'P2', 'P3', 'P4', 'P6', 'P7', 'P6x20', 'P7x20',...
        'G0', 'G1', 'G2', 'G3', 'G4', 'G6', 'G7', 'G6x20', 'G7x20'};
    modeSize = length(modes);
    groups = {1:9, 10:18, 19:modeSize};
    compareShape = [7 1 2 3 4 5 6 7 8 9];
    relCompares = [compareShape; compareShape+9; compareShape+18];
    relCmpNames = {'Blank', 'Preliminary', 'Ground Truth'};
    vsCompare = [17 18];
elseif strcmp(runName, 'relationMCTSHaiti30')
    simPath = 'd:\VisualStudio\AgentDraw\IRIDIS\relationTest\current_thesis_version\run30';
    modes_renamed = true;
    modes = {'Z1', 'Z2', 'Z3', 'Z4', 'Z5', 'Z6', 'Z7',...% 'B6x20', 'B7x20',...
        'R1', 'R2', 'R3', 'R4', 'R5', 'R6', 'R7',...% 'P6x20', 'P7x20',...
        'C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7'};%, 'G6x20', 'G7x20'};
    modeSize = length(modes);
    groups = {1:7, 8:14, 15:modeSize};
    compareShape = [7 1 2 3 4 5 6 7];
    relCompares = [compareShape; compareShape+7; compareShape+14];
    relCmpNames = {'Zero information', 'Resilient', 'Complete information'};
    vsCompare = [5 7]+7;
elseif strcmp(runName, 'relationMCTSHaiti31')
    simPath = 'd:\VisualStudio\AgentDraw\IRIDIS\relationTest\current_thesis_version\run31';
    modes_renamed = true;
    modes = {'Z1', 'Z2', 'Z3', 'Z4', 'Z5', 'Z6', 'Z7',...% 'B6x20', 'B7x20',...
        'R1', 'R2', 'R3', 'R4', 'R5', 'R6', 'R7',...% 'P6x20', 'P7x20',...
        'C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7'};%, 'G6x20', 'G7x20'};
    modeSize = length(modes);
    groups = {1:7, 8:14, 15:modeSize};
    compareShape = [7 1 2 3 4 5 6 7];
    relCompares = [compareShape; compareShape+7; compareShape+14];
    relCmpNames = {'Zero information', 'Resilient', 'Complete information'};
    vsCompare = [6 7]+7;
elseif strcmp(runName, 'relationCollabHaiti30')
    simPath = 'd:\VisualStudio\AgentDraw\IRIDIS\relationTest\current_thesis_version\run30';
    modes_renamed = true;
    modes = {'Z1', 'Z2', 'Z3', 'Z4', 'Z5', 'B6', 'B7',...% 'B6x20', 'B7x20',...
        'R1', 'R2', 'R3', 'R4', 'R5', 'P6', 'P7',...% 'P6x20', 'P7x20',...
        'C1', 'C2', 'C3', 'C4', 'C5', 'G6', 'G7'};%, 'G6x20', 'G7x20'};
    modeSize = length(modes);
    groups = {1:5, 8:12, 15:19};
    compareShape = [5 1 2 3 4 5];
    relCompares = [compareShape; compareShape+7; compareShape+14];
    relCmpNames = {'Zero information', 'Resilient', 'Complete information'};
    vsCompare = [1 5];
elseif strcmp(runName, 'relationMCTSAjka28')
    simPath = 'd:\VisualStudio\AgentDraw\IRIDIS\relationTest\current_thesis_version\run28';
    modes = {'B0', 'B1', 'B2', 'B3', 'B4', 'B6', 'B7',...% 'B6x20', 'B7x20',...
        'P0', 'P1', 'P2', 'P3', 'P4', 'P6', 'P7', 'P6x20', 'P7x20',...
        'G0', 'G1', 'G2', 'G3', 'G4', 'G6', 'G7', 'G6x20', 'G7x20'};
    modeSize = length(modes);
%     groups = {1:9, 10:18, 19:modeSize};
    groups = {1:7, 8:16, 17:modeSize};
    compareShape = [7 1 2 3 4 5 6 7 8 9];
    relCompares = [compareShape; compareShape+7; compareShape+16];
    relCmpNames = {'Blank', 'Preliminary', 'Ground Truth'};
    vsCompare = [15 16];
    simIds = setdiff(simIds, [109 ]);
elseif strcmp(runName, 'relationMCTSAjka29')
    simPath = 'd:\VisualStudio\AgentDraw\IRIDIS\relationTest\current_thesis_version\run29';
    modes_renamed = true;
    separate_bars = true;
    modes = {'Z1', 'Z2', 'Z3', 'Z4', 'Z5', 'Z6', 'Z7',...% 'B6x20', 'B7x20',...
        'R1', 'R2', 'R3', 'R4', 'R5', 'R6', 'R7',...% 'P6x20', 'P7x20',...
        'C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7'};%, 'G6x20', 'G7x20'};
    modeSize = length(modes);
    groups = {1:7, 8:14, 15:modeSize};
    compareShape = [7 1 2 3 4 5 6 7];
    relCompares = [compareShape; compareShape+7; compareShape+14];
    relCmpNames = {'Zero information', 'Resilient', 'Complete information'};
    vsCompare = [5 7]+7;
elseif strcmp(runName, 'relationMCTSAjka40')
    simPath = 'd:\VisualStudio\AgentDraw\IRIDIS\relationTest\current_thesis_version\run40';
    modes_renamed = true;
    separate_bars = true;
    modes = {'Z1', 'Z2', 'Z3', 'Z4', 'Z5', 'Z6', 'Z7',...% 'B6x20', 'B7x20',...
        'R1', 'R2', 'R3', 'R4', 'R5', 'R6', 'R7',...% 'P6x20', 'P7x20',...
        'C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7'};%, 'G6x20', 'G7x20'};
    modeSize = length(modes);
    groups = {1:7, 8:14, 15:modeSize};
    compareShape = [7 1 2 3 4 5 6 7];
    relCompares = [compareShape; compareShape+7; compareShape+14];
    relCmpNames = {'Zero information', 'Resilient', 'Complete information'};
    vsCompare = [5 7]+7;
elseif strcmp(runName, 'relationCollabAjka29')
    simPath = 'd:\VisualStudio\AgentDraw\IRIDIS\relationTest\current_thesis_version\run29';
    modes_renamed = true;
    separate_bars = true;
    modes = {'Z1', 'Z2', 'Z3', 'Z4', 'Z5', 'B6', 'B7',...% 'B6x20', 'B7x20',...
        'R1', 'R2', 'R3', 'R4', 'R5', 'P6', 'P7',...% 'P6x20', 'P7x20',...
        'C1', 'C2', 'C3', 'C4', 'C5', 'G6', 'G7'};%, 'G6x20', 'G7x20'};
    modeSize = length(modes);
    groups = {1:5, 8:12, 15:19};
    compareShape = [5 1 2 3 4 5];
    relCompares = [compareShape; compareShape+7; compareShape+14];
    relCmpNames = {'Zero information', 'Resilient', 'Complete information'};
    vsCompare = [1 5];
end

%http://www.colourlovers.com/palette/2829194/spring_garden
%malefactor, honey glow, basement blues, worry, stark
myColours = {[232/255, 0, 46/255], [1, 187/255, 46/255], [180/255, 152/255, 0],...
    [0, 111/255, 47/255], [25/255, 73/255, 0]};
cmap = [[1:-0.01:0]' [1:-0.01:0]' [1:-0.01:0]'];

nsim = length(simIds);

figCnt = 1;

%% read data
matFileName = [simPath '.mat'];
if exist(matFileName, 'file') == 2
    load(matFileName);
else
    if modes_renamed
        error('Datafile necessary when using renamed modes!')
    end
    utilHi = zeros(modeSize, nsim);
    utilLo = zeros(modeSize, nsim);
    avgTime = zeros(modeSize, nsim);
    exploreTime = zeros(modeSize, nsim);
    executeTime = zeros(modeSize, nsim);
    allTaskTime = cell(modeSize, nsim);
    startdir = cd(evalPath);
    for modeid = 1:modeSize
        cd(strcat(simPath, modes{modeid}))
        for id = 1:nsim
            eval(strcat('Sim', int2str(simIds(id))))
            utilHi(modeid,id) =...
                sum(TimeStepData(size(TimeStepData,1),[3 6]));
            utilLo(modeid,id) =...
                sum(TimeStepData(size(TimeStepData,1),9:3:size(TimeStepData,2)));
            if(utilLo(modeid,id) > 100)
                utilLo(modeid,id) = utilLo(modeid,id) / 1000;
                utilHi(modeid,id) = utilHi(modeid,id) / 1000;
            end
            allTaskTime{modeid,id} = TaskTime;
            explore = ~TaskTime(:,3);
            exploreTime(modeid,id) = max(explore.*TaskTime(:,4));
            executeTime(modeid,id) = max(~explore.*TaskTime(:,4));
            avgTime(modeid,id) = sum(~explore.*TaskTime(:,4))/sum(~explore);
        end
    end
    cd(startdir)
    save(matFileName, 'utilHi', 'utilLo', 'avgTime', 'exploreTime', 'executeTime', 'allTaskTime')
end
%util = utilLo';
util = avgTime'/10; %rescue time in s

%% absolute bars
figure(figCnt)
clf
figCnt = figCnt+1;
%ratio = bsxfun (@rdivide, util(:, 1:end), util(:,1));
ratio = util;
rmean = mean(ratio);

allBars = [groups{1}, groups{2}];
barcolours = [repmat(myColours{5}, [length(groups{1}), 1]);...
                repmat(myColours{3}, [length(groups{2}), 1])];
groupX = { (length(groups{1})+1):(length(groups{1})+length(groups{2})) };
if length(groups)>2
    allBars = [allBars, groups{3}];
    groupX{2} = (length(groups{1})+length(groups{2})+1):length(allBars);
    barcolours = [barcolours; repmat(myColours{4}, [length(groups{3}), 1])];
end
if length(groups)>3
    allBars = [allBars, groups{4}];
    groupX{3} = (length(groups{1})+length(groups{2})+length(groups{3})+1):length(allBars);
    barcolours = [barcolours; repmat(myColours{3}, [length(groups{4}), 1])];
end
x_values = rmean(allBars);
std_values = 1.96*std(ratio(:,allBars))/sqrt(size(ratio,1));
superbar(x_values, 'E', std_values, 'BarFaceColor', barcolours)
ax = gca;
ax.XTick = 1:modeSize;
ax.XTickLabel = modes(allBars);
%ax.XTickLabelRotation = 45;
ylabel('Average Rescue Time [s]')
xlabel('Test Case')
ax = axis;
if separate_bars
    % left side
    selectidx = 1:size(groups{1},2);
    minrangeval = floor2(min(min(x_values(selectidx)-std_values(selectidx))), 10);
    maxrangeval = ceil2(max(max(x_values(selectidx)+std_values(selectidx))), 10);
    axis([selectidx(1)-0.5 selectidx(end)+0.5 minrangeval maxrangeval])
    set(gcf, 'PaperPosition', [0 0 6 10]);
    set(gcf, 'PaperSize', [6 10]);
    saveas(gcf, [runName '_barsLeft.pdf']);
    % right side
    selectidx = size(groups{1},2)+1:size(allBars,2);
    minrangeval = floor2(min(min(x_values(selectidx)-std_values(selectidx))), 10);
    maxrangeval = ceil2(max(max(x_values(selectidx)+std_values(selectidx))), 10);
    axis([selectidx(1)-0.5 selectidx(end)+0.5 minrangeval maxrangeval])
    set(gcf, 'PaperPosition', [0 0 10 10]);
    set(gcf, 'PaperSize', [10 10]);
    saveas(gcf, [runName '_barsRight.pdf']);
else
    axis([0.5 size(allBars,2)+0.5 floor2(min(min(x_values-std_values)), 10) ax(4)])
    set(gcf, 'PaperPosition', [0 0 13 10]);
    set(gcf, 'PaperSize', [13 10]);
    saveas(gcf, [runName '_bars.pdf']);
end

%% absolute bars per group
groupColours = {[55/255, 122/255, 237/255], [81/255, 205/255, 229/255], [66/255, 82/255, 45/255],...
    [22/255, 15/255, 20/255], [50/255, 47/255, 68/255]};
for row=1:size(groups,2)
    figure(figCnt)
    clf
    figCnt = figCnt+1;
    data = util(:, groups{row});
    rmean = mean(data);
    stdError = 1.96*std(data)/sqrt(size(data,1));
    minValue = min(rmean-stdError);
    maxValue = max(rmean+stdError);
    minRange = floor2(minValue, 50);
    maxRange = ceil2(maxValue, 50);

    bar(rmean, 'FaceColor', groupColours{1})
    set(gca,'XTickLabel',modes(groups{row}))
    hold on
    bar(2, rmean(2), 'FaceColor', groupColours{3})
    bar(3, rmean(3), 'FaceColor', groupColours{2})
    bar(4, rmean(4), 'FaceColor', groupColours{5})
    errorbar(1:size(data,2), rmean, stdError, '.', 'MarkerSize', 1,...
        'LineWidth', 1, 'Color', groupColours{4})
    hold off
    ylabel('Average Identification Time [s]')
    xlabel([relCmpNames{row} ' setting'])
    ax = axis;
    axis([ax(1) size(data,2)+.5 minRange maxRange])
    set(gcf, 'PaperPosition', [0 0 9 7]);
    set(gcf, 'PaperSize', [9 7]);
    saveas(gcf, [runName '_' strrep(relCmpNames{row}, ' ', '_') '-absolute_bars.pdf']);
end

%% relative bars
barRangeStep = 0.1;

barcolours = [myColours{5}; myColours{5}; myColours{2}; myColours{2}; myColours{4}; myColours{3}; myColours{1}; myColours{3}; myColours{1}];

for row=1:size(relCompares,1)
    cComp = relCompares(row, :);
    figure(figCnt)
    clf
    figCnt = figCnt+1;
    ratio = bsxfun(@rdivide, util(:,cComp(2:end)), util(:,cComp(1)));
%     tRescue = cell2mat(cellfun(@processTasks, allTaskTime(cComp, :)', 'UniformOutput', false));
%     ratio = -tRescue(:, 6:3:end) + repmat(tRescue(:, 3), 1, length(cComp)-1);
    rmean = mean(ratio);
    stdError = 1.96*std(ratio)/sqrt(size(ratio,1));
    pValues = 1-normcdf(rmean, ones(1,length(rmean)), std(ratio)/sqrt(size(ratio,1)));
    pValues(stdError == 0) = 0.5;
    
    minValue = min(rmean-stdError);
    maxValue = max(rmean+stdError);
    minRange = floor2(minValue, barRangeStep);
    maxRange = ceil2(maxValue, barRangeStep);
    if minRange == 1
        minRange = 1-barRangeStep;
    end
    if maxRange == 1
        maxRange = 1+barRangeStep;
    end
    superbar(rmean, 'E', stdError, 'P', pValues,...
        'BarFaceColor', barcolours(1:length(rmean),:),...
        'PStarShowGT', false, 'PStarThreshold', [0.05],...
        'PStarShowNS', false, 'PStarOffset', 0.013)
    hold on
    plot(0:(length(rmean)+1), ones(1,length(rmean)+2), 'r')
    hold off
    ax = gca;
    ax.XTick = 1:length(rmean);
    ax.XTickLabel = modes(cComp(2:end));
    %ax.XTickLabelRotation = 45;
    ylabel(['Average Rescue Time Ratio to ' modes{cComp(1)}])
    xlabel([relCmpNames{row} ' setting'])
    ax = axis;
    axis([0.5 size(rmean,2)+.5 minRange maxRange])
    
    set(gcf, 'PaperPosition', [0 0 6 10]);
    set(gcf, 'PaperSize', [6 10]);
    saveas(gcf, [runName '_' strrep(relCmpNames{row}, ' ', '_') '-relative_bars.pdf']);
end

%% avg time
figure(figCnt)
clf
figCnt = figCnt+1;
xy = util(:,vsCompare(1:2));
plotPointDensity(xy, 15, true, cmap);
hold on
plot(xy(:,1), xy(:,2), 'ro');
%text(xy(:,1), xy(:,2), cellstr(num2str(simIds')), 'VerticalAlignment','bottom', ...
%                                                'HorizontalAlignment','right')
axis equal;
axis tight;
xlabel(['$\bar{t}$ ' modes(vsCompare(1))],'Interpreter','latex')
ylabel(['$\bar{t}$ ' modes(vsCompare(2))],'Interpreter','latex')
set(gcf, 'PaperPosition', [0 0 10 10]);
set(gcf, 'PaperSize', [10 10]);
saveas(gcf, [runName, '_', modes{vsCompare(1)}, 'vs', modes{vsCompare(2)}, '.pdf']);
