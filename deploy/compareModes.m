evalPath = 'd:\VisualStudio\AgentDraw\TestResults\';
simPath = 'd:\VisualStudio\AgentDraw\IRIDIS\relationTest\run07';
%modes = ['GN'; 'GM'; 'GS'; 'NN'; 'NM'; 'NS'];
%modes = ['NN'; 'NM'; 'NS'; 'SN'; 'SM'];
modes = ['G4'; 'G5'];
modeSize = size(modes,1);
simId = 1;
%% paths
startPath = cd;
TaskTimes = cell(1,modeid);
figure(1)
for modeid = 1:modeSize
    cd(strcat(simPath, modes(modeid, :)))
    eval(strcat('Sim', int2str(simId)))
    TaskTimes{modeid} = TaskTime;
    stepdataW = size(TimeStepData, 2);
    stepdataL = size(TimeStepData, 1);
    subplot(1, modeSize, modeid)
    hold off
    plot(TimeStepData(:, 1:3:stepdataW), TimeStepData(:, 2:3:stepdataW))
    hold on
    plot(TaskTime(:, 1), TaskTime(:, 2), 'rX', 'MarkerSize', 10)
    axis equal
    title(modes(modeid,:))
end

cd(startPath)
%% compare times
search = ~TaskTime(:,3);
nresq = sum(~search);
[~, sridx] = sort(search);
data = zeros(nresq, modeSize);
for modeid = 1:modeSize
    data(:,modeid) = TaskTimes{modeid}(sridx(1:nresq), 4);
end

resolution = 30;
figure(2)
maxorig = max(max(data));
minorig = min(min(data));
step = ((maxorig-minorig)/resolution);
edges = minorig:step:maxorig+step;
[X,Y] = meshgrid(edges, edges);
n = hist3(data, {edges, edges});
n1 = n';
pcolor(X, Y, n1)
colormap(cmap)
shading flat
hold on
plot([minorig maxorig+step],[minorig maxorig+step],'r-')
hold off
xlabel(['$\bar{t}$ ' modes(1,:)],'Interpreter','latex')
ylabel(['$\bar{t}$ ' modes(2,:)],'Interpreter','latex')
axis equal
axis tight