rundir = 'run26B4';
ids = 0:31;

startdir = cd(rundir);
eval('Sim0')
timings = cell(length(ids));
figure(1)
clf
id_seq = 1;
for id = ids
    eval(['Sim' int2str(id)])
    %plot(TimingData(:,1), TimingData(:,2))
    %hold on
    timings{id_seq} = TimingData(:,[1 3 5])';
    id_seq = id_seq+1;
end
cd(startdir)

XY = [timings{:}]';
max(XY(:,3))
XY = XY(2:end,1:2);
XY(:,2) = XY(:,2)/1000;
f=fit(XY(:,1), XY(:,2), 'poly3')
plot(f, XY(:,1), XY(:,2))
hold on
plot(0:500, f(0:500))
