#!/bin/bash 

export ID="26"
export NAME=ADTimingAjkaThesis_$ID
export PROG_DIR=/home/zb1f12/AgentDraw/timingTest
export SCRIPT2=$PROG_DIR/16runnerSleep.sh
export OUTDIR=run$ID
export SOLVER=solvertime
export SAMPLENO=100
export SAMPLEMC=5
export TSTEP=30
export BATCH16="0-7"
export TSLEEP=1

#BLANK MAP ----------------------------------------------------------------
#100 SAMPLES mode 0 take   0:00:23
#100 SAMPLES mode 1 take   0:00:23
#100 SAMPLES mode 2 take   0:40:40
#100 SAMPLES mode 3 take   2:49:46

export MODELDIR=../relationTest/ajkaModels07-20tasks
export SAMPLER=../relationTest/ajkaBlankSampler.sam

export SID=B4
mkdir $PROG_DIR/$OUTDIR$SID
qsub -l walltime=02:00:00,nodes=1:ppn=16 -t $BATCH16 -v MODELDIR=$MODELDIR,OUTDIR=$OUTDIR$SID,SAMPLER=$SAMPLER,SOLVER=$SOLVER,SMODE=5,SAMPLENO=$SAMPLENO,TSTEP=$TSTEP,TSLEEP=$TSLEEP -N ${NAME}_$SID $SCRIPT2
# qsub -l nodes=1:ppn=16 -t 0-1 -v MODELDIR=$MODELDIR,OUTDIR=$OUTDIR$SID,SAMPLER=$SAMPLER,SOLVER=$SOLVER,SMODE=5,SAMPLENO=$SAMPLENO,TSTEP=$TSTEP,TSLEEP=$TSLEEP -N ${NAME}_$SID -q test $SCRIPT2
