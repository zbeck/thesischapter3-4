#!/bin/bash

CMD=$1
TMPDIRABS=$2


mkdir -p $TMPDIRABS
echo "Running $CMD"
$CMD
echo "deleting $TMPDIRABS"
rm -f -r $TMPDIRABS