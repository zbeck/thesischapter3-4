#!/bin/bash
module load numpy
module load python
module load cplex
module load mono

export PROG_DIR=/home/zb1f12/AgentDraw/timingTest
export TMP_DIR=/scratch/zb1f12/AgentDraw
export MODELFILES=($PROG_DIR/$MODELDIR/*)
export OUTDIRABS=$PROG_DIR/$OUTDIR



PID=()
for i in {0..15}
do
    if [ "$i" != "0" ]; then
        echo "Sleeping $TSLEEP s"
        sleep $TSLEEP
    fi
    
    FILEID=`expr $i + $PBS_ARRAYID \\* 16`
    echo "run $i started, file ID: $FILEID"
    date
    MODELABS=${MODELFILES[$FILEID]}
    TMPDIRABS=$TMP_DIR/$OUTDIR/tmp$FILEID

    CMD="mono $PROG_DIR/ConsoleApp.exe $FILEID $SAMPLER $MODELABS $PROG_DIR $SOLVER $SMODE $OUTDIRABS $TMPDIRABS $SAMPLENO $TSTEP"
    $PROG_DIR/consoleApp_piece.sh "$CMD" $TMPDIRABS &
    PID[$i]=$!
done
for i in {0..15}
do
    #echo ${PID[$i]}
    wait ${PID[$i]}
done
