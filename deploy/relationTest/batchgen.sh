#!/bin/bash
ITERS=127
MYDIR=$1
BANNED=()
NBANNED=0
for i in $(seq 0 1 $ITERS)
do
    BANNED[$i]=0
done

for mydir in $MYDIR
do
    START=1000
    LAST=1000
    printf "BATCH$mydir=\""
    for i in $(seq 0 1 $ITERS)
    do
        if [ ! -f $mydir/Summary$i.txt ]; then
            BANNED[$i]=1
            if (( ($i - 1) == $LAST)); then
                # echo "debuga"
                LAST=$i
            else
                # echo "debugb"
                if (( ($START == $LAST) && ($LAST  != 1000) )); then
                    printf "$START,"
                elif (($LAST  != 1000)); then
                    printf "$START-$LAST,"
                fi
                START=$i
                LAST=$i
            fi
        fi
    done
    if (( ($START == $LAST) && ($LAST  != 1000) )); then
        printf "$START,"
    elif (($LAST  != 1000)); then
        printf "$START-$LAST,"
    fi
    echo -e '\b"'
done

printf "simIds = setdiff(simIds, ["
for i in $(seq 0 1 $ITERS)
do
    if ((${BANNED[$i]} == 1)); then
        NBANNED=$(($NBANNED + 1))
        printf "$i "
    fi
done
echo "]);"
echo $NBANNED
