# -*- coding: utf-8 -*-
"""
Created on Mon Jul 14 16:29:13 2014

@author: Zoli
"""

import sys
import glob
import os
import operator

def readParamOneLine(f, data):
    line = f.readline().strip().split('\t')
    data[line[0].replace('/', 'P')] = line[1].replace(' ', '_')
    
def readParamTwoLines(f, data, newName=''):
    name = f.readline().strip().strip(':').replace(' ', '_')
    if newName:
        name = newName
    data[name] = f.readline().strip().replace(' ', '_')
    
def readParamsTwoLines(f, data):
    name = f.readline().strip().strip(':').replace(' ', '_')
    vals = f.readline().strip().split('\t')
    for i in range(len(vals)):
        data[name+str(i)] = vals[i]

def getSummaryData(summaryFile):
    f = open(summaryFile, 'r')
    data = dict()
    readParamOneLine(f, data)   #Solver
    readParamOneLine(f, data)   #SamplePstep
    readParamOneLine(f, data)   #Timestep
    readParamsTwoLines(f, data) #Battery_time
    readParamTwoLines(f, data)  #Time
    readParamsTwoLines(f, data) #Utilities
    readParamsTwoLines(f, data) #Disatances
    f.readline()
    readParamsTwoLines(f, data) #Start
    readParamsTwoLines(f, data) #End
    readParamTwoLines(f, data)  #Computation_time
    readParamTwoLines(f, data, 'Solver_comp_time')  #Solver_comp_time
    readParamTwoLines(f, data)  #Average_time_per_step
    readParamTwoLines(f, data, 'Solver_avg_time')  #Solver_avg_time
    readParamTwoLines(f, data)  #Average_time_per_sample_by_solver
    readParamsTwoLines(f, data) #Overall_expansions_per_level
    readParamOneLine(f, data)   #overall
    readParamOneLine(f, data)   #first_level
    readParamsTwoLines(f, data) #Initial_expansions_per_level
    readParamsTwoLines(f, data) #First_step_utilities
    return data

if __name__ == '__main__':
    outf = open('summary.m', 'w')
    outf.write('dual = 2;\n')
    outf.write('mcsearch = 3;\n')
    outf.write('mcsearchUCB = 4;\n')
    outf.write('mcsnobottom = 5;\n')
    outf.write('mcsUCBnobottom = 6;\n')
    outf.write('No_Sampling = -1;\n')
    workdir = os.getcwd()
    for dataDic in sys.argv[1:]:
        allValues = [[] for i in range(31)]
        os.chdir(dataDic)
        for summaryFile in glob.glob('*.txt'):
            data = getSummaryData(summaryFile)
            i=0
            for pair in sorted(data.iteritems(), key=operator.itemgetter(0)):
                allValues[i].append(pair[1])
                i = i+1
        outf.write(dataDic.strip('/\\ \n')+' = [')
        outf.write(';...\n'.join([' '.join(row) for row in allValues]))
        outf.write('];\n')
        os.chdir(workdir)
    
    outf.write('names = {')
    outf.write('; '.join(['\''+pair[0]+'\'' for pair in sorted(data.iteritems(), key=operator.itemgetter(0))]))
    outf.write('};\n')
    outf.close()