function comparePaths(id)

    evalPath = 'd:\VisualStudio\AgentDraw\TestResults';
    simPath = 'd:\VisualStudio\AgentDraw\IRIDIS\relationTest\run15';
    modes = ['P5'; 'P6'; 'P7'; 'P8'];
    %sampledMode = [0, 1, 2, 3];
    modeSize = size(modes,1);
    
    figure(modeSize+1)
    clf

    startdir = cd(evalPath);
    for modeid = 1:modeSize
        cd(strcat(simPath, modes(modeid, :)))
        eval(strcat('Sim', int2str(id)))
        cd(evalPath);
%         figure(modeid);
%         sampled = sampledMode(modeid);
%         EvalSimFig(sampled, modeSize+1, TimeStepData, TaskTime)
        figure(modeid);
        EvalSim
        title(modes(modeid,:))
    end
    figure(modeSize+1)
    subplot(211)
    legend(modes)
    legend('Location', 'NorthWest')
    hold off
    cd(startdir)
end