%% setup
evalPath = 'd:\VisualStudio\AgentDraw\TestResults';
simIds = 0:127;

% runName = 'relationThesisHaiti17';
% runName = 'relationThesisHaiti17Test';
% runName = 'relationThesisAjka25';
runName = 'relationThesisAjka25Test';

if strcmp(runName, 'relationThesisHaiti17')
    simPath = 'd:\VisualStudio\AgentDraw\IRIDIS\relationTest\run17';
    modes = {'B0', 'B1', 'B2', 'B3', 'B4', 'B6', 'B7', 'B6x20', 'B7x20', 'P0', 'P1', 'P2', 'P3', 'P4', 'P6', 'P7', 'P6x20', 'P7x20', 'G0', 'G1', 'G2', 'G3', 'G4', 'G6', 'G7', 'G6x20', 'G7x20'};
    modeSize = length(modes);
    groups = {1:9, 10:18, 19:modeSize};
    compareShape = [9 1 2 3 4 5 6 7 8 9];
    relCompares = [compareShape; compareShape+9; compareShape+18];
    relCmpNames = {'Blank', 'Preliminary', 'Ground Truth'};
    vsCompare = [17 18];
elseif strcmp(runName, 'relationThesisAjka25')
    simPath = 'd:\VisualStudio\AgentDraw\IRIDIS\relationTest\run25';
    modes = {'B0', 'B1', 'B2', 'B3', 'B4', 'B6', 'B7', 'B6x20', 'B7x20', 'P0', 'P1', 'P2', 'P3', 'P4', 'P6', 'P7', 'P6x20', 'P7x20', 'G0', 'G1', 'G2', 'G3', 'G4', 'G6', 'G7', 'G6x20', 'G7x20'};
    modeSize = length(modes);
    groups = {1:9, 10:18, 19:modeSize};
    compareShape = [9 1 2 3 4 5 6 7 8 9];
    relCompares = [compareShape; compareShape+9; compareShape+18];
    relCmpNames = {'Blank', 'Preliminary', 'Ground Truth'};
    vsCompare = [17 18];
    simIds = setdiff(simIds, [0 3 9 13 27 48 106 112 121 ]);
elseif strcmp(runName, 'relationThesisAjka25Test')
    simPath = 'd:\VisualStudio\AgentDraw\IRIDIS\relationTest\run25';
    modes = {'B0', 'B1', 'B2', 'B3', 'B4', 'P0', 'P1', 'P2', 'P3', 'P4', 'G0', 'G1', 'G2', 'G3', 'G4'};
    modeSize = length(modes);
    groups = {1:5, 6:10, 11:modeSize};
    relCmpNames = {'Blank', 'Preliminary', 'Ground Truth'};
elseif strcmp(runName, 'relationThesisHaiti17Test')
    simPath = 'd:\VisualStudio\AgentDraw\IRIDIS\relationTest\run17';
    modes = {'B0', 'B1', 'B2', 'B3', 'B4', 'B6', 'P0', 'P1', 'P2', 'P3', 'P4', 'P6', 'G0', 'G1', 'G2', 'G3', 'G6', 'G7'};
    modeSize = length(modes);
    groups = {1:6, 7:12, 13:modeSize};
    relCmpNames = {'Blank', 'Preliminary', 'Ground Truth'};
end

%http://www.colourlovers.com/palette/2829194/spring_garden
%malefactor, honey glow, basement blues, worry, stark
myColours = {[232/255, 0, 46/255], [1, 187/255, 46/255], [180/255, 152/255, 0],...
    [0, 111/255, 47/255], [25/255, 73/255, 0]};

nsim = length(simIds);

%% read data
matFileName = [simPath '_timing.mat'];
if exist(matFileName, 'file') == 2
    load(matFileName);
else
    stepTimings = cell(modeSize, nsim);
    firstStepTime = zeros(modeSize, nsim);
    startdir = cd(evalPath);
    for modeid = 1:modeSize
        cd(strcat(simPath, modes{modeid}))
        for id = 1:nsim;
            eval(strcat('Sim', int2str(simIds(id))))
            stepTimings{modeid, id} = diff(UtilsPerStep(:, end))';
            firstStepTime(modeid, id) = UtilsPerStep(1, end);
        end
    end
    cd(startdir)
    save(matFileName, 'stepTimings', 'firstStepTime')
end

%% plot data
groupsize = length(groups{1});
figure(1)
for modeid = 1:modeSize
    subplot(length(groups), groupsize, modeid)
    histogram([stepTimings{modeid,:}])
    title(modes{modeid})
    xlabel('Time [ms]')
end
