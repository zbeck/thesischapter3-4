evalPath = 'c:\Zoli\SOTON\AgentDraw\relationTest\';
new_modes = {'B6', 'B7', 'B6x20', 'B7x20', 'P6', 'P7', 'P6x20', 'P7x20', 'G6', 'G7', 'G6x20', 'G7x20'};

modesize = length(new_modes);

runName = 'relationThesisHaiti16';
% runName = 'relationThesisAjka24';

startdir = cd(evalPath);
if strcmp(runName, 'relationThesisHaiti16')
    titleName = 'Haiti';
    modeStart = 'run16';
    eval('summary16')
elseif strcmp(runName, 'relationThesisAjka24')
    titleName = 'Ajka';
    modeStart = 'run24';
    eval('summary24')
end

cd(startdir);

row=17:19;
names(row)
expansions = zeros(length(row), modesize);
for modeid = 1:modesize
    data = eval([modeStart new_modes{modeid}]);
    expansions(:, modeid) = sum(data(row, :), 2);
end
%expansions = expansions ./ repmat(sum(expansions, 1), [4 1]);

figure(3)
clf
bar(expansions')
set(gca,'XTick', 1:modesize)
set(gca,'XTickLabel',new_modes)
set(gca,'YScale','log')
legend('Rescue solution', 'Rescue problem', 'Search solution (root)')
legend('Location', 'East')
title([titleName, ' overall expansions'])
ylabel('Expansions')
xlabel('Method')
ax = gca;
ax.YGrid = 'on';
ax.YMinorGrid = 'on';

set(gcf, 'PaperPosition', [0 0 20 15]);
set(gcf, 'PaperSize', [20 15]);
saveas(gcf, [runName '_overall_expansion.pdf']);

row=13:15;
names(row)
expansions = zeros(length(row), modesize);
for modeid = 1:modesize
    data = eval([modeStart new_modes{modeid}]);
    expansions(:, modeid) = sum(data(row, :), 2);
end


figure(2)
clf
bar(expansions')
set(gca,'XTick', 1:modesize)
set(gca,'XTickLabel',new_modes)
set(gca,'YScale','log')
legend('Rescue solution', 'Rescue problem', 'Search solution (root)')
legend('Location', 'East')
legend('Location', 'East')
title([titleName, ' initial expansions'])
ylabel('Expansions')
xlabel('Method')
ax = gca;
ax.YGrid = 'on';
ax.YMinorGrid = 'on';

set(gcf, 'PaperPosition', [0 0 20 15]);
set(gcf, 'PaperSize', [20 15]);
saveas(gcf, [runName '_initial_expansion.pdf']);
