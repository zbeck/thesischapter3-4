SimFolder = 'd:\VisualStudio\AgentDraw\IRIDIS\relationTest\v_18mresults\run11';
AnimFolder = 'd:\VisualStudio\AgentDraw\TestResults';
runA = 'P5';
runB = 'P6';
Sim = 'Sim117';

startdir = cd(strcat(SimFolder, runA));
eval(Sim);
cd(AnimFolder);
eval('AnimateSimExploration')
title(runA)

cd(strcat(SimFolder, runB));
eval(Sim);
cd(AnimFolder);
eval('AnimateSimExploration')
title(runB)

cd(startdir);