#!/bin/bash 

export MODELS=intmodel_real_*
SCRIPT=/home/zb1f12/AgentDraw/CP_TimingTest/timeTestRunner.sh
for f in $MODELS
do
    len=${#f}
    case "$len" in
    20) qsub -l walltime=00:00:20,nodes=1:ppn=4 -t 0-9 -v MODEL=$f -N CP_timing2_$f $SCRIPT
        ;;
    21) qsub -l walltime=00:00:40,nodes=1:ppn=4 -t 0-9 -v MODEL=$f -N CP_timing2_$f $SCRIPT
        ;;
    22) qsub -l walltime=00:05:20,nodes=1:ppn=4 -t 0-9 -v MODEL=$f -N CP_timing2_$f $SCRIPT
        ;;
    23) qsub -l walltime=00:52:00,nodes=1:ppn=4,mem=16gb -v MODEL=$f -N CP_timing2_$f $SCRIPT
        ;;
    24) qsub -l walltime=02:52:00,nodes=1:ppn=4,mem=16gb -v MODEL=$f -N CP_timing2_$f $SCRIPT
        ;;
    esac
done
echo "done"