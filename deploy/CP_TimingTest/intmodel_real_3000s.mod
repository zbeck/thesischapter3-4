using CP;

int nofAgents0 = ...;
int nofSlots0 = ...;
int nofTasks0 = ...;
range agents0 = 1..nofAgents0;
range slots0 = 1..nofSlots0;
range tasks0 = 1..nofTasks0;
int travelTT0[tasks0][tasks0] = ...;
int travelAT0[agents0][tasks0] = ...;
int battery0 = ...;
float P0 = ...;
float lam0 = ...;
float epsilon0 = ...;

dvar int x0[agents0][slots0] in tasks0;
dvar int executed0[agents0][slots0];

dexpr int activation0[j in tasks0] = min(i in agents0, k in slots0) ((x0[i][k] == j) ? executed0[i][k] : maxint);
dexpr float util0[i in agents0][k in slots0] = (executed0[i][k] > battery0) ? -battery0 : ((x0[i][k] == nofTasks0)? 0 : ((P0 - lam0 * executed0[i][k]) * pow(epsilon0,k-1)));

int nofAgents1 = ...;
int nofSlots1 = ...;
int nofTasks1 = ...;
range agents1 = 1..nofAgents1;
range slots1 = 1..nofSlots1;
range tasks1 = 1..nofTasks1;
int travelTT1[tasks1][tasks1] = ...;
int travelAT1[agents1][tasks1] = ...;
int battery1 = ...;
float P1 = ...;
float lam1 = ...;
float epsilon1 = ...;
float degrade1 = ...;
int enableMatrix1[tasks0][tasks1] = ...;

dvar int x1[agents1][slots1] in tasks1;
dvar int executed1[agents1][slots1];

dexpr int enabled1[i in agents1][k in slots1] = (sum(j in tasks0) enableMatrix1[j][x1[i][k]] == 0)? 0 : min(j in tasks0) ((enableMatrix1[j][x1[i][k]] != 0) ? activation0[j] : maxint);
dexpr int notDiscovered1[i in agents1][k in slots1] = (executed1[i][k] < enabled1[i][k]);
dexpr int batteryRemain1[i in agents1] = battery1 - sum(k in slots1) ((executed1[i][k] < enabled1[i][k]) ? (enabled1[i][k] - executed1[i][k]) : 0);
dexpr float util1[i in agents1][k in slots1] = (executed1[i][k] > batteryRemain1[i]) ? -battery1 : ((x1[i][k] == nofTasks1)? 0 : ((P1 - lam1 * executed1[i][k] - notDiscovered1[i][k] * lam1 * (enabled1[i][k] - executed1[i][k])) * pow(epsilon1,k-1)));

execute{
  cp.param.timeMode = "ElapsedTime";
  cp.param.timeLimit = 3000;
  cp.param.LogVerbosity = "Quiet";
  cp.param.workers = 4;
}

maximize
  (sum(i in agents0, k in slots0) util0[i][k]) + (sum(i in agents1, k in slots1) util1[i][k]) - (sum(i in agents1, k in slots1) notDiscovered1[i][k]*lam1*(enabled1[i][k] - executed1[i][k])*(maxl(nofSlots1 - k - count(all(kk in slots1) x1[i][kk], nofTasks1), 0)));

subject to {
  TaskOnce:
	forall(i, ii in agents0, k, kk in slots0: (i!=ii) || (k!=kk))
	  (x0[i][k] != x0[ii][kk]) || (x0[i][k] == nofTasks0);
	forall(i, ii in agents1, k, kk in slots1: (i!=ii) || (k!=kk))
	  (x1[i][k] != x1[ii][kk]) || (x1[i][k] == nofTasks1);

  forall(i in agents0)
    forall( k in slots0){
    if(k == 1)
	  executed0[i][k] == travelAT0[i][x0[i][1]];
    else
	  executed0[i][k] == travelTT0[x0[i][k-1]][x0[i][k]] + executed0[i][k-1];
  }
  forall(i in agents1)
    forall(k in slots1){
    if(k == 1)
	  executed1[i][k] == travelAT1[i][x1[i][1]];
    else
	  executed1[i][k] == travelTT1[x1[i][k-1]][x1[i][k]] + executed1[i][k-1];
  }
}
execute{
  write("[[");
  for(var i in agents0){
    write("[");
	for(var k in slots0){
	  if(x0[i][k] < nofTasks0)
	  	write(x0[i][k] -1);
      if((k < nofSlots0) && (x0[i][k+1] < nofTasks0))
      	write(", ");
    }
	write("]");
    if(i < nofAgents0)
  	  write(", ");
  }
  write("], [");
    
  for(var i in agents1){
    write("[");
	for(var k in slots1){
      if(x1[i][k] < nofTasks1)
	  	write(x1[i][k] -1);
      if((k < nofSlots1) && (x1[i][k+1] < nofTasks1))
      	write(", ");
	}
	write("]");
    if(i < nofAgents1)
  	  write(", ");
  }
  write("]]");
}
