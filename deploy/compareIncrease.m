%% setup
evalPath = 'd:\VisualStudio\AgentDraw\IRIDIS\';
simIds = 0:127;

% runName = 'relationThesisHaiti18';
% runName = 'relationThesisHaiti18Local';
% runName = 'relationThesisAjka25';
runName = 'relationThesisAjka26Local';

if strcmp(runName, 'relationThesisHaiti18')
    simPath = 'd:\VisualStudio\AgentDraw\IRIDIS\increaseTest\run18';
    modes = {'B6I', 'B7I', 'P6I', 'P7I', 'G6I', 'G7I'};
    modeSize = length(modes);
    groups = {[1 3 5], [2 4 6]};
    groupNames = {'UCB MCTS', 'VoI MCTS'};
    groupLegend = {'Haiti \emph{Zero information} setting', 'Haiti \emph{Mediocre information} setting', 'Haiti \emph{Ground Truth information} setting'};
elseif strcmp(runName, 'relationThesisHaiti18Local')
    simPath = 'd:\VisualStudio\AgentDraw\TestResults\consoleRun\run18';
    modes = {'B6I', 'B7I', 'P6I', 'P7I', 'G6I', 'G7I'};
    modeSize = length(modes);
    groups = {[1 3 5], [2 4 6]};
    groupNames = {'UCB', 'VoI'};
    groupLegend = {'Haiti \emph{Zero information} setting', 'Haiti \emph{Resilient setting}', 'Haiti \emph{Complete information} setting'};
elseif strcmp(runName, 'relationThesisHaiti25')
    simPath = 'd:\VisualStudio\AgentDraw\IRIDIS\increaseTest\run25';
elseif strcmp(runName, 'relationThesisAjka26Local')
    simPath = 'd:\VisualStudio\AgentDraw\TestResults\consoleRun\run26';
    modes = {'B6I', 'B7I', 'P6I', 'P7I', 'G6I', 'G7I'};
    modeSize = length(modes);
    groups = {[1 3 5], [2 4 6]};
    groupNames = {'UCB', 'VoI'};
    groupLegend = {'Ajka \emph{Zero information} setting', 'Ajka \emph{Resilient setting}', 'Ajka \emph{Complete information} setting'};
    simIds = setdiff(simIds, [0]);
end

%http://www.colourlovers.com/palette/2829194/spring_garden
%malefactor, honey glow, basement blues, worry, stark
myColours = {[232/255, 0, 46/255], [1, 187/255, 46/255], [180/255, 152/255, 0],...
    [0, 111/255, 47/255], [25/255, 73/255, 0]};

nsim = length(simIds);

%% read data
matFileName = [simPath '_inc.mat'];
if exist(matFileName, 'file') == 2
    load(matFileName);
else
    increaseData = cell(modeSize, nsim);
    firstStepTime = zeros(modeSize, nsim);
    startdir = cd(evalPath);
    for modeid = 1:modeSize
        cd(strcat(simPath, modes{modeid}))
        for id = 1:nsim
            eval(strcat('Sim', int2str(simIds(id))))
            firstRow = [UtilsPerStep(1,1), UtilsPerStep(1,2), 0, 0, 0, 0, 0];
            %UtilsPerStep(:,5) = UtilsPerStep(:,5) - UtilsPerStep(1,5) + 20;
            increaseData{modeid, id} = [firstRow; UtilsPerStep];
        end
    end
    cd(startdir)
    save(matFileName, 'increaseData')
end

%% process data
npoints = 200;
xstart = 1000;
xend = 100000;
xvals = logspace(log10(xstart), log10(xend), npoints);
% xvals = 1000:1000:200000;
npoints = length(xvals);
yvals = zeros(4, npoints, modeSize);
ygroup = zeros(4, npoints, length(groups));
endvals = zeros(modeSize, nsim, 2); %final greedy value and standard error
for modeid = 1:modeSize
    for id = 1:nsim
        data = increaseData{modeid, id};
        endvals(modeid, id, :) = data(end, [2 4]);
        [C, idx] = mergesa(data(:,5), xvals);
        mask = idx<0;
        dataidx = idx([mask(2:end), false]);
        for i = 1: length(dataidx)
            if dataidx(i) < 0
                dataidx(i) = dataidx(i-1);
            end
        end
        yvals(:, :,modeid) = yvals(:, :,modeid) + data(dataidx,1:4)';
    end
end
yvals = yvals / nsim;
for gr = 1:length(groups)
    for modeid = groups{gr}
        ygroup(:, :, gr) = ygroup(:, :, gr) + yvals(:,:,modeid);
    end
    ygroup(:, :, gr) = ygroup(:, :, gr)/length(groups{gr});
end

% uncertainty
stderr = zeros(modeSize, nsim, 2);
for modeid = 1:modeSize
    for id = 1:nsim
        stderr(modeid, id, :) = increaseData{modeid, id}(end, 3:4);
    end
end

refvals = zeros(1,length(groupLegend));
for gind = 1:length(groupLegend)
    modeids = zeros(1,length(groups));
    for gr = 1:length(groups)
        modeids(gr) = groups{gr}(gind);
    end
    [~, mind] = min(endvals(modeids, :, 2), [], 1);
    refelements = zeros(1,nsim);
    for id = 1:nsim
        refelements(id) = endvals(modeids(mind(id)), id, 1);
    end
    refvals(gind) = mean(refelements); 
end

increasenum = zeros(modeSize, 3);
avgincrease = zeros(modeSize, 3);
sumincrease = zeros(modeSize, 3);
avgsteptime = zeros(modeSize, 3);
sumsteptime = zeros(modeSize, 3);
initialIncrease = zeros(modeSize, nsim);
stepdiff = cell(1, modeSize);
steptiming = cell(1, modeSize);
for modeid = 1:modeSize
    stepcount = 0;
    for id = 1:nsim
        data = increaseData{modeid, id};
        for type = 1:3
            increasenum(modeid, type) = increasenum(modeid, type) + sum(data(:,7)==type);
        end
        initialIncrease(modeid, nsim) = data(2,1)-data(2,2);
        stepcount = stepcount + max(0, size(data, 1) - 2);
    end
    stepdiff{modeid} = zeros(stepcount, 3);
    steptiming{modeid} = zeros(stepcount, 1);
    startind = 1;
    for id = 1:nsim
        data = increaseData{modeid, id};
        steplen = max(0, size(data, 1) - 2);
        stepdiff{modeid}(startind:(startind+steplen-1), :) = [diff(data(2:end, [1 5]),1,1) data(3:end, 7)];
        steptiming{modeid}(startind:(startind+steplen-1)) = data(2:end-1, 5);
        startind = startind + steplen;
    end
    for type = 1:3
        typeindices = stepdiff{modeid}(:,3) == type;
        avgincrease(modeid, type) = mean(stepdiff{modeid}(typeindices,1));
        sumincrease(modeid, type) = sum(stepdiff{modeid}(typeindices,1));
        if isnan(avgincrease(modeid, type))
            sumincrease(modeid, type) = NaN;
        end
        avgsteptime(modeid, type) = mean(stepdiff{modeid}(typeindices,2));
        sumsteptime(modeid, type) = sum(stepdiff{modeid}(typeindices,2));
        if isnan(avgsteptime(modeid, type))
            sumsteptime(modeid, type) = NaN;
        end
    end
end

%time in seconds
xvals = xvals/1000;

%% plot data over time
figure(1)
clf
ymax = 0;
ymin = 0;
ncols = 2;
% for gind = 1:length(groupLegend)
%     [~, bestRef] = min(
%     refvals(gind) = 
% for modeid = 1:modeSize
%     subplot(modeSize/ncols, ncols, modeid)
%     plot(xvals, zeros(npoints, 1), 'k-');
%     hold on
%     for type = [1 ]
%         indices = (stepdiff{modeid}(:,3)==type) & (stepdiff{modeid}(:,1)~=0);
%         ax = gca;
%         ax.ColorOrderIndex = type;
%         plot(steptiming{modeid}(indices)/1000, stepdiff{modeid}(indices, 1)*5000, '.');
%         hold on
%     end
% end
for gr = 1:length(groups)
    subplot(length(groups)/ncols, ncols, gr)
    plot(xvals, (squeeze(yvals(1,:,groups{gr}))-repmat(refvals, [npoints 1]))*5000, 'LineWidth', 2)
%     plot(xvals, [squeeze(yvals(1,:,groups{gr})) squeeze(yvals(2,:,groups{gr}))])
    hold on
%     plot(xvals, squeeze(ygroup(1,:,gr)-ygroup(2,:,gr))*5000, 'r', 'LineWidth', 2)
    ax = gca;
    ax.XScale = 'log';
    ax.XGrid = 'on';
    ax.YGrid = 'on';
    title(groupNames{gr})
    xlabel('Computation Time [s]')
    ylabel('Average rescue time improvement [s]')
    ax = axis;
    ymax = max(ymax, ax(4));
    ymin = min(ymin, ax(3));
%     h = legend({groupLegend{:}, 'Average'}, 'Location', 'NorthWest');
    h = legend(groupLegend, 'Location', 'NorthWest');
    h.Interpreter = 'latex';
    h.FontSize = 10;
end

% for gr = 1:length(groups)
%     subplot(length(groups)/ncols, ncols, gr)
%     ax = axis;
%     axis([min(xvals) max(xvals) ymin ymax])
% end

set(gcf, 'PaperPosition', [-2 0 30 15]);
set(gcf, 'PaperSize', [26 14.7]);
saveas(gcf, [runName '_increase.pdf']);

%mean(stderr, 2)*5000/sqrt(nsim)

%% plot expansion statistics

xrange = [0.5 2.5];
for gind = 1:length(groupLegend)
    figure(1+gind)
    clf
    subfigID = 1;
    modeids = zeros(1,length(groups));
    for gr = 1:length(groups)
        modeids(gr) = groups{gr}(gind);
    end
    
    subplot(1, 3, subfigID)
    subfigID = subfigID+1;
    bar(increasenum(modeids, :), 'LineWidth', 1);
    axis([xrange 0.66 max(max(increasenum(modeids, :)))*1.5])
    ylabel('expansion count', 'FontSize', 12);
    ax = gca;
    ax.YScale = 'log';
    ax.XTickLabel = groupNames;
    
    subplot(1, 3, subfigID)
    subfigID = subfigID+1;
    y = sumincrease*5000;
    rounding = 10^(floor(log10(max(y(modeids(gr), [1 3]))))-1);
    bar(y(modeids, :), 'LineWidth', 1.3);
%     limits = axis;
    ymax = ceil2(max(y(modeids(gr),[1 3])+1), rounding);
    axis([xrange -ymax*0.05 ymax])
    ylabel('solution quality increase', 'FontSize', 12);
%     h = title(groupLegend{gind}, 'Interpreter', 'Latex', 'FontSize', 18);
    ax = gca;
    ax.XTickLabel = groupNames;
    h = legend('Rescue solution', 'Rescue problem', 'Search solution');
    if strcmp(runName, 'relationThesisAjka26Local') && (gind == 3)
        h.Location = 'NorthWest';
    else
        h.Location = 'West';
    end
%     h.Position = [0 0.03 0 0] + h.Position;
    
    subplot(1, 3, subfigID)
    subfigID = subfigID+1;
%     y = avgincrease(modeids, :)*5000 ./ avgsteptime(modeids, :);
    y = sumsteptime/1000;
    bar(y(modeids, :), 'LineWidth', 1);
%     limits = axis;
    axis([xrange min(min(y))*0.6 max(max(y))/0.6])
    ylabel('expansion time', 'FontSize', 12);
    ax = gca;
    ax.YScale = 'log';
    ax.XTickLabel = groupNames;
    
    set(gcf, 'PaperPosition', [-1.5 0 20 8]);
    set(gcf, 'PaperSize', [17 8]);
    group_string = strsplit(groupLegend{gind}, {'{', '}'});
    group_string = strrep(group_string{2}, ' ', '_');
    saveas(gcf, [runName group_string '_incstat.pdf']);
end
% for column = 1:2
%     figure(1+column)
%     clf
%     subfigID = 1;
%     for gind = 1:length(groupLegend)
%         for type = 1:3
%             subplot(length(groupLegend), 3, subfigID)
%             subfigID = subfigID+1;
%             for gr = 1:length(groups)
%                 data = stepdiff{groups{gr}(gind)};
%                 histogram(data(data(:,3)==type, column))
%                 hold on
%             end
%             h = title([groupLegend{gind}, ' type ', int2str(type)]);
%             h.Interpreter = 'latex';
%         end
%     end
% end