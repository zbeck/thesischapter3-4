﻿using MyExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AStarSolver
{
    public class TeamEstimator<Node> : TeamPath<Node>
    {
        public static readonly Func<AStarSolver.TeamPath<Node>, double> BestEstimate = PreciseOverestimateNoStar;

        private TeamEstimator() :
            base(new List<Node>(0), 0, new List<Node>(0), null) { }
        //public static int EstimateCompare=0;

        private static double CompareEstimates(TeamPath<Node> tp)
        {
            double old = DumbOverestimate(tp);
            double n = PreciseOverestimate(tp);
            /*if (n <= old)
                EstimateCompare++;*/
            return old;
        }

        private static double DumbOverestimate(TeamPath<Node> tp)
        {
            double mintime = tp.Agents.Min(agent => agent.TravelTime);
            double estimate = (P - mintime) * tp.AvailableTasks;
            return estimate;
        }

        private static double PreciseOverestimate(TeamPath<Node> tp)
        {
            //only if the battery time is the same
            double sumBat = tp.Agents.Length * tp.Agents[0].BatteryTime;
            foreach (Agent a in tp.Agents)
                sumBat -= a.TravelTime;
            List<double> agentTime = new List<double>(tp.Agents.Length);
            for (int i = 0; i < tp.Agents.Length; i++)
                agentTime.Add(tp.Agents[i].TravelTime);
            agentTime.Sort();

            double estimate = 0;
            int count = 0;
            foreach (Tuple<int, int> e in Egdes)
            {
                if (count == tp.AvailableTasks) //if done maximum allowed number of edges
                    break;
                if (e.Item1 >= tp.nTasks)
                {
                    if (!((tp.Agents[e.Item1 - tp.nTasks].Position >= tp.nTasks) &&
                        tp.AvailableTask[e.Item2]))                                     //agent-task edge not available
                        continue;
                }
                else
                {
                    if (!((tp.AvailableTask[e.Item1] ||
                            MyExtensionMethods.Contains<Agent, int>(tp.Agents, e.Item1, agent => agent.Position)) &&
                        tp.AvailableTask[e.Item2]))   //task-task edge not available
                        continue;
                }
                sumBat -= DistanceTable[e.Item1, e.Item2];
                if (sumBat < 0)  //if sum battery time exceeded
                    break;
                agentTime[0] += DistanceTable[e.Item1, e.Item2];    //add edge to earliest agent
                estimate += P - agentTime[0];
                agentTime.Sort();
                count++;
            }
            return estimate;
        }

        private static double PreciseOverestimateNoStar(TeamPath<Node> tp)
        {
            //only if the battery time is the same
            double sumBat = tp.Agents.Length * tp.Agents[0].BatteryTime;
            foreach (Agent a in tp.Agents)
                sumBat -= a.TravelTime;
            List<double> agentTime = new List<double>(tp.Agents.Length);
            for (int i = 0; i < tp.Agents.Length; i++)
                agentTime.Add(tp.Agents[i].TravelTime);
            agentTime.Sort();
            int[] chosenEdges = new int[tp.nTasks];
            for (int i = 0; i < tp.nTasks; i++)
                chosenEdges[i] = 0;

            double estimate = 0;
            int count = 0;
            foreach (Tuple<int, int> e in Egdes)
            {
                if (count == tp.AvailableTasks) //if done maximum allowed number of edges
                    break;
                if (e.Item1 >= tp.nTasks)
                {
                    if (!((tp.Agents[e.Item1 - tp.nTasks].Position >= tp.nTasks) &&
                        tp.AvailableTask[e.Item2]))                                     //agent-task edge not available
                        continue;
                }
                else
                {
                    if (!((tp.AvailableTask[e.Item1] ||
                            MyExtensionMethods.Contains<Agent, int>(tp.Agents, e.Item1, agent => agent.Position)) &&
                        tp.AvailableTask[e.Item2]))   //task-task edge not available
                        continue;
                    if (chosenEdges[e.Item1] > 1)
                        continue;
                    chosenEdges[e.Item1]++;
                }
                if (chosenEdges[e.Item2] > 1)
                    continue;
                chosenEdges[e.Item2]++;

                sumBat -= DistanceTable[e.Item1, e.Item2];
                if (sumBat < 0)  //if sum battery time exceeded
                    break;
                agentTime[0] += DistanceTable[e.Item1, e.Item2];    //add edge to earliest agent
                estimate += P - agentTime[0];
                agentTime.Sort();
                count++;
            }
            return estimate;
        }

        private static double PreciseOverestimate2(TeamPath<Node> tp)
        {
            //only if the battery time is the same
            double sumBat = tp.Agents.Length * tp.Agents[0].BatteryTime;
            foreach (Agent a in tp.Agents)
                sumBat -= a.TravelTime;
            List<double> agentTime = new List<double>(tp.Agents.Length);
            for (int i = 0; i < tp.Agents.Length; i++)
                agentTime.Add(tp.Agents[i].TravelTime);
            agentTime.Sort();
            List<HashSet<int>> nodes = new List<HashSet<int>>(tp.AvailableTasks);

            double estimate = 0;
            int count = 0;
            foreach (Tuple<int, int> e in Egdes)
            {
                if (count == tp.AvailableTasks) //if done maximum allowed number of edges
                    break;
                if (e.Item1 >= tp.nTasks)
                {
                    if (!((tp.Agents[e.Item1 - tp.nTasks].Position >= tp.nTasks) &&
                        tp.AvailableTask[e.Item2]))                                     //agent-task edge not available
                        continue;
                }
                else
                {
                    if (!( (tp.AvailableTask[e.Item1] || 
                            MyExtensionMethods.Contains<Agent, int>(tp.Agents, e.Item1, agent => agent.Position)) &&
                        tp.AvailableTask[e.Item2]))   //task-task edge not available
                        continue;
                }
                List<HashSet<int>> connected = new List<HashSet<int>>();
                bool doesLoop = false;
                foreach (HashSet<int> s in nodes)
                {
                    if (s.Contains(e.Item1) && s.Contains(e.Item2))
                        doesLoop = true;
                    else if (s.Contains(e.Item1) || s.Contains(e.Item2))
                        connected.Add(s);
                }
                if (doesLoop)
                    continue;
                if (connected.Count == 0)
                {
                    HashSet<int> hs = new HashSet<int>();
                    hs.Add(e.Item1);
                    hs.Add(e.Item2);
                    connected.Add(hs);
                }
                else
                {
                    connected[0].Add(e.Item1);
                    connected[0].Add(e.Item2);
                    for (int i = 1; i < connected.Count; i++)
                    {
                        connected[0].UnionWith(connected[i]);
                        nodes.Remove(connected[i]);
                    }
                }
                sumBat -= DistanceTable[e.Item1, e.Item2];
                if (sumBat < 0)  //if sum battery time exceeded
                    break;
                agentTime[0] += DistanceTable[e.Item1, e.Item2];    //add edge to earliest agent
                estimate += P - agentTime[0];
                agentTime.Sort();
                count++;
            }
            return estimate;
        }

        private static double MediumOverestimate(TeamPath<Node> tp)
        {
            //only if the battery time is the same
            double sumBat = tp.Agents.Length * tp.Agents[0].BatteryTime;
            foreach (Agent a in tp.Agents)
                sumBat -= a.TravelTime;
            List<double> agentTime = new List<double>(tp.Agents.Length);
            for (int i = 0; i < tp.Agents.Length; i++)
                agentTime.Add(tp.Agents[i].TravelTime);
            agentTime.Sort();

            double estimate = 0;
            int count = 0;
            foreach (Tuple<int, int> e in Egdes)
            {
                if (count == tp.AvailableTasks) //if done maximum allowed number of edges
                    break;
                if(!tp.AvailableTask[e.Item2])  //edge not availabel if destination is done
                        continue;
                sumBat -= DistanceTable[e.Item1, e.Item2];
                if (sumBat < 0)  //if sum battery time exceeded
                    break;
                agentTime[0] += DistanceTable[e.Item1, e.Item2];    //add edge to earliest agent
                estimate += P - agentTime[0];
                agentTime.Sort();
                count++;
            }
            return estimate;
        }

        private static double ParallelOverestimate(TeamPath<Node> tp)
        {
            //only if the battery time is the same
            double sumBat = tp.Agents.Length * tp.Agents[0].BatteryTime;
            foreach (Agent a in tp.Agents)
                sumBat -= a.TravelTime;
            double mintime = tp.Agents.Min(agent => agent.TravelTime);

            double estimate = 0;
            int count = 0;
            foreach (Tuple<int, int> e in Egdes)
            {
                if (count == tp.AvailableTasks) //if done maximum allowed number of edges
                    break;
                if(!tp.AvailableTask[e.Item2])  //edge not availabel if destination is done
                        continue;
                sumBat -= DistanceTable[e.Item1, e.Item2];
                if (sumBat < 0)  //if sum battery time exceeded
                    break;
                estimate += P - DistanceTable[e.Item1, e.Item2] - mintime;
                count++;
            }
            return estimate;
        }

        /*public static double DictionaryOverestimate(TeamPath<Node> tp)
        {
            if (tp.nTasks > 64)
                throw new Exception("DictionaryOverestimate doesn't support more than 64 tasks!");
            long[] availableTask = new long[1];

            return 0;
        }*/
            
    }
}
