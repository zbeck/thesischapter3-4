﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AStarSolver
{
    public class TeamPath<Node> : IHasNeighbours<TeamPath<Node>>
    {
        public static long seenStates;
        public static double P = 1000;
        public double TotalUtil { get; private set; }
        public Agent[] Agents { get; private set; }
        public BitArray AvailableTask { get; private set; }
        Func<Node, Node, double> FuncDistance;
        protected static TeamPath<Node> DummyElement = new TeamPath<Node>(new List<Node>(0), 0, new List<Node>(0), null);
        public static double[,] DistanceTable { get; private set; }
        public static List<Tuple<int, int>> Egdes { get; private set; } //a sorted list of the edges by the length for the utility estimate
        public int AvailableTasks { get; private set; }
        public int nTasks { get { return AvailableTask.Length; } }

        /// <summary>
        /// Incremental constructor, creates a new node by manipulating an agent's schedule (adding a new task or removing the last one)
        /// </summary>
        /// <param name="movedAgent">the manipulated Agent class that represents the schedule</param>
        /// <param name="agentIndex">the index of the manipulated agent</param>
        /// <param name="utilGrowth">increase in the gained utility</param>
        /// <param name="orig">original teampath(parent)</param>
        /// <param name="backwards">if a task is removed or added</param>
        private TeamPath(Agent movedAgent, int agentIndex, double utilGrowth, TeamPath<Node> orig, bool backwards = false)
        {
            Agents = new Agent[orig.Agents.Length];
            AvailableTask = new BitArray(orig.AvailableTask);
            Array.Copy(orig.Agents, Agents, Agents.Length);
            if (backwards)
            {
                AvailableTask[Agents[agentIndex].Position] = true;
                AvailableTasks = orig.AvailableTasks + 1;
            }
            else
            {
                AvailableTask[movedAgent.Position] = false;
                AvailableTasks = orig.AvailableTasks - 1;
            }
            Agents[agentIndex] = movedAgent;
            TotalUtil = orig.TotalUtil + utilGrowth;
                
            FuncDistance = orig.FuncDistance;
        }

        /// <summary>
        /// Initial constructor, creates a root node given the problem parameters
        /// </summary>
        /// <param name="start">start location of agents</param>
        /// <param name="battery">battery time of agents</param>
        /// <param name="tasks">task locations</param>
        /// <param name="distance">distance function</param>
        public TeamPath(List<Node> start, double battery, List<Node> tasks, Func<Node, Node, double> distance)
        {
            TotalUtil = 0;
            AvailableTask = new BitArray(tasks.Count, true);
            AvailableTasks = tasks.Count;

            FuncDistance = distance;

            //build distance table and edge list
            DistanceTable = new double[tasks.Count + start.Count, tasks.Count];
            Egdes = new List<Tuple<int, int>>();
            int index = 0;
            while (index < tasks.Count)
            {
                for (int j = 0; j < tasks.Count; j++)
                {
                    if (j < index)
                        Egdes.Add(new Tuple<int, int>(index, j));
                    DistanceTable[index, j] = FuncDistance(tasks[index], tasks[j]);
                }
                index++;
            }
            for (int i = 0; i < start.Count; i++)
            {
                for (int j = 0; j < tasks.Count; j++)
                {
                    DistanceTable[index, j] = FuncDistance(start[i], tasks[j]);
                    Egdes.Add(new Tuple<int, int>(index, j));
                }
                index++;
            }
            EdgeSorter es = new EdgeSorter(DistanceTable);
            Egdes.Sort(es);

            Agents = new Agent[start.Count];
            for (int i = 0; i < start.Count; i++)
                Agents[i] = new Agent(tasks.Count + i, battery);
            seenStates = 0;
        }

        public override bool Equals(object obj)
        {
            TeamPath<Node> tp = obj as TeamPath<Node>;

            IEnumerator Aenum1 = Agents.GetEnumerator();
            IEnumerator Aenum2 = tp.Agents.GetEnumerator();
            while (Aenum1.MoveNext())
            {
                if (!Aenum2.MoveNext())
                {
                    IEnumerator Nenum1 = (Aenum1.Current as Agent).Path.GetEnumerator();
                    IEnumerator Nenum2 = (Aenum2.Current as Agent).Path.GetEnumerator();
                    while (Nenum1.MoveNext())
                    {
                        if (!Nenum2.MoveNext() || Nenum1.Current.Equals(Nenum2.Current))    //this will call the overrided function
                        {
                            return false;
                        }
                    }
                    if (Nenum2.MoveNext())
                    {
                        return false;
                    }
                }
            }
            if (Aenum2.MoveNext())
            {
                return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            int result = 37;
            foreach (Agent a in Agents)
            {
                result *= 1369;
                foreach (int n in a.Path)
                {
                    result *= 397;
                    result += n.GetHashCode();
                }
            }
            return result;
        }

        /// <summary>
        /// Tries to insert a task to the end of an agent's schedule and generate that Neighbour of a TeamPath
        /// </summary>
        /// <param name="task">the index of the task to be inserted</param>
        /// <param name="agentIndex">the index of the agent that's schedule will be modified</param>
        /// <param name="tp">the generated neighbour output parameter</param>
        /// <returns>if the generation is successful (if not, the dummy element is in the output parameter)</returns>
        public bool GenerateNeighbour(int task, int agentIndex, out TeamPath<Node> tp)
        {
            Agent ag;
            bool success = Agents[agentIndex].MoveTo(task, DistanceTable[Agents[agentIndex].Position, task], out ag);
            if (!success)
                tp = DummyElement;
            else
                tp = new TeamPath<Node>(ag, agentIndex, P - ag.TravelTime, this);
            return success;
        }

        /// <summary>
        /// Represents neighbours (children) for IHasNeighbours interface. As we go along the enumeration a new neighbour is generated (GenerateNeighbour)
        /// </summary>
        public IEnumerable<TeamPath<Node>> Neighbours
        {
            get {
                for (int task = 0; task < nTasks; task++)
                {
                    if (AvailableTask[task])
                        for (int agent = 0; agent < Agents.Length; agent++)
                        {
                            TeamPath<Node> tp;
                            if (GenerateNeighbour(task, agent, out tp))
                                yield return tp;
                        }
                }
            }
        }

        /// <summary>
        /// Tries to remove last task from an agent's schedule
        /// </summary>
        /// <param name="agentIndex">the index of the agent to remove the last task from</param>
        /// <param name="prev">output parameter with the resulting teampath (~parent)</param>
        /// <returns>if the output is valid (if the agent has any tasks to remove)</returns>
        public bool StepBack(int agentIndex, out TeamPath<Node> prev)
        {
            Agent ag;
            if (Agents[agentIndex].StepBack(DistanceTable, out ag))
            {
                prev = new TeamPath<Node>(ag, agentIndex, -(P - Agents[agentIndex].TravelTime), this, true);
                return true;
            }
            prev = null;
            return false;
        }
    }

    class EdgeSorter : IComparer<Tuple<int, int>>
    {
        double[,] ValueTable;
        public EdgeSorter(double[,] valueTable)
        {
            ValueTable = valueTable;
        }

        public int Compare(Tuple<int, int> x, Tuple<int, int> y)
        {
            double diff = ValueTable[x.Item1, x.Item2] - ValueTable[y.Item1, y.Item2];
            return Math.Sign(diff);
        }
    }
}
