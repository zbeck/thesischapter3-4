﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AStarSolver
{
    public class Program
    {
        static void Main(string[] args)
        {
        }

        static public TeamPath<Node> FindPath<Node>(
            List<Node> start, double battery, List<Node> tasks,
            Func<Node, Node, double> distance,
            Func<TeamPath<Node>, double> estimate)
        {
            // nodes that are investigated
            var closed = new HashSet<TeamPath<Node>>();
            // a queue of the other investigated nodes sorted by the estimate
            var queue = new PriorityQueue<TeamPath<Node>>();
            // queue root node
            queue.Enqueue(0, new TeamPath<Node>(start, battery, tasks, distance));
            // till no more nodes needs to be investigated
            while (!queue.IsEmpty)
            {
                // current investigated node
                var state = queue.Dequeue();
                // add to the already investigated nodes
                closed.Add(state);
                // if we managed to add any of its neighbours to the queue
                bool expanded = false;
                foreach (TeamPath<Node> tp in state.Neighbours) // iterate though all neighbours
                {
                    // a counter counting how many neighbour we check
                    TeamPath<Node>.seenStates++;
                    // if investigated already, drop it
                    if (closed.Contains(tp))
                        continue;
                    // otherwise queue neighbour
                    expanded = true;
                    queue.Enqueue(-(tp.TotalUtil + estimate(tp)), tp);
                }
                //if didn't manage to expand best node, we have reached optimal solution
                if (!expanded)
                    return state;
            }
            return null;
        }
    }
}
