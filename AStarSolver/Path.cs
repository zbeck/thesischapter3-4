﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AStarSolver
{
    public class Path<Node> : IEnumerable<Node>
    {
        public Node LastStep { get; private set; }
        public Path<Node> PreviousSteps { get; private set; }
        private Path(Node lastStep, Path<Node> previousSteps)
        {
            LastStep = lastStep;
            PreviousSteps = previousSteps;
        }
        public Path(Node start) : this(start, null) { }
        public Path<Node> AddStep(Node step)
        {
            return new Path<Node>(step, this);
        }
        public bool StepBack(out Path<Node> prev)
        {
            if (PreviousSteps == null)
            {
                prev = null;
                return false;
            }
            prev = new Path<Node>(PreviousSteps.LastStep, PreviousSteps.PreviousSteps);
            return true;
        }
        public IEnumerator<Node> GetEnumerator()
        {
            for (Path<Node> p = this; p != null; p = p.PreviousSteps)
                yield return p.LastStep;
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
