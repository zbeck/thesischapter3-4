﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AStarSolver
{
    interface IDistanceMetric<Node>
    {
        double Distance(Node a, Node b);
    }
}
