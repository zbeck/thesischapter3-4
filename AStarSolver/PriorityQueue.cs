﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AStarSolver
{
    class PriorityQueue<V>
    {
        private SortedList<double, V> list = new SortedList<double, V>();
        public void Enqueue(double priority, V value)
        {
            V val;
            if (list.TryGetValue(priority, out val)){
                if(!val.Equals(value))
                    throw new Exception("This is really unlikely to happen!");
                return;
            }
            list.Add(priority, value);
        }
        public V Dequeue()
        {
            // will throw if there isn’t any first element!
            var pair = list.First();
            list.RemoveAt(0);
            var v = pair.Value;
            return v;
        }
        public bool IsEmpty
        {
            get { return !list.Any(); }
        }
    }
}
