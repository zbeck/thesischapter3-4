﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AStarSolver
{
    public class Agent    {
        public Path<int> Path { get; private set; }
        public double TravelTime { get; private set; }
        public double BatteryTime { get; private set; }
        public int Position { get { return Path.LastStep; } }

        /// <summary>
        /// Default constructor, generates an agent with a its parameters
        /// </summary>
        /// <param name="path">agent schedule</param>
        /// <param name="travelTime">time travelled so far</param>
        /// <param name="batteryTime">battery time</param>
        private Agent(Path<int> path, double travelTime, double batteryTime)
        {
            Path = path;
            TravelTime = travelTime;
            BatteryTime = batteryTime;
        }

        /// <summary>
        /// Initial constructor, generates an agent with an empty schedule given the start conditions
        /// </summary>
        /// <param name="start">start location</param>
        /// <param name="batteryTime">battery time</param>
        public Agent(int start, double batteryTime)
        {
            Path = new Path<int>(start);
            TravelTime = 0;
            BatteryTime = batteryTime;
        }

        public static readonly Agent DummyElement = new Agent(null, 0, 0);

        public bool MoveTo(int task, double timeToGetThere, out Agent moved)
        {
            if (timeToGetThere + TravelTime > BatteryTime)
            {
                moved = DummyElement;
                return false;
            }
            moved = new Agent(Path.AddStep(task), TravelTime + timeToGetThere, BatteryTime);
            return true;
        }

        public bool StepBack(double[,] distanceTable, out Agent prev)
        {
            Path<int> p;
            if (Path.StepBack(out p))
            {
                prev = new Agent(p, TravelTime - distanceTable[Path.PreviousSteps.LastStep, Position], BatteryTime);
                return true;
            }
            prev = null;
            return false;
        }
    }
}
