﻿using AgentDraw;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeSearchSolver
{
    public class TreeStructure : SolverStructure
    {
        public static double U0 = 1;
        //public static double lambda = -1.0 / 432000.0;
        //public static double lambda = -1.0 / 8000.0;    //makes a larger difference of utilities when the simulation only goes for 6000 time
        public static double lambda = -1.0 / 50000.0;    //based on the length of the Ajka blank scenario
        public static bool DisableBottomExpand = false;

        public TreeStructure(IExtractableModel model, List<Sampler> rand)
        {
            InitRndSeed();
            Dictionary<PointF, int>[] tasks;
            Dictionary<int, PointF>[] agents;
            Dictionary<int, double> stayTime;
            double[] speed, battery, radius, taskTime;
            model.ExtractFixData(out tasks, out speed, out radius, out taskTime);
            model.ExtractCurrentData(out agents, out battery, out stayTime);
            double[][] agentStay = new double[agents.Length][];
            for (int i = 0; i < agents.Length; i++)
            {
                agentStay[i] = new double[agents[i].Count];
                int j = 0;
                foreach (var e in agents[i])
                {
                    agentStay[i][j] = stayTime[e.Key];
                    j++;
                }
            }
            topData = new GroupData(null, tasks[0], agents[0].Values.ToArray(), agentStay[0], speed[0], taskTime[0], battery[0], radius[0], SolverStructure.UtilityFactors[0], null, DefaultPercentile[0]);
            GroupData latestGroup = topData;
            for (int i = 1; i < agents.Length; i++)
            {
                latestGroup = new GroupData(rand[i-1], tasks[i], agents[i].Values.ToArray(), agentStay[i], speed[i], taskTime[i], battery[i], radius[i], SolverStructure.UtilityFactors[1], latestGroup, DefaultPercentile[1]);
            }
            double[,] dTable;
            PointF[] taskLoc;
            topData.GetProblem(out dTable, out taskLoc);
            rootNode = new GroupProblem(0, dTable, taskLoc, new double[taskLoc.Length], battery[0], gdata: topData);
        }

        internal static List<int> GetDiscovery(PointF t, PointF[] discoveryLocations, double discoveryRadius)
        {
            List<int> disc = new List<int>();
            for (int i = 0; i < discoveryLocations.Length; i++)
                if (Discovers(discoveryLocations[i], t, discoveryRadius))
                    disc.Add(i);
            return disc;
        }

        private static bool Discovers(PointF d, PointF t, double discoveryRadius)
        {
            if (Math.Abs(t.X - d.X) > discoveryRadius)
                return false;
            if (Math.Abs(t.Y - d.Y) > discoveryRadius)
                return false;
            return true;
        }

        public static double Distance(PointF a, PointF b)
        {
            return Math.Sqrt((a.X - b.X) * (a.X - b.X) + (a.Y - b.Y) * (a.Y - b.Y));
        }

        public double[][] TestBottomExpand(int nSamples)
        {
            nSamples++;
            double[][] data = new double[nSamples][];
            var solution = rootNode.AddGreedy();
            int cnt = 0;
            GroupProblem botproblem = TestBottomExpand_Grow(ref nSamples, solution, ref cnt);
            int i = 0;
            foreach (GroupSoltion child in botproblem.Children)
            {
                data[i] = child.GetStatisticsData();
                i++;
            }
            return data;
        }

        public double[][] TestBottomExpandStatistics(int nSamples, int problemSamples)
        {
            var topSolution = rootNode.AddGreedy();
            double[][] data = new double[problemSamples][];
            nSamples++;
            int failcount = 0;
            for (int i = 0; i < problemSamples; i++)
            {
                GroupProblem botproblem = TestBottomExpand_Grow(ref nSamples, topSolution, ref failcount, true, (int)(problemSamples * 0.2));
                data[i] = botproblem.GetStatisticsData();
                Console.Write(".");
            }
            return data;
        }

        private GroupProblem TestBottomExpand_Grow(ref int nSamples, GroupSoltion topSolution, ref int failcount, bool decrease_sample_on_failure = false, int failLimit = 20)
        {
            var bprob = topSolution.MakeChild();
            if (bprob == null)
                throw new Exception("cannot make bottom problem!");
            GroupProblem botproblem = bprob as GroupProblem;
            botproblem.AddGreedy();
            List<GroupSoltion> added = botproblem.MakeChildren(nSamples - 1) as List<GroupSoltion>;
            int expanded = added.Count + 1;
            while (expanded != nSamples)
            {
                added = botproblem.MakeChildren(nSamples - expanded) as List<GroupSoltion>;
                if (added == null)
                {
                    var sol = botproblem.MakeChild(nSamples);
                    if (sol == null)
                    {
                        if (decrease_sample_on_failure)
                        {
                            Console.Write('f');
                            failcount++;
                            if (failcount > failLimit)
                            {
                                nSamples /= 2;
                                failcount = 0;
                                Console.WriteLine("Samples decreased to " + nSamples.ToString());
                            }
                        }
                        else
                            Console.WriteLine("Cannot make more children than " + expanded.ToString());
                        return TestBottomExpand_Grow(ref nSamples, topSolution, ref failcount, decrease_sample_on_failure);
                    }
                    else
                        expanded++;
                }
                else
                    expanded += added.Count;
            }
            botproblem.Initialise();
            return botproblem;
        }

        public double[][][] TestBottomExpansionSuccess(double[] factors, int problemSamples, int StartSolutions, int tries)
        {
            double[][][] datab = new double[problemSamples][][];
            var sw = new System.Diagnostics.Stopwatch();
            var topSolution = rootNode.AddGreedy();
            for (int i = 0; i < problemSamples; i++)
            {
                GroupProblem botProblem = topSolution.MakeChild() as GroupProblem;
                datab[i] = new double[factors.Length][];
                for (int f = 0; f < factors.Length; f++)
                {
                    GroupSoltion.SolverPowerFactor = factors[f];
                    sw.Restart();
                    botProblem.ClearChildren();
                    botProblem.AddGreedy();
                    int triedtoadd = 0;
                    int trylimit = StartSolutions * 1000;
                    while (botProblem.ChildrenCount < StartSolutions)
                    {
                        if (triedtoadd > trylimit)
                            break;
                        triedtoadd += StartSolutions - botProblem.ChildrenCount;
                        List<GroupSoltion> addedCh = botProblem.MakeChildren(StartSolutions - botProblem.ChildrenCount) as List<GroupSoltion>;
                        botProblem.ResetClosed();
                    }
                    if (botProblem.ChildrenCount == StartSolutions)
                    {
                        int success = 0;
                        for (int j = 0; j < tries; j++)
                        {
                            var ch = botProblem.MakeChild(1);
                            if (ch != null)
                            {
                                success++;
                                botProblem.RemoveNonBestChildren(StartSolutions);
                            }
                            botProblem.ResetClosed();
                        }
                        datab[i][f] = new double[]{(double)success / (double)tries, (double)botProblem.NTasks, (double)botProblem.NAgents};
                    }
                    else
                        datab[i][f] = new double[] { Double.NaN, (double)botProblem.NTasks, (double)botProblem.NAgents };
                    sw.Stop();
                    Console.Write(String.Format("{1}:{0}s ", sw.ElapsedMilliseconds / 1000.0, factors[f]));
                }
                Console.WriteLine(String.Format("problem {0} done.", i));
            }
            return datab;
        }

        public double[][] TestTopExpansionSuccess(double[] factors, int StartSolutions, int tries)
        {
            double[][] datat = new double[factors.Length][];
            var sw = new System.Diagnostics.Stopwatch();
            var topSolution = rootNode.AddGreedy();
            for (int f = 0; f < factors.Length; f++)
            {
                GroupSoltion.SolverPowerFactor = factors[f];
                sw.Restart();
                int triedtoadd = 0;
                int trylimit = StartSolutions * 1000;
                while (rootNode.ChildrenCount < StartSolutions)
                {
                    if (triedtoadd > trylimit)
                        break;
                    triedtoadd += StartSolutions - rootNode.ChildrenCount;
                    List<GroupSoltion> addedCh = rootNode.MakeChildren(StartSolutions - rootNode.ChildrenCount) as List<GroupSoltion>;
                    rootNode.ResetClosed();
                }
                if (rootNode.ChildrenCount == StartSolutions)
                {
                    int success = 0;
                    for (int j = 0; j < tries; j++)
                    {
                        var ch = rootNode.MakeChild(1);
                        if (ch != null)
                        {
                            success++;
                            rootNode.RemoveNonBestChildren(StartSolutions);
                        }
                        rootNode.ResetClosed();
                    }
                    datat[f] = new double[] { (double)success / (double)tries, (double)rootNode.NTasks, (double)rootNode.NAgents };
                }
                else
                    datat[f] = new double[] { Double.NaN, Double.NaN, Double.NaN };
                sw.Stop();
                Console.WriteLine(String.Format("{1}: {0}s", sw.ElapsedMilliseconds / 1000.0, factors[f]));
            }
            return datat;
        }

        public double[][][] TestBottomExpandMultipleFactors(double[] factors, int problemSamples, int solutions)
        {
            double[][][] datab = new double[problemSamples][][];
            var sw = new System.Diagnostics.Stopwatch();
            var topSolution = rootNode.AddGreedy();
            for (int i = 0; i < problemSamples; i++)
            {
                GroupProblem botProblem = topSolution.MakeChild() as GroupProblem;
                sw.Restart();
                datab[i] = TestBottomExpandMultipleFactors(botProblem, solutions, factors);
                sw.Stop();
                Console.WriteLine(String.Format("{1} factors took {0}s", sw.ElapsedMilliseconds / 1000.0, factors.Length));
            }
            return datab;
        }

        private double[][] TestBottomExpandMultipleFactors(GroupProblem botProblem, int nSamples, double[] factors)
        {
            double[][] statdata = new double[factors.Length][];
            for (int i = 0; i < factors.Length; i++)
            {
                GroupSoltion.SolverPowerFactor = factors[i];
                botProblem.ClearChildren();
                botProblem.AddGreedy();
                int triedtoadd = 0;
                int trylimit = nSamples * 10;
                while (botProblem.ChildrenCount < nSamples)
                {
                    if (triedtoadd > trylimit)
                        break;
                    triedtoadd += nSamples - botProblem.ChildrenCount;
                    List<GroupSoltion> addedCh = botProblem.MakeChildren(nSamples - botProblem.ChildrenCount) as List<GroupSoltion>;
                    botProblem.ResetClosed();
                }
                botProblem.Initialise();
                Console.Write(botProblem.ChildrenCount.ToString() + " ");
                statdata[i] = botProblem.GetStatisticsData();
            }
            botProblem.ClearChildren();
            return statdata;
        }

        public double[][][] TestTopExpandMultipleFactors(ref double[] factors, int problemSamples, int solutions)
        {
            double[][][] datat = new double[factors.Length][][];
            var sw = new System.Diagnostics.Stopwatch();
            int faci = 0;
            int failLimit = 100;
            foreach (double factor in factors)
            {
                Console.Write(factor);
                GroupSoltion.SolverPowerFactor = factor;
                int failcnt = 0;
                rootNode.ClearChildren();
                sw.Restart();
                GroupSoltion topSolution = rootNode.AddGreedy();
                datat[faci] = new double[solutions + 1][];
                datat[faci][0] = TestTopExpandPopulateProblems(topSolution, problemSamples);
                for (int i = 1; i < solutions + 1; i++)
                {
                    topSolution = rootNode.MakeChild(100000) as GroupSoltion;
                    if (topSolution == null)
                    {
                        failcnt++;
                        if (failcnt > failLimit)
                            break;
                        Console.Write('f');
                        i--;
                        rootNode.ResetClosed();
                    }
                    else
                        datat[faci][i] = TestTopExpandPopulateProblems(topSolution, problemSamples);
                }
                sw.Stop();
                Console.WriteLine(String.Format(" Top took {0}s", sw.ElapsedMilliseconds / 1000.0));
                if (failcnt > failLimit)
                {
                    double[][][] datatt = new double[faci][][];
                    Array.Copy(datat, datatt, faci);
                    datat = datatt;
                    double[] factors2 = new double[faci];
                    Array.Copy(factors, factors2, faci);
                    factors = factors2;
                    Console.WriteLine(String.Format("Giving up at factor {0}", factor));
                    break;
                }
                faci++;
            }
            rootNode.ClearChildren();
            return datat;
        }

        private double[] TestTopExpandPopulateProblems(GroupSoltion topSolution, int problemSamples)
        {
            List<GroupProblem> problems = topSolution.MakeChildren(problemSamples) as List<GroupProblem>;
            foreach (var problem in problems)
                problem.AddGreedy();
            topSolution.Initialise();
            double[] stat = topSolution.GetStatisticsData();
            topSolution.ClearChildren();
            return stat;
        }

        public double[][] TestMidExpand(int nSamples)
        {
            double[][] data = new double[nSamples][];
            ITreeNode ch0 = rootNode.MakeChild();
            GroupData bottomLevel = topData.NextGroup;
            for (int i = 0; i < nSamples; i++)
            {
                GroupProblem ch1 = ch0.MakeChild() as GroupProblem;
                GroupSoltion ch2 = ch1.AddGreedy();
                data[i] = ch2.GetStatisticsData();
            }
            return data;
        }

        public double[][] TestTopExpand(int nSolutions, int nSamples)
        {
            for (int i = 0; i < nSolutions; i++)
            {
                GroupSoltion sol;
                if (i > 0)
                    sol = rootNode.MakeChild() as GroupSoltion;
                else    //first soution is greedy
                    sol = rootNode.AddGreedy() as GroupSoltion;
                if (sol == null)
                {
                    nSolutions = i;
                    break;
                }
                var botproblems = sol.MakeChildren(nSamples);
                foreach (GroupProblem prob in botproblems)
                    prob.AddGreedy();
            }
            rootNode.Initialise();
            double[][] data = new double[nSolutions][];
            int ind = 0;
            foreach (GroupSoltion child in rootNode.Children)
            {
                data[ind] = child.GetStatisticsData();
                ind++;
            }
            return data;
        }

        public static double Utility(double t)
        {
            return Math.Max(0.0, U0 + t * lambda);
        }
    }
}
