﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace TreeSearchSolver
{
    public struct Decision
    {
        public double[] x;
        public double[] y;
        public PointF?[] assigned;
        int[] critical;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="critical"></param>
        public Decision(double[] x, double[] y, PointF?[] assigned, int[] critical)
        {
            this.x = x;
            this.y = y;
            this.assigned = assigned;
            this.critical = critical;
        }

        /// <summary>
        /// Constructor that aggregates solutions. Leaves critical blank.
        /// </summary>
        /// <param name="dlist"></param>
        public Decision(IEnumerable<Decision> dlist)
        {
            int len = dlist.First().x.Length;
            double[] X = new double[len];
            double[] Y = new double[len];
            assigned = new PointF?[len];
            Dictionary<PointF, int>[] assignment = new Dictionary<PointF, int>[len];
            for (int i = 0; i < len; i++)
                assignment[i] = new Dictionary<PointF,int>();
            foreach (Decision d in dlist)
            {
                for (int i = 0; i < len; i++)
                {
                    X[i] += d.x[i] * d.critical[i];
                    Y[i] += d.y[i] * d.critical[i];
                    if (d.assigned[i] != null)
                    {
                        int value;
                        assignment[i].TryGetValue((PointF)d.assigned[i], out value);
                        assignment[i][(PointF)d.assigned[i]] = value + d.critical[i];
                    }
                }
            }
            for (int i = 0; i < len; i++)
            {
                double vecLen = Math.Sqrt(X[i] * X[i] + Y[i] * Y[i]);
                if (vecLen != 0)
                {
                    X[i] /= vecLen;
                    Y[i] /= vecLen;
                }
                int overallAssignment = assignment[i].Sum(e => e.Value);
                if (overallAssignment == 0)
                    continue;
                var maxAssignment = assignment[i].Aggregate((l, r) => l.Value > r.Value ? l : r);
                if (maxAssignment.Value >= overallAssignment / 2)
                    assigned[i] = maxAssignment.Key;
            }
            this.x = X;
            this.y = Y;
            critical = new int[len];
        }

        public static double DirectionErrorLossFactor(IEnumerable<Decision> dlist)
        {
            if (dlist.Count() == 0)
                return 0.0;
            int len = dlist.First().x.Length;
            double err = 0.0;
            double[] X = new double[len];
            double[] Y = new double[len];
            int[] maxLen = new int[len];
            int decCount = 0;
            foreach (Decision d in dlist)
            {
                for (int i = 0; i < len; i++)
                {
                    X[i] += d.x[i] * d.critical[i];
                    Y[i] += d.y[i] * d.critical[i];
                    maxLen[i] += d.critical[i];
                }
                decCount++;
            }
            for (int i = 0; i < len; i++)
            {
                err += (((double)maxLen[i]) - Math.Sqrt(X[i] * X[i] + Y[i] * Y[i])) / ((double)decCount);
            }
            return err;
        }
    }
}
