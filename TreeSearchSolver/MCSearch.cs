﻿using AgentDraw;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;


namespace TreeSearchSolver
{
    public class MCSearch : SolverStructure
    {
        GroupData bottomData { get { return topData.NextGroup; } }
        ITreeNode searchRoot;
        public static double TimeStep = 1.0;

        /// <summary>
        /// Current constructor only compatible with 2 level problems!
        /// Initalises a new tree
        /// </summary>
        /// <param name="model"></param>
        /// <param name="rand"></param>
        /// <param name="initChildren"></param>
        public MCSearch(IExtractableModel model, Sampler rand, IEnumerable<int> initChildren = null, MCSearch lastProblem = null, int fixedExplorationSteps = 0)
        {
            InitRndSeed();
            AbstractTreeNode.ResetNodeCount();
            double[] battery;
            GroupData botData = ExtractDualProblem(model, rand, out battery);
            if (initChildren != null)
            {
                topData.InitChildrenOfProblem = initChildren.ElementAt(0);
                topData.InitChildrenOfSolution = initChildren.ElementAt(1);
                botData.InitChildrenOfProblem = initChildren.ElementAt(2);
            }
            double[,] dTable;
            PointF[] taskLoc;
            topData.GetProblem(out dTable, out taskLoc);
            GroupProblem oldRoot = null;
            if (lastProblem != null)
                oldRoot = lastProblem.rootNode;
            //create initial tree
            rootNode = new GroupProblem(0, dTable, taskLoc, new double[taskLoc.Length], battery[0], gdata: topData, oldRoot: oldRoot, fixedExplorationSteps: fixedExplorationSteps);
            if (fixedExplorationSteps < 0)
                rootNode.InitPopulateMCTSSingle(oldRoot);
            else
                rootNode.InitPopulateMCTS(oldRoot);
            //set up statistical data for children structure
            rootNode.Initialise();

            if (fixedExplorationSteps < 0)  //if fixed search, start expansion at the top solution
                searchRoot = rootNode.BestChild;
            else
                searchRoot = rootNode;
        }

        /// <summary>
        /// Constructor to carry on from an old tree with new agent positions
        /// </summary>
        /// <param name="old"></param>
        /// <param name="model"></param>
        public MCSearch(MCSearch old, IExtractableModel model, int fixedExplorationSteps = 0)
        {
            Dictionary<int, PointF>[] agents;
            Dictionary<int, double> stayTime;
            double[] battery;
            model.ExtractCurrentData(out agents, out battery, out stayTime);
            double[][] agentStay = new double[agents.Length][];
            for (int i = 0; i < agents.Length; i++)
            {
                agentStay[i] = new double[agents[i].Count];
                int j = 0;
                foreach (var e in agents[i])
                {
                    agentStay[i][j] = stayTime[e.Key];
                    j++;
                }
            }
            topData = new GroupData(old.topData, agents[0].Values.ToArray(), agentStay[0], battery[0], null);
            GroupData botData = new GroupData(old.bottomData, agents[1].Values.ToArray(), agentStay[1], battery[1], topData);
            rootNode = old.rootNode;
            rootNode.InvalidateSubtree();

            if (fixedExplorationSteps<0)
            {
                if (rootNode.ChildrenCount > 1)
                    rootNode.RemoveNonBestChildren();
                searchRoot = rootNode.BestChild;
            }
            else
                searchRoot = rootNode;
        }

        public void Solve(int sampleSize, ref int limitStatus, out List<List<int>> assignedIds, out List<List<double>> directions, ref int[] levelsExpanded)
        {
            int nodeCount = AbstractTreeNode.NodeCount;
            //int firstLimit = nodeCount + (int)(0.9 * (nodeLimit - nodeCount));
            if (nodeCount == 0)
                throw new Exception("Initialized tree is empty, cannot run MC search!");
            int lastUpdate = -1;
            while (limitStatus < 2)
            {
                if (nodeCount - lastUpdate > lastUpdate * 0.1)
                {
                    topData.RefreshExpectedChildren(rootNode);
                    lastUpdate = nodeCount;
                    //Console.WriteLine("\n"+lastUpdate.ToString());
                }
                ITreeNode bestNode;
                double bestInc;
                //until first limit, we expand the whole tree (from root)
                if (limitStatus == 0)
                    bestInc = searchRoot.Increase(sampleSize, out bestNode, true);
                //after that, we will fix the top level solution and expand it
                else
                    bestInc = rootNode.BestChild.Increase(sampleSize, out bestNode, true);
                //if didn't manage to find anything to increase utility, stop
                if (bestInc == 0.0)
                {
                    //Console.Write('b');
                    break;
                }
                int level = 0;
                ITreeNode node = bestNode;
                while (!node.IsBottomLevel)
                {
                    level++;
                    node = node.Children.First();
                }
                int added = bestNode.Expand(sampleSize);
                nodeCount = AbstractTreeNode.NodeCount;
                levelsExpanded[level]++;
                //Console.Write(level);
            }
            //Console.WriteLine();
            Stopwatch sw = new Stopwatch();
            Stopwatch sw2 = new Stopwatch();
            sw.Start();
            rootNode.AggregateDecision();
            List<int> topTaskID, botTaskID;
            List<double> topDir, botDir;
            sw.Stop();
            sw2.Start();
            var topDecision = topData.GetAggregatedDecision();
            topData.GetSolutionDataStopWhenIdle(topDecision, out topTaskID, out topDir);
            sw2.Stop();
            sw.Start();
            var bottomDecision = bottomData.GetAggregatedDecision();
            bottomData.GetSolutionDataStopWhenIdle(bottomDecision, out botTaskID, out botDir);
            sw.Stop();
            assignedIds = new List<List<int>>(2);
            directions = new List<List<double>>(2);
            assignedIds.Add(topTaskID);
            assignedIds.Add(botTaskID);
            directions.Add(topDir);
            directions.Add(botDir);
        }

        public double ExpandOnce(int sampleSize, ref int? lastUpdate, out int expandedLevel)
        {
            if (lastUpdate == null)
                lastUpdate = -1;
            if (AbstractTreeNode.NodeCount - lastUpdate > lastUpdate * 0.1)
            {
                topData.RefreshExpectedChildren(rootNode);
                lastUpdate = AbstractTreeNode.NodeCount;
            }
            ITreeNode bestNode;
            double bestInc;
            bestInc = searchRoot.Increase(sampleSize, out bestNode, true);
            expandedLevel = 0;
            //if didn't manage to find anything to increase utility, stop
            if (bestInc > 0.0)
            {
                ITreeNode node = bestNode;
                while (!node.IsBottomLevel)
                {
                    expandedLevel++;
                    node = node.Children.First();
                }
                bestNode.Expand(sampleSize);
            }
            return bestInc;
        }

        public List<double[][]> Test4(IEnumerable<int> cmd)
        {
            List<double[][]> stats = new List<double[][]>(3);
            int nSample = cmd.ElementAt(0);
            int limit = cmd.ElementAt(1) * 1000;
            Stopwatch limiter = new Stopwatch();
            int lastUpdate = -1;
            int i = 0;
            for (int level = 0; level < 3; level++)
            {
                List<double[]> stat = new List<double[]>();
                ITreeNode nodeToExpand = rootNode;
                int currentLevel = 0;
                while (level > currentLevel)
                {
                    nodeToExpand = nodeToExpand.BestChild;
                    currentLevel++;
                }
                limiter.Restart();
                while (limiter.ElapsedMilliseconds < limit)
                {
                    if ((i - lastUpdate) > (i * 0.1))
                    {
                        topData.RefreshExpectedChildren(rootNode);
                        //rootNode.PrintPretty("", true, 1);
                        lastUpdate = i;
                    }
                    double inc = nodeToExpand.OwnIncrease(nSample);
                    stat.Add(new double[] {inc, nodeToExpand.ChildrenCount, nodeToExpand.Utility, nodeToExpand.Value});
                    int added = nodeToExpand.Expand(nSample);
                    i++;
                }
                stat.Add(new double[] { nodeToExpand.OwnIncrease(nSample), nodeToExpand.ChildrenCount, nodeToExpand.Utility, nodeToExpand.Value });
                stats.Add(stat.ToArray());
                Console.WriteLine("Level " + level.ToString() + " done.");
            }
            return stats;
        }


        /// <summary>
        /// Tests MCTS behaviour with time limited expansion
        /// </summary>
        /// <param name="cmd">list of parameters (nSample, run_time)
        ///     nSample: how many nodes to expand with
        ///     run_time: time limit for growth in seconds</param>
        public void Test3(IEnumerable<int> cmd, string logfilename)
        {
            int nSample = cmd.ElementAt(0);
            int limit = cmd.ElementAt(1)*1000;
            Stopwatch limiter = new Stopwatch();
            Dictionary<ITreeNode, int> expandedNodes = new Dictionary<ITreeNode, int>();
            int newID = 0;
            int lastUpdate = -1;
            int i = 0;
            var logfields = new string[] {"i", "util", "inc", "chosen_times", "lvl", "nchildren", "nadded"};
            var log = new List<Tuple<int, double, double, int, int, int, int>>();
            limiter.Start();
            while(limiter.ElapsedMilliseconds < limit)
            {
                if ((i - lastUpdate) > (i * 0.1))
                {
                    topData.RefreshExpectedChildren(rootNode);
                    //rootNode.PrintPretty("", true, 1);
                    lastUpdate = i;
                }
                ITreeNode bestNode;
                double bestInc = rootNode.Increase(nSample, out bestNode);
                int level = 0;
                ITreeNode node = bestNode;
                while (!node.IsBottomLevel)
                {
                    level++;
                    node = node.Children.First();
                }
                if (!expandedNodes.ContainsKey(bestNode))
                    expandedNodes.Add(bestNode, newID++);
                int added = bestNode.Expand(nSample);
                log.Add(new Tuple<int, double, double, int, int, int, int>(i, rootNode.Utility, bestInc, expandedNodes[bestNode], level, bestNode.ChildrenCount, added));
                i++;
            }
            Console.WriteLine(rootNode.Utility);
            Console.WriteLine(rootNode.GreedyUtility());
            rootNode.AggregateDecision();
            List<int> topTaskID, botTaskID;
            List<double> topDir, botDir;
            var topDecision = topData.GetAggregatedDecision();
            topData.GetSolutionDataStopWhenIdle(topDecision, out topTaskID, out topDir);
            var bottomDecision = bottomData.GetAggregatedDecision();
            bottomData.GetSolutionDataStopWhenIdle(bottomDecision, out botTaskID, out botDir);
            Console.WriteLine(MyExtensions.MyExtensionMethods.ToDelimitedString(topTaskID, " ") + ", " + MyExtensions.MyExtensionMethods.ToDelimitedString(botTaskID, " "));
            Console.WriteLine(MyExtensions.MyExtensionMethods.ToDelimitedString(topDir, " ") + ", " + MyExtensions.MyExtensionMethods.ToDelimitedString(botDir, " "));
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(logfilename))
            {
                writer.WriteLine(MyExtensions.MyExtensionMethods.ToDelimitedString(logfields, ";"));
                foreach (var logline in log)
                {
                    writer.WriteLine(string.Format("{0};{1};{2};{3};{4};{5};{6}", logline.Item1, logline.Item2, logline.Item3, logline.Item4, logline.Item5, logline.Item6, logline.Item7));
                }
            }
        }

        /// <summary>
        /// Tests MCTS behaviour
        /// </summary>
        /// <param name="cmd">list of parameters (nSample, steps, [omit_levels])
        ///     nSample: how many nodes to expand with
        ///     steps: how many expansion steps to make
        ///     omit_levels: how many levels to skip for printing the tree, not printed if missing</param>
        public void Test2(IEnumerable<int> cmd)
        {
            int nSample = cmd.ElementAt(0);
            int steps = cmd.ElementAt(1);
            Dictionary<ITreeNode, int> expandedNodes = new Dictionary<ITreeNode, int>();
            int newID = 0;
            int lastUpdate = -1;
            for (int i = 0; i < steps; i++)
            {
                if ((i - lastUpdate) > (i * 0.1))
                {
                    topData.RefreshExpectedChildren(rootNode);
                    //rootNode.PrintPretty("", true, 1);
                    lastUpdate = i;
                }
                ITreeNode bestNode;
                double bestInc = rootNode.Increase(nSample, out bestNode);
                int level = 0;
                ITreeNode node = bestNode;
                while (!node.IsBottomLevel)
                {
                    level++;
                    node = node.Children.First();
                }
                if (!expandedNodes.ContainsKey(bestNode))
                    expandedNodes.Add(bestNode, newID++);
                int added = bestNode.Expand(nSample);
                Console.WriteLine(String.Format("{0,6:D}: U {1,8:E}   I {2,8:E}   N{3,4:D4}   L{4}   C{5,6:D}   A{6,6:D}",
                    i, rootNode.Utility, bestInc, expandedNodes[bestNode], level, bestNode.ChildrenCount, added));
            }
            if (cmd.Skip(2).Any())
                rootNode.PrintPretty("", true, cmd.ElementAt(2));
            Console.WriteLine(rootNode.Utility);
            Console.WriteLine(rootNode.GreedyUtility());
            rootNode.AggregateDecision();
            List<int> topTaskID, botTaskID;
            List<double> topDir, botDir;
            var topDecision = topData.GetAggregatedDecision();
            topData.GetSolutionDataStopWhenIdle(topDecision, out topTaskID, out topDir);
            var bottomDecision = bottomData.GetAggregatedDecision();
            bottomData.GetSolutionDataStopWhenIdle(bottomDecision, out botTaskID, out botDir);
        }

        /// <summary>
        /// Test basic behaviour
        /// </summary>
        /// <param name="cmd">list of ints [omit_levels, nchildren1, nchildren2, cnhildren3, cmd1, cmd2, ...]
        ///     omit_levels: the tree print will not expand these levels
        ///     nchildreni: the number of initial children at a specific level
        ///     cmdi: command
        ///         positive int n: select nth child
        ///         -1: expand current node with 10 and select root
        ///         -2: add greedy solution to current node, then select root
        ///         -3: make expansions from this level, determined by the next cmdi arguments, then select root</param>
        /// <param name="flatRandom">non greedy expansion is flat random or guided random</param>
        public void Test(List<int> cmd, bool flatRandom)
        {
            if (flatRandom)
                GroupData.DefaultSolutionPolicy = GroupSoltion.NewSolutionPolicyType.FlatRandom;
            else
                GroupData.DefaultSolutionPolicy = GroupSoltion.NewSolutionPolicyType.GuidedRandom;
            ITreeNode node = rootNode;
            int omitLevels = cmd[0];
            var nChildren = cmd.Skip(1).Take(3);
            MakeChildren(rootNode, nChildren);
            topData.RefreshExpectedChildren(rootNode);
            rootNode.PrintPretty("", true, omitLevels);
            var cmd2 = cmd.Skip(4);
            if (!cmd2.Any())
                return;
            int skip = 0;
            List<int> nCh = new List<int>(3);
            foreach (int c in cmd2)
            {
                if (skip > 0)   //this is for command -3, stores the next skip=levelsBelow integers in the row, to call MakeChildren
                {
                    nCh.Add(c);
                    skip--;
                    if (skip == 0)
                    {
                        MakeChildren(node, nCh);
                        nCh.Clear();
                        node = rootNode;
                    }
                    continue;
                }
                else if (c == -3)
                {
                    int levelsBelow = 0;
                    ITreeNode child = node;
                    while (!child.IsBottomLevel)
                    {
                        child = child.Children.First();
                        levelsBelow++;
                    }
                    //we will skip those numbers
                    skip = levelsBelow;
                }
                else if (c == -2)
                {
                    ((GroupProblem)node).AddGreedy();
                    node = rootNode;
                }
                else if (c == -1)
                {
                    node.MakeChild(10);
                    node = rootNode;
                }
                else
                    node = node.Children.ElementAt(c);
            }
            rootNode.PrintPretty("", true, omitLevels);
        }

        void MakeChildren(ITreeNode node, IEnumerable<int> nChildren)
        {
            if (node.IsBottomLevel)
                return;
            List<ITreeNode> newChildren = new List<ITreeNode>(nChildren.First()+1);
            if ((node is GroupProblem) && (!node.Children.Any()))
                newChildren.Add((node as GroupProblem).AddGreedy());
            for (int i = 0; i < nChildren.First(); i++)
            {
                var child = node.MakeChild();
                if (child != null)
                    newChildren.Add(child);
            }
            foreach (ITreeNode child in newChildren)
                MakeChildren(child, nChildren.Skip(1));
        }

        public string PopulationSummary()
        {
            return String.Format("overall\t{0}\nfirst level\t{1}", AbstractTreeNode.NodeCount, rootNode.ChildrenCount);
        }

        protected override double[] UtilityData()
        {
            return new double[] { rootNode.Utility, rootNode.GreedyUtility(), (rootNode.BestChild as GroupSoltion).StdErr, rootNode.GreedyStdErr() };
        }
    }
}
