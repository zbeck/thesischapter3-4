﻿using AgentDraw;
using MyExtensions;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeSearchSolver
{
    public class GroupData
    {
        public static GroupSoltion.NewSolutionPolicyType DefaultSolutionPolicy = GroupSoltion.NewSolutionPolicyType.GuidedRandom;
        public Sampler sampler{get; private set;}
        private List<InstanceData> samples;
        private HashSet<InstanceData> samplesSet;
        private readonly IDictionary<PointF, int> tasks;
        public PointF[] Agents { get; private set; }
        double[] agentStay;
        private double discoveryRadius;
        public double MovementSpeed { get; private set; }
        public double TaskTime { get; private set; }
        public double BatteryCapacity { get; private set; }
        public GroupData NextGroup { get; protected set; }
        public GroupProxy Proxy { get; private set; }
        public double ExpectedChildrenOfProblem { get; private set; }
        public double ExpectedChildrenOfSolution { get; private set; }
        public double UtilityFactor { get; private set; }
        public int level { get { return (NextGroup == null) ? 1 : 0; } }
        public double Percentile;
        private int initChildrenOfProblem = 0;
        public int InitChildrenOfProblem { get { return initChildrenOfProblem; } set { if (value >= 0) initChildrenOfProblem = value; } }
        private int initChildrenOfSolution = 0;
        public int InitChildrenOfSolution
        {
            get { return initChildrenOfSolution; }
            set
            {
                if (NextGroup == null) initChildrenOfSolution = 0;
                else if (value >= 0) initChildrenOfSolution = value;
            }
        }
        private List<Decision> toAggregate;

        public GroupData(Sampler smplr, IDictionary<PointF, int> fixTasks, PointF[] agentpos, double[] agentStay, double speed, double taskTime, double battery, double discRadius, double utilityFactor, GroupData PreviousGroup, double percentile)
        {
            sampler = smplr;
            tasks = fixTasks;
            Agents = agentpos;
            this.agentStay = agentStay;
            MovementSpeed = speed;
            TaskTime = taskTime;
            BatteryCapacity = battery;
            discoveryRadius = discRadius;
            UtilityFactor = utilityFactor;
            Percentile = percentile;
            samples = new List<InstanceData>();
            samplesSet = new HashSet<InstanceData>();
            toAggregate = new List<Decision>();
            NextGroup = null;
            if (PreviousGroup != null)
                PreviousGroup.NextGroup = this;
            Proxy = new GroupProxy(this);
        }

        /// <summary>
        /// Constructod from old groupdata instance
        /// </summary>
        /// <param name="old">old instance</param>
        /// <param name="agentpos">updated agent locations</param>
        /// <param name="battery">updated battery level</param>
        /// <param name="PreviousGroup">previous group in chain</param>
        public GroupData(GroupData old, PointF[] agentpos, double[] agentStay, double battery, GroupData PreviousGroup)
        {
            sampler = old.sampler;
            tasks = old.tasks;
            Agents = agentpos;
            this.agentStay = agentStay;
            MovementSpeed = old.MovementSpeed;
            TaskTime = old.TaskTime;
            BatteryCapacity = battery;
            discoveryRadius = old.discoveryRadius;
            UtilityFactor = old.UtilityFactor;
            Percentile = old.Percentile;
            samples = old.samples;
            samplesSet = old.samplesSet;
            ExpectedChildrenOfProblem = old.ExpectedChildrenOfProblem;
            ExpectedChildrenOfSolution = old.ExpectedChildrenOfSolution;
            initChildrenOfProblem = old.InitChildrenOfProblem;
            initChildrenOfSolution = old.InitChildrenOfSolution;
            foreach (var sam in samples)
                sam.Invalidate();
            //the hash should remain the same as it depends on the task positions
            toAggregate = new List<Decision>();
            NextGroup = null;
            if (PreviousGroup != null)
                PreviousGroup.NextGroup = this;
            Proxy = old.Proxy;
            Proxy.ChangeGroup(old, this);
        }

        /// <summary>
        /// Get problem sample (won't work with fixed levels)
        /// </summary>
        /// <param name="id">sample id, has to be requested gradually increasing, if one requested without the previous, exception is thrown</param>
        /// <param name="discoveryLocations">locations of the discovery tasks to determine constraints</param>
        /// <param name="distances">distance table output</param>
        /// <param name="constraints">constraints output</param>
        /// <param name="locations">task locations output</param>
        public bool GetProblem(int id, PointF[] discoveryLocations, out double[,] distances, out List<int>[] constraints, out PointF[] locations)
        {
            // if the level is fixed
            if (sampler == null)
                throw new Exception("Error, trying to sample form a fixed problem!");
            if (id < samples.Count)
            {
                constraints = samples[id].Constraints;
                locations = samples[id].Locations;
                if (samples[id].IsInvalid)
                {
                    distances = MakeTable(locations);
                    samples[id].Validate(distances);
                }
                else
                    distances = samples[id].DistanceTable;
                return true;
            }
            else if (id == samples.Count)
            {
                //sample task locations
                List<PointF> locs = sampler.SampleLocationsF();
                //remove duplicate task locations by diplacing tasks
                var duplicates = locs.GroupBy(s => s).SelectMany(grp => grp.Skip(1));
                while (duplicates.Any())
                {
                    foreach (PointF p in duplicates)
                    {
                        locs.Remove(p);
                        locs.Add(p + new SizeF(0.1F, 0.1F));
                    }
                    duplicates = locs.GroupBy(s => s).SelectMany(grp => grp.Skip(1));
                }

                constraints = ProcessSampledLocations(locs, tasks.Keys, discoveryLocations);
                locations = locs.ToArray();

                distances = MakeTable(locations);
                InstanceData inst = new InstanceData(distances, constraints, locations);
                if (samplesSet.Add(inst))
                {
                    samples.Add(inst);
                    return true;
                }
                else
                    return false;
            }
            else
                throw new Exception("Problem instance inconsistency!");
        }
        void MovePoint(PointF p)
        {
            p.X += 0.1F;
            p.Y += 0.1F;
        }

        /// <summary>
        /// Get fixed (or known part of the) problem
        /// </summary>
        /// <param name="distances">distance table output</param>
        /// <param name="locations">task locations output</param>
        public void GetProblem(out double[,] distances, out PointF[] locations)
        {
            // if the level is uncertain
            if (sampler != null)
            {
                locations = this.tasks.Keys.ToArray<PointF>();
                distances = this.MakeTable(locations);
            }
            else if (samples.Count == 0)
            {
                locations = tasks.Keys.ToArray();
                distances = MakeTable(locations);
                samples.Add(new InstanceData(distances, null, locations));
            }
            else
            {
                locations = samples[0].Locations;
                distances = samples[0].DistanceTable;
            }
        }

        public double ExtractUtility(GroupSoltion sol)
        {
            double util = 0;
            double[] execTimes = new double[sol.parent.NTasks];
            Array.Copy(sol.executionTimes, execTimes, sol.parent.NTasks);
            foreach (double t in execTimes)
            {
                util += TreeStructure.Utility(t);
            }
            //normalisation added at 17.01.2015
            if (execTimes.Length > 0)
                util = util / (double)execTimes.Length;
            return util * UtilityFactor;
        }

        /// <summary>
        /// Merges sampled locations and fix tasks and generates the constraints, also removes tasks that cannot be discovered
        /// </summary>
        /// <param name="locs">sampled task locations, adds fix locations to it</param>
        /// <param name="fixTasks">fix (existing) task locations</param>
        /// <param name="discoveryLocations">the locations of the discovery tasks, the element indices will be added to the constraints</param>
        /// <returns>constraints indexing: [explored_task][exploration_task]</returns>
        private List<int>[] ProcessSampledLocations(List<PointF> locs, ICollection<PointF> fixTasks, PointF[] discoveryLocations)
        {
            List<List<int>> constraints = new List<List<int>>(locs.Count + fixTasks.Count);
            List<PointF> tasksToDelete = new List<PointF>();
            foreach (PointF t in locs)
            {
                List<int> discoveryList = TreeStructure.GetDiscovery(t, discoveryLocations, discoveryRadius);
                if (discoveryList.Count == 0)   //if noone discovers sampled task, remove it from tasks
                    tasksToDelete.Add(t);
                else
                    constraints.Add(discoveryList);
            }
            locs.AddRange(fixTasks);
            foreach (PointF t in fixTasks)       // fixed tasks already discovered
            {
                constraints.Add(new List<int>(0));
            }
            if (tasksToDelete.Count > 0)
                locs.RemoveAll(t => tasksToDelete.Contains(t));
            return constraints.ToArray();
        }

        private double[,] MakeTable(PointF[] list)
        {
            //!TODO add time of execution!!!!
            int nTask = list.Length;
            int nAgent = Agents.Length;
            double[,] table = new double[nTask + nAgent, nTask];
            for (int i = 0; i < nTask; i++)
                for (int j = 0; j < nTask; j++)
                    table[i, j] = TreeStructure.Distance(list[i], list[j]) / MovementSpeed + TaskTime;
            for (int i = 0; i < nAgent; i++)
                for (int j = 0; j < nTask; j++)
                    table[i + nTask, j] = TreeStructure.Distance(Agents[i], list[j]) / MovementSpeed + TaskTime + agentStay[i];
            return table;
        }

        internal void GetSolutionData(GroupSoltion sol, IdleAction idleAction, out List<int> taskIDs, out List<double> directions, Sampler beliefSampler, Sampler gradientSampler)
        {
            taskIDs = new List<int>(Agents.Length);
            directions = new List<double>(Agents.Length);
            var firstTaskPositions = sol.FirstAllocated;
            Point meanLocation = new Point();
            if (idleAction != IdleAction.Stop)
            {
                meanLocation = beliefSampler.GetMeanLocation();
            }
            for (int i = 0; i < this.Agents.Length; i++)
            {
                PointF agentPos = Agents[i];
                Point agentPoint = new Point((int)agentPos.X, (int)agentPos.Y);
                if (firstTaskPositions[i] == null)
                {
                    taskIDs.Add(-1);
                    switch (idleAction)
                    {
                        default:
                        case IdleAction.Stop:
                            directions.Add(MyExtensions.MyExtensionMethods.StopAngle);
                            break;
                        case IdleAction.TowardsMean:
                            directions.Add(Math.Atan2((double)(meanLocation.Y - agentPos.Y), (double)(meanLocation.X - agentPos.X)));
                            break;
                        case IdleAction.Gradient:
                            double gradientDirection = beliefSampler.GetGradientDirection(agentPoint);
                            if (gradientDirection == MyExtensionMethods.StopAngle)
                            {
                                gradientDirection = gradientSampler.GetGradientDirection(agentPoint);
                            }
                            if (gradientDirection != MyExtensionMethods.StopAngle)
                            {
                                directions.Add(gradientDirection);
                            }
                            else
                                goto case IdleAction.TowardsMean;
                            break;
                    }
                }
                else
                {
                    PointF taskPos = (PointF)firstTaskPositions[i];
                    int taskID;
                    if (tasks.TryGetValue(taskPos, out taskID))
                        taskIDs.Add(taskID);
                    else
                        taskIDs.Add(-1);
                    directions.Add(Math.Atan2(taskPos.Y - agentPos.Y, taskPos.X - agentPos.X));
                }
            }
        }

        internal void GetSolutionDataStopWhenIdle(GroupSoltion sol, out List<int> taskIDs, out List<double> directions)
        {
            GetSolutionData(sol, IdleAction.Stop, out taskIDs, out directions, null, null);
        }

        internal void GetSolutionDataStopWhenIdle(Decision d, out List<int> taskIDs, out List<double> directions)
        {
            taskIDs = new List<int>(Agents.Length);
            directions = new List<double>(Agents.Length);
            var firstTaskPositions = d.assigned;
            for (int i = 0; i < Agents.Length; i++)
            {
                PointF agentPos = Agents[i];
                if (firstTaskPositions[i] == null)
                {
                    taskIDs.Add(-1);
                    if ((d.x[i] == 0) && (d.y[i] == 0))
                        directions.Add(MyExtensions.MyExtensionMethods.StopAngle);
                    else
                        directions.Add(Math.Atan2(d.y[i], d.x[i]));
                }
                else
                {
                    PointF taskPos = (PointF)firstTaskPositions[i];
                    int taskID;
                    if (tasks.TryGetValue(taskPos, out taskID))
                        taskIDs.Add(taskID);
                    else
                        taskIDs.Add(-1);
                    directions.Add(Math.Atan2(d.y[i], d.x[i]));
                }
            }
        }

        internal void CreateSolutionStatistics(IEnumerable<ITreeNode> problems, out List<List<int>> taskIDs, out List<List<double>> directions)
        {
            taskIDs = new List<List<int>>();
            directions = new List<List<double>>();
            foreach (var prob in problems)
            {
                List<int> ids;
                List<double> dirs;
                GetSolutionDataStopWhenIdle(prob.BestChild as GroupSoltion, out ids, out dirs);
                taskIDs.Add(ids);
                directions.Add(dirs);
            }
        }

        internal void RefreshExpectedChildren(ITreeNode root)
        {
            GroupData current = this;
            List<double> nChildren = root.CountChildren();
            nChildren.Insert(0,0);
            List<GroupData> glist = new List<GroupData>();
            while (current != null)
            {
                glist.Add(current);
                current = current.NextGroup;
            }
            glist.Reverse();
            int i = 0;
            double chNum = 0;
            foreach (GroupData g in glist)
            {
                chNum *= nChildren[i];
                chNum++;
                g.ExpectedChildrenOfSolution = chNum;
                i++;
                chNum *= nChildren[i];
                chNum++;
                g.ExpectedChildrenOfProblem = chNum;
                i++;
            }
        }

        internal void UploadDecisionRequest(GroupSoltion groupSoltion)
        {
            toAggregate.Add(groupSoltion.GetDecisionData(Agents));
        }

        internal Decision GetAggregatedDecision()
        {
            return new Decision(toAggregate);
        }

        internal bool CheckFixTasks(IDictionary<PointF, int> topTasks)
        {
            return tasks.SequenceEqual(topTasks);
        }
    }

    public class InstanceData
    {
        private static System.Runtime.Serialization.IFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
        public double[,] DistanceTable { get; private set; }
        public List<int>[] Constraints { get; private set; }
        public PointF[] Locations { get; private set; }
        int hash;
        public bool IsInvalid { get; private set; }

        public InstanceData(double[,] dtable, List<int>[] constraints, PointF[] locations)
        {
            DistanceTable = dtable;
            Constraints = constraints;
            Locations = locations;
            using (MemoryStream fs = new MemoryStream())
            {
                formatter.Serialize(fs, Locations);
                hash = fs.ToArray().GetHashCode();
            }
        }

        public override int GetHashCode()
        {
            return hash;
        }

        public void Invalidate()
        {
            IsInvalid = true;
        }

        public void Validate(double[,] dtable)
        {
            DistanceTable = dtable;
            IsInvalid = false;
        }
    }

    public class GroupProxy
    {
        public GroupData g { get; private set; }

        public GroupProxy(GroupData gd)
        {
            g = gd;
        }

        public void ChangeGroup(GroupData oldG, GroupData newG)
        {
            if (g == oldG)
                g = newG;
            else
                throw new Exception("Could not find GroupData to replace in GroupProxy!");
        }
    }
}
