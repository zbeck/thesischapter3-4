﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeSearchSolver
{
    public interface IExtractableModel
    {
        /// <summary>
        /// Extracts the parameters of the problem that do not change dynamically
        /// </summary>
        /// <param name="tasks">dictionary of tasks by locations with their IDs for each level</param>
        /// <param name="speed">movement speed of agents at each level</param>
        /// <param name="radius">radius of discovery at each level</param>
        /// <param name="taskTime">the time of the execution of a task at each level</param>
        void ExtractFixData(out Dictionary<PointF, int>[] tasks, out double[] speed, out double[] radius, out double[] taskTime);

        /// <summary>
        /// Extracts parameters that change from one timestep to another
        /// </summary>
        /// <param name="agents">dictionary of agent locations by their ID for each level</param>
        /// <param name="battery">battery time at each level</param>
        /// <param name="stayTime">the time each agent has to stay still because of currently executing a task by their ID</param>
        void ExtractCurrentData(out Dictionary<int, PointF>[] agents, out double[] battery, out Dictionary<int, double> stayTime);
    }
}
