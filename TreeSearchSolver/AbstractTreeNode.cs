﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TreeSearchSolver
{
    public abstract class AbstractTreeNode : ITreeNode
    {
        static int nNodes = 0;
        public static int NodeCount { get { return nNodes; } }
        public static void ResetNodeCount() { nNodes = 0; }
        protected static int triesBeforeClosing = 100;
        public abstract ITreeNode Parent { get; }
        public abstract ITreeNode BestChild { get; }
        public abstract ITreeNode MakeChild(int tries = 100);
        public abstract int Expand(int nChildren);
        public abstract IEnumerable<AbstractTreeNode> Children { get; }
        public abstract void UpdateChildrenStatistics(ITreeNode node);
        public abstract void InvalidateChildrenStatistics();
        public abstract int ChildrenCount { get; }
        public abstract GroupData Group { get; }
        public abstract double Value { get; }
        public abstract double Utility { get; }
        public abstract double Increase(int samples, out ITreeNode best, bool topNode = false);
        public abstract double OwnIncrease(int nSamples);
        public abstract bool IsBottomLevel { get; }
        public bool IsRoot { get { return Parent == null; } }
        public abstract bool IsGreedy { get; }
        public bool IsInit { get; private set; }
        public bool IsInvalid { get; protected set; }
        public bool IsClosed { get; protected set; }
        public void ResetClosed() { IsClosed = false; }
        protected static double ConfidenceInterval = 1;
        public abstract void AggregateDecision(bool isTop = true);
        public abstract void FillStatistics();
        protected abstract void RecalculateStatistics();
        public abstract void ClearChildren();

        public AbstractTreeNode() { nNodes++; }

        /// <summary>
        /// Counts the number of nodes in the levels below by level
        /// </summary>
        /// <returns>A list of number of nodes</returns>
        List<double> ITreeNode.CountChildren()
        {
            if (IsBottomLevel)
                return null;
            List<double> l = new List<double>();
            List<double[]> fromChildren = new List<double[]>();
            foreach (ITreeNode child in Children)
            {
                var got = child.CountChildren();
                if (got == null)
                    break;
                fromChildren.Add(got.ToArray());
            }
            if (fromChildren.Count > 0)
            {
                int size = fromChildren[0].Length;
                for (int i = 0; i < size; i++)
                    l.Add(0);
                foreach (double[] got in fromChildren)
                {
                    for (int i = 0; i < size; i++)
                        l[i] += got[i];
                }
                for (int i = 0; i < size; i++)
                    l[i] = l[i] / fromChildren.Count;
            }
            l.Add(ChildrenCount);
            return l;
        }

        public void UpdateParent()
        {
            if (Parent != null)
                Parent.UpdateChildrenStatistics(this);
        }

        public void PrintPretty(string indent = "", bool last = true, int omitLevels = 0)
        {
            Console.Write(indent);
            if (last)
            {
                Console.Write("\\-");
                indent += "  ";
            }
            else
            {
                Console.Write("|-");
                indent += "| ";
            }
            Console.WriteLine(this.ToString());

            AbstractTreeNode ch = this;
            for (int i = 0; i < omitLevels; i++)
                ch = ch.Children.First();
            if (ch.IsBottomLevel)
                return;
            else
            {
                foreach (AbstractTreeNode child in Children)
                    child.PrintPretty(indent, child == Children.Last(), omitLevels);
            }
        }

        /// <summary>
        /// Tree initialisation and refresh
        /// </summary>
        /// <param name="stage">0-initialisation, 3-statistical refresh</param>
        /// <param name="topNode"></param>
        public void Initialise(int stage = 0, bool topNode = true)
        {
            List<AbstractTreeNode> children;
            switch (stage)
            {
                case 0:
                    //this copy is necessary because the order of Children might change
                    children = new List<AbstractTreeNode>(Children);
                    foreach (var ch in children)
                        ch.Initialise(0, false);
                    IsInit = true;
                    FillStatistics();
                    break;
                case 1:
                    //this copy is necessary because the order of Children might change
                    children = new List<AbstractTreeNode>(Children);
                    foreach (var ch in children)
                        ch.Initialise(1, false);
                    UpdateFromParent();
                    break;
                case 3:
                    //this copy is necessary because the order of Children might change
                    children = new List<AbstractTreeNode>(Children);
                    foreach (var ch in children)
                        ch.Initialise(3, false);
                    RecalculateStatistics();
                    break;
                default:
                    return;
            }
            if (topNode)
                Initialise(stage + 1, true);
        }

        protected virtual void UpdateFromParent() { }

        public void InvalidateSubtree()
        {
            IsInvalid = true;
            InvalidateChildrenStatistics();
            foreach (var ch in Children)
            {
                ch.InvalidateSubtree();
            }
        }

        public virtual void Validate()
        {
            if (IsInvalid)
            {
                Parent.Validate();
                IsInvalid = false;
            }
        }

    }
}
