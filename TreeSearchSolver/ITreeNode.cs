﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeSearchSolver
{
    public interface ITreeNode
    {
        ITreeNode Parent { get; }
        ITreeNode BestChild { get; }
        ITreeNode MakeChild(int tries = 100);
        int Expand(int nChildren);
        IEnumerable<AbstractTreeNode> Children { get; }
        void UpdateChildrenStatistics(ITreeNode node);
        int ChildrenCount { get; }
        GroupData Group { get; }
        double Value { get; }
        double Utility { get; }
        double Increase(int nSamples, out ITreeNode best, bool topNode = false);
        double OwnIncrease(int nSamples);
        bool IsBottomLevel { get; }
        bool IsRoot { get; }
        bool IsGreedy { get; }
        bool IsInit { get; }
        bool IsInvalid { get; }
        bool IsClosed { get; }
        List<double> CountChildren();
        void UpdateParent();
        void AggregateDecision(bool isTop = true);
        void FillStatistics();
        void InvalidateSubtree();
        void Validate();
    }
}
