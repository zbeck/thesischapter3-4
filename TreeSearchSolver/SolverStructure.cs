﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeSearchSolver
{
    public class SolverStructure
    {
        public static double[] DefaultPercentile = new double[] { 0.2, 0.1};
        protected static double[] UtilityFactors = new double[] { 0.2, 1 };
        protected GroupData topData;
        protected GroupProblem rootNode;
        public static Random RndSeed
        {
            set
            {
                GroupSoltion.rndSeed = value;
                isSeedSet = true;
            }
        }
        protected static bool isSeedSet = false;
        protected ExplorationComparer explorationComparer;

        protected static void InitRndSeed()
        {
            if (!isSeedSet)
                RndSeed = new Random();
        }

        protected GroupData ExtractDualProblem(IExtractableModel model, AgentDraw.Sampler rand, out double[] battery)
        {
            Dictionary<PointF, int>[] tasks;
            Dictionary<int, PointF>[] agents;
            Dictionary<int, double> stayTime;
            double[] speed, radius, taskTime;
            model.ExtractFixData(out tasks, out speed, out radius, out taskTime);
            model.ExtractCurrentData(out agents, out battery, out stayTime);
            double[][] agentStay = new double[agents.Length][];
            for (int i = 0; i < agents.Length; i++)
            {
                agentStay[i] = new double[agents[i].Count];
                int j = 0;
                foreach (var e in agents[i])
                {
                    agentStay[i][j] = stayTime[e.Key];
                    j++;
                }
            }
            explorationComparer = new ExplorationComparer(rand, radius[0]);
            SortedDictionary<PointF, int> topTasks = new SortedDictionary<PointF, int>(tasks[0], explorationComparer);
            topData = new GroupData(null, topTasks, agents[0].Values.ToArray(), agentStay[0], speed[0], taskTime[0], battery[0], radius[0], UtilityFactors[0], null, DefaultPercentile[0]);
            return new GroupData(rand, tasks[1], agents[1].Values.ToArray(), agentStay[1], speed[1], taskTime[1], battery[1], radius[1], UtilityFactors[1], topData, DefaultPercentile[1]);
        }

        public double[] GetUtilities(double[] extraData = null)
        {
            if (extraData == null)
                extraData = new double[0];
            double[] retval = UtilityData();
            int origLen = retval.Length;
            Array.Resize(ref retval, retval.Length + extraData.Length);
            Array.Copy(extraData, 0, retval, origLen, extraData.Length);
            return retval;
        }

        protected virtual double[] UtilityData()
        {
            return new double[] { rootNode.Utility, rootNode.GreedyUtility() };
        }
    }
}
