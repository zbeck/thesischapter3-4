﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace TreeSearchSolver
{
    /// <summary>
    /// Sorts exploration points according to the belief map in descending order by the expeted number of explored tasks
    /// </summary>
    public class ExplorationComparer : IComparer<PointF>
    {
        AgentDraw.Sampler dist;
        double radius;
        Dictionary<PointF, double> eVals;

        public ExplorationComparer(AgentDraw.Sampler sam, double rad)
        {
            dist = sam;
            radius = rad;
            eVals = new Dictionary<PointF, double>();
        }

        double getEVal(PointF p)
        {
            if (eVals.ContainsKey(p))
                return eVals[p];
            else
            {
                double eVal = dist.GetExpectedValueForRectangle(makeRect(p));
                eVals.Add(p, eVal);
                return eVal;
            }
        }

        private Rectangle makeRect(PointF p)
        {
            return new Rectangle((int)(p.X - radius), (int)(p.Y - radius), (int)(2 * radius), (int)(2 * radius));
        }

        public int Compare(PointF v1, PointF v2)
        {
            double E1 = getEVal(v1);
            double E2 = getEVal(v2);
            if (E1 == E2)
            {
                if (v1.X == v2.X)
                    return v1.Y.CompareTo(v2.Y);
                else
                    return v1.X.CompareTo(v2.X);
            }
            return -E1.CompareTo(E2);
        }
    }
}
