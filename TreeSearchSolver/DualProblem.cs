﻿using AgentDraw;
using MyExtensions;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace TreeSearchSolver
{
    public class DualProblem : SolverStructure
    {
        GroupData bottomData;
        GroupSoltion topSolution
        {
            get
            {
                GroupSoltion sol = rootNode.Children.FirstOrDefault() as GroupSoltion;
                if (sol == null)
                    return rootNode.AddGreedy();
                else
                    return sol;
            }
        }
        GroupProblem[] bottomProblems;
        List<double>[] bottomFeedback;
        public Sampler gradientSampler;

        public DualProblem(IExtractableModel model, Sampler rand, DualProblem lastProblem = null, bool problemChanged = false, int fixedExplorationSteps = 0)
        {
            InitRndSeed();
            double[] battery;
            bottomData = ExtractDualProblem(model, rand, out battery);
            double[,] dTable;
            PointF[] taskLoc;
            topData.GetProblem(out dTable, out taskLoc);
            GroupProblem oldRoot = null;
            if (lastProblem != null)
                oldRoot = lastProblem.rootNode;
            rootNode = new GroupProblem(0, dTable, taskLoc, new double[taskLoc.Length], battery[0], gdata: topData, oldRoot: oldRoot, fixedExplorationSteps: fixedExplorationSteps);
            rootNode.InitPopulateClear();
            if (lastProblem == null || problemChanged)
            {
                bottomFeedback = new List<double>[rootNode.NTasks];
                for (int j = 0; j < rootNode.NTasks; j++)
                    bottomFeedback[j] = new List<double>(0);
            }
            else
            {
                if (lastProblem.bottomFeedback.Length != rootNode.NTasks)
                    throw new Exception("Cannot use previous problem if it has changed!");
                bottomFeedback = lastProblem.bottomFeedback;
            }
        }

        /// <summary>
        /// Solves the dual level problem structure.
        /// </summary>
        /// <param name="dependencyMode">specifies the dependencies between the level solutions
        /// -n - top first, n planning steps
        /// -1 - independent with gradient descent
        ///  0 - independent with HOP
        ///  1 - top -> bottom
        ///  2 - bottom -> top
        ///  3 - top <-> bottom (bottom first, 5 planning steps) (bottom-top-bottom-top-bottom)
        ///  n - bottom first, n-2 planning steps</param>
        /// <param name="sampleNum">number of samples used to solve bottom level</param>
        /// <param name="assignedIds">output first assigend task IDs for agents by level and agent (-1 if not existing task)</param>
        /// <param name="directions">output heading direction for each agent by level and agent (MyextensionMethods.StopAngle if decides to stop)</param>
        public void Solve(int dependencyMode, int sampleNum, out List<List<int>> assignedIds, out List<List<double>> directions, Stopwatch botSolveTime = null)
        {
            assignedIds = new List<List<int>>(2);
            directions = new List<List<double>>(2);
            if (botSolveTime != null) botSolveTime.Reset();
            if (dependencyMode < -1)
            {//Solve top first, custom number of iterations
                List<int> list;
                List<double> list2;
                this.SolveTopSeparately(out list, out list2);
                assignedIds.Add(list);
                directions.Add(list2);
                if (botSolveTime != null) botSolveTime.Start();
                this.SolveBottom(sampleNum, true, out list, out list2);
                if (botSolveTime != null) botSolveTime.Stop();
                assignedIds.Add(list);
                directions.Add(list2);
                for (int i = 2; i < -dependencyMode; i++)
                {
                    if ((i % 2) == 0)
                    {
                        this.SolveTopByFeedback(out list, out list2);
                        assignedIds[0] = list;
                        directions[0] = list2;
                    }
                    else
                    {
                        if (botSolveTime != null) botSolveTime.Start();
                        this.SolveBottom(sampleNum, true, out list, out list2);
                        if (botSolveTime != null) botSolveTime.Stop();
                        assignedIds[1] = list;
                        directions[1] = list2;
                    }
                }
            }
            else if (dependencyMode > 3)
            {//Solve bottom first, custom number of iterations
                List<int> list3;
                List<double> list4;
                assignedIds.Add(null);
                directions.Add(null);
                if (botSolveTime != null) botSolveTime.Stop();
                this.SolveBottom(sampleNum, false, out list3, out list4);
                if (botSolveTime != null) botSolveTime.Stop();
                assignedIds.Add(list3);
                directions.Add(list4);
                for (int j = 1; j < (dependencyMode - 2); j++)
                {
                    if ((j % 2) == 1)
                    {
                        this.SolveTopByFeedback(out list3, out list4);
                        assignedIds[0] = list3;
                        directions[0] = list4;
                    }
                    else
                    {
                        if (botSolveTime != null) botSolveTime.Stop();
                        this.SolveBottom(sampleNum, true, out list3, out list4);
                        if (botSolveTime != null) botSolveTime.Stop();
                        assignedIds[1] = list3;
                        directions[1] = list4;
                    }
                }
            }
            else
            {
                if (dependencyMode < 2)
                {
                    List<int> list5;
                    List<double> list6;
                    this.SolveTopSeparately(out list5, out list6);
                    assignedIds.Add(list5);
                    directions.Add(list6);
                    if (dependencyMode == -1)
                    {
                        if (botSolveTime != null) botSolveTime.Stop();
                        this.SolveBottomNoHindsight(out list5, out list6);
                        if (botSolveTime != null) botSolveTime.Stop();
                    }
                    else
                    {
                        if (botSolveTime != null) botSolveTime.Stop();
                        this.SolveBottom(sampleNum, dependencyMode == 1, out list5, out list6);
                        if (botSolveTime != null) botSolveTime.Stop();
                    }
                    assignedIds.Add(list5);
                    directions.Add(list6);
                }
                else if (dependencyMode >= 2)
                {
                    List<int> list7;
                    List<double> list8;
                    assignedIds.Add(null);
                    directions.Add(null);
                    if (botSolveTime != null) botSolveTime.Stop();
                    this.SolveBottom(sampleNum, false, out list7, out list8);
                    if (botSolveTime != null) botSolveTime.Stop();
                    assignedIds.Add(list7);
                    directions.Add(list8);
                    this.SolveTopByFeedback(out list7, out list8);
                    assignedIds[0] = list7;
                    directions[0] = list8;
                }
                if (dependencyMode == 3)
                {
                    List<int> list9;
                    List<double> list10;
                    if (botSolveTime != null) botSolveTime.Stop();
                    this.SolveBottom(sampleNum, true, out list9, out list10);
                    if (botSolveTime != null) botSolveTime.Stop();
                    assignedIds[1] = list9;
                    directions[1] = list10;
                    for (int k = 0; k < 2; k++)
                    {
                        this.SolveTopByFeedback(out list9, out list10);
                        assignedIds[0] = list9;
                        directions[0] = list10;
                        if (botSolveTime != null) botSolveTime.Stop();
                        this.SolveBottom(sampleNum, true, out list9, out list10);
                        if (botSolveTime != null) botSolveTime.Stop();
                        assignedIds[1] = list9;
                        directions[1] = list10;
                    }
                }
            }
        }

        protected void PopulateBottomProblems(int sampleNum, bool useTopSolution)
        {
            bottomProblems = new GroupProblem[sampleNum];
            for (int i = 0; i < sampleNum; i++)
            {
                double[,] dTable;
                PointF[] taskLoc;
                List<int>[] constraints;
                bottomData.GetProblem(i, rootNode.Tasks, out dTable, out constraints, out taskLoc);
                double[] activations;
                if (useTopSolution)
                    activations = topSolution.ExtractActivation(constraints);
                else
                    activations = new double[taskLoc.Length];
                GroupProblem sample = new GroupProblem(i, dTable, taskLoc, activations, bottomData.BatteryCapacity, gdata: bottomData);
                //an initial greedy solution is given to the problem
                GroupSoltion greedy = sample.AddGreedy();
                constraints = rootNode.ReverseConstraints(constraints);
                var sampleFeedback = greedy.FeedBackForActivators(constraints);
                for (int j = 0; j < rootNode.NTasks; j++)
                    bottomFeedback[j].AddRange(sampleFeedback[j]);
                bottomProblems[i] = sample;
            }
            for (int j = 0; j < rootNode.NTasks; j++)
                bottomFeedback[j].Sort();
        }

        protected void SolveTopSeparately(out List<int> solutionIDs, out List<double> directions)
        {
            topData.GetSolutionDataStopWhenIdle(topSolution, out solutionIDs, out directions);
        }

        /// <summary>
        /// Gives greedy solution for top level considering the feedback from the bottom level
        /// bottomFeedback has to be sorted!
        /// </summary>
        /// <param name="solutionIDs"></param>
        /// <param name="directions"></param>
        protected void SolveTopByFeedback(out List<int> solutionIDs, out List<double> directions)
        {
            rootNode.ClearChildren();
            GroupSoltion sol = rootNode.AddByActivation(bottomFeedback);
            topData.GetSolutionDataStopWhenIdle(sol, out solutionIDs, out directions);
        }

        protected void SolveBottom(int samplenum, bool useTopSolution, out List<int> solutionIDs, out List<double> directions)
        {
            List<List<int>> idSamples;
            List<List<double>> dirSamples;
            PopulateBottomProblems(samplenum, useTopSolution);
            bottomData.CreateSolutionStatistics(bottomProblems, out idSamples, out dirSamples);
            solutionIDs = DecideAssignment(idSamples);
            directions = AverageDirections(dirSamples);
        }

        protected void SolveBottomNoHindsight(out List<int> solutionIDs, out List<double> directions)
        {
            double[,] numArray;
            PointF[] tfArray;
            this.bottomData.GetProblem(out numArray, out tfArray);
            GroupSoltion sol = new GroupProblem(0, numArray, tfArray, new double[tfArray.Length], this.bottomData.BatteryCapacity, null, this.bottomData).AddGreedy();
            this.bottomData.GetSolutionData(sol, IdleAction.Gradient, out solutionIDs, out directions, this.bottomData.sampler, this.gradientSampler);
        }
        
        private List<double> AverageDirections(List<List<double>> dirSamples)
        {
            List<double> avg = new List<double>(bottomData.Agents.Length);
            for(int agent=0; agent<bottomData.Agents.Length; agent++)
            {
                double sin = 0.0, cos = 0.0;
                foreach (var dirsam in dirSamples)
                {
                    double dir = dirsam[agent];
                    if (dir != MyExtensionMethods.StopAngle)
                    {
                        sin += Math.Sin(dir);
                        cos += Math.Cos(dir);
                    }
                }
                if ((sin == 0.0) && (cos == 0.0))
                    avg.Add(MyExtensionMethods.StopAngle);
                else
                    avg.Add(Math.Atan2(sin, cos));
            }
            return avg;
        }

        private List<int> DecideAssignment(List<List<int>> idSamples)
        {
            int majority = idSamples.Count / 2 + 1;
            List<int> decided = new List<int>(idSamples.Count);
            int nAgents = idSamples[0].Count;
            for(int agent=0;agent<nAgents;agent++)
            {
                Dictionary<int, int> chosen = new Dictionary<int, int>();
                int nonExisting = 0;
                foreach (var idSample in idSamples)
                {
                    int id = idSample[agent];
                    if (id == -1)
                        nonExisting++;
                    else if (chosen.ContainsKey(id))
                        chosen[id]++;
                    else
                        chosen.Add(id, 1);
                }
                if (nonExisting >= majority)
                    decided.Add(-1);
                else
                {
                    KeyValuePair<int, int> maxChosen = chosen.Aggregate((l, r) => l.Value > r.Value ? l : r);
                    decided.Add(maxChosen.Key);
                }
            }
            return decided;
        }
    }
}
