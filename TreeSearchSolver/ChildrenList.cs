﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeSearchSolver
{
    public class SolutionList : ChildrenList<GroupSoltion>
    {
        double percentile;
        public double Percentile { get { return percentile; } set { if ((value > 0) && (value <= 1)) percentile = value; } }
        public double PercentileAverage { get; private set; }
        public double PercentileBoundary { get; private set; }
        public double PercentileMax
        {
            get
            {
                if (list.Any())
                {
                    return list[0].Utility;
                }
                return 0;
            }
        }
        
        public SolutionList(ITreeNode owner) : base(owner) { }

        private Decision bestDecision;
        private GroupSoltion bestDecisionSolution = null;
        public Decision BestSolutionData(System.Drawing.PointF[] agentPos)
        {
            if (list.Count > 0)
                if ((bestDecisionSolution == null) || (bestDecisionSolution.GetHashCode() != list.First().GetHashCode()))
                {
                    bestDecisionSolution = list.First();
                    bestDecision = bestDecisionSolution.GetDecisionData(agentPos);
                }
            return bestDecision;
        }
        
        protected override void InitStatistics()
        {
            base.InitStatistics();
            Percentile = owner.Group.Percentile;
            PercentileAverage = 0;
            PercentileBoundary = 0;
            bestDecisionSolution = null;
        }

        public override void RecalculateStatistics()
        {
            if (Count == 0)
                return;
            // calculate percentile statistics
            int count = Count;
            int percCount = (int)Math.Round(Percentile * count);
            double percentileSum = 0;
            int startCount = IsGreedyDominant ? 1 : 0;
            for (int i = startCount; i < percCount + startCount; i++)
            {
                //if invalid validate and refresh list position
                if (list[i].IsInvalid)
                {
                    GroupSoltion element = list[i];
                    element.Validate();
                    //if new position changes for later, we should not include it in the sum at this time, and next iteration should be at location i again
                    if (ChildChanged(element) > i)
                        i--;
                    else
                        percentileSum += element.Utility;
                }
                else
                    percentileSum += list[i].Utility;
            }

            PercentileAverage = percentileSum / percCount;
            int beforePercentile = (percCount == 0) ? 0 : percCount - 1;
            int afterPercentile = percCount;
            if (IsGreedyDominant && (Count > 1))
            {
                beforePercentile++;
                afterPercentile++;
            }
            PercentileBoundary = (list[beforePercentile].Utility + list[afterPercentile].Utility) / 2;
        }

        internal double GetMeanDiff(GroupSoltion groupSoltion, out bool isBest)
        {
            if (Count < 2)
            {
                isBest = true;
                return 0;
            }
            //it is the highest value child the differece with the second best
            if (groupSoltion == list[0])
            {
                isBest = true;
                return list[0].Utility - list[1].Utility;
            }
            //otherwise the difference with the best
            else
            {
                isBest = false;
                return list[0].Utility - groupSoltion.Utility;
            }
        }

        protected override bool _add(GroupSoltion item)
        {
            bool isAdded = base._add(item);

            //don't update Value if bottom level
            if (!item.IsBottomLevel)
            {
                bool isBest = item == list[0];
                if (isBest)
                    foreach (var ch in list)
                    {
                        ch.UpdateValue();
                    }
                else
                    item.UpdateValue();
            }
            return isAdded;
        }

        public override bool Update(GroupSoltion item)
        {
            bool wasBest = item == list[0];

            bool isRefreshed = base.Update(item);
            
            //don't update Value if bottom level
            if (!item.IsBottomLevel)
            {
                bool isBest = item == list[0];
                if (wasBest || isBest)
                    foreach (var ch in list)
                        ch.UpdateValue();
                else
                    item.UpdateValue();
            }
            return isRefreshed;
        }
    }

    public class ProblemList : ChildrenList<GroupProblem>
    {
        public double Average { get; private set; }
        private double stdDev2;
        //the measured variance of the children utility distribution
        public double StdDev2
        {
            get { return stdDev2; }
            private set
            {
                if (Count == 0)
                    StdErr = Double.PositiveInfinity;
                else
                    StdErr = Math.Sqrt(value / Count);
                stdDev2 = value;
            }
        }
        //standard error of the mean estimate for the children utility distribution
        public double StdErr { get; private set; }

        public ProblemList(ITreeNode owner) : base(owner) { }

        protected override void InitStatistics()
        {
            base.InitStatistics();
            Average = 0;
            StdDev2 = 0;
        }

        public override void RecalculateStatistics()
        {
            //validate children
            if (list.Any(e => e.IsInvalid))
            {
                GroupProblem[] listCopy = list.ToArray();
                foreach (var ch in listCopy)
                    ch.Validate();
            }
            // calculate average
            double sum = this.Sum(e => e.Utility);
            Average = sum / Count;
            // calculate standard deviation
            sum = list.Sum(e => (e.Utility - Average) * (e.Utility - Average));
            StdDev2 = sum / (Count - 1);
        }
    }

    public class ChildrenList<T> : ICollection<T> where T : ITreeNode
    {
        protected class ByUtilType : IComparer<T>
        {
            public int Compare(T x, T y)
            {
                double vx = x.Utility;
                double vy = y.Utility;
                if (vx == vy)
                    return x.GetHashCode().CompareTo(y.GetHashCode());
                else
                    return -vx.CompareTo(vy);
            }
        }
        protected List<T> list;
        static IComparer<T> byUtil = new ByUtilType();
        static double recalculateRatio = 0.1;

        protected ITreeNode owner;
        private int statisticsChangeCounter;
        public bool IsGreedyDominant
        {
            get
            {
                if (list.Any())
                    return list[0].IsGreedy;
                return false;
            }
        }

        public ITreeNode BestChild()
        {
            return list.Aggregate((a, b) => (a.Value > b.Value) ? a : b);
        }

        public T this[int index]
        {
            get
            {
                return list[index];
            }
        }

        public ChildrenList(ITreeNode owner)
        {
            this.owner = owner;
            list = new List<T>();
            InitStatistics();
        }

        protected virtual void InitStatistics()
        {
            statisticsChangeCounter = 0;
        }

        public void FillInitStatistics()
        {
            var children = list.ToArray();
            foreach (var ch in children)
                ChildChanged(ch);
            RecalculateStatistics();
        }

        /// <summary>
        /// Finds the location for an item to be inserted in the list
        /// </summary>
        /// <param name="item">item to find the location of</param>
        /// <param name="location">the found location</param>
        /// <returns>if the item was found in the list already</returns>
        private bool findLocation(T item, out int location)
        {
            location = list.BinarySearch(item, byUtil);
            if (location >= 0)
                return true;            
            location = ~location;
            return false;
        }

        void ICollection<T>.Add(T item)
        {
            this.Add(item);
        }

        public bool Add(T item)
        {
            bool added = _add(item);
            refreshStatistics(added ? 1 : 0);
            return added;
        }

        public List<T> AddRange(IEnumerable<T> collection)
        {
            List<T> added = new List<T>(collection);
            foreach (T item in collection)
                if (!_add(item))
                    added.Remove(item);
            refreshStatistics(added.Count);
            return added;
        }

        protected virtual bool _add(T item)
        {
            int location;
            if (findLocation(item, out location))
                return false;
            list.Insert(location, item);
            return true;
        }

        protected bool refreshStatistics(int newItems)
        {
            //skip if no new items or in initialization period
            if ((newItems == 0) || !owner.IsInit)
                return false;
            statisticsChangeCounter += newItems;
            if (statisticsChangeCounter > Count * recalculateRatio)
            {
                statisticsChangeCounter = 0;
                RecalculateStatistics();
                UpdateParent();
                return true;
            }
            return false;
        }

        public virtual void RecalculateStatistics() { }

        private void UpdateParent()
        {
            owner.UpdateParent();
        }

        public virtual bool Update(T child)
        {
            return refreshStatistics((ChildChanged(child)<0) ? 0 : 1);
        }

        protected int ChildChanged(T child)
        {
            int oldLoc = list.FindIndex(e => e.GetHashCode() == child.GetHashCode());
            if (oldLoc == -1)   //this means it is not added to the list yet because we are still in its constructor
                return -1;
            list.RemoveAt(oldLoc);
            int newLoc;
            findLocation(child, out newLoc);
            list.Insert(newLoc, child);
            return newLoc;
        }

        public void Clear()
        {
            list.Clear();
            InitStatistics();
        }

        public bool Contains(T item)
        {
            int location;
            return findLocation(item, out location);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            list.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return list.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(T item)
        {
            int location;
            bool found = findLocation(item, out location);
            if (found)
                list.RemoveAt(location);
            return found;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return list.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return list.GetEnumerator();
        }

        public void InvalidateStatistics()
        {
            //this will trigger an updatestatistics as soon as any element has changed
            statisticsChangeCounter = Count;
        }

        internal void RemoveNonBestChildren(int nchildrenToKeep)
        {
            list = new List<T>(list.Take(nchildrenToKeep));
        }
    }
}
