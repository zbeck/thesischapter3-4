﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyExtensions;
using System.Drawing;

namespace TreeSearchSolver
{
    public class GroupSoltion : AbstractTreeNode
    {
        private static bool debug = false;
        private int nSamples = 5;
        public static double U0 = 0.02;//2.1;
        public static int expectedHorisonSize = 5;
        public static double SolverPowerFactor = 20.0;
        public static Random rndSeed;
        public static SolutionExpansionPolicyType ExpansionPolicy = SolutionExpansionPolicyType.MixedUCBVoI;
        public readonly GroupProblem parent;
        public override GroupData Group { get { return parent.Group; } }
        public List<int>[] Allocation { get; private set; }
        public PointF?[] FirstAllocated
        {
            get
            {
                PointF?[] fa = new PointF?[Allocation.Length];
                for (int i = 0; i < Allocation.Length; i++)
                {
                    if (Allocation[i].Count < 2)
                        fa[i] = null;
                    else
                        fa[i] = parent.Tasks[Allocation[i][1]];
                }
                return fa;
            }
        }
        public double StdErr { get { return children.StdErr; } }
        public int[][] GetInitialAllocation(int nSteps)
        {
            int[][] alloc = new int[nSteps][];
            for (int i = 0; i < nSteps; i++)
            {
                alloc[i] = new int[parent.NAgents];
                for (int a = 0; a < parent.NAgents; a++)
                {
                    if (Allocation[a].Count > i+1)
                        alloc[i][a] = Allocation[a][i+1];
                    else
                        alloc[i][a] = -1;
                }
            }
            return alloc;
        }
        internal double[] executionTimes { get; private set; }
        private ProblemList children;
        public override IEnumerable<AbstractTreeNode> Children
        {
            get
            {
                if (IsBottomLevel)
                    return Enumerable.Empty<AbstractTreeNode>();
                return children.AsEnumerable();
            }
        }
        public override void UpdateChildrenStatistics(ITreeNode node)
        {
            children.Update(node as GroupProblem);
            UpdateValue();
        }
        public override int ChildrenCount
        {
            get
            {
                if (IsBottomLevel)
                    return 0;
                return children.Count;
            }
        }
        public override bool IsBottomLevel
        {
            get
            {
                if (Group != null)
                    return Group.NextGroup == null;
                return false;
            }
        }
        private bool isGreedy = false;
        public override bool IsGreedy { get { return isGreedy; } }
        private double utility = 0;
        private int hashCode = 0;

        private void InitVars()
        {
            if(!IsBottomLevel)
                children = new ProblemList(this);
        }

        private void PostInit()
        {
            utility = Group.ExtractUtility(this);
            if (!IsBottomLevel && (Group.InitChildrenOfSolution > 0))
                MakeChildren(Group.InitChildrenOfSolution);
        }

        /// <summary>
        /// Normal constructor, maked a greedy or random (enhanced to dominant) solution
        /// </summary>
        /// <param name="parentProblem">problem</param>
        /// <param name="isRandom">solution policy (flat random, random with heuristic, greedy, etc.)</param>
        public GroupSoltion(GroupProblem parentProblem, NewSolutionPolicyType policy = NewSolutionPolicyType.Default, int[][] fixedTasks = null)
            : base()
        {
            parent = parentProblem;
            InitVars();
            if (policy == NewSolutionPolicyType.Default)
                policy = GroupData.DefaultSolutionPolicy;
            if (policy == NewSolutionPolicyType.FlatRandom)
            {
                bool[] takenTasks = MakeFullRandomAllocation(fixedTasks);
                ChangeTowardsDominant(takenTasks);
            }
            else if (policy == NewSolutionPolicyType.Greedy)
            {
                MakeGreedyAllocation(fixedTasks: fixedTasks);
                isGreedy = true;
            }
            else if (policy == NewSolutionPolicyType.GuidedRandom)
            {
                bool[] takenTasks = MakeGuidedAllocation(fixedTasks: fixedTasks);
                ChangeTowardsDominant(takenTasks);
            }
            else
                throw new System.InvalidOperationException("GroupProblem.DefaultSolutionPolicy cannot be Default.");
            PostInit();
        }

        /// <summary>
        /// Constructor that tries to copy the schedule from a previous solution. Skips tasks that are no more existing.
        /// Caution! The greedy flag is set for proper statstics.
        /// </summary>
        /// <param name="groupProblem">problem</param>
        /// <param name="original">previous solution</param>
        public GroupSoltion(GroupProblem groupProblem, GroupSoltion original, int[][] fixedTasks = null)
            : base()
        {
            parent = groupProblem;
            InitVars();
            bool[] takenTasks = InitAllocation(fixedTasks);
            for (int agent = 0; agent < original.Allocation.Length; agent++)
            {
                foreach (int origTask in original.Allocation[agent].Skip(1))
                {
                    int task = parent.FindTask(original.parent.Tasks[origTask]);
                    if ((task >= 0) && !takenTasks[task])
                    {
                        AppendTask(Allocation[agent], task);
                        takenTasks[task] = true;
                    }
                    //Console.Write(" " + task.ToString() + "-" + origTask.ToString());
                }
                //Console.WriteLine();
            }
            ChangeTowardsDominant(takenTasks);
            //Console.WriteLine();
            isGreedy = true;
            PostInit();
        }

        public GroupSoltion(GroupProblem parentProblem, List<double>[] activatedCompetitions)
            : base()
        {
            parent = parentProblem;
            InitVars();
            bool[] takenTasks = InitAllocation();
            takenTasks = MakeAllocationByActivation(activatedCompetitions, takenTasks);
            ChangeTowardsDominant(takenTasks);
            PostInit();
        }

        public GroupSoltion(GroupProblem parentProblem, out int iternum)
            : base()
        {
            parent = parentProblem;
            InitVars();
            bool[] takenTasks = MakeFullRandomAllocation();
            iternum = ChangeTowardsDominant(takenTasks);
            PostInit();
        }

        public GroupSoltion(GroupProblem parentProblem, out int iternum, out string midSolution)
            : base()
        {
            parent = parentProblem;
            InitVars();
            bool[] takenTasks = MakeFullRandomAllocation();
            midSolution = ToString();
            iternum = ChangeTowardsDominant(takenTasks);
            PostInit();
        }

        /// <summary>
        /// UNSAFE constructor for debug
        /// </summary>
        /// <param name="parentProblem"></param>
        /// <param name="alloc"></param>
        internal GroupSoltion(GroupProblem parentProblem, List<int>[] alloc)
            : base()
        {
            debug = true;
            parent = parentProblem;
            InitVars();
            bool[] takentTasks = new bool[parent.NTasks];
            foreach (var ag in alloc)
                foreach (int t in ag.Skip(1))
                    takentTasks[t] = true;
            Allocation = alloc;
            executionTimes = new double[parent.NTasks + parent.NAgents];
            for (int i = 0; i < parent.NTasks; i++)
            {
                //fill task executions with infty
                executionTimes[i] = Double.PositiveInfinity;
            }
            for (int i = 0; i < parent.NAgents; i++)
                ChangeExecution(i, 1);
            ChangeTowardsDominant(takentTasks);
        }

        /// <summary>
        /// Returns the activation times for the lower level
        /// </summary>
        /// <param name="C">array by tasks of the other (bottom) level, list of tasks activating in this (top) level</param>
        /// <returns>array of activation times for bottom tasks</returns>
        public double[] ExtractActivation(List<int>[] C)
        {
            double[] act = new double[C.Length];
            for (int i = 0; i < C.Length; i++)
            {
                if (C[i].Count == 0)
                    act[i] = 0;
                else
                {
                    act[i] = Double.PositiveInfinity;
                    foreach (int t in C[i])
                        if (executionTimes[t] < act[i])
                            act[i] = executionTimes[t];
                }
            }
            return act;
        }

        public List<double>[] FeedBackForActivators(List<int>[] C)
        {
            List<double>[] fb = new List<double>[C.Length];
            for (int i = 0; i < C.Length; i++)
            {
                fb[i] = new List<double>();
                foreach (int task in C[i])
                {
                    fb[i].Add(executionTimes[task]);
                }
            }
            return fb;
        }

        private static int GetRandomIndex(bool[] array, bool filter = false)
        {
            int numvalid = array.Count(e => e == filter);
            int idf = rndSeed.Next(numvalid);
            int id = 0;
            while (array[id] != filter)
                id++;
            for (; idf > 0; idf--)
            {
                id++;
                while (array[id] != filter)
                    id++;
            }
            return id;
        }

        /// <summary>
        /// returns the index of a random element from a list of cumulative weights.
        /// </summary>
        /// <param name="weights">cumulative weight list (increasing)</param>
        /// <returns>chosen item index</returns>
        private static int GetWeightedRandomIndex(List<double> weights)
        {
            double value = rndSeed.NextDouble() * weights.Last();
            int index =  weights.BinarySearch(value);
            if (index < 0)
                index = ~index;
            return index;
        }

        /// <summary>
        /// Creates a full depth random allocation
        /// </summary>
        /// <returns>a boolean array of which tasks are scheduled</returns>
        private bool[] MakeFullRandomAllocation(int[][] fixedTasks = null)
        {
            bool[] takenTasks = InitAllocation(fixedTasks);
            bool[][] banned = new bool[parent.NAgents][];
            for (int i = 0; i < parent.NAgents; i++)
                banned[i] = new bool[parent.NTasks];
            bool[] fullAgents = new bool[parent.NAgents];
            while (!Array.TrueForAll(fullAgents, a => a))
            {
                AllocateRandom(banned, fullAgents, takenTasks);
            }
            return takenTasks;
        }

        /// <summary>
        /// Allocates a given number of random tasks to agents
        /// </summary>
        /// <param name="scheduleSize">number of tasks to allocate</param>
        /// <returns>a boolean array of which tasks are scheduled</returns>
        private bool[] MakeLimitedRandomAllocation(int scheduleSize, int[][] fixedTasks = null)
        {
            bool[] takenTasks = InitAllocation(fixedTasks);
            bool[][] banned = new bool[parent.NAgents][];
            for (int i = 0; i < parent.NAgents; i++)
                banned[i] = new bool[parent.NTasks];
            bool[] fullAgents = new bool[parent.NAgents];
            int allocated = 0;
            while ((!Array.TrueForAll(fullAgents, a => a)) && (allocated < scheduleSize))
            {
                if (AllocateRandom(banned, fullAgents, takenTasks))
                    allocated++;
            }
            return takenTasks;
        }

        private bool AllocateRandom(bool[][] banned, bool[] fullAgents, bool[] takenTasks)
        {
            int agent = GetRandomIndex(fullAgents);
            bool[] notPossbleTasks = new bool[parent.NTasks];
            Array.Copy(takenTasks, notPossbleTasks, parent.NTasks);
            for (int i = 0; i < parent.NTasks; i++)
                notPossbleTasks[i] |= banned[agent][i];
            if (Array.TrueForAll(notPossbleTasks, a => a))
                fullAgents[agent] = true;
            else
            {
                int task = GetRandomIndex(notPossbleTasks);
                if (AppendTask(Allocation[agent], task))
                {
                    takenTasks[task] = true;
                    return true;
                }
                else
                    banned[agent][task] = true;
            }
            return false;
        }

        /// <summary>
        /// Makes a greedy, deterministic allocation. Assumes equal battery life of agents.
        /// </summary>
        /// <param name="fixedTasks">fixed allocation, size [nSteps][NAgents], parent.FixedSteps used if null</param>
        private bool[] MakeGreedyAllocation(int[][] fixedTasks = null)
        {
            bool[] takenTasks = InitAllocation(fixedTasks);
            while (true)
            {
                double minTime = Double.PositiveInfinity;
                int chosenAgent = -1;
                int chosenTask = -1;
                for (int agent = 0; agent < parent.NAgents; agent++)
                {
                    for (int task = 0; task < parent.NTasks; task++)
                        if (!takenTasks[task])
                        {
                            double taskTime = FitTime(Allocation[agent].Last(), task);
                            if (taskTime < minTime)
                            {
                                minTime = taskTime;
                                chosenAgent = agent;
                                chosenTask = task;
                            }
                        }
                }
                if (minTime > parent.BatteryCapacity)
                    break;
                AppendTask(Allocation[chosenAgent], chosenTask);
                takenTasks[chosenTask] = true;
            }
            return takenTasks;
        }

        private bool[] MakeGuidedAllocation(int? scheduleSize = null, int[][] fixedTasks = null)
        {
            bool[] takenTasks = InitAllocation(fixedTasks);
            double factor = SolverPowerFactor;
            if (SolverPowerFactor < 0)
                factor = Math.Max(0.0, 3.24817 * Math.Log10(11.2115 * (takenTasks.Count(e => !e) - 1)));
            while (scheduleSize == null || scheduleSize != 0)
            {
                var possibleActions = new List<Tuple<int, int>>(parent.NTasks * parent.NAgents);
                var actionCumulativeWeights = new List<double>(parent.NTasks * parent.NAgents);
                double time = Allocation.Min(alloc => executionTimes[alloc.Last()]);
                for (int agent = 0; agent < parent.NAgents; agent++)
                {
                    double cumulativeWeight = 0.0;
                    for (int task = 0; task < parent.NTasks; task++)
                        if (!takenTasks[task])
                        {
                            double fitTime = FitTime(Allocation[agent].Last(), task);
                            if (fitTime < parent.BatteryCapacity)
                            {
                                cumulativeWeight += 1.0 / Math.Pow(fitTime - time, factor);
                                actionCumulativeWeights.Add(cumulativeWeight);
                                possibleActions.Add(new Tuple<int, int>(agent, task));
                            }
                        }
                }
                if (actionCumulativeWeights.Count == 0)
                    break;
                int chosen = GetWeightedRandomIndex(actionCumulativeWeights);
                int chosenAgent = possibleActions[chosen].Item1;
                int chosenTask = possibleActions[chosen].Item2;
                AppendTask(Allocation[chosenAgent], chosenTask);
                takenTasks[chosenTask] = true;
                scheduleSize -= 1;
            }
            return takenTasks;
        }

        private bool[] MakeAllocationByActivation(List<double>[] activatedCompetitions, bool[] takenTasks)
        {
            bool[] triedTasks = new bool[parent.NTasks];
            Array.Copy(takenTasks, triedTasks, parent.NTasks);
            int[] fixedStartLen = new int[parent.NAgents];
            for (int i = 0; i < parent.NAgents; i++)
                fixedStartLen[i] = Allocation[i].Count;
            double[] cost = new double[parent.NAgents];
            while (!Array.TrueForAll(triedTasks, a => a))
            {
                int task = ChooseTaskByActivation(triedTasks, activatedCompetitions);
                triedTasks[task] = true;
                KeyValuePair<int, KeyValuePair<int, double>> minIncrease = new KeyValuePair<int, KeyValuePair<int, double>>(-1, new KeyValuePair<int, double>(-1, Double.PositiveInfinity));
                double allocated = Double.PositiveInfinity;
                for (int i = 0; i < parent.NAgents; i++)
                {
                    KeyValuePair<int, double> increase = FindBestInsertion(i, task, fixedStartLen[i], cost[i], activatedCompetitions);
                    if (increase.Key < 0)
                        continue;   //skip invalid isertions
                    if ((increase.Value < minIncrease.Value.Value) || ((increase.Value == minIncrease.Value.Value) && (FitTime(Allocation[i][increase.Key-1], task) < allocated)))
                    {
                        minIncrease = new KeyValuePair<int, KeyValuePair<int, double>>(i, increase);
                        allocated = FitTime(Allocation[i][increase.Key-1], task);
                    }
                }
                //do the best found change in the schedule if it is valid
                if (!Double.IsPositiveInfinity(minIncrease.Value.Value))
                {
                    FitTask(minIncrease.Key, task, minIncrease.Value.Key);
                    cost[minIncrease.Key] += minIncrease.Value.Value;
                    takenTasks[task] = true;
                }
            }
            return takenTasks;
        }

        private KeyValuePair<int, double> FindBestInsertion(int agent, int task, int fixStartLen, double initCost, List<double>[] activatedCompetitions)
        {
            KeyValuePair<int, double> best = new KeyValuePair<int, double>(-1, Double.PositiveInfinity);
            for (int pos = fixStartLen; pos < Allocation[agent].Count + 1; pos++)
            {
                double increase = CalculateCost(agent, task, pos, activatedCompetitions) - initCost;
                if (increase < best.Value)
                    best = new KeyValuePair<int, double>(pos, increase);
            }
            return best;
        }

        private double CalculateCost(int agent, int task, int pos, List<double>[] activatedCompetitions)
        {
            double cost = 0;
            var exectuion = CalcuateExecutionAfterFitted(agent, task, pos);
            if (exectuion == null)
                return Double.PositiveInfinity;
            foreach (var t in exectuion)
                cost += CostFunction(t.Value, activatedCompetitions[t.Key]);
            return cost;
        }

        private double CostFunction(double p, List<double> list)
        {
            //double cost = 0;
            //Zoli 22/07/2014
            //Changed cost function to tackle "rare samples": there is an initial time cost for each task
            double cost = p;
            int foundIndex = list.BinarySearch(p);
            if (foundIndex < 0)
                foundIndex = -foundIndex - 1;
            //now foundIndex will be the index of the first greater or equal element to p
            for (int i = 0; i < foundIndex; i++)
                cost += p - list[i];
            return cost;
        }

        private int ChooseTaskByActivation(bool[] triedTasks, List<double>[] activatedCompetitions)
        {
            KeyValuePair<int, double> best = new KeyValuePair<int,double>(-1, Double.PositiveInfinity);
            for(int i=0; i<parent.NTasks; i++)
                if (!triedTasks[i])
                {
                    double currentTime = Double.PositiveInfinity;
                    if (activatedCompetitions[i].Count > 0)
                        currentTime = activatedCompetitions[i][0];
                    if (currentTime <= best.Value)
                        best = new KeyValuePair<int,double>(i, currentTime);
                }
            return best.Key;
        }

        private bool[] InitAllocation(int[][] fixedTasks = null)
        {
            bool[] takenTasks = new bool[parent.NTasks];
            executionTimes = new double[parent.NTasks + parent.NAgents];
            Allocation = new List<int>[parent.NAgents];
            if (fixedTasks == null)
                fixedTasks = parent.FixedSteps;
            for (int i = 0; i < parent.NAgents; i++)
            {
                Allocation[i] = new List<int>();
                //first 'allocate' the agent
                Allocation[i].Add(parent.NTasks + i);
            }
            for (int i = 0; i < parent.NTasks; i++)
            {
                //fill task executions with infty
                executionTimes[i] = Double.PositiveInfinity;
            }
            for (int i = parent.NTasks; i < (parent.NTasks + parent.NAgents); i++)
            {
                //fill agent poitions with 0
                executionTimes[i] = 0;
            }
            for (int i = 0; i < fixedTasks.Length; i++)
            {
                for (int a = 0; a < parent.NAgents; a++)
                    if (fixedTasks[i][a] >= 0)
                    {
                        AppendTask(Allocation[a], fixedTasks[i][a]);
                        takenTasks[fixedTasks[i][a]] = true;
                    }
            }
            return takenTasks;
        }

        private bool AppendTask(List<int> alloc, int task)
        {
            double execTime = Math.Max(parent.DistanceTable[alloc.Last(), task] + executionTimes[alloc.Last()], parent.ActivationTime[task]);
            if (execTime > parent.BatteryCapacity)
                return false;
            else
            {   //add task
                alloc.Add(task);
                executionTimes[task] = execTime;
                return true;
            }
        }

        private double FitTime(int beforeTask, int fittedTask)
        {
            return Math.Max(executionTimes[beforeTask] + parent.DistanceTable[beforeTask, fittedTask], parent.ActivationTime[fittedTask]);
        }

        private bool IsTaskFits(List<int> alloc, int tofit, int location)
        {
            int afterTask = alloc[location];
            double fitTime = FitTime(alloc[location - 1], tofit);
            if (fitTime + parent.DistanceTable[tofit, afterTask] <= executionTimes[afterTask])
                return true;
            return false;
        }

        private int AlterPlanTowardsDominant(bool[] takenTasks)
        {
            int overallFitted = 0;
            bool[] checkedAgents = new bool[parent.NAgents];
            while (!Array.TrueForAll(checkedAgents, c => c))
            {
                int agent = GetRandomIndex(checkedAgents);
                checkedAgents[agent] = true;
                int fitted = 0;
                for (int i = 1; i < Allocation[agent].Count; i++)
                {
                    bool taskFitted = false;
                    int task = Allocation[agent][i];
                    if (executionTimes[task] > parent.ActivationTime[task])
                        continue;
                    foreach (int t in Allocation[agent].Skip(i + 1))
                        if (IsTaskFits(Allocation[agent], t, i))
                        {
                            fitted++;
                            taskFitted = true;
                            Allocation[agent].Remove(t);
                            FitTask(agent, t, i);
                            if (debug)
                                Console.WriteLine(String.Format("FIT a{0} t{1} @{2}", agent, t, i));
                            i--;    //try the fitted location again
                            break;
                        }
                    if (!taskFitted)
                    {
                        bool[] nonAvailable = new bool[parent.NTasks];
                        Array.Copy(takenTasks, nonAvailable, parent.NTasks);
                        while (!Array.TrueForAll(nonAvailable, c => c))
                        {
                            int t = GetRandomIndex(nonAvailable);
                            if (IsTaskFits(Allocation[agent], t, i))
                            {
                                fitted++;
                                takenTasks[t] = true;
                                FitTask(agent, t, i);
                                if (debug)
                                    Console.WriteLine(String.Format("FIT a{0} t{1} @{2}", agent, t, i));
                                i--;    //try the fitted location again
                                break;
                            }
                            nonAvailable[t] = true;
                        }
                    }
                }
                //try to fit available tasks at the end now
                //!TODO make the structure a method that calls the inside thing for all agents
                bool[] nonAvail = new bool[parent.NTasks];
                Array.Copy(takenTasks, nonAvail, parent.NTasks);
                while (!Array.TrueForAll(nonAvail, c => c))
                {
                    int t = GetRandomIndex(nonAvail);
                    if (AppendTask(Allocation[agent], t))
                    {
                        fitted++;
                        takenTasks[t] = true;
                        if (debug)
                            Console.WriteLine(String.Format("Append a{0} t{1}", agent, t));
                    }
                    nonAvail[t] = true;
                }
                overallFitted += fitted;
            }
            return overallFitted;
        }

        /// <summary>
        /// Fits a task in the given location of the schedule of the agent
        /// </summary>
        /// <param name="agent"></param>
        /// <param name="t"></param>
        /// <param name="i"></param>
        private void FitTask(int agent, int t, int i)
        {
            if (i < 1)
                throw new Exception("Cannot fit task. Indices are 1 based.");
            //change allocation
            Allocation[agent].Insert(i, t);
            //change execution times
            ChangeExecution(agent, i);
        }

        /// <summary>
        /// Recalculates the execution times of the given agent from slot i
        /// </summary>
        /// <param name="agent">index of agent</param>
        /// <param name="i">1-based index of first modified task</param>
        private void ChangeExecution(int agent, int i)
        {
            if (i < 1)
                throw new Exception("Cannot change execution. Indices are 1 based.");
            for (int j = i; j < Allocation[agent].Count; j++)
            {
                int currT = Allocation[agent][j];
                int prevT = Allocation[agent][j - 1];
                executionTimes[currT] = Math.Max(executionTimes[prevT] + parent.DistanceTable[prevT, currT], parent.ActivationTime[currT]);
            }
        }

        /// <summary>
        /// Calculates the execution times of the given agent with a task fitted at a position
        /// returns null if it doesn't fit in the battery time
        /// </summary>
        /// <param name="agent">index of agent</param>
        /// <param name="task">fitted task</param>
        /// <param name="i">index of fitted task</param>
        private Dictionary<int, double> CalcuateExecutionAfterFitted(int agent, int task, int i)
        {
            Dictionary<int, double> xt = new Dictionary<int, double>();
            int currentTask;
            int prevTask = Allocation[agent][0];
            double time = 0;
            for (int j = 1; j < Allocation[agent].Count + 1; j++)
            {
                if (j < i)
                {
                    currentTask = Allocation[agent][j];
                    xt.Add(currentTask, executionTimes[currentTask]);
                    time = executionTimes[currentTask];
                }
                else
                {
                    if (j == i)
                        currentTask = task;
                    else
                        currentTask = Allocation[agent][j - 1];
                    time = Math.Max(time + parent.DistanceTable[prevTask, currentTask], parent.ActivationTime[currentTask]);
                    xt.Add(currentTask, time);
                }
                prevTask = currentTask;
            }
            if (time > parent.BatteryCapacity)
                return null;
            return xt;
        }

        private int PlanExchangeToDominant()
        {
            int exchanged = 0;
            int randomSteps = 0;
            int[] taskPointer = new int[parent.NAgents];
            for (int i = 0; i < parent.NAgents; i++)
            {
                //default is 0 and step, to ensure it works even with empty allocations, and gets to the first task
                taskPointer[i] = 0;
                StepAgentPointer(i, taskPointer);
            }
            var graph = StepTaskPointers(taskPointer);
            bool[] agentOut = ExtractFinishedAgents(taskPointer);
            IEnumerable<int> circle = null;
            while (!Array.TrueForAll(agentOut, c => c))
            {
                bool[] deadEnd = new bool[parent.NAgents];
                Array.Copy(agentOut, deadEnd, parent.NAgents);
                while (!Array.TrueForAll(deadEnd, c => c))
                {
                    int agent = GetRandomIndex(deadEnd);
                    circle = DFSFindCircle(graph, new List<int>(), agent, deadEnd);
                    if (circle != null)
                    {
                        exchanged++;
                        ExchangeCircle(circle, taskPointer);
                        if (debug)
                            Console.WriteLine("Circle " + MyExtensionMethods.ToDelimitedString(circle, " "));
                        break;
                    }
                }
                if (circle == null)
                {
                    randomSteps++;
                    StepAgentPointer(GetRandomIndex(agentOut), taskPointer);
                }
                graph = StepTaskPointers(taskPointer);
                agentOut = ExtractFinishedAgents(taskPointer);
            }
            return exchanged;
        }

        private void ExchangeCircle(IEnumerable<int> circle, int[] taskPointer)
        {
            IEnumerable<int> replacedPart = null;
            foreach (int n in circle)
            {
                var newPart = Allocation[n].Skip(taskPointer[n]);
                if (replacedPart != null)
                {
                    ReplaceSchedule(n, replacedPart, taskPointer);
                }
                replacedPart = newPart;
            }
            ReplaceSchedule(circle.First(), replacedPart, taskPointer);
        }

        private void ReplaceSchedule(int agent, IEnumerable<int> partToReplace, int[] taskPointer)
        {
            Allocation[agent] = new List<int>(Allocation[agent].Take(taskPointer[agent]));
            Allocation[agent].AddRange(partToReplace);
            ChangeExecution(agent, taskPointer[agent]);
        }

        private Dictionary<int, Dictionary<int, double>> StepTaskPointers(int[] taskPointer)
        {
            Dictionary<int, Dictionary<int, double>> graph = new Dictionary<int, Dictionary<int, double>>(parent.NAgents);
            bool exit = false;
            while (!exit)
            {
                for (int agent = 0; agent < parent.NAgents; agent++)
                {
                    if (taskPointer[agent] >= 0)
                    {
                        graph.Add(agent, NonNegEdges(agent, taskPointer));
                        if (graph[agent].Count == 0)
                        {
                            //if no non-negative edges, step pointer
                            StepAgentPointer(agent, taskPointer);
                            if (debug)
                                Console.WriteLine(String.Format("STEP a{0} to {1}", agent, taskPointer[agent]));
                            exit = false;
                            graph.Clear();
                            break;
                        }
                    }
                    exit = true;
                }
            }
            return graph;
        }

        private IEnumerable<int> DFSFindCircle(Dictionary<int, Dictionary<int, double>> graph, List<int> path, int newNode, bool[] deadEnd)
        {
            if (path.Contains(newNode))
            {
                IEnumerable<int> circle = path.Skip(path.IndexOf(newNode));
                int prev = -1;
                foreach (int n in circle)
                {
                    if (prev > 0)
                    {
                        if (graph[prev][n] > 0)
                            return circle;
                    }
                    prev = n;
                }
                //Check closing edge
                if (graph[prev][circle.First()] > 0)
                    return circle;
                return null;
            }
            foreach (var e in graph[newNode])
            {
                if (!deadEnd[e.Key])
                {
                    path.Add(newNode);
                    IEnumerable<int> circle = DFSFindCircle(graph, path, e.Key, deadEnd);
                    if (circle != null)
                        return circle;
                    path.Remove(newNode);
                }
            }
            deadEnd[newNode] = true;
            return null;
        }

        private void StepAgentPointer(int agent, int[] taskPointer)
        {
            taskPointer[agent]++;
            if (taskPointer[agent] >= Allocation[agent].Count)
                taskPointer[agent] = -1;
        }

        private bool[] ExtractFinishedAgents(int[] taskPointer)
        {
            bool[] agentOut = new bool[taskPointer.Length];
            for (int i = 0; i < taskPointer.Length; i++)
                agentOut[i] = (taskPointer[i] < 0);
            return agentOut;
        }

        private Dictionary<int, double> NonNegEdges(int agent, int[] taskPointer)
        {
            Dictionary<int, double> edges = new Dictionary<int, double>(parent.NAgents - 1);
            for (int a = 0; a < parent.NAgents; a++)
            {
                if ((a == agent) || (taskPointer[a] < 0))
                    continue;
                double edgeVal = CheckTaskSwitch(agent, a, taskPointer[agent], taskPointer[a]);
                if (edgeVal >= 0)
                    edges.Add(a, edgeVal);
            }
            return edges;
        }

        private double CheckTaskSwitch(int agent, int otherAgent, int location, int otherLocation)
        {
            int task = Allocation[agent][location];
            double otherTime = FitTime(Allocation[otherAgent][otherLocation - 1], task);
            return executionTimes[task] - otherTime;
        }

        private int ChangeTowardsDominant(bool[] takenTasks)
        {
            int iterNum = 0;
            while (true)
            {
                iterNum++;
                int alterred = AlterPlanTowardsDominant(takenTasks);
                if ((iterNum > 1) && (alterred == 0))
                    break;
                iterNum++;
                int exchanged = PlanExchangeToDominant();
                if (exchanged == 0)
                    break;
            }
            return iterNum;
        }

        private int CountCriticalTasks(int agent, double deltaT = 0.0)
        {
            int numCritical = 0;
            List<int> alloc = Allocation[agent];
            for (int i = 1; i < alloc.Count; i++)
            {
                int task = alloc[i];
                //task is critical if activation time is 0 (task already exists) or if executed later than the activation time
                if ((parent.ActivationTime[task] == 0) || (executionTimes[task] > (parent.ActivationTime[task] + deltaT)))
                    numCritical++;
                else
                    break;
            }
            return numCritical;
        }

        private PointF GetDirection(int agent, PointF agentPos)
        {
            if (Allocation[agent].Count <= 2)
                return new PointF(0, 0);
            PointF ltask = parent.Tasks[Allocation[agent][1]];
            float dx = ltask.X - agentPos.X;
            float dy = ltask.Y - agentPos.Y;
            float len = (float)Math.Sqrt(dx * dx + dy * dy);
            if(len==0)
                return new PointF(0,0);
            dx /= len;
            dy /= len;
            return new PointF(dx, dy);
        }

        public double[] GetStatisticsData()
        {
            int selfData = 2;
            int childrenData = 2;
            if(IsBottomLevel)
                childrenData = 0;
            double[] data = new double[selfData + childrenData];
            //Self
            data[0] = Utility;
            data[1] = IsGreedy?1.0:0.0;
            //children
            if(IsBottomLevel)
                return data;
            data[selfData + 0] = children.StdDev2;
            data[selfData + 1] = children.Average;
            return data;
        }

        public Decision GetDecisionData(PointF[] agentPos)
        {
            double[] dirX = new double[parent.NAgents];
            double[] dirY = new double[parent.NAgents];
            int[] critical = new int[parent.NAgents];
            for (int i = 0; i < parent.NAgents; i++)
            {
                PointF dir = GetDirection(i, agentPos[i]);
                dirX[i] = dir.X;
                dirY[i] = dir.Y;
                critical[i] = CountCriticalTasks(i);
            }
            return new Decision(dirX, dirY, FirstAllocated, critical);
        }

        public override void AggregateDecision(bool isTop = true)
        {
            if (isTop)
                //refresh all statistical info
                this.Initialise();
            Group.UploadDecisionRequest(this);
            if (!IsBottomLevel)
                foreach (var ch in children)
                    ch.AggregateDecision(false);
        }

        public override ITreeNode BestChild { get { return children.First(); } }

        public override ITreeNode Parent { get { return parent; } }

        public override ITreeNode MakeChild(int tries = 0)
        {
            if (IsBottomLevel || IsClosed)
                return null;
            if (tries < 1)
                tries = triesBeforeClosing;
            double[,] distT = null;
            List<int>[] constraints = null;
            System.Drawing.PointF[] tasks = null;
            GroupData g = Group.NextGroup;
            bool childAdded = false;
            for (int i = 0; i < tries; i++)
                if (g.GetProblem(children.Count, parent.Tasks, out distT, out constraints, out tasks))
                {
                    childAdded = true;
                    break;
                }
            if (!childAdded)
            {
                IsClosed = true;
                return null;
            }
            // calculate activation times for the child problem
            var act = ExtractActivation(constraints);
            GroupProblem newProblem = new GroupProblem(children.Count, distT, tasks, act, g.BatteryCapacity, this, g);
            children.Add(newProblem);
            return newProblem;
        }

        public override int Expand(int nChildren)
        {
            var added = MakeChildren(nChildren);
            foreach (GroupProblem child in added)
                child.Initialise();
            return added.Count();
        }

        public IEnumerable<GroupProblem> MakeChildren(int nChildren)
        {
            if (IsBottomLevel || IsClosed)
                return Enumerable.Empty<GroupProblem>();
            List<GroupProblem> ch = new List<GroupProblem>(nChildren);
            for (int i = 0; i < nChildren; i++)
            {
                double[,] distT = null;
                List<int>[] constraints = null;
                System.Drawing.PointF[] tasks = null;
                GroupData g = Group.NextGroup;
                bool childAdded = false;
                for (int _ = 0; _ < triesBeforeClosing; _++)
                    if (g.GetProblem(children.Count + i, parent.Tasks, out distT, out constraints, out tasks))
                    {
                        childAdded = true;
                        break;
                    }
                if (!childAdded)
                {
                    IsClosed = true;
                    break;
                }
                // calculate activation times for the child problem
                var act = ExtractActivation(constraints);
                GroupProblem newProblem = new GroupProblem(children.Count + i, distT, tasks, act, g.BatteryCapacity, this, g);
                ch.Add(newProblem);
            }
            return children.AddRange(ch);
        }

        private double value;
        public override double Value
        {
            get
            {
                if (IsBottomLevel || !IsInit)
                    return 0;
                return value;
            }
        }

        public override double Utility
        {
            get
            {
                if (IsBottomLevel || !IsInit)
                    return utility;
                return children.Average + utility;
            }
        }

        /// <summary>
        /// Called with no parameter, it will update the value, called with a number of samples, it will only recalculate if that number is not what is already calculated with
        /// </summary>
        /// <param name="nsamp">adjust number of samples optional parameter</param>
        public void UpdateValue(int nsamp = 0)
        {
            if (nsamp <= 0)
                nsamp = nSamples;
            else if (nSamples == nsamp)
                return;
            else
                nSamples = nsamp;

            bool isBestSolution;
            //get delta mu
            double meanDiff = parent.GetMeanDiff(this, out isBestSolution);

            //calculate value

            //extra value for rescue noise
            double extra_value = 0.0;
            if (isBestSolution)
            {
                List<Decision> dlist = new List<Decision>(ChildrenCount);
                foreach (var ch in children)
                    dlist.Add(ch.GetSolutionData());
                double factor = Decision.DirectionErrorLossFactor(dlist);
                extra_value = -MCSearch.TimeStep * factor * TreeStructure.lambda * (double)nSamples / (((double)ChildrenCount + (double)nSamples) * (double)ChildrenCount);
            }

            //sigma is the std dev of the change of the mu after the sampling
            double sigma = children.StdErr * (double)nSamples / (double)(children.Count + nSamples) * Math.Sqrt(1.0 / (double)children.Count + 1.0 / (double)nSamples);
            value = sigma * VoIApprox(meanDiff / sigma) + extra_value;
        }

        /// <summary>
        /// Calculates an approximate value for phi(x)-Phi(-x)*x where phi() is the PDF, and Phi() is the CDF of the normal distribution
        /// </summary>
        /// <param name="x">POSITIVE delta mu / sigma</param>
        /// <returns>value approximation</returns>
        private static double VoIApprox(double x)
        {
            // constants
            double a1 = 0.254829592;
            double a2 = -0.284496736;
            double a3 = 1.421413741;
            double a4 = -1.453152027;
            double a5 = 1.061405429;
            double p = 0.3275911;

            x = x / Math.Sqrt(2.0);

            double t = 1.0 / (1.0 + p * x);
            double AT = (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t;
            double y = (1 / Math.Sqrt(2 * Math.PI) - x / Math.Sqrt(2.0) * AT) * Math.Exp(-x * x);

            return y;
        }

        public static void TestVoIApprox()
        {
            double[] x =
            {
                0.1,
                0.8,
                2,
                5,
                8,
                12
            };
            // Output computed by Mathematica
            // N[Table[-CDF[NormalDistribution[0, 1], -x]*x + PDF[NormalDistribution[0, 1], x], {x, {0.1, 0.8, 2, 5, 8, 12}}]]
            double[] y =
            {
                0.350935,
                0.120207,
                0.00849070261683,
                5.34616553383E-8,
                7.55026241195E-17,
                1.46052011698E-34
            };

            double maxError = 0.0;
            double maxRelError = 0.0;
            for (int i = 0; i < x.Length; ++i)
            {
                double appr = VoIApprox(x[i]);
                Console.WriteLine(appr);
                double error = Math.Abs(y[i] - appr);
                double relError = Math.Abs((y[i] - appr) / y[i]);
                if (error > maxError)
                    maxError = error;
                if (relError > maxRelError)
                    maxRelError = relError;
            }

            Console.WriteLine("Maximum error: {0}", maxError);
            Console.WriteLine("Maximum relative error: {0}", maxRelError);
        }

        /// <summary>
        /// Finds the best increase of expanding this or any nodes lower among the tree
        /// </summary>
        /// <param name="samples">Number of samples to be taken</param>
        /// <returns>The best node to expand and its expected value increase to created node ratio</returns>
        public override double Increase(int nSamples, out ITreeNode best, bool topNode = false)
        {
            KeyValuePair<ITreeNode, double> bestFound;
            // increase from expanding this node
            if ((topNode == false) && (ExpansionPolicy == SolutionExpansionPolicyType.MixedUCBVoI))
                bestFound = OwnIncreaseVoI(nSamples);
            else
                bestFound = OwnIncreaseUCB(nSamples);
            // increase from children
            foreach (ITreeNode child in Children)
            {
                ITreeNode node;
                double ei = child.Increase(nSamples, out node);
                // effect on this average
                ei /= ChildrenCount;
                if (ei > bestFound.Value)
                    bestFound = new KeyValuePair<ITreeNode, double>(node, ei);
            }
            best = bestFound.Key;
            return bestFound.Value;
        }

        /*private KeyValuePair<ITreeNode, double> OwnIncreaseVoI(int nSamples)
        {
            if (IsBottomLevel || IsClosed)
                return new KeyValuePair<ITreeNode, double>(this, 0);
            double inc = (double)nSamples / (ChildrenCount + nSamples) * value;
            double ecount = Group.NextGroup.ExpectedChildrenOfProblem * nSamples;
            KeyValuePair<ITreeNode, double> bestFound = new KeyValuePair<ITreeNode, double>(this, inc / ecount);
            return bestFound;
        }*/

        public override double OwnIncrease(int nSamples)
        {
            KeyValuePair<ITreeNode, double> bestFound;
            if (ExpansionPolicy == SolutionExpansionPolicyType.MixedUCBVoI)
                bestFound = OwnIncreaseVoI(nSamples);
            else
                bestFound = OwnIncreaseUCB(nSamples);
            return bestFound.Value;
        }

        private KeyValuePair<ITreeNode, double> OwnIncreaseVoI(int nSamples)
        {
            if (IsBottomLevel || IsClosed)
                return new KeyValuePair<ITreeNode, double>(this, 0);
            //to update number of samples
            UpdateValue(nSamples);
            double inc = value;
            double ecount = Group.NextGroup.ExpectedChildrenOfProblem * nSamples;
            KeyValuePair<ITreeNode, double> bestFound = new KeyValuePair<ITreeNode, double>(this, inc / ecount);
            return bestFound;
        }

        private KeyValuePair<ITreeNode, double> OwnIncreaseUCB(int nSamples)
        {
            if (IsBottomLevel || IsClosed)
                return new KeyValuePair<ITreeNode, double>(this, 0);
            double inc = ConfidenceInterval * children.StdErr * Math.Sqrt((double)ChildrenCount / (ChildrenCount + nSamples));
            double ecount = Group.NextGroup.ExpectedChildrenOfProblem * nSamples;
            KeyValuePair<ITreeNode, double> bestFound = new KeyValuePair<ITreeNode, double>(this, inc / ecount);
            return bestFound;
        }

        public int GetHashCodeOld()
        {
            int seed = 23;
            int[] primes = { 53, 97, 193 };
            for (int a = 0; a < parent.NAgents; a++)
            {
                foreach (int t in Allocation[a])
                    seed += t * primes[2];
                seed += Allocation[a].Count * primes[0];
                seed *= primes[1];
            }
            return seed;
        }
        public override int GetHashCode()
        {
            if (hashCode != 0)
                return hashCode;
            int seed = 0;
            for (int a = 0; a < parent.NAgents; a++)
            {
                if (a == 0)
                    seed = Allocation[0].Count;
                else
                    seed = unchecked(seed * 53 + Allocation[a].Count);
                foreach (int t in Allocation[a])
                    seed = unchecked(seed * 193 + t);
            }
            hashCode = seed;
            return seed;
        }

        public List<List<int>> ToSolution()
        {
            List<List<int>> sol = new List<List<int>>(Allocation.Length);
            for (int i = 0; i < Allocation.Length; i++)
                sol.Add(new List<int>(Allocation[i].Skip(1)));
            return sol;
        }

        public override string ToString()
        {
            string toto = "[[";
            toto += Allocation.ToDelimitedString(a => a.Skip(1).ToDelimitedString(", "), "], [");
            toto += "]]";
            return String.Format("S{3} U:{0,8:F2}  V:{1,8:F2}  I:{4,8:E2}   {2:D}", Utility, Value, GetHashCode()/*toto*/, IsGreedy ? "G" : " ", OwnIncreaseVoI(10).Value);
        }

        public override void FillStatistics() { if (!IsBottomLevel) children.FillInitStatistics(); }
        protected override void RecalculateStatistics() { if (!IsBottomLevel) children.RecalculateStatistics(); }

        protected override void UpdateFromParent()
        {
            base.UpdateFromParent();
            if (!IsBottomLevel)
                UpdateValue();
        }

        internal double GreedyUtility()
        {
            if (IsBottomLevel || ChildrenCount == 0)
                return utility;
            else
            {
                double util = 0;
                foreach (var ch in children)
                    util += ch.GreedyUtility();
                return (util / ChildrenCount) + utility;
            }
        }

        public override void Validate()
        {
            if (IsInvalid)
            {
                if (Parent != null)
                    Parent.Validate();
                for (int agent = 0; agent < parent.NAgents; agent++)
                    ChangeExecution(agent, 1);
                IsInvalid = false;
                if (IsBottomLevel)
                {
                    utility = Group.ExtractUtility(this);
                    UpdateParent();
                }
            }
        }

        public double[] GetActivation(List<int>[] constraints)
        {
            if (IsInvalid)
                Validate();
            return ExtractActivation(constraints);
        }

        public override void InvalidateChildrenStatistics()
        {
            if (!IsBottomLevel)
                children.InvalidateStatistics();
        }

        public enum SolutionExpansionPolicyType
        {
            UCBOnly,
            MixedUCBVoI
        };
        public enum NewSolutionPolicyType
        {
            FlatRandom,
            GuidedRandom,
            Greedy,
            Default
        };

        public override void ClearChildren()
        {
            children.Clear();
        }
    }
}
