﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeSearchSolver
{
    public class GroupProblem : AbstractTreeNode
    {
        public readonly GroupSoltion parent;
        public override GroupData Group { get { return proxy.g; } }
        private GroupProxy proxy;
        private readonly int problemID;
        //fixed allocation, size [nSteps][NAgents]
        public readonly int[][] FixedSteps;
        public double[,] DistanceTable { get; private set; }
        public readonly System.Drawing.PointF[] Tasks;
        public int NTasks;
        public int NAgents;
        public double[] ActivationTime { get; private set; }
        public readonly double BatteryCapacity;
        private SolutionList children;
        public override IEnumerable<AbstractTreeNode> Children { get { return children.AsEnumerable(); } }
        public override void UpdateChildrenStatistics(ITreeNode node) { children.Update(node as GroupSoltion); }
        public override int ChildrenCount { get { return children.Count; } }
        public override bool IsBottomLevel { get { return false; } }
        public override bool IsGreedy { get { return false; } }

        public GroupProblem(int id, double[,] dist, System.Drawing.PointF[] tasks, double[] act, double batt, GroupSoltion parentSol = null, GroupData gdata = null, GroupProblem oldRoot = null, int fixedExplorationSteps = 0)
            : base()
        {
            problemID = id;
            parent = parentSol;
            if ((gdata == null) && (parent != null))
                proxy = parent.Group.NextGroup.Proxy;
            else if (gdata != null)
                proxy = gdata.Proxy;
            children = new SolutionList(this);
            NTasks = dist.GetLength(1);
            NAgents = dist.GetLength(0) - NTasks;
            if (NAgents <= 0)
                throw new Exception("Wrong distance table format in GroupProblem constructor!");
            if (act.Count() != NTasks)
                throw new Exception("Wrong activation table format in GroupProblem constructor!");
            DistanceTable = dist;
            Tasks = tasks;
            ActivationTime = act;
            BatteryCapacity = batt;
            FixedSteps = GetFixedSteps(fixedExplorationSteps, oldRoot);
            if (fixedExplorationSteps < 0)
            {   //if fixed solution
                if (oldRoot == null)
                    AddGreedy();
                else
                    MakeCopyChild(oldRoot.BestChild as GroupSoltion);
            }
            else
            {
                if (oldRoot != null)
                {
                    //copy best old solutions
                    int nToCopy = Math.Min(Group.InitChildrenOfProblem + 1, oldRoot.ChildrenCount);
                    for (int i = 0; i < nToCopy; i++)
                        MakeCopyChild(oldRoot.children[i]);
                }

                if (Group.InitChildrenOfProblem > 0)
                {   //add new solutions
                    AddGreedy();
                    MakeChildren(Group.InitChildrenOfProblem);
                }
            }
        }

        public void InitPopulateMCTS(GroupProblem oldRoot = null)
        {
            if (oldRoot != null)
            {
                //copy best old solutions
                int nToCopy = Math.Min(Group.InitChildrenOfProblem + 1, oldRoot.ChildrenCount);
                for (int i = 0; i < nToCopy; i++)
                    MakeCopyChild(oldRoot.children[i]);
            }
            AddGreedy();
            MakeChildren(Group.InitChildrenOfProblem);
        }

        public void InitPopulateMCTSSingle(GroupProblem oldRoot = null)
        {
            if (oldRoot == null)
                AddGreedy();
            else
                MakeCopyChild(oldRoot.BestChild as GroupSoltion);
        }

        public void InitPopulateClear()
        {
            ClearChildren();
        }

        /// <summary>
        /// Returns the fixed part of the schedule. Note that a child is added if fixedExplorationSteps > 0.
        /// </summary>
        /// <param name="fixedExplorationSteps">Number of steps included in the returned fix schedule.</param>
        /// <param name="oldRoot">The root (problem) node of the old tree. The schedule is either taken from its best child or from a greedy solution when null.</param>
        /// <returns>The value of FixedSteps, size [fixedExplorationSteps][NAgents]. Contains -1 at unallocated locations.</returns>
        private int[][] GetFixedSteps(int fixedExplorationSteps, GroupProblem oldRoot = null)
        {
            int[][] emptyAlloc = new int[0][];
            if (fixedExplorationSteps <= 0)
                return emptyAlloc;
            GroupSoltion best;
            if (oldRoot == null)
                best = AddGreedy(emptyAlloc);
            else if (oldRoot.ChildrenCount == 0)
                return emptyAlloc;
            else
                best = MakeCopyChild(oldRoot.children.First(), emptyAlloc);
            return best.GetInitialAllocation(fixedExplorationSteps);
        }

        public override ITreeNode BestChild { get { return children.First(); } }

        public override ITreeNode Parent { get { return parent; } }

        public override ITreeNode MakeChild(int tries = 0)
        {
            if (IsClosed)
                return null;
            if (tries < 1)
                tries = triesBeforeClosing;
            GroupSoltion child = new GroupSoltion(this);
            bool added = false;
            for (int i = 0; i < tries; i++)
            {
                if (children.Add(child))
                {
                    added = true;
                    break;
                }
                child = new GroupSoltion(this);
            }
            if (!added)
            {
                IsClosed = true;
                return null;
            }
            return child;
        }

        public GroupSoltion MakeCopyChild(GroupSoltion original, int[][] fixedTasks = null)
        {
            GroupSoltion child = new GroupSoltion(this, original, fixedTasks);
            children.Add(child);
            return child;
        }

        public override int Expand(int nChildren)
        {
            var added = MakeChildren(nChildren);
            foreach (GroupSoltion child in added)
            {
                child.Initialise();
                children.Update(child);
            }
            return added.Count();
        }

        public IEnumerable<GroupSoltion> MakeChildren(int nChildren)
        {
            if (IsClosed || nChildren == 0)
                return Enumerable.Empty<GroupSoltion>();
            List<GroupSoltion> ch = new List<GroupSoltion>(nChildren);
            for (int i = 0; i < nChildren; i++)
            {
                ch.Add(new GroupSoltion(this));
            }
            var added = children.AddRange(ch);
            if (added.FirstOrDefault() == null)
            {
                var child = (GroupSoltion)MakeChild();
                //if didn't manage to add children, try to force one and close node if unsuccessful
                if (child != null)
                    added.Add(child);
            }
            return added;
        }

        public GroupSoltion AddGreedy(int[][] fixedTasks = null)
        {
            GroupSoltion child = new GroupSoltion(this, GroupSoltion.NewSolutionPolicyType.Greedy, fixedTasks);
            children.Add(child);
            return child;
        }

        /// <summary>
        /// Creates greedy solution consideing the feedback from a lower level.
        /// </summary>
        /// <param name="activatedCompetitions">feedback of sorted execution times by exploration tasks</param>
        /// <returns>the resulting solution</returns>
        public GroupSoltion AddByActivation(List<double>[] activatedCompetitions)
        {
            GroupSoltion child = new GroupSoltion(this, activatedCompetitions);
            children.Add(child);
            return child;
        }

        public override double Value
        {
            get
            {
                return children.PercentileMax;
            }
        }

        public override double Utility
        {
            get { return Value; }
        }

        public override double Increase(int nSamples, out ITreeNode best, bool topNode = false)
        {
            // increase from expanding this node
            KeyValuePair<ITreeNode, double> bestFound = ownIncrease(nSamples);
            // check increase from expanding best child
            ITreeNode node;
            double ei = children.BestChild().Increase(nSamples, out node);
            if (ei > bestFound.Value)
                bestFound = new KeyValuePair<ITreeNode, double>(node, ei);
            best = bestFound.Key;
            return bestFound.Value;
        }

        /// <summary>
        /// Estimates utility increase at the current node by expanding it with children
        /// </summary>
        /// <param name="nSamples">number of samples to be taken</param>
        /// <returns>the best node to increase and its utility increase</returns>
        private KeyValuePair<ITreeNode, double> ownIncrease(int nSamples)
        {
            if (IsClosed || (TreeStructure.DisableBottomExpand && parent != null))
                return new KeyValuePair<ITreeNode, double>(this, 0);
            double relMax = children.PercentileMax - children.PercentileBoundary;
            double relAvg = children.PercentileAverage - children.PercentileBoundary;
            double expEstimation, randomEstimation;
            //if greedy solution is the best, use exponential assumption only, as the PercentileMax is not a result of random sampling
            if (children.IsGreedyDominant)
            {
                //estimation expecting an exponential distribution
                expEstimation = Math.Exp(-relMax / relAvg) * relAvg;
                randomEstimation = 0;
            }
            else
            {
                //estimation expecting an exponential distribution decreased with a factor of e^(1-sqrt(ChildrenCount *percentile)
                //expEstimation = Math.Exp(-relMax / relAvg + 1 - Math.Sqrt(ChildrenCount * children.Percentile)) * relAvg;
                //estimation calculating probability of increase by existing children only, expected increase is still from exponential
                //decreased with a factor of 1-e^(1-sqrt(ChildrenCount *percentile)
                //randomEstimation = relAvg / (ChildrenCount + nSamples) * (1 - Math.Exp(1 - Math.Sqrt(ChildrenCount * children.Percentile)));
                expEstimation = 0;
                randomEstimation = relAvg / (ChildrenCount + nSamples);
            }
            if (expEstimation < 0)
                expEstimation = 0;
            if (randomEstimation < 0)
                randomEstimation = 0;
            double inc = children.Percentile * nSamples * (expEstimation + randomEstimation);
            double ecount = Group.ExpectedChildrenOfSolution * nSamples;
            double bestValue = inc / ecount;
            if (Double.IsNaN(bestValue) || Double.IsInfinity(bestValue) || (bestValue < 0))
                bestValue = 0;
            KeyValuePair<ITreeNode, double> bestFound = new KeyValuePair<ITreeNode, double>(this, bestValue);
            return bestFound;
        }

        public override double OwnIncrease(int nSamples)
        {
            return ownIncrease(nSamples).Value;
        }

        internal List<int>[] ReverseConstraints(List<int>[] constraints)
        {
            List<int>[] rev = new List<int>[NTasks];
            for (int i = 0; i < NTasks; i++)
                rev[i] = new List<int>();
            for (int i = 0; i < constraints.Length; i++)
                foreach (int t in constraints[i])
                    rev[t].Add(i);
            return rev;
        }

        public override void AggregateDecision(bool isTop = true)
        {
            if (isTop)
                //refresh all statistical info
                this.Initialise(3);
            BestChild.AggregateDecision(false);
        }

        public override string ToString()
        {
            return String.Format("P V:{0,8:F2}  M:{3,8:F2}  A:{4,8:F2}  B:{5,8:F2}  I:{2,8:E2}   {1:D}",
                Value, GetHashCode(), OwnIncrease(10), children.PercentileMax, children.PercentileAverage, children.PercentileBoundary);
        }

        internal double GetMeanDiff(GroupSoltion groupSoltion, out bool isBest)
        {
            return children.GetMeanDiff(groupSoltion, out isBest);
        }

        public override void FillStatistics() { children.FillInitStatistics(); }
        protected override void RecalculateStatistics() { children.RecalculateStatistics(); }

        public double GreedyUtility()
        {
            foreach (var ch in children)
                if (ch.IsGreedy)
                    return ch.GreedyUtility();
            return 0;
        }
        public double GreedyStdErr()
        {
            foreach (var ch in children)
                if (ch.IsGreedy)
                    return ch.StdErr;
            return 0.0;
        }

        public Decision GetSolutionData()
        {
            return children.BestSolutionData(Group.Agents);
        }

        public double[] GetStatisticsData()
        {
            double[] data = new double[6];
            data[0] = Utility;
            data[1] = GreedyUtility();
            data[2] = children.PercentileAverage;
            int betterThanGreedy = 0;
            while (!children[betterThanGreedy].IsGreedy) betterThanGreedy++;
            data[3] = (double)betterThanGreedy / ((double)ChildrenCount-1);
            data[4] = ChildrenCount;
            data[5] = NTasks;
            return data;
        }

        public override void Validate()
        {
            if (IsInvalid)
            {
                if (!IsRoot)
                    Parent.Validate();
                System.Drawing.PointF[] locs;
                double[,] distTable;
                List<int>[] c=null;
                if (IsRoot)
                {
                    Group.GetProblem(out distTable, out locs);
                    ActivationTime = new double[locs.Length];
                }
                else
                {
                    Group.GetProblem(problemID, null, out distTable, out c, out locs);
                    ActivationTime = parent.GetActivation(c);
                }
                DistanceTable = distTable;
                IsInvalid = false;
            }
        }

        public override void InvalidateChildrenStatistics()
        {
            children.InvalidateStatistics();
        }

        public int FindTask(System.Drawing.PointF taskLoc)
        {
            for (int i = 0; i < Tasks.Length; i++)
                if (taskLoc == Tasks[i])
                    return i;
            return -1;
        }

        internal void RemoveNonBestChildren(int nchildrenToKeep = 1)
        {
            if (nchildrenToKeep < 1)
                nchildrenToKeep = 1;
            children.RemoveNonBestChildren(nchildrenToKeep);
        }

        public override void ClearChildren()
        {
            children.Clear();
            ResetClosed();
        }
    }
}
