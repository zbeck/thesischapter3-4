﻿using AgentDraw;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeSearchSolver
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Nothing to do here.");
            Console.WriteLine("Maybe just to test the VoI approximation...");
            GroupSoltion.TestVoIApprox();
            Console.ReadKey();
        }
    }
}
