﻿namespace AgentDraw
{
    partial class SamplerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.openBitmapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sampleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.setNumberToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newSolutionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oldSolutionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.problemDescriptionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.factorGraphDescriptionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.revertStructureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setTravelSpeedsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.checkBoxRadius = new System.Windows.Forms.CheckBox();
            this.checkBoxName = new System.Windows.Forms.CheckBox();
            this.trackBarRadius0 = new System.Windows.Forms.TrackBar();
            this.fileNameTextBox = new System.Windows.Forms.TextBox();
            this.nofSlotsLabel = new System.Windows.Forms.Label();
            this.nofSlotsBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.checkBoxConnections = new System.Windows.Forms.CheckBox();
            this.checkBoxActivation = new System.Windows.Forms.CheckBox();
            this.labelRadius = new System.Windows.Forms.Label();
            this.placementView = new AgentDraw.PlacementView();
            this.trackBarRadius1 = new System.Windows.Forms.TrackBar();
            this.solveLowLevelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarRadius0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarRadius1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openBitmapToolStripMenuItem,
            this.sampleToolStripMenuItem,
            this.runToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.clearToolStripMenuItem,
            this.loadToolStripMenuItem,
            this.generateToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(766, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // openBitmapToolStripMenuItem
            // 
            this.openBitmapToolStripMenuItem.Name = "openBitmapToolStripMenuItem";
            this.openBitmapToolStripMenuItem.Size = new System.Drawing.Size(89, 20);
            this.openBitmapToolStripMenuItem.Text = "Open Bitmap";
            this.openBitmapToolStripMenuItem.Click += new System.EventHandler(this.openBitmapToolStripMenuItem_Click);
            // 
            // sampleToolStripMenuItem
            // 
            this.sampleToolStripMenuItem.Name = "sampleToolStripMenuItem";
            this.sampleToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.sampleToolStripMenuItem.Text = "Sample";
            this.sampleToolStripMenuItem.Click += new System.EventHandler(this.sampleToolStripMenuItem_Click);
            // 
            // runToolStripMenuItem
            // 
            this.runToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.runToolStripMenuItem1,
            this.setNumberToolStripMenuItem,
            this.toolStripMenuItem2});
            this.runToolStripMenuItem.Name = "runToolStripMenuItem";
            this.runToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.runToolStripMenuItem.Text = "Run";
            // 
            // runToolStripMenuItem1
            // 
            this.runToolStripMenuItem1.Name = "runToolStripMenuItem1";
            this.runToolStripMenuItem1.Size = new System.Drawing.Size(138, 22);
            this.runToolStripMenuItem1.Text = "Run";
            this.runToolStripMenuItem1.Click += new System.EventHandler(this.runToolStripMenuItem1_Click);
            // 
            // setNumberToolStripMenuItem
            // 
            this.setNumberToolStripMenuItem.Name = "setNumberToolStripMenuItem";
            this.setNumberToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.setNumberToolStripMenuItem.Text = "Set Number";
            this.setNumberToolStripMenuItem.Click += new System.EventHandler(this.setNumberToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(138, 22);
            this.toolStripMenuItem2.Text = "NofSamples";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            this.clearToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.clearToolStripMenuItem.Text = "Clear";
            this.clearToolStripMenuItem.Click += new System.EventHandler(this.clearToolStripMenuItem_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modelToolStripMenuItem,
            this.newSolutionToolStripMenuItem,
            this.oldSolutionToolStripMenuItem});
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.loadToolStripMenuItem.Text = "Load";
            // 
            // modelToolStripMenuItem
            // 
            this.modelToolStripMenuItem.Name = "modelToolStripMenuItem";
            this.modelToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.modelToolStripMenuItem.Text = "Model";
            this.modelToolStripMenuItem.Click += new System.EventHandler(this.modelToolStripMenuItem_Click);
            // 
            // newSolutionToolStripMenuItem
            // 
            this.newSolutionToolStripMenuItem.Name = "newSolutionToolStripMenuItem";
            this.newSolutionToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.newSolutionToolStripMenuItem.Text = "New Solution";
            this.newSolutionToolStripMenuItem.Click += new System.EventHandler(this.newSolutionToolStripMenuItem_Click);
            // 
            // oldSolutionToolStripMenuItem
            // 
            this.oldSolutionToolStripMenuItem.Name = "oldSolutionToolStripMenuItem";
            this.oldSolutionToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.oldSolutionToolStripMenuItem.Text = "Old Solution";
            this.oldSolutionToolStripMenuItem.Click += new System.EventHandler(this.oldSolutionToolStripMenuItem_Click);
            // 
            // generateToolStripMenuItem
            // 
            this.generateToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.problemDescriptionToolStripMenuItem,
            this.factorGraphDescriptionToolStripMenuItem,
            this.revertStructureToolStripMenuItem,
            this.setTravelSpeedsToolStripMenuItem,
            this.solveLowLevelToolStripMenuItem});
            this.generateToolStripMenuItem.Name = "generateToolStripMenuItem";
            this.generateToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.generateToolStripMenuItem.Text = "Model";
            // 
            // problemDescriptionToolStripMenuItem
            // 
            this.problemDescriptionToolStripMenuItem.Name = "problemDescriptionToolStripMenuItem";
            this.problemDescriptionToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.problemDescriptionToolStripMenuItem.Text = "Generate Problem Description";
            this.problemDescriptionToolStripMenuItem.Click += new System.EventHandler(this.problemDescriptionToolStripMenuItem_Click);
            // 
            // factorGraphDescriptionToolStripMenuItem
            // 
            this.factorGraphDescriptionToolStripMenuItem.Name = "factorGraphDescriptionToolStripMenuItem";
            this.factorGraphDescriptionToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.factorGraphDescriptionToolStripMenuItem.Text = "Factor Generate Graph Description";
            this.factorGraphDescriptionToolStripMenuItem.Click += new System.EventHandler(this.factorGraphDescriptionToolStripMenuItem_Click);
            // 
            // revertStructureToolStripMenuItem
            // 
            this.revertStructureToolStripMenuItem.Name = "revertStructureToolStripMenuItem";
            this.revertStructureToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.revertStructureToolStripMenuItem.Text = "Revert Structure";
            this.revertStructureToolStripMenuItem.Click += new System.EventHandler(this.revertStructureToolStripMenuItem_Click);
            // 
            // setTravelSpeedsToolStripMenuItem
            // 
            this.setTravelSpeedsToolStripMenuItem.Name = "setTravelSpeedsToolStripMenuItem";
            this.setTravelSpeedsToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.setTravelSpeedsToolStripMenuItem.Text = "Set Travel Speeds";
            this.setTravelSpeedsToolStripMenuItem.Click += new System.EventHandler(this.setTravelSpeedsToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 343);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(766, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(101, 17);
            this.statusLabel.Text = "No image loaded.";
            // 
            // checkBoxRadius
            // 
            this.checkBoxRadius.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxRadius.AutoSize = true;
            this.checkBoxRadius.Location = new System.Drawing.Point(669, 323);
            this.checkBoxRadius.Name = "checkBoxRadius";
            this.checkBoxRadius.Size = new System.Drawing.Size(40, 17);
            this.checkBoxRadius.TabIndex = 25;
            this.checkBoxRadius.Text = "R=";
            this.checkBoxRadius.UseVisualStyleBackColor = true;
            this.checkBoxRadius.CheckedChanged += new System.EventHandler(this.checkBoxRadius_CheckedChanged);
            // 
            // checkBoxName
            // 
            this.checkBoxName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxName.AutoSize = true;
            this.checkBoxName.Location = new System.Drawing.Point(669, 74);
            this.checkBoxName.Name = "checkBoxName";
            this.checkBoxName.Size = new System.Drawing.Size(59, 17);
            this.checkBoxName.TabIndex = 24;
            this.checkBoxName.Text = "Names";
            this.checkBoxName.UseVisualStyleBackColor = true;
            this.checkBoxName.CheckedChanged += new System.EventHandler(this.checkBoxName_CheckedChanged);
            // 
            // trackBarRadius0
            // 
            this.trackBarRadius0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBarRadius0.Location = new System.Drawing.Point(670, 217);
            this.trackBarRadius0.Maximum = 1000;
            this.trackBarRadius0.Name = "trackBarRadius0";
            this.trackBarRadius0.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBarRadius0.Size = new System.Drawing.Size(45, 104);
            this.trackBarRadius0.TabIndex = 23;
            this.trackBarRadius0.Scroll += new System.EventHandler(this.trackBarRadius_Scroll);
            // 
            // fileNameTextBox
            // 
            this.fileNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.fileNameTextBox.Location = new System.Drawing.Point(670, 162);
            this.fileNameTextBox.Name = "fileNameTextBox";
            this.fileNameTextBox.Size = new System.Drawing.Size(84, 20);
            this.fileNameTextBox.TabIndex = 22;
            this.fileNameTextBox.Text = "../../example.txt";
            // 
            // nofSlotsLabel
            // 
            this.nofSlotsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.nofSlotsLabel.AutoSize = true;
            this.nofSlotsLabel.Location = new System.Drawing.Point(741, 145);
            this.nofSlotsLabel.Name = "nofSlotsLabel";
            this.nofSlotsLabel.Size = new System.Drawing.Size(13, 13);
            this.nofSlotsLabel.TabIndex = 20;
            this.nofSlotsLabel.Text = "1";
            // 
            // nofSlotsBox
            // 
            this.nofSlotsBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.nofSlotsBox.Location = new System.Drawing.Point(669, 122);
            this.nofSlotsBox.Name = "nofSlotsBox";
            this.nofSlotsBox.Size = new System.Drawing.Size(85, 20);
            this.nofSlotsBox.TabIndex = 19;
            this.nofSlotsBox.TextChanged += new System.EventHandler(this.nofSlotsBox_TextChanged);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(666, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Number of slots";
            // 
            // checkBoxConnections
            // 
            this.checkBoxConnections.AccessibleDescription = "";
            this.checkBoxConnections.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxConnections.AutoSize = true;
            this.checkBoxConnections.Checked = true;
            this.checkBoxConnections.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxConnections.Location = new System.Drawing.Point(669, 28);
            this.checkBoxConnections.Name = "checkBoxConnections";
            this.checkBoxConnections.Size = new System.Drawing.Size(85, 17);
            this.checkBoxConnections.TabIndex = 17;
            this.checkBoxConnections.Text = "Connections";
            this.checkBoxConnections.UseVisualStyleBackColor = true;
            this.checkBoxConnections.CheckedChanged += new System.EventHandler(this.checkBoxConnections_CheckedChanged);
            // 
            // checkBoxActivation
            // 
            this.checkBoxActivation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxActivation.AutoSize = true;
            this.checkBoxActivation.Checked = true;
            this.checkBoxActivation.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxActivation.Location = new System.Drawing.Point(669, 51);
            this.checkBoxActivation.Name = "checkBoxActivation";
            this.checkBoxActivation.Size = new System.Drawing.Size(78, 17);
            this.checkBoxActivation.TabIndex = 16;
            this.checkBoxActivation.Text = "Activations";
            this.checkBoxActivation.UseVisualStyleBackColor = true;
            this.checkBoxActivation.CheckedChanged += new System.EventHandler(this.checkBoxActivation_CheckedChanged);
            // 
            // labelRadius
            // 
            this.labelRadius.Location = new System.Drawing.Point(705, 324);
            this.labelRadius.Name = "labelRadius";
            this.labelRadius.Size = new System.Drawing.Size(49, 13);
            this.labelRadius.TabIndex = 28;
            this.labelRadius.Text = "0";
            // 
            // placementView
            // 
            this.placementView.Location = new System.Drawing.Point(13, 28);
            this.placementView.Name = "placementView";
            this.placementView.ShowActivations = true;
            this.placementView.ShowArea = false;
            this.placementView.ShowConnections = true;
            this.placementView.ShowNames = true;
            this.placementView.ShowRadius = false;
            this.placementView.Size = new System.Drawing.Size(647, 312);
            this.placementView.TabIndex = 29;
            // 
            // trackBarRadius1
            // 
            this.trackBarRadius1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBarRadius1.Location = new System.Drawing.Point(709, 217);
            this.trackBarRadius1.Maximum = 1000;
            this.trackBarRadius1.Name = "trackBarRadius1";
            this.trackBarRadius1.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBarRadius1.Size = new System.Drawing.Size(45, 104);
            this.trackBarRadius1.TabIndex = 30;
            this.trackBarRadius1.Scroll += new System.EventHandler(this.trackBarRadius1_Scroll);
            // 
            // solveLowLevelToolStripMenuItem
            // 
            this.solveLowLevelToolStripMenuItem.Name = "solveLowLevelToolStripMenuItem";
            this.solveLowLevelToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.solveLowLevelToolStripMenuItem.Text = "Solve low level";
            this.solveLowLevelToolStripMenuItem.Click += new System.EventHandler(this.solveLowLevelToolStripMenuItem_Click);
            // 
            // SamplerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(766, 365);
            this.Controls.Add(this.trackBarRadius1);
            this.Controls.Add(this.labelRadius);
            this.Controls.Add(this.placementView);
            this.Controls.Add(this.checkBoxRadius);
            this.Controls.Add(this.checkBoxName);
            this.Controls.Add(this.trackBarRadius0);
            this.Controls.Add(this.fileNameTextBox);
            this.Controls.Add(this.nofSlotsLabel);
            this.Controls.Add(this.nofSlotsBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.checkBoxConnections);
            this.Controls.Add(this.checkBoxActivation);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(170, 404);
            this.Name = "SamplerForm";
            this.Text = "SamplerForm";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarRadius0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarRadius1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem openBitmapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sampleToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.CheckBox checkBoxRadius;
        private System.Windows.Forms.CheckBox checkBoxName;
        private System.Windows.Forms.TrackBar trackBarRadius0;
        private System.Windows.Forms.TextBox fileNameTextBox;
        private System.Windows.Forms.Label nofSlotsLabel;
        private System.Windows.Forms.TextBox nofSlotsBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBoxConnections;
        private System.Windows.Forms.CheckBox checkBoxActivation;
        private PlacementView placementView;
        private System.Windows.Forms.Label labelRadius;
        private System.Windows.Forms.ToolStripMenuItem runToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem runToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem setNumberToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem problemDescriptionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem factorGraphDescriptionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newSolutionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oldSolutionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem revertStructureToolStripMenuItem;
        private System.Windows.Forms.TrackBar trackBarRadius1;
        private System.Windows.Forms.ToolStripMenuItem setTravelSpeedsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem solveLowLevelToolStripMenuItem;
    }
}