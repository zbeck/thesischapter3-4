﻿namespace AgentDraw
{
    partial class SolutionExplorer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxSolutions = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.loadProblemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshSolutionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.placementView = new AgentDraw.PlacementView();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxSolutions
            // 
            this.textBoxSolutions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSolutions.Location = new System.Drawing.Point(13, 457);
            this.textBoxSolutions.Multiline = true;
            this.textBoxSolutions.Name = "textBoxSolutions";
            this.textBoxSolutions.Size = new System.Drawing.Size(750, 152);
            this.textBoxSolutions.TabIndex = 0;
            this.textBoxSolutions.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxSolutions_KeyPress);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadProblemToolStripMenuItem,
            this.refreshSolutionsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(775, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // loadProblemToolStripMenuItem
            // 
            this.loadProblemToolStripMenuItem.Name = "loadProblemToolStripMenuItem";
            this.loadProblemToolStripMenuItem.Size = new System.Drawing.Size(93, 20);
            this.loadProblemToolStripMenuItem.Text = "Load Problem";
            this.loadProblemToolStripMenuItem.Click += new System.EventHandler(this.loadProblemToolStripMenuItem_Click);
            // 
            // refreshSolutionsToolStripMenuItem
            // 
            this.refreshSolutionsToolStripMenuItem.Name = "refreshSolutionsToolStripMenuItem";
            this.refreshSolutionsToolStripMenuItem.Size = new System.Drawing.Size(110, 20);
            this.refreshSolutionsToolStripMenuItem.Text = "Refresh Solutions";
            this.refreshSolutionsToolStripMenuItem.Click += new System.EventHandler(this.refreshSolutionsToolStripMenuItem_Click);
            // 
            // placementView
            // 
            this.placementView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.placementView.Location = new System.Drawing.Point(13, 27);
            this.placementView.Name = "placementView";
            this.placementView.ShowActivations = true;
            this.placementView.ShowArea = false;
            this.placementView.ShowConnections = true;
            this.placementView.ShowNames = true;
            this.placementView.ShowRadius = false;
            this.placementView.Size = new System.Drawing.Size(750, 424);
            this.placementView.TabIndex = 1;
            // 
            // SolutionExplorer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(775, 621);
            this.Controls.Add(this.placementView);
            this.Controls.Add(this.textBoxSolutions);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "SolutionExplorer";
            this.Text = "SolutionExplorer";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxSolutions;
        private PlacementView placementView;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem loadProblemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshSolutionsToolStripMenuItem;
    }
}