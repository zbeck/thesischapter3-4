﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using MyExtensions;
using AStarSolver;
using System.Diagnostics;

namespace AgentDraw
{
    public abstract class AgentController
    {
        protected AgentModel model;
        protected AgentProg program;

        protected Node dragNode = null;
        protected Size dragOffset;
        public string TmpFolder;
        public static bool absoluteTmpPath = false;

        internal AgentModel Model { set { Reset();  model = value; } }
        static int sampleCounter;
        protected static int SampleCounter
        {
            get
            {
                sampleCounter++;
                return sampleCounter;
            }
        }

        public AgentController(AgentModel model, AgentProg program)
        {
            sampleCounter = 0;
            this.model = model;
            this.program = program;
            TmpFolder = "temp/";
        }

        public abstract void MouseClickEventHandler(object sender, MouseEventArgs e);

        public virtual void MouseDragStart(Node node, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Middle)
            {
                dragNode = node;
                dragOffset = new Size(e.Location);
                program.SetCapture();
            }
        }

        public virtual void MouseDragMove(MouseEventArgs e)
        {
            if ((dragNode != null) && (e.Button == System.Windows.Forms.MouseButtons.Middle))
            {
                Point oldPos = dragNode.Position;
                dragNode.Location = Point.Subtract(e.Location, dragOffset);
                model.UpdateLocation(oldPos, dragNode.Position);
            }
        }

        public virtual void MouseDragEnd(MouseEventArgs e)
        {
            if ((dragNode != null) && (e.Button == System.Windows.Forms.MouseButtons.Middle))
            {
                dragNode = null;
                program.StopCapture();
            }
        }

        public static string Solve(AgentModel model, string solvers, string tmpdir, string workFolder, string solutionFolder, List<String> tmpFiles, int[] nofslots = null)
        {
            string sampleid = SimController.SampleCounter.ToString();
            string solutionFile = String.Concat(solutionFolder, "solution_", sampleid, ".txt");
            string output = "";
            //run the script
            string outFile = String.Concat(tmpdir, "sampleProblem", sampleid, ".txt"); //the output of the most recent script (which might give the input of the new one
            bool inputGenerated = false; //indicates if the model output has to be generated or not
            string dataFile;
            foreach (string slvr in solvers.Split(';'))  //parse given solvers
            {
                dataFile = outFile;
                string[] solverParts = slvr.Trim().Split('>');
                string solver = solverParts[0].Trim();
                if (solver == "")
                    continue;
                if (solverParts.Length > 1)
                    outFile = String.Concat(dataFile.Split('.')[0], ".", solverParts[1].Trim());
                else
                    outFile = null;
                if (!AgentProg.ValidSolver(solver))
                {
                    Console.WriteLine(String.Format("!!!!!!!!!!! Solver {0} is not a valid script path!!!!!!!!!!!", solver));
                    MessageBox.Show(String.Format("Cannot find specified solver script: \"{0}\"", solver), "Solver error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return "";
                }
                if (!inputGenerated)
                {
                    string datF;
                    if (absoluteTmpPath)
                        datF = dataFile;
                    else
                        datF = String.Concat(workFolder, dataFile);
                    //Console.WriteLine("file: {0}\nabsoluteTmpPath: {1}", datF, absoluteTmpPath);
                    using (TextWriter tw = new StreamWriter(datF))
                    {
                        model.GenerateProblemOutput(tw, nofslots);
                        inputGenerated = true;
                    }
                    tmpFiles.Add(datF);
                }
                //execute script for solution
                output = MyExtensionMethods.RunScript(solver, workFolder, dataFile);
                /*if (outFile == null)
                    using (StreamWriter sFile = new StreamWriter(String.Concat(workFolder, solutionFile), false))
                    {
                        sFile.Write(output);
                    }*/
            }
            return output;
        }

        public static SolutionModel SolveAStar(AgentModel model, int level, bool showSolution = false)
        {
            Stopwatch sw = new Stopwatch();
            List<List<List<int>>> solution = model.CompleteSolutionForLevelAStar(level, -1, sw);
            string solver = String.Format("A-Star on level{0}, took {1:D}ms", level, sw.ElapsedMilliseconds);
            if (showSolution)
            {
                Solution s = new Solution(model, solution, solver);
                s.Show();
                return s.Model as SolutionModel;
            }
            else
                return new SolutionModel(model, solution, solver);
        }

        public abstract void Reset();
    }
}
