﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AgentDraw
{
    public partial class Solution : Form
    {
        private AgentModel model;
        public AgentModel Model { get { return model; } }

        public Solution(AgentModel agentModel, List<int> values, string solver)
        {
            InitializeComponent();
            this.Text = String.Concat("Solution by ", solver);

            this.model = new SolutionModel(agentModel, values, solver, solutionContainer);
        }
        public Solution(AgentModel agentModel, List<List<List<int>>> values, string solver)
        {
            InitializeComponent();
            this.Text = String.Concat("Solution by ", solver);

            this.model = new SolutionModel(agentModel, values, solver, solutionContainer);
            richTextBox1.Text = this.model.ToString();
        }

        public Solution(SolutionModel model)
        {
            InitializeComponent();
            this.Text = model.Solver;
            solutionContainer.ShowConnections = false;
            solutionContainer.ShowActivations = true;
            solutionContainer.ShowNames = false;
            solutionContainer.ShowRadius = true;

            this.model = model;
            (this.model as SolutionModel).View = solutionContainer;
            richTextBox1.Text = this.model.ToString();
        }

        public Solution(AgentModel model, string id = "Problem")
        {
            InitializeComponent();
            this.Text = id;
            solutionContainer.ShowConnections = true;
            solutionContainer.ShowActivations = true;
            solutionContainer.ShowNames = true;//false;
            solutionContainer.ShowRadius = true;

            this.model = model;
            this.model.View = solutionContainer;
            solutionContainer.Invalidate();
        }
    }
}
