﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Collections;
using System.Runtime.Serialization;
using MyExtensions;

namespace AgentDraw
{
    [Serializable]
    public class NodeList : List<NodeModel>, IDeserializationCallback
    {
        public event ChangedEventHandler Changed;

        protected virtual void ElementChanged(object sender)
        {
            if (Changed != null)
                Changed(sender);
        }

        public new void Add(NodeModel value)
        {
            NodeModel n = value as NodeModel;
            n.Changed += ElementChanged;
            base.Add(value);
        }

        /// <summary>
        /// Copy constructor that copies every element using its copy constructor (won't set views)
        /// </summary>
        /// <param name="source">source list</param>
        public NodeList(NodeList source)
        {
            foreach (NodeModel node in source)
            {
                this.Add(new NodeModel(node));
            }
        }

        public NodeList() : base() { }

        internal static List<List<List<int>>> ParseNewSolution(string input, List<NodeList> agents)
        {
            string[] lines = input.Split('\n');
            List<List<List<int>>> solution = null;
            foreach (string lineraw in lines)
            {
                string line = lineraw.Trim();
                if ((line.Length == 0) || (line[0] != '['))
                    continue;
                try
                {
                    solution = MyExtensionMethods.Parse3DList(line);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e); continue;
                }
                if (solution.Count > 0)
                {
                    bool valid = true;
                    for (int i = 0; i < solution.Count; i++)
                        if (solution[i].Count != agents[i].Count)
                            valid = false;
                    if (valid)
                        return solution;
                }
            }
            return null;
        }

        public void OnDeserialization(object sender)
        {
            foreach (NodeModel node in this)
                node.Changed += ElementChanged;
        }
    }

    public class NodeViewList<T> : List<T>
    {
        public event ChangedEventHandler Changed;

        protected virtual void ElementChanged(object sender, EventArgs e)
        {
            if (Changed != null)
                Changed(sender);
        }

        public new void Add(T value)
        {
            Node n = value as Node;
            n.LocationChanged += new EventHandler(ElementChanged);
            base.Add(value);
        }
    }
}