﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace AgentDraw
{
    public class SolutionModel : AgentModel
    {
        string solver;
        public string Solver { get { return solver; } }
        List<RelationModel> Paths;
        bool hasBadPath;
        public bool HasBadPath { get { return hasBadPath; } }
        bool hasBadExecution;
        public bool HasBadExecution { get { return hasBadExecution; } }
        bool exceedsBattery;
        public bool ExceedsBattery { get { return exceedsBattery; } }

        public string debugtext;

        internal override PlacementView View
        {
            set
            {
                base.View = value;
                SetPaths();
            }
        }
        public double TaskCompletitionRatioLow
        {
            get { return CalculateRatio(1); }
        }
        public double TaskCompletitionRatioHigh
        {
            get { return CalculateRatio(0); }
        }
        public double TaskCompletitionRatioFirstSlot
        {
            get
            {
                int max0 = Math.Min(UAVNodes[0].Count, TaskNodes[0].Count);
                int max1 = Math.Min(UAVNodes[1].Count, TaskNodes[1].Count);
                int done = 0;
                foreach (NodeList l in UAVNodes)
                    foreach (NodeModel uav in l)
                    {
                        RelationModel firstPath = Paths.Find(p => uav.Equals(p.From));
                        if (firstPath != null)
                            done++;
                    }
                if (max0 + max1 == 0)
                    return 1;
                return done / (max0 + max1);
            }
        }
        double executionTime;
        double discoveryTime;
        double sumTime;
        public double ExecutionTime
        {
            get { return executionTime; }
        }
        public double DiscoveryTime
        {
            get { return discoveryTime; }
        }
        public double SumTime
        {
            get { return sumTime; }
        }

        double lowUtil;
        double highUtil;
        public double LowUtil { get { return lowUtil; } }
        public double HiUtil { get { return highUtil; } }
        public double Util { get { return lowUtil + highUtil; } }

        //  0   Everything is fine
        //  1   Fatal error
        //  2   Minor error
        public int ErrorLevel
        {
            get
            {
                if (HasBadPath)
                    return 1;
                if (HasBadExecution)
                    return 2;
                if (ExceedsBattery)
                    return 2;
                return 0;
            }
        }

        public SolutionModel(AgentModel source, List<int> variableValues, string solver, SolutionView view = null) :
            base(source)
        {
            this.solver = solver;
            InitSolution(source.Speed, view);

            Paths = ExtractPaths(ExtractVars(variableValues));
            SetPaths();
        }

        public SolutionModel(AgentModel source, List<List<List<int>>> variableValues, string solver, SolutionView view = null) :
            base(source)
        {
            this.solver = solver;
            InitSolution(source.Speed, view);

            Paths = ExtractPaths(ExtractVars(variableValues));
            SetPaths();
        }

        private void InitSolution(double[] speeds, SolutionView view)
        {
            debugtext = "";

            if ((speeds == null) || (speeds.Length == 0))
            {
                speeds = new double[UAVNodes.Count];
                for (int i = 0; i < speeds.Length; i++)
                {
                    speeds[i] = 1;
                }
            }

            this.Speed = speeds;

            hasBadPath = false;
            hasBadExecution = false;
            exceedsBattery = false;
            Paths = new List<RelationModel>();
            executionTime = 0;
            discoveryTime = 0;
            sumTime = 0;
            lowUtil = 0;
            highUtil = 0;

            if (view != null)
            {
                this.View = view;
                view.ShowActivations = true;
                view.ShowConnections = false;
                view.ShowNames = true;
                view.ShowRadius = true;
            }
        }

        private List<Variable> ExtractVars(List<List<List<int>>> variableValues)
        {
            /*Console.WriteLine(variableValues);
            foreach(var l1 in variableValues)
                Console.WriteLine(l1);
            foreach (var l1 in variableValues)
                foreach (var l2 in l1)
                    Console.WriteLine(l2);
            foreach (var l1 in variableValues)
                foreach (var l2 in l1)
                    foreach (int e in l2)
                        Console.WriteLine(e);*/

            List<Variable> activeVars = new List<Variable>();
            for (int l = 0; l < UAVNodes.Count; l++)
                for (int u = 0; u < UAVNodes[l].Count; u++)
                    for (int s = 0; s < variableValues[l][u].Count; s++)
                        activeVars.Add(new Variable(l, UAVNodes, u, TaskNodes, variableValues[l][u][s], s));
            return activeVars;
        }

        private List<Variable> ExtractVars(List<int> variableValues)
        {
            List<Variable> allVars = GenerateAllVariables(Connections);
            if (allVars.Count != variableValues.Count)
                throw new Exception("Different number of variables and values!");

            //find active variables (variables with one value)
            List<Variable> activeVars = new List<Variable>();
            int index = 0;
            foreach (int v in variableValues)
            {
                if (v != 0)
                    activeVars.Add(allVars[index]);
                index++;
            }
            return activeVars;
        }

        protected Dictionary<int, double> executionTimes;
        protected List<RelationModel> ExtractPaths(List<Variable> activeVars)
        {
            executionTimes = new Dictionary<int, double>(TaskCount);
            List<RelationModel> paths = new List<RelationModel>();
            //sort active variables by level (descending) then UAV then timeslot then task
            activeVars.Sort(Variable.CompareUST);
            Dictionary<int, double> discoveryTimes = new Dictionary<int, double>();
            NodeModel lastNode = null;
            int cuav = -1;
            int slot = -1;
            double time = 0;
            foreach (Variable v in activeVars)
            {
                bool pathValid = true;
                if (cuav != v.uav) //next UAV first path
                {
                    cuav = v.uav;
                    lastNode = UAVNodes[v.level][v.reluav];
                    slot = v.slot;
                    time = 0;
                }
                else if (slot == v.slot)    //same timeslot (not really valid)
                {
                    lastNode = paths[paths.Count - 1].From;
                    hasBadPath = true;
                    pathValid = false;
                }
                else    //valid path
                {
                    slot = v.slot;
                    lastNode = paths[paths.Count - 1].To;
                }

                if (pathValid)  //calculate with timing
                {
                    time += AgentModel.Distance(lastNode, TaskNodes[v.level][v.reltask]) / speed[v.level] + TaskTime[v.level];
                    if (v.level == 0)   //dicovering UAV
                    {
                        List<RelationModel> acts = Activations.FindAll(r => TaskNodes[v.level][v.reltask].ID.Equals(r.From.ID));
                        foreach (RelationModel r in acts)
                        {
                            if (!discoveryTimes.ContainsKey(r.To.ID))
                                discoveryTimes.Add(r.To.ID, time);
                            else
                                discoveryTimes[r.To.ID] = Math.Min(time, discoveryTimes[r.To.ID]);
                        }
                        highUtil += CalculateUtility(this, v.level, time, v.slot);
                        sumTime += time;
                        discoveryTime = Math.Max(time, discoveryTime);
                    }
                    else    //executing UAV
                    {
                        NodeModel currentTask = TaskNodes[v.level][v.reltask];
                        if (discoveryTimes.ContainsKey(currentTask.ID)) //discovered
                            time = Math.Max(discoveryTimes[currentTask.ID] + TaskTime[v.level], time);
                        else if (Activations.Find(r => currentTask.ID.Equals(r.To.ID)) != null) //should be discovered but isn't
                            hasBadExecution = true;
                        lowUtil += CalculateUtility(this, v.level, time, v.slot);
                        sumTime += time;
                        executionTime = Math.Max(time, executionTime);
                    }
                    executionTimes.Add(TaskNodes[v.level][v.reltask].ID, time);
                    if (time > TravelTime[v.level])
                        exceedsBattery = true;
                }
                paths.Add(new RelationModel(lastNode, TaskNodes[v.level][v.reltask]));
            }
            return paths;
        }

        protected double CalculateRatio(int level)
        {
            int max = Math.Min(UAVNodes[level].Count * nofSlots, TaskNodes[level].Count);
            int done = 0;
            foreach (NodeModel task in TaskNodes[level])
            {
                RelationModel firstPath = Paths.Find(p => task.ID.Equals(p.To.ID));
                if (firstPath != null)
                    done++;
            }
            if (max == 0)
                return 1;
            return (double)done / max;
        }

        internal void SetPaths()
        {
            foreach (PlacementView view in views)
            {
                (view as SolutionView).Paths = Paths;
                view.Invalidate();
            }
        }

        public List<List<double>> GetDirections(List<List<NodeModel>> AssignedUAVs, Sampler BeliefSampler = null)
        {
            System.Drawing.Point mean = new System.Drawing.Point();
            if(BeliefSampler != null)
            mean = BeliefSampler.GetMeanLocation();
            AssignedUAVs.Clear();
            List<List<double>> angles = new List<List<double>>();
            foreach (NodeList list in UAVNodes)
            {
                List<NodeModel> assignedList = new List<NodeModel>();
                List<double> currentlist = new List<double>();
                foreach (NodeModel uav in list)
                {
                    RelationModel firstPath = Paths.Find(p => uav.Equals(p.From));
                    if (firstPath == null)
                    {
                        if(BeliefSampler == null)
                        currentlist.Add(SimController.StopAngle);
                        else
                        currentlist.Add(Math.Atan2(mean.Y - uav.Position.Y, mean.X - uav.Position.X));
                        assignedList.Add(null);
                    }
                    else
                    {
                        currentlist.Add(Math.Atan2(firstPath.To.Position.Y - firstPath.From.Position.Y, firstPath.To.Position.X - firstPath.From.Position.X));
                        assignedList.Add(firstPath.To);
                    }
                }
                angles.Add(currentlist);
                AssignedUAVs.Add(assignedList);
            }
            return angles;
        }

        public double[] WriteOutput(TextWriter tw)
        {
            Dictionary<int, double> xCoord = new Dictionary<int, double>(UAVCount);
            Dictionary<int, double> yCoord = new Dictionary<int, double>(UAVCount);
            Dictionary<int, double> utility = new Dictionary<int, double>(UAVCount);
            Dictionary<int, double> stayTime = new Dictionary<int, double>(UAVCount);
            Dictionary<int, double> discoveryTime = new Dictionary<int, double>();
            Dictionary<int, double> executionTime = new Dictionary<int, double>();
            List<int> doneTasks = new List<int>();
            double time = 0;
            foreach (NodeList l in UAVNodes)
                foreach (NodeModel u in l)
                {
                    xCoord.Add(u.ID, u.Position.X);
                    yCoord.Add(u.ID, u.Position.Y);
                    utility.Add(u.ID, 0);
                    stayTime.Add(u.ID, 0);
                }
            for (int level = 0; level < TaskNodes.Count; level++)
                foreach (NodeModel u in TaskNodes[level])
                {
                    if (level == 0)
                        discoveryTime.Add(u.ID, 0);
                    else
                        discoveryTime.Add(u.ID, -1);
                    executionTime.Add(u.ID, -1);
                }
            RefreshRelations();

            tw.WriteLine("TimeStepData = [...");
            tw.WriteLine(GetStepData(xCoord, yCoord, utility));
            while (MakeStep(ref time, xCoord, yCoord, utility, stayTime, TaskTime, discoveryTime, executionTime))
            {
                tw.WriteLine(GetStepData(xCoord, yCoord, utility));
            }
            tw.WriteLine("];\n\nTaskTime=[...");
            foreach (NodeList l in TaskNodes)
                foreach (NodeModel n in l)
                {
                    tw.WriteLine(String.Format("{0} {1} {2} {3};...", n.Position.X, -n.Position.Y, discoveryTime[n.ID], executionTime[n.ID]));
                }
            tw.WriteLine("];\n");
            double[] utils = new double[UAVNodes.Count];
            for (int l = 0; l < UAVNodes.Count; l++)
            {
                utils[l] = 0;
                foreach (NodeModel u in UAVNodes[l])
                {
                    utils[l] += utility[u.ID];
                }
            }
            return utils;
        }

        private string GetStepData(Dictionary<int, double> xCoord, Dictionary<int, double> yCoord, Dictionary<int, double> utility)
        {
            string retval = "";
            foreach (NodeList l in UAVNodes)
                foreach (NodeModel n in l)
                {
                    retval = String.Format("{0}{1} {2} {3} ", retval, xCoord[n.ID], -yCoord[n.ID], utility[n.ID]);
                }
            retval += ";...";
            return retval;
        }

        protected bool MakeStep(ref double time, Dictionary<int, double> xCoord, Dictionary<int, double> yCoord, Dictionary<int, double> utility, Dictionary<int, double> stayTime,
            double[] TaskTime, Dictionary<int, double> discoveryTime, Dictionary<int, double> executionTime)
        {
            if (Paths.Count == 0)
                return false;
            List<List<NodeModel>> AssignedUAVs = new List<List<NodeModel>>(2);
            double minTime = Double.PositiveInfinity;
            for (int level = 0; level < UAVNodes.Count; level++)
            {
                List<NodeModel> assignedList = new List<NodeModel>();
                foreach (NodeModel uav in UAVNodes[level])
                {
                    RelationModel firstPath = Paths.Find(p => uav.ID.Equals(p.From.ID));
                    if (firstPath == null)
                        assignedList.Add(null);
                    else
                    {
                        assignedList.Add(firstPath.To);
                        double t = AgentModel.Distance(uav, firstPath.To) / speed[level] + stayTime[uav.ID];
                        if (t < minTime)
                            minTime = t;
                    }
                }
                AssignedUAVs.Add(assignedList);
            }
            if (Double.IsInfinity(minTime))
                return false;
            time += minTime;
            for (int level = 0; level < UAVNodes.Count; level++)
                for (int id = 0; id < UAVNodes[level].Count; id++)
                {
                    NodeModel uav = UAVNodes[level][id];
                    NodeModel assigned = AssignedUAVs[level][id];
                    double moveTime = minTime;
                    if (assigned != null)
                    {
                        stayTime[uav.ID] -= minTime;
                        if (stayTime[uav.ID] <= 0)
                        {
                            moveTime = -stayTime[uav.ID];
                            stayTime[uav.ID] = 0;
                            double mul = moveTime * speed[level] / AgentModel.Distance(UAVNodes[level][id], assigned);
                            xCoord[uav.ID] += (assigned.Position.X - uav.Position.X) * mul;
                            yCoord[uav.ID] += (assigned.Position.Y - uav.Position.Y) * mul;

                            System.Drawing.Point newPos = new System.Drawing.Point((int)Math.Round(xCoord[uav.ID]), (int)Math.Round(yCoord[uav.ID]));
                            uav.Position = newPos;
                            if ((Math.Abs(newPos.X - assigned.Position.X) + Math.Abs(newPos.Y - assigned.Position.Y)) < 3) //moved close enough to a task
                                ExecuteTask(uav, assigned, xCoord, yCoord, utility, stayTime, TaskTime, level, time, discoveryTime, executionTime);
                        }
                    }
                }
            return true;
        }

        private void ExecuteTask(NodeModel uav, NodeModel assigned, Dictionary<int, double> xCoord, Dictionary<int, double> yCoord, Dictionary<int, double> utility,
            Dictionary<int, double> stayTime, double[] taskTime, int level, double time, Dictionary<int, double> discoveryTime, Dictionary<int, double> executionTime)
        {
            xCoord[uav.ID] = assigned.Position.X;
            yCoord[uav.ID] = assigned.Position.Y;
            uav.Position = assigned.Position;
            if (level == 1) //low level
            {
                if (IsDiscovered(discoveryTime, assigned.ID))
                {
                    executionTime[assigned.ID] = time + taskTime[level];
                    utility[uav.ID] += CalculateUtility(this, level, time + taskTime[level], 0);
                    stayTime[uav.ID] = taskTime[level];
                }
                else
                    stayTime[uav.ID] = travelTime[1];
            }
            else
            {
                stayTime[uav.ID] = taskTime[level];
                DoDiscovery(discoveryTime, executionTime, assigned.ID, utility, stayTime, taskTime[level], taskTime[1], time);
                executionTime[assigned.ID] = time + taskTime[level];
                utility[uav.ID] += CalculateUtility(this, level, time + taskTime[level], 0);
            }

            Paths.RemoveAll(p => uav.Equals(p.From));
            List<RelationModel> newFirstPath = Paths.FindAll(p => assigned.ID.Equals(p.From.ID));
            foreach (RelationModel p in newFirstPath)
                Paths[Paths.FindIndex(pth => p.Equals(pth))] = new RelationModel(uav, p.To);
        }

        private bool IsDiscovered(Dictionary<int, double> discoveryTime, int task)
        {
            return (discoveryTime[task] >= 0);
        }

        private void DoDiscovery(Dictionary<int, double> discoveryTime, Dictionary<int, double> executionTime, int task,
            Dictionary<int, double> utility, Dictionary<int, double> stayTime, double discoverTime, double executeTime, double time)
        {
            List<RelationModel> discs = Activations.FindAll(r => task.Equals(r.From.ID));
            foreach (RelationModel r in discs)
            {
                if (discoveryTime[r.To.ID] < 0)
                {
                    discoveryTime[r.To.ID] = time + discoverTime;
                    NodeModel uav = UAVNodes[1].Find(u => (u.Position.Equals(r.To.Position)));
                    if (uav != null)
                    {
                        stayTime[uav.ID] = discoverTime + executeTime;
                        utility[uav.ID] += CalculateUtility(this, 1, time + discoverTime + executeTime, 0);
                        executionTime[r.To.ID] = time + discoverTime + executeTime;
                    }
                }
            }
        }
        /*
        protected string GetTimestepData()
        {
            string stout = "";
            foreach (NodeList l in UAVNodes)
                foreach (NodeModel n in l)
                {
                    stout = String.Format("{0}{1} {2} {3} ", stout, xCoord[n.ID], -yCoord[n.ID], gainedUtility[n.ID]);
                }
            stout = String.Format("{0};...\n", stout);
            return stout;
        }
        
        protected string GetSummaryData()
        {
            string stout = "TaskTime=[...\n";
            foreach (NodeList l in TaskNodes)
                foreach (NodeModel n in l)
                {
                    stout = GetSummaryData(stout, n);
                }
            foreach (NodeModel n in undiscoveredTasks)
                stout = GetSummaryData(stout, n);
            foreach (NodeModel n in executedTasks)
                stout = GetSummaryData(stout, n);
            stout = String.Format("{0}];\n\n", stout);
            return stout;
        }*/

        public override string ToString()
        {
            string retval = string.Format("Solver: {8}\nTask Completition:\n\tlow: {0:P2}\n\thigh: {1:P2}\n\tfirst: {2:P2}\nTime:\n\texecution: {3:.00}\n\tdiscovery: {4:.00}\n\tsum: {5:.00}\nHi Utility:\n\tLow Level: {6:.0}\n\tOverall: {7:.0}",
                TaskCompletitionRatioLow, TaskCompletitionRatioHigh, TaskCompletitionRatioFirstSlot, ExecutionTime, DiscoveryTime, SumTime, LowUtil, Util, solver);
            if (hasBadExecution)
                retval = string.Format("{0}\nInvalid execution!", retval);
            if (ExceedsBattery)
                retval = string.Format("{0}\nPlan exceeds battery life!", retval);
            if (HasBadPath)
                retval = string.Format("{0}\nPath given!", retval);
            foreach (KeyValuePair<int, double> e in executionTimes)
                retval = String.Format("{0}\n{1}\t{2:.00}", retval, e.Key, e.Value);
            return retval;
            //return base.ToString();
        }
    }
}
