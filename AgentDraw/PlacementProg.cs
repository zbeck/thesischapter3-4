﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace AgentDraw
{
    public class PlacementProg : AgentProg
    {
        public PlacementProg(PlacementView pview, IAgentForm form) :
            base(pview, form)
        {
            model = new PlacementModel(View);
            controller = new PlacementController(model, this);
            InitView();
        }

        internal void OpenFile(System.IO.Stream fs)
        {
            IFormatter formatter = new BinaryFormatter();
            PlacementModel newModel = formatter.Deserialize(fs) as PlacementModel;
            View.Clear();
            newModel.View = View;
            newModel.RefreshRelations();
            newModel.SelectLevel(0);    //only highest level tasks and UAVs selected
            controller.Model = newModel;
            model = newModel;
            form.SetNofSlot(model.NofSlots);
            form.SetTravelTime(model.TravelTime);
        }

        public void Clear()
        {
            PlacementModel model = new PlacementModel(View);
            Clear(model);
        }
    }
}
