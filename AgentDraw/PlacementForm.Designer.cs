﻿namespace AgentDraw
{
    partial class PlacementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkBoxActivation = new System.Windows.Forms.CheckBox();
            this.checkBoxConnections = new System.Windows.Forms.CheckBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.nofSlotsBox = new System.Windows.Forms.TextBox();
            this.nofSlotsLabel = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.fileNameTextBox = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadSolutionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trackBarDistance = new System.Windows.Forms.TrackBar();
            this.checkBoxName = new System.Windows.Forms.CheckBox();
            this.checkBoxRadius = new System.Windows.Forms.CheckBox();
            this.placementView = new AgentDraw.PlacementView();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarDistance)).BeginInit();
            this.SuspendLayout();
            // 
            // checkBoxActivation
            // 
            this.checkBoxActivation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxActivation.AutoSize = true;
            this.checkBoxActivation.Checked = true;
            this.checkBoxActivation.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxActivation.Location = new System.Drawing.Point(647, 54);
            this.checkBoxActivation.Name = "checkBoxActivation";
            this.checkBoxActivation.Size = new System.Drawing.Size(78, 17);
            this.checkBoxActivation.TabIndex = 1;
            this.checkBoxActivation.Text = "Activations";
            this.checkBoxActivation.UseVisualStyleBackColor = true;
            this.checkBoxActivation.CheckedChanged += new System.EventHandler(this.checkBoxActivation_CheckedChanged);
            // 
            // checkBoxConnections
            // 
            this.checkBoxConnections.AccessibleDescription = "";
            this.checkBoxConnections.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxConnections.AutoSize = true;
            this.checkBoxConnections.Checked = true;
            this.checkBoxConnections.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxConnections.Location = new System.Drawing.Point(647, 31);
            this.checkBoxConnections.Name = "checkBoxConnections";
            this.checkBoxConnections.Size = new System.Drawing.Size(85, 17);
            this.checkBoxConnections.TabIndex = 2;
            this.checkBoxConnections.Text = "Connections";
            this.checkBoxConnections.UseVisualStyleBackColor = true;
            this.checkBoxConnections.CheckedChanged += new System.EventHandler(this.checkBoxConnections_CheckedChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 331);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(744, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "status";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 339);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(705, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Left: Task, Right: UAV, Middle: Grab, DRight: Delete; Task Left: Selection, Task " +
    "Right: Activation; UAV Left: Disconnect/Connect with Active, UAV";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(644, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Number of slots";
            // 
            // nofSlotsBox
            // 
            this.nofSlotsBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.nofSlotsBox.Location = new System.Drawing.Point(647, 125);
            this.nofSlotsBox.Name = "nofSlotsBox";
            this.nofSlotsBox.Size = new System.Drawing.Size(85, 20);
            this.nofSlotsBox.TabIndex = 6;
            this.nofSlotsBox.TextChanged += new System.EventHandler(this.nofSlotsBox_TextChanged);
            // 
            // nofSlotsLabel
            // 
            this.nofSlotsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.nofSlotsLabel.AutoSize = true;
            this.nofSlotsLabel.Location = new System.Drawing.Point(719, 148);
            this.nofSlotsLabel.Name = "nofSlotsLabel";
            this.nofSlotsLabel.Size = new System.Drawing.Size(13, 13);
            this.nofSlotsLabel.TabIndex = 7;
            this.nofSlotsLabel.Text = "1";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(647, 191);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Generate";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // fileNameTextBox
            // 
            this.fileNameTextBox.Location = new System.Drawing.Point(648, 165);
            this.fileNameTextBox.Name = "fileNameTextBox";
            this.fileNameTextBox.Size = new System.Drawing.Size(84, 20);
            this.fileNameTextBox.TabIndex = 9;
            this.fileNameTextBox.Text = "../../example.txt";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.openToolStripMenuItem,
            this.clearToolStripMenuItem,
            this.loadSolutionToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(744, 24);
            this.menuStrip1.TabIndex = 11;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            this.clearToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.clearToolStripMenuItem.Text = "Clear";
            this.clearToolStripMenuItem.Click += new System.EventHandler(this.clearToolStripMenuItem_Click);
            // 
            // loadSolutionToolStripMenuItem
            // 
            this.loadSolutionToolStripMenuItem.Name = "loadSolutionToolStripMenuItem";
            this.loadSolutionToolStripMenuItem.Size = new System.Drawing.Size(92, 20);
            this.loadSolutionToolStripMenuItem.Text = "Load Solution";
            this.loadSolutionToolStripMenuItem.Click += new System.EventHandler(this.loadSolutionToolStripMenuItem_Click);
            // 
            // trackBarDistance
            // 
            this.trackBarDistance.Location = new System.Drawing.Point(648, 220);
            this.trackBarDistance.Maximum = 1000;
            this.trackBarDistance.Name = "trackBarDistance";
            this.trackBarDistance.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBarDistance.Size = new System.Drawing.Size(45, 104);
            this.trackBarDistance.TabIndex = 12;
            this.trackBarDistance.Scroll += new System.EventHandler(this.trackBarDistance_Scroll);
            // 
            // checkBoxName
            // 
            this.checkBoxName.AutoSize = true;
            this.checkBoxName.Checked = true;
            this.checkBoxName.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxName.Location = new System.Drawing.Point(647, 77);
            this.checkBoxName.Name = "checkBoxName";
            this.checkBoxName.Size = new System.Drawing.Size(59, 17);
            this.checkBoxName.TabIndex = 14;
            this.checkBoxName.Text = "Names";
            this.checkBoxName.UseVisualStyleBackColor = true;
            // 
            // checkBoxRadius
            // 
            this.checkBoxRadius.AutoSize = true;
            this.checkBoxRadius.Location = new System.Drawing.Point(679, 261);
            this.checkBoxRadius.Name = "checkBoxRadius";
            this.checkBoxRadius.Size = new System.Drawing.Size(53, 17);
            this.checkBoxRadius.TabIndex = 15;
            this.checkBoxRadius.Text = "Show";
            this.checkBoxRadius.UseVisualStyleBackColor = true;
            this.checkBoxRadius.CheckedChanged += new System.EventHandler(this.checkBoxRadius_CheckedChanged);
            // 
            // placementView
            // 
            this.placementView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.placementView.Location = new System.Drawing.Point(0, 27);
            this.placementView.Name = "placementView";
            this.placementView.ShowActivations = true;
            this.placementView.ShowConnections = true;
            this.placementView.ShowNames = true;
            this.placementView.ShowRadius = false;
            this.placementView.Size = new System.Drawing.Size(641, 301);
            this.placementView.TabIndex = 13;
            // 
            // PlacementForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 353);
            this.Controls.Add(this.checkBoxRadius);
            this.Controls.Add(this.checkBoxName);
            this.Controls.Add(this.placementView);
            this.Controls.Add(this.trackBarDistance);
            this.Controls.Add(this.fileNameTextBox);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.nofSlotsLabel);
            this.Controls.Add(this.nofSlotsBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.checkBoxConnections);
            this.Controls.Add(this.checkBoxActivation);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "PlacementForm";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarDistance)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBoxActivation;
        private System.Windows.Forms.CheckBox checkBoxConnections;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox nofSlotsBox;
        private System.Windows.Forms.Label nofSlotsLabel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox fileNameTextBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadSolutionToolStripMenuItem;
        private System.Windows.Forms.TrackBar trackBarDistance;
        private PlacementView placementView;
        private System.Windows.Forms.CheckBox checkBoxName;
        private System.Windows.Forms.CheckBox checkBoxRadius;
    }
}

