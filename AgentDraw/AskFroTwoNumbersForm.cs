﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AgentDraw
{
    public partial class AskForTwoNumbersForm : AgentDraw.AskForNumberForm
    {
        public AskForTwoNumbersForm()
        {
            InitializeComponent();
        }

        public AskForTwoNumbersForm(string title)
            : base(title)
        {
            InitializeComponent();
        }

        private void textBoxEval2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
                this.Close();
        }
    }
}
