﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace AgentDraw
{
    public class PlacementController : AgentController
    {

        public PlacementController(AgentModel model, PlacementProg program) :
            base(model, program) { }

        public override void MouseClickEventHandler(object sender, MouseEventArgs e)
        {
            if (sender is PlacementView)
            {
                switch (e.Button)
                {
                    case System.Windows.Forms.MouseButtons.Left:
                        model.AddTask(new NodeModel(e.Location, true));
                        break;
                    case System.Windows.Forms.MouseButtons.Right:
                        NodeModel newUav = new NodeModel(e.Location);
                        model.AddUAV(newUav);
                        (model as PlacementModel).ConnectUAV(newUav);
                        break;
                }
            }
            else if (sender is UAV)
            {
                switch (e.Button)
                {
                    case System.Windows.Forms.MouseButtons.Right:
                        if (e.Clicks > 1)
                            model.RemoveNode(sender as UAV);
                        break;
                    case System.Windows.Forms.MouseButtons.Left:
                        if (!(model as PlacementModel).UnconnectUAV(sender as UAV))
                            (model as PlacementModel).ConnectUAV((sender as UAV).Model);
                        break;
                }
            }
            else if (sender is Task)
            {
                switch (e.Button)
                {
                    case System.Windows.Forms.MouseButtons.Right:
                        if (e.Clicks > 1)
                            model.RemoveNode(sender as Task);
                        else
                            (model as PlacementModel).ActivateFrom(sender as Task);
                        break;
                    case System.Windows.Forms.MouseButtons.Left:
                        if (e.Clicks > 1)
                            model.ClearSelected();
                        else
                        {
                            model.ToggleSelection(sender as Node);
                        }
                        break;
                }
            }
        }

        public override void Reset()
        {
        }
    }
}
