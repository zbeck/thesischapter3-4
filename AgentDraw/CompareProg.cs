﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace AgentDraw
{
    public class CompareProg
    {
        List<CompareElement> comparsions;
        static string problemListPath = String.Concat(AgentProg.WorkFolder, "compare/");
        static string tmpFolder = "compareTmp/";
        Dictionary<int, Form> forms;

        Color[] bgColors;

        public List<ListViewGroup> ListViewGroups
        {
            get
            {
                List<ListViewGroup> l = new List<ListViewGroup>(comparsions.Count);
                foreach (CompareElement c in comparsions)
                {
                    l.Add(c.Group);
                }
                return l;
            }
        }

        public List<ListViewItem> ListViewItems
        {
            get
            {
                List<ListViewItem> l = new List<ListViewItem>();
                foreach (CompareElement c in comparsions)
                {
                    List<ListViewItem> group = c.getListViewItemList();
                    for (int i = 1; i < group.Count; i++)
                    {
                        group[i].BackColor = bgColors[c.ErrorLevel(i-1)];
                    }
                    l.AddRange(group);
                }
                return l;
            }
        }

        public int[] TravelTime
        {
            get
            {
                if (comparsions.Count > 0)
                    return comparsions[0].TravelTime;
                else
                    return new int[] { 100, 100 };
            }
            set
            {
                foreach (CompareElement e in comparsions)
                {
                    e.TravelTime = value;
                }
            }
        }

        public double[] Speed
        {
            get
            {
                if (comparsions.Count > 0)
                    return comparsions[0].Speed;
                else
                    return new double[] { 1.0, 1.0 };
            }
            set
            {
                foreach (CompareElement e in comparsions)
                {
                    e.Speed = value;
                }
            }
        }

        public CompareProg()
        {
            comparsions = new List<CompareElement>();
            bgColors = new Color[3];
            bgColors[0]=Color.White;
            bgColors[1]=Color.FromArgb(255, 187, 46);   //honey glow color (yellowish)
            bgColors[2]=Color.FromArgb(232, 0, 46);     //Malefactor color (quite red)
            List<string> fileNames;
            System.IO.StreamReader fs = new System.IO.StreamReader(String.Concat(problemListPath, "filelist.xml"));
            XmlSerializer x = new XmlSerializer(typeof(List<string>));
            fileNames = x.Deserialize(fs) as List<string>;
            foreach (string file in fileNames)
            {
                int nameBegin = file.LastIndexOfAny(new char[] { '/', '\\' }) + 1;
                int nameEnd = file.LastIndexOf('.');
                string name;
                if (nameEnd > nameBegin)
                    name = file.Substring(nameBegin, nameEnd - nameBegin);
                else
                    name = file;
                comparsions.Add(new CompareElement(name, new FileStream(String.Concat(problemListPath, file), FileMode.Open, FileAccess.Read)));
            }
            forms = new Dictionary<int, Form>();
            string tmpF = String.Concat(AgentProg.WorkFolder, tmpFolder);
            if(!Directory.Exists(tmpF))
                Directory.CreateDirectory(tmpF);
        }

        internal void ChangeSelected(ListView.SelectedIndexCollection selected)
        {
            int index=0;
            foreach(CompareElement c in comparsions)
                for (int i = 0; i < c.NofItems; i++)
                {
                    if (selected.Contains(index))
                    {
                        if (!forms.ContainsKey(index))
                            forms.Add(index, c.GetForm(i));
                        if (forms[index].IsDisposed)
                            forms[index] = c.GetForm(i);
                        forms[index].Show();
                    }
                    else
                    {
                        if (forms.ContainsKey(index))
                            forms[index].Hide();
                    }
                    index++;
                }
        }

        internal void Solve(string solver, double[] speed, ProgressBar pBar)
        {
            pBar.Value = 0;
            pBar.Visible = true;
            CloseWindows();
            forms.Clear();

            string solverFolderName = solver.Replace(';', '!').Replace('>', '-').Replace(' ', '_').Replace('.', '-');
            string solutionFolder = String.Concat(problemListPath, solverFolderName, "/");
            bool overwriteSolutions = false;
            if (Directory.Exists(solutionFolder))
            {
                if (Directory.EnumerateFiles(solutionFolder).Count<string>() > 0)
                {
                    DialogResult res = MessageBox.Show(String.Format("The folder {0} already contains some solutions. Would you like to RECALCULATE those solutions?", solverFolderName), "Solutions Exist", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                    if (res == DialogResult.Cancel)
                        return;
                    else if (res == DialogResult.Yes)
                        overwriteSolutions = true;
                }
            }
            else
                Directory.CreateDirectory(solutionFolder);

            for (int i = 0; i < comparsions.Count; i++)
            {
                comparsions[i].Solve(solver, tmpFolder, AgentProg.WorkFolder, speed, solutionFolder, overwriteSolutions);
                pBar.Value = 100 * (i+1) / comparsions.Count;
            }
        }

        internal void CloseWindows()
        {
            foreach (KeyValuePair<int, Form> e in forms)
                e.Value.Close();
        }

        internal void Test()
        {
            List<string> fileNames;
            System.IO.StreamReader fs = new System.IO.StreamReader(String.Concat(problemListPath, "filelist.xml"));
            XmlSerializer x = new XmlSerializer(typeof(List<string>));
            fileNames = x.Deserialize(fs) as List<string>;
            for(int i=0; i<comparsions.Count; i++)
            {
                string file = fileNames[i];
                int nameBegin = file.LastIndexOfAny(new char[] { '/', '\\' }) + 1;
                int nameEnd = file.LastIndexOf('.');
                string name;
                if (nameEnd > nameBegin)
                    name = file.Substring(nameBegin, nameEnd - nameBegin);
                else
                    name = file;

                //comparsions[i].SetDefaultParams();
                //comparsions[i].RevertActivations();
                //comparsions[i].Save(new FileStream(String.Concat(problemListPath, file), FileMode.Open, FileAccess.Write));
                using (StreamWriter sw = new StreamWriter(String.Concat(problemListPath, name, "_graph.txt")))
                {
                    comparsions[i].GenerateOutput(sw, true);
                }
            }

        }
    }
}
