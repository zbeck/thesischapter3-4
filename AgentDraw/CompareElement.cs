﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Windows.Forms;
using MyExtensions;

namespace AgentDraw
{
    public class CompareElement
    {
        string id;
        public string ID { get { return id; } }
        GeneratorModel problem;
        List<SolutionModel> solution;
        ListViewGroup group;
        public ListViewGroup Group { get { return group; } }
        public int NofItems { get { return solution.Count + 1; } }
        
        public int[] TravelTime {
            get
            {
                return problem.TravelTime;
            }
            set
            {
                for (int i = 0; i < value.Length; i++)
                    problem.SetTravelTime(value[i], i);
            }
        }

        public double[] Speed
        {
            get
            {
                return problem.Speed;
            }
            set
            {
                problem.Speed = value;
            }
        }

        public CompareElement(string id, System.IO.Stream fs)
        {
            solution = new List<SolutionModel>();
            this.id = id;
            group = new ListViewGroup(id);
            IFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            problem = formatter.Deserialize(fs) as GeneratorModel;
            fs.Close();
        }

        static string[] GetItems(SolutionModel m)
        {
            return new string[] { m.Solver, m.TaskCompletitionRatioLow.ToString("P"),
                m.TaskCompletitionRatioHigh.ToString("P"), m.TaskCompletitionRatioFirstSlot.ToString("P"),
                m.DiscoveryTime.ToString("F1"), m.ExecutionTime.ToString("F1"), m.SumTime.ToString("F1"), m.LowUtil.ToString("F1"), m.Util.ToString("F1") };
        }

        public int ErrorLevel(int solutionIndex)
        {
            return solution[solutionIndex].ErrorLevel;
        }

        public List<ListViewItem> getListViewItemList()
        {
            List<ListViewItem> retval = new List<ListViewItem>(solution.Count+1);
            retval.Add(new ListViewItem("Problem", this.Group));
            foreach (SolutionModel s in this.solution)
            {
                retval.Add(new ListViewItem(GetItems(s), this.Group));
            }
            return retval;
        }


        public void Solve(string solvers, string tmpdir, string workFolder, double[] speeds, string solutionFolder, bool overwrite)
        {
            string output = "";
            string solutionFile = String.Concat(solutionFolder, "solution_", id, ".txt");
            if (File.Exists(solutionFile) && !overwrite) //solution file exists and we want to read it
                using (StreamReader sFile = new StreamReader(solutionFile))
                {
                    output = sFile.ReadToEnd();
                }
            else
            { //run the script
                problem.Speed = speeds;
                string outFile = String.Concat(tmpdir, "compareProblem_", id, ".txt"); //the output of the most recent script (which might give the input of the new one
                bool inputGenerated = false; //indicates if the model output has to be generated or not
                string dataFile;
                foreach (string slvr in solvers.Split(';'))  //parse given solvers
                {
                    dataFile = outFile;
                    string[] solverParts = slvr.Trim().Split('>');
                    string solver = solverParts[0].Trim();
                    if (solver == "")
                        continue;
                    if (solverParts.Length > 1)
                        outFile = String.Concat(dataFile.Split('.')[0], ".", solverParts[1].Trim());
                    else
                        outFile = null;
                    if (!AgentProg.ValidSolver(solver))
                    {
                        MessageBox.Show(String.Format("Cannot find specified solver script: \"{0}\"", solver), "Solver error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    if (!inputGenerated)
                        using (TextWriter tw = new StreamWriter(String.Concat(workFolder, dataFile)))
                        {
                            problem.GenerateProblemOutput(tw);
                            inputGenerated = true;
                        }
                    //execute python script for solution
                    output = MyExtensionMethods.RunScript(solver, workFolder, dataFile);
                    if (outFile == null)
                        using (StreamWriter sFile = new StreamWriter(solutionFile, false))
                        {
                            sFile.Write(output);
                        }
                }
            }
            //extract final solution
            try
            {
                solution.Add(problem.ReadSolution(output, solvers));
            }
            catch (Exception e)
            {
                Console.WriteLine("Solution compile failed!");
                Console.WriteLine(e);
            }
        }

        public Solution GetForm(int index)
        {
            if (index == 0)
                return new Solution(problem);
            else
                return new Solution(solution[index-1]);
        }

        internal void GenerateOutput(TextWriter tw, bool oldformat = false)
        {
            if (oldformat)
                problem.GenerateMaxSumOutput(tw);
            else
                problem.GenerateProblemOutput(tw);
        }

        internal void Save(FileStream fs)
        {
            IFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            formatter.Serialize(fs, problem);
        }

        internal void SetDefaultParams()
        {
            problem.SetDefaultParams();
        }

        internal void RevertActivations()
        {
            problem.RevertActivations();
        }
    }
}
