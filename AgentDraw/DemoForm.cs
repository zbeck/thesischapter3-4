﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AgentDraw
{
    public partial class DemoForm : Form
    {
        string folderPath = "D:\\ORCHID\\Showcase\\Demo";
        int run = 4;
        int nSamples = 100;
        int timeStep = 10;
        bool playing = false;
        bool interaction = false;
        SimFormMinimal[] simForms = {};
        List<System.IO.FileStream> openStreams;
        System.IO.FileStream bgStream;
        int frameCounter = 0;
        bool refreshBelief = false;
        
        public DemoForm()
        {
            InitializeComponent();
            comboBoxScenario.SelectedIndex = 0;
            textBoxRunSelect.Text = run.ToString();
            textBoxNSamples.Text = nSamples.ToString();
            textBoxTimeStep.Text = ((double)timeStep / 10.0).ToString();
            openStreams = new List<System.IO.FileStream>(3);
        }

        private void setPathToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderDialog = new FolderBrowserDialog();
            folderDialog.ShowNewFolderButton = false;
            if (folderDialog.ShowDialog() == DialogResult.OK)
                folderPath = folderDialog.SelectedPath;
        }

        private enum FileType
        {
            Preliminary,
            PreliminaryBG,
            GroundTruth,
            GroundTruthBG,
            Gradient,
            Satellite,
            Model
        }
        private string getPath(FileType type)
        {
            string path = folderPath;
            switch (type)
            {
                case FileType.Preliminary:
                    if (comboBoxScenario.SelectedIndex == 0)
                        return path + "\\scale_pre_haiti10_v-20_c.1.png";
                    else
                        return path + "\\pre_ajka6_10pc.png";
                case FileType.PreliminaryBG:
                    if (comboBoxScenario.SelectedIndex == 0)
                        return path + "\\scale_pre_haiti10_v-20_c.1_bg.png";
                    else
                        return path + "\\pre_ajka6_10pc_bg.png";
                case FileType.GroundTruth:
                    if (comboBoxScenario.SelectedIndex == 0)
                        return path + "\\scale_gt_haiti10_2-4-10-40-125_E-61.062_1pxPm.png";
                    else
                        return path + "\\gt_ajka6_10pc.png";
                case FileType.GroundTruthBG:
                    if (comboBoxScenario.SelectedIndex == 0)
                        return path + "\\scale_gt_haiti10_2-4-10-40-125_E-61.062_1pxPm_bg.png";
                    else
                        return path + "\\gt_ajka6_10pc_bg.png";
                case FileType.Gradient:
                    if (comboBoxScenario.SelectedIndex == 0)
                        return path + "\\scale_pre_haiti10_v-20_c.1_gradient.png";
                    else
                        return path + "\\pre_ajka6_10pc_gradient.png";
                case FileType.Satellite:
                    if (comboBoxScenario.SelectedIndex == 0)
                        return path + "\\scale___map_haiti10.png";
                    else
                        return path + "\\earth_ajka6.png";
                case FileType.Model:
                    if (comboBoxScenario.SelectedIndex == 0)
                        return path + "\\haitiModels11-61tasks\\scaledmodel" + run.ToString("D3") +".mus";
                    else
                        return path + "\\ajkaModels07pre-20tasks\\model" + run.ToString("D3") + ".mus";
                default:
                    return path;
            }
        }

        private int enTasks {
            get
            {
                if (comboBoxScenario.SelectedIndex == 0)
                    return 61;
                else
                    return 20;
            }
        }

        private void textBoxRunSelect_Leave(object sender, EventArgs e)
        {
            try
            {
                run = Int32.Parse(textBoxRunSelect.Text);
            }
            catch (FormatException ex) { ex.ToString(); }
            finally
            {
                textBoxRunSelect.Text = run.ToString();
            }
        }

        private void textBoxNSamples_Leave(object sender, EventArgs e)
        {
            try
            {
                nSamples = Int32.Parse(textBoxNSamples.Text);
            }
            catch (FormatException ex) { ex.ToString(); }
            finally
            {
                textBoxNSamples.Text = nSamples.ToString();
            }
        }

        private void textBoxTimeStep_Leave(object sender, EventArgs e)
        {
            try
            {
                int tStep = (int)(Double.Parse(textBoxTimeStep.Text) * 10.0);
                if (tStep < 1)
                    throw new FormatException();
                if (tStep > 9000)
                    throw new FormatException();
                timeStep = tStep;
            }
            catch (FormatException ex) { ex.ToString(); }
            finally
            {
                textBoxTimeStep.Text = ((double)timeStep/10.0).ToString();
                SimController.timeStep = timeStep;
            }
        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            if (playing)
                return;
            if (folderPath == "")
                return;
            UnloadForms();
            simForms = new SimFormMinimal[] { new SimFormMinimal(interaction), new SimFormMinimal(interaction), new SimFormMinimal(interaction) };
            string[] simFormNames = { "State of The Art", "Hindsight Planning", "Joint Planning" };
            int[] nofSlotParameters = { -1, 0, -5 };
            SimController.timeStep = timeStep;
            if (bgStream != null)
                bgStream.Close();
            var gradient = new System.IO.FileStream(getPath(FileType.Gradient), System.IO.FileMode.Open);
            openStreams.Add(gradient);
            using (var fspre = new System.IO.FileStream(getPath(FileType.Preliminary), System.IO.FileMode.Open))
            {
                for (int i = 0; i < simForms.Length; i++)
                {
                    SimFormMinimal form = simForms[i];
                    form.Text = simFormNames[i];
                    form.Program.OpenBitmapAsDensityMap(fspre, enTasks);
                    form.Program.OpenBitmapAsGradient(gradient);
                    using (var fsmodel = new System.IO.FileStream(getPath(FileType.Model), System.IO.FileMode.Open))
                        form.Program.OpenFile(fsmodel);
                    //if (i == 0)
                    //    form.Program.OpenBeliefSampler(new System.IO.FileStream(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\haitiBlankSampler.sam", System.IO.FileMode.Open));
                    //if (i == 1)
                    //    form.Program.OpenBeliefSampler(new System.IO.FileStream(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\haiti10pcPreSampler.sam", System.IO.FileMode.Open));
                    //if (i == 2)
                    //    form.Program.OpenBeliefSampler(new System.IO.FileStream(@"d:\VisualStudio\AgentDraw\IRIDIS\relationTest\haitiGTSampler.sam", System.IO.FileMode.Open));
                    form.Program.NofSlots = nofSlotParameters[i];
                    form.InitProgress();
                    form.Show();
                }
                simForms.Last().Program.playTimer.Tick += playTimer_Tick;
            }
            updateBg();
            playing = false;
            this.BringToFront();
        }

        void playTimer_Tick(object sender, EventArgs e)
        {
            foreach (var form in simForms)
            {
                if (radioButtonBgBelief.Checked && refreshBelief)
                    form.Program.RefreshBitmap();
            }
            foreach (var form in simForms)
            {
                Bitmap bmp = form.GrabFrame();
                bmp.Save(folderPath + "\\frames\\" + form.Text + frameCounter.ToString("D4") + ".png");
            }
            frameCounter++;
        }

        private void UnloadForms()
        {
            if (playing)
                buttonLoad_Click(this, null);
            foreach (var form in simForms)
                form.Close();
            foreach (var fs in openStreams)
                fs.Close();
            openStreams.Clear();
        }

        private void buttonPlay_Click(object sender, EventArgs e)
        {
            if (simForms.Length == 0)
            {
                buttonPlay.Text = "Play";
                return;
            }
            if (playing)
            {
                buttonPlay.Text = "Play";
                foreach (var form in simForms)
                    form.Program.Stop();
            }
            else
            {
                buttonPlay.Text = "Stop";
                foreach (var form in simForms)
                    form.Program.Play();
            }
            playing = !playing;
        }

        private void buttonStep_Click(object sender, EventArgs e)
        {
            foreach (var form in simForms)
                form.Program.Step();
        }

        private void DemoForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            UnloadForms();
        }

        private void enableInteractionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (interaction)
            {
                enableInteractionToolStripMenuItem.Text = "Enable Interaction";
            }
            else
            {
                enableInteractionToolStripMenuItem.Text = "Disable Interaction";
            }
            interaction = !interaction;
            if(simForms.Length >0)
                buttonLoad_Click(this, null);
        }

        private void fromStateOfTheArtToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateModels(0);
        }

        private void fromJointApproachToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateModels(1);
        }

        private void UpdateModels(int source)
        {
            for (int i = 0; i < simForms.Length; i++)
            {
                simForms[i].Program.Model = simForms[source].Program.Model;
                simForms[i].Invalidate();
            }
        }

        private void radioButtonBgSatellite_CheckedChanged(object sender, EventArgs e)
        {
            updateBg();
        }

        private void radioButtonBgBelief_CheckedChanged(object sender, EventArgs e)
        {
            refreshBelief = false;
            updateBg();
        }

        private void radioButtonBgGTIntensity_CheckedChanged(object sender, EventArgs e)
        {
            updateBg();
        }

        private void updateBg()
        {
            if (bgStream != null)
                bgStream.Close();
            if (radioButtonBgSatellite.Checked)
            {
                bgStream = new System.IO.FileStream(getPath(FileType.Satellite), System.IO.FileMode.Open);
            }
            else if (radioButtonBgGTIntensity.Checked)
            {
                bgStream = new System.IO.FileStream(getPath(FileType.GroundTruthBG), System.IO.FileMode.Open);
            }
            else if (radioButtonBgBelief.Checked)
            {
                bgStream = new System.IO.FileStream(getPath(FileType.PreliminaryBG), System.IO.FileMode.Open);
            }
            foreach (var form in simForms)
            {
                form.Program.OpenBitmapAsBackground(bgStream);
            }
        }

        private void buttonRefreshBelief_Click(object sender, EventArgs e)
        {
            refreshBelief = true;
            /*if (radioButtonBgBelief.Checked)
            {
                foreach (var form in simForms)
                {
                    form.Program.RefreshBitmap();
                }
            }*/
        }

        private void grabFrameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var form in simForms)
            {
                Bitmap bmp = form.GrabFrame();
                bmp.Save(folderPath + "\\" + form.Text + ".jpg");
            }
        }


    }
}
