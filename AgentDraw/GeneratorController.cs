﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AgentDraw
{
    class GeneratorController: AgentController
    {
        public GeneratorController(AgentModel model, AgentProg program) :
            base(model, program)
        { }

        public override void MouseClickEventHandler(object sender, MouseEventArgs e)
        {
            GeneratorModel model = this.model as GeneratorModel;
            if (sender is PlacementView)
            {
                switch (e.Button)
                {
                    case System.Windows.Forms.MouseButtons.Left:
                        model.AddDiscoveryTask(new NodeModel(e.Location));
                        break;
                    case System.Windows.Forms.MouseButtons.Right:
                        NodeModel newUav = new NodeModel(e.Location);
                        model.AddLoUAV(newUav);
                        break;
                }
            }
            else if (sender is UAV)
            {
                UAV node = sender as UAV;
                switch (e.Button)
                {
                    case System.Windows.Forms.MouseButtons.Right:
                        if (e.Clicks > 1)
                            model.RemoveNode(node);
                        break;
                    case System.Windows.Forms.MouseButtons.Left:
                        model.RemoveNode(node);
                        NodeModel newUav = new NodeModel(node.Position);
                        model.AddHiUAV(newUav);
                        break;
                }
            }
            else if (sender is Task)
            {
                Task node = sender as Task;
                switch (e.Button)
                {
                    case System.Windows.Forms.MouseButtons.Right:
                        if (e.Clicks > 1)
                            model.RemoveNode(node);
                        break;
                    case System.Windows.Forms.MouseButtons.Left:
                        model.RemoveNode(node);
                        NodeModel newTask = new NodeModel(node.Position);
                        model.AddSampledTask(newTask);
                        break;
                }
            }
        }

        public override void Reset()
        {
        }
    }
}
