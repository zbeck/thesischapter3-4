﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AgentDraw
{
    public partial class SimRunnerForm : Form
    {
        SimProg program;
        public SimRunnerForm(SimProg prog)
        {
            InitializeComponent();
            program = prog;
        }

        private void textBoxSolver_TextChanged(object sender, EventArgs e)
        {
            if (AgentProg.ValidSolver(textBoxSolver.Text))
                labelSolver.Text = "Solver - OK";
            else
                labelSolver.Text = "Solver - not valid";
        }

        private void buttonRun_Click(object sender, EventArgs e)
        {
            richTextBoxResult.Text = program.RunSimulationNoUI(textBoxSolver.Text, (int)numericUpDownSampleNum.Value, (int)numericUpDownTimeStep.Value, progressBar1, checkBoxSampling.Checked);
        }

        private void checkBoxSampling_CheckedChanged(object sender, EventArgs e)
        {
            numericUpDownSampleNum.ReadOnly = !checkBoxSampling.Checked;
        }
    }
}
