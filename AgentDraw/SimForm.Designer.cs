﻿namespace AgentDraw
{
    partial class SimForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveXMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.batchSaveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadBitmapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshBitmapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearNondiscoveredToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveBitmapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openSamplerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stepToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.playToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setSpeedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setUAVSpeedsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.solveWithoutUIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.discoveryProblemOutputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.solveDiscoveryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetSpeedsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.solveGroundTruthToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setDefaultParametersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addDiscoveryGridToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeEmptyDiscoveriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sampleNumTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.nofSlotsBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.checkBoxName = new System.Windows.Forms.CheckBox();
            this.checkBoxConnections = new System.Windows.Forms.CheckBox();
            this.checkBoxActivation = new System.Windows.Forms.CheckBox();
            this.checkBoxSolutions = new System.Windows.Forms.CheckBox();
            this.checkBoxRadius = new System.Windows.Forms.CheckBox();
            this.trackBarDistance0 = new System.Windows.Forms.TrackBar();
            this.checkBoxDiscovery = new System.Windows.Forms.CheckBox();
            this.trackBarDistance1 = new System.Windows.Forms.TrackBar();
            this.placementView = new AgentDraw.PlacementView();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarDistance0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarDistance1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.runToolStripMenuItem,
            this.modelToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(620, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.saveXMLToolStripMenuItem,
            this.openToolStripMenuItem,
            this.batchSaveToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveXMLToolStripMenuItem
            // 
            this.saveXMLToolStripMenuItem.Name = "saveXMLToolStripMenuItem";
            this.saveXMLToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.saveXMLToolStripMenuItem.Text = "Save XML";
            this.saveXMLToolStripMenuItem.Click += new System.EventHandler(this.saveXMLToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // batchSaveToolStripMenuItem
            // 
            this.batchSaveToolStripMenuItem.Name = "batchSaveToolStripMenuItem";
            this.batchSaveToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.batchSaveToolStripMenuItem.Text = "Batch Save";
            this.batchSaveToolStripMenuItem.Click += new System.EventHandler(this.batchSaveToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadBitmapToolStripMenuItem,
            this.refreshBitmapToolStripMenuItem,
            this.clearNondiscoveredToolStripMenuItem,
            this.saveBitmapToolStripMenuItem,
            this.openSamplerToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.editToolStripMenuItem.Text = "Bitmap";
            // 
            // loadBitmapToolStripMenuItem
            // 
            this.loadBitmapToolStripMenuItem.Name = "loadBitmapToolStripMenuItem";
            this.loadBitmapToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.loadBitmapToolStripMenuItem.Text = "Load Bitmap";
            this.loadBitmapToolStripMenuItem.Click += new System.EventHandler(this.loadBitmapToolStripMenuItem_Click);
            // 
            // refreshBitmapToolStripMenuItem
            // 
            this.refreshBitmapToolStripMenuItem.Name = "refreshBitmapToolStripMenuItem";
            this.refreshBitmapToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.refreshBitmapToolStripMenuItem.Text = "Refresh Bitmap";
            this.refreshBitmapToolStripMenuItem.Click += new System.EventHandler(this.refreshBitmapToolStripMenuItem_Click);
            // 
            // clearNondiscoveredToolStripMenuItem
            // 
            this.clearNondiscoveredToolStripMenuItem.Name = "clearNondiscoveredToolStripMenuItem";
            this.clearNondiscoveredToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.clearNondiscoveredToolStripMenuItem.Text = "Clear non-discovered";
            this.clearNondiscoveredToolStripMenuItem.Click += new System.EventHandler(this.clearNondiscoveredToolStripMenuItem_Click);
            // 
            // saveBitmapToolStripMenuItem
            // 
            this.saveBitmapToolStripMenuItem.Name = "saveBitmapToolStripMenuItem";
            this.saveBitmapToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.saveBitmapToolStripMenuItem.Text = "Save Sampler";
            this.saveBitmapToolStripMenuItem.Click += new System.EventHandler(this.saveBitmapToolStripMenuItem_Click);
            // 
            // openSamplerToolStripMenuItem
            // 
            this.openSamplerToolStripMenuItem.Name = "openSamplerToolStripMenuItem";
            this.openSamplerToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.openSamplerToolStripMenuItem.Text = "Open Sampler";
            this.openSamplerToolStripMenuItem.Click += new System.EventHandler(this.openSamplerToolStripMenuItem_Click);
            // 
            // runToolStripMenuItem
            // 
            this.runToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stepToolStripMenuItem,
            this.playToolStripMenuItem,
            this.stopToolStripMenuItem,
            this.resetToolStripMenuItem,
            this.setSpeedToolStripMenuItem});
            this.runToolStripMenuItem.Name = "runToolStripMenuItem";
            this.runToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.runToolStripMenuItem.Text = "Run";
            this.runToolStripMenuItem.Click += new System.EventHandler(this.runToolStripMenuItem_Click);
            // 
            // stepToolStripMenuItem
            // 
            this.stepToolStripMenuItem.Name = "stepToolStripMenuItem";
            this.stepToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.stepToolStripMenuItem.Text = "Step";
            this.stepToolStripMenuItem.Click += new System.EventHandler(this.stepToolStripMenuItem_Click);
            // 
            // playToolStripMenuItem
            // 
            this.playToolStripMenuItem.Name = "playToolStripMenuItem";
            this.playToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.playToolStripMenuItem.Text = "Play";
            this.playToolStripMenuItem.Click += new System.EventHandler(this.playToolStripMenuItem_Click);
            // 
            // stopToolStripMenuItem
            // 
            this.stopToolStripMenuItem.Name = "stopToolStripMenuItem";
            this.stopToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.stopToolStripMenuItem.Text = "Stop";
            this.stopToolStripMenuItem.Click += new System.EventHandler(this.stopToolStripMenuItem_Click);
            // 
            // resetToolStripMenuItem
            // 
            this.resetToolStripMenuItem.Name = "resetToolStripMenuItem";
            this.resetToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.resetToolStripMenuItem.Text = "Reset";
            this.resetToolStripMenuItem.Click += new System.EventHandler(this.resetToolStripMenuItem_Click);
            // 
            // setSpeedToolStripMenuItem
            // 
            this.setSpeedToolStripMenuItem.Name = "setSpeedToolStripMenuItem";
            this.setSpeedToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.setSpeedToolStripMenuItem.Text = "Set Speed";
            this.setSpeedToolStripMenuItem.Click += new System.EventHandler(this.setSpeedToolStripMenuItem_Click);
            // 
            // modelToolStripMenuItem
            // 
            this.modelToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setUAVSpeedsToolStripMenuItem,
            this.solveWithoutUIToolStripMenuItem,
            this.discoveryProblemOutputToolStripMenuItem,
            this.solveDiscoveryToolStripMenuItem,
            this.resetSpeedsToolStripMenuItem,
            this.solveGroundTruthToolStripMenuItem,
            this.setDefaultParametersToolStripMenuItem,
            this.addDiscoveryGridToolStripMenuItem,
            this.removeEmptyDiscoveriesToolStripMenuItem});
            this.modelToolStripMenuItem.Name = "modelToolStripMenuItem";
            this.modelToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.modelToolStripMenuItem.Text = "Model";
            // 
            // setUAVSpeedsToolStripMenuItem
            // 
            this.setUAVSpeedsToolStripMenuItem.Name = "setUAVSpeedsToolStripMenuItem";
            this.setUAVSpeedsToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.setUAVSpeedsToolStripMenuItem.Text = "Set UAV Speeds";
            this.setUAVSpeedsToolStripMenuItem.Click += new System.EventHandler(this.setUAVSpeedsToolStripMenuItem_Click);
            // 
            // solveWithoutUIToolStripMenuItem
            // 
            this.solveWithoutUIToolStripMenuItem.Name = "solveWithoutUIToolStripMenuItem";
            this.solveWithoutUIToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.solveWithoutUIToolStripMenuItem.Text = "Solve Without UI";
            this.solveWithoutUIToolStripMenuItem.Click += new System.EventHandler(this.solveWithoutUIToolStripMenuItem_Click);
            // 
            // discoveryProblemOutputToolStripMenuItem
            // 
            this.discoveryProblemOutputToolStripMenuItem.Name = "discoveryProblemOutputToolStripMenuItem";
            this.discoveryProblemOutputToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.discoveryProblemOutputToolStripMenuItem.Text = "Discovery Problem Output";
            this.discoveryProblemOutputToolStripMenuItem.Click += new System.EventHandler(this.discoveryProblemOutputToolStripMenuItem_Click);
            // 
            // solveDiscoveryToolStripMenuItem
            // 
            this.solveDiscoveryToolStripMenuItem.Name = "solveDiscoveryToolStripMenuItem";
            this.solveDiscoveryToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.solveDiscoveryToolStripMenuItem.Text = "Solve Discovery";
            this.solveDiscoveryToolStripMenuItem.Click += new System.EventHandler(this.solveDiscoveryToolStripMenuItem_Click);
            // 
            // resetSpeedsToolStripMenuItem
            // 
            this.resetSpeedsToolStripMenuItem.Name = "resetSpeedsToolStripMenuItem";
            this.resetSpeedsToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.resetSpeedsToolStripMenuItem.Text = "Reset Speeds";
            this.resetSpeedsToolStripMenuItem.Click += new System.EventHandler(this.resetSpeedsToolStripMenuItem_Click);
            // 
            // solveGroundTruthToolStripMenuItem
            // 
            this.solveGroundTruthToolStripMenuItem.Name = "solveGroundTruthToolStripMenuItem";
            this.solveGroundTruthToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.solveGroundTruthToolStripMenuItem.Text = "Solve Ground Truth";
            this.solveGroundTruthToolStripMenuItem.Click += new System.EventHandler(this.solveGroundTruthToolStripMenuItem_Click);
            // 
            // setDefaultParametersToolStripMenuItem
            // 
            this.setDefaultParametersToolStripMenuItem.Name = "setDefaultParametersToolStripMenuItem";
            this.setDefaultParametersToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.setDefaultParametersToolStripMenuItem.Text = "Set Default Parameters";
            this.setDefaultParametersToolStripMenuItem.Click += new System.EventHandler(this.setDefaultParametersToolStripMenuItem_Click);
            // 
            // addDiscoveryGridToolStripMenuItem
            // 
            this.addDiscoveryGridToolStripMenuItem.Name = "addDiscoveryGridToolStripMenuItem";
            this.addDiscoveryGridToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.addDiscoveryGridToolStripMenuItem.Text = "Add Discovery Grid";
            this.addDiscoveryGridToolStripMenuItem.Click += new System.EventHandler(this.addDiscoveryGridToolStripMenuItem_Click);
            // 
            // removeEmptyDiscoveriesToolStripMenuItem
            // 
            this.removeEmptyDiscoveriesToolStripMenuItem.Name = "removeEmptyDiscoveriesToolStripMenuItem";
            this.removeEmptyDiscoveriesToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.removeEmptyDiscoveriesToolStripMenuItem.Text = "Remove Empty Discoveries";
            this.removeEmptyDiscoveriesToolStripMenuItem.Click += new System.EventHandler(this.removeEmptyDiscoveriesToolStripMenuItem_Click);
            // 
            // sampleNumTextBox
            // 
            this.sampleNumTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sampleNumTextBox.Location = new System.Drawing.Point(508, 157);
            this.sampleNumTextBox.Name = "sampleNumTextBox";
            this.sampleNumTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.sampleNumTextBox.Size = new System.Drawing.Size(100, 20);
            this.sampleNumTextBox.TabIndex = 2;
            this.sampleNumTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.sampleNumTextBox_KeyPress);
            this.sampleNumTextBox.Leave += new System.EventHandler(this.sampleNumTextBox_Leave);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.label1.Location = new System.Drawing.Point(505, 140);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Samples/step";
            // 
            // nofSlotsBox
            // 
            this.nofSlotsBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.nofSlotsBox.Location = new System.Drawing.Point(508, 205);
            this.nofSlotsBox.Name = "nofSlotsBox";
            this.nofSlotsBox.Size = new System.Drawing.Size(100, 20);
            this.nofSlotsBox.TabIndex = 21;
            this.nofSlotsBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.nofSlotsBox_KeyPress);
            this.nofSlotsBox.Leave += new System.EventHandler(this.nofSlotsBox_Leave);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(505, 188);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Number of slots";
            // 
            // checkBoxName
            // 
            this.checkBoxName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxName.AutoSize = true;
            this.checkBoxName.Location = new System.Drawing.Point(508, 73);
            this.checkBoxName.Name = "checkBoxName";
            this.checkBoxName.Size = new System.Drawing.Size(59, 17);
            this.checkBoxName.TabIndex = 27;
            this.checkBoxName.Text = "Names";
            this.checkBoxName.UseVisualStyleBackColor = true;
            this.checkBoxName.CheckedChanged += new System.EventHandler(this.checkBoxName_CheckedChanged);
            // 
            // checkBoxConnections
            // 
            this.checkBoxConnections.AccessibleDescription = "";
            this.checkBoxConnections.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxConnections.AutoSize = true;
            this.checkBoxConnections.Location = new System.Drawing.Point(508, 27);
            this.checkBoxConnections.Name = "checkBoxConnections";
            this.checkBoxConnections.Size = new System.Drawing.Size(85, 17);
            this.checkBoxConnections.TabIndex = 26;
            this.checkBoxConnections.Text = "Connections";
            this.checkBoxConnections.UseVisualStyleBackColor = true;
            this.checkBoxConnections.CheckedChanged += new System.EventHandler(this.checkBoxConnections_CheckedChanged);
            // 
            // checkBoxActivation
            // 
            this.checkBoxActivation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxActivation.AutoSize = true;
            this.checkBoxActivation.Location = new System.Drawing.Point(508, 50);
            this.checkBoxActivation.Name = "checkBoxActivation";
            this.checkBoxActivation.Size = new System.Drawing.Size(78, 17);
            this.checkBoxActivation.TabIndex = 25;
            this.checkBoxActivation.Text = "Activations";
            this.checkBoxActivation.UseVisualStyleBackColor = true;
            this.checkBoxActivation.CheckedChanged += new System.EventHandler(this.checkBoxActivation_CheckedChanged);
            // 
            // checkBoxSolutions
            // 
            this.checkBoxSolutions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxSolutions.AutoSize = true;
            this.checkBoxSolutions.Location = new System.Drawing.Point(508, 97);
            this.checkBoxSolutions.Name = "checkBoxSolutions";
            this.checkBoxSolutions.Size = new System.Drawing.Size(69, 17);
            this.checkBoxSolutions.TabIndex = 28;
            this.checkBoxSolutions.Text = "Solutions";
            this.checkBoxSolutions.UseVisualStyleBackColor = true;
            this.checkBoxSolutions.CheckedChanged += new System.EventHandler(this.checkBoxSolutions_CheckedChanged);
            // 
            // checkBoxRadius
            // 
            this.checkBoxRadius.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxRadius.AutoSize = true;
            this.checkBoxRadius.Location = new System.Drawing.Point(528, 334);
            this.checkBoxRadius.Name = "checkBoxRadius";
            this.checkBoxRadius.Size = new System.Drawing.Size(53, 17);
            this.checkBoxRadius.TabIndex = 30;
            this.checkBoxRadius.Text = "Show";
            this.checkBoxRadius.UseVisualStyleBackColor = true;
            this.checkBoxRadius.CheckedChanged += new System.EventHandler(this.checkBoxRadius_CheckedChanged);
            // 
            // trackBarDistance0
            // 
            this.trackBarDistance0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBarDistance0.Location = new System.Drawing.Point(524, 233);
            this.trackBarDistance0.Maximum = 1000;
            this.trackBarDistance0.Name = "trackBarDistance0";
            this.trackBarDistance0.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBarDistance0.Size = new System.Drawing.Size(45, 104);
            this.trackBarDistance0.TabIndex = 29;
            this.trackBarDistance0.Scroll += new System.EventHandler(this.trackBarDistance_Scroll);
            // 
            // checkBoxDiscovery
            // 
            this.checkBoxDiscovery.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxDiscovery.AutoSize = true;
            this.checkBoxDiscovery.Checked = true;
            this.checkBoxDiscovery.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxDiscovery.Location = new System.Drawing.Point(508, 120);
            this.checkBoxDiscovery.Name = "checkBoxDiscovery";
            this.checkBoxDiscovery.Size = new System.Drawing.Size(73, 17);
            this.checkBoxDiscovery.TabIndex = 31;
            this.checkBoxDiscovery.Text = "Discovery";
            this.checkBoxDiscovery.UseVisualStyleBackColor = true;
            this.checkBoxDiscovery.CheckedChanged += new System.EventHandler(this.checkBoxDiscovery_CheckedChanged);
            // 
            // trackBarDistance1
            // 
            this.trackBarDistance1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBarDistance1.Location = new System.Drawing.Point(563, 233);
            this.trackBarDistance1.Maximum = 1000;
            this.trackBarDistance1.Name = "trackBarDistance1";
            this.trackBarDistance1.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBarDistance1.Size = new System.Drawing.Size(45, 104);
            this.trackBarDistance1.TabIndex = 32;
            this.trackBarDistance1.Scroll += new System.EventHandler(this.trackBarDistance1_Scroll);
            // 
            // placementView
            // 
            this.placementView.Location = new System.Drawing.Point(12, 27);
            this.placementView.Name = "placementView";
            this.placementView.ShowActivations = true;
            this.placementView.ShowArea = false;
            this.placementView.ShowConnections = true;
            this.placementView.ShowNames = true;
            this.placementView.ShowRadius = false;
            this.placementView.Size = new System.Drawing.Size(490, 334);
            this.placementView.TabIndex = 1;
            // 
            // SimForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 373);
            this.Controls.Add(this.trackBarDistance1);
            this.Controls.Add(this.checkBoxDiscovery);
            this.Controls.Add(this.checkBoxRadius);
            this.Controls.Add(this.trackBarDistance0);
            this.Controls.Add(this.checkBoxSolutions);
            this.Controls.Add(this.checkBoxName);
            this.Controls.Add(this.checkBoxConnections);
            this.Controls.Add(this.checkBoxActivation);
            this.Controls.Add(this.nofSlotsBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.sampleNumTextBox);
            this.Controls.Add(this.placementView);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(16, 360);
            this.Name = "SimForm";
            this.Text = "SimForm";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarDistance0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarDistance1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private PlacementView placementView;
        private System.Windows.Forms.TextBox sampleNumTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nofSlotsBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBoxName;
        private System.Windows.Forms.CheckBox checkBoxConnections;
        private System.Windows.Forms.CheckBox checkBoxActivation;
        private System.Windows.Forms.CheckBox checkBoxSolutions;
        private System.Windows.Forms.CheckBox checkBoxRadius;
        private System.Windows.Forms.TrackBar trackBarDistance0;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem runToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadBitmapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stepToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem playToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshBitmapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setSpeedToolStripMenuItem;
        private System.Windows.Forms.CheckBox checkBoxDiscovery;
        private System.Windows.Forms.TrackBar trackBarDistance1;
        private System.Windows.Forms.ToolStripMenuItem modelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setUAVSpeedsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem solveWithoutUIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearNondiscoveredToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveBitmapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem discoveryProblemOutputToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem solveDiscoveryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveXMLToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem batchSaveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetSpeedsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem solveGroundTruthToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setDefaultParametersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openSamplerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addDiscoveryGridToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeEmptyDiscoveriesToolStripMenuItem;
    }
}