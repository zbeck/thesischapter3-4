﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Timers;
using TreeSearchSolver;

namespace AgentDraw
{
    public class ConsoleProg: AgentProg
    {
        public static double RealtimeMultiplier = 1.0;
        public static bool Debug = false;
        public int limitStatus;
        public SimController Controller { get { return controller as SimController; } }
        protected int samplePerStep;
        public int SamplePerStep
        {
            get { return samplePerStep; }
            set
            {
                if (value > 0)
                    samplePerStep = value;
            }
        }
        public string OutputFolder;

        public ConsoleProg(string workFolder, string tmpFolder, bool tmpAbsolute, string outputFolder) :
            base(null, null)
        {
            if (tmpAbsolute)
            {
                if (Directory.Exists(tmpFolder))
                    Directory.Delete(tmpFolder, true);
                Directory.CreateDirectory(tmpFolder);
            }
            model = new SimModel();
            controller = new SimController(model, this);
            AgentController.absoluteTmpPath = tmpAbsolute;
            AgentProg.WorkFolder = workFolder;
            controller.TmpFolder = tmpFolder;
            OutputFolder = outputFolder;
        }

        ~ConsoleProg()
        {
            if (Directory.Exists(controller.TmpFolder))
                Directory.Delete(controller.TmpFolder, true);
        }

        public void OpenBitmapAsDensityMap(System.IO.Stream fs, double eNTasks)
        {
            System.Drawing.Bitmap densityMap = new System.Drawing.Bitmap(fs);
            (controller as SimController).OpenBitmapAsDensityMap(densityMap, eNTasks);
        }

        public Sampler OpenBeliefSampler(System.IO.Stream fs)
        {
            return (controller as SimController).LoadBeliefSampler(fs);
        }

        public SimModel OpenFile(System.IO.Stream fs)
        {
            IFormatter formatter = new BinaryFormatter();
            SimModel newModel = formatter.Deserialize(fs) as SimModel;
            controller.Model = newModel;
            model = newModel;
            return newModel;
        }

        protected bool TryWrite(string text, TextWriter outFile)
        {
            try
            {
                outFile.Write(text);
                return true;
            }
            catch (System.IO.IOException)
            {
                return false;
            }
        }
        protected void SafeWrite(string text, TextWriter outFile, ref string outAlternate, ref bool forceAlternate)
        {
            if (forceAlternate)
                outAlternate += text;
            else if (!TryWrite(text, outFile))
            {
                outAlternate += text;
                forceAlternate = true;
            }
        }

        public string RunSimulationNoUI(string solver, int sampleNum, double timeStep, int ID, int solutionMode)
        {
            SimModel sModel = model as SimModel;
            samplePerStep = sampleNum;
            SimController.timeStep = timeStep;
            //Console.WriteLine(String.Format("Solver B\t{0}", (controller as SimController).Solver));
            (controller as SimController).Solver = solver;
            //Console.WriteLine(String.Format("Solver A\t{0}", (controller as SimController).Solver));
            int[] taskNumStart = new int[2] { sModel.GetGroundTruthNofTasks(0), sModel.GetGroundTruthNofTasks(1) };
            int[] batteryTime = new int[2] { sModel.TravelTime[0], sModel.TravelTime[1] };

            Stopwatch computationTime = new Stopwatch();
            Stopwatch solverTime = new Stopwatch();
            int nofSteps = 0;
            string stamp = ID.ToString();
            string statusSummary = "";
            List<double[]> utilitiesPerStep = new List<double[]>();
            if (solver == "dual")
            {
                using (TextWriter matlabOut = new StreamWriter(String.Concat(OutputFolder, "Sim", stamp, ".m"), false))
                {
                    int fixedExplorationSteps = 0;
                    bool problemChanged = true;
                    DualProblem lastProblem = null;
                    string emergencyOut = "";
                    bool IOError = false;
                    SafeWrite("TimeStepData = [...\n", matlabOut, ref emergencyOut, ref IOError);
                    while (!sModel.IsCompleted())
                    {
                        computationTime.Start();
                        List<List<double>> directions;
                        List<List<int>> assignedIds;
                        DualProblem problem;
                        if (nofSteps > 0)
                            fixedExplorationSteps = 1;
                        problem = new DualProblem(model as SimModel, (controller as SimController).BeliefSampler, lastProblem, problemChanged, fixedExplorationSteps);
                        problem.gradientSampler = (controller as SimController).GradientSampler;
                        solverTime.Start();
                        problem.Solve(solutionMode, samplePerStep, out assignedIds, out directions);
                        solverTime.Stop();
                        problemChanged = (controller as SimController).SimulateOneStepWithExternalResults(directions, assignedIds);
                        lastProblem = problem;
                        computationTime.Stop();
                        SafeWrite(sModel.GetTimestepData(), matlabOut, ref emergencyOut, ref IOError);
                        utilitiesPerStep.Add(problem.GetUtilities(new double[]{solverTime.ElapsedMilliseconds}));
                        nofSteps++;
                    }
                    SafeWrite("];\n\n", matlabOut, ref emergencyOut, ref IOError);
                    SafeWrite(sModel.GetSummaryData(), matlabOut, ref emergencyOut, ref IOError);
                    SafeWrite(MyExtensions.MyExtensionMethods.Convert2MFileVariable(utilitiesPerStep, "UtilsPerStep"), matlabOut, ref emergencyOut, ref IOError);
                    if (IOError)
                    {
                        int erCount = 0;
                        while ((erCount < 100) && (!TryWrite(emergencyOut, matlabOut)))
                            erCount++;
                    }
                    matlabOut.Close();
                }
                statusSummary = BuildStatusSummary(sampleNum, solutionMode, sModel, taskNumStart, batteryTime, computationTime, solverTime, nofSteps, statusSummary);
            }
            else if ((solver == "mcsearch") || (solver == "mcsearchUCB") ||
                (solver == "mcsearchx20") || (solver == "mcsearchUCBx20") ||
                (solver == "mcsdebug") || (solver == "mcsdebugUCB") ||
                (solver == "mcssingle") || (solver == "mcsUCBsingle") ||
                (solver == "mcsnobottom") || (solver == "mcsUCBnobottom"))
            {
                if ((solver == "mcsdebug") || (solver == "mcsdebugUCB"))
                    Debug = true;
                if ((solver == "mcsearchx20") || (solver == "mcsearchUCBx20"))
                    RealtimeMultiplier = 20.0;
                MCSearch.TimeStep = timeStep;
                //limiter timers first to initial growth
                Timer limiter = new Timer(solutionMode * 1000 * RealtimeMultiplier);
                Timer softLimiter = new Timer(limiter.Interval * 0.9);
                limiter.AutoReset = false;
                softLimiter.AutoReset = false;
                limiter.Elapsed += limiter_Elapsed;
                softLimiter.Elapsed += softLimiter_Elapsed;
                string initSummary = "";
                if (Debug)
                {
                    Timer debugTimer = new Timer(60000); //every minute
                    debugTimer.Elapsed += debugTimer_Elapsed;
                    debugTimer.AutoReset = true;
                    debugTimer.Start();
                }

                int[] levelsAssigned = new int[4] { 0, 0, 0, 0 };
                if ((solver == "mcsearchUCB") || (solver == "mcsdebugUCB") || (solver == "mcsearchUCBx20") || (solver == "mcsUCBsingle") || (solver == "mcsUCBnobottom"))
                    GroupSoltion.ExpansionPolicy = GroupSoltion.SolutionExpansionPolicyType.UCBOnly;
                if ((solver == "mcsnobottom") || (solver == "mcsUCBnobottom"))
                    TreeStructure.DisableBottomExpand = true;
                using (TextWriter matlabOut = new StreamWriter(String.Concat(OutputFolder, "Sim", stamp, ".m"), false))
                {
                    bool problemChanged = true;
                    bool firstRun = true;
                    //start with 0 fixed explotarion steps
                    int fixedExplorationSteps = 0;
                    MCSearch lastProblem = null;
                    string emergencyOut = "";
                    bool IOError = false;
                    SafeWrite("TimeStepData = [...\n", matlabOut, ref emergencyOut, ref IOError);
                    while (!sModel.IsCompleted())
                    {
                        computationTime.Start();
                        List<List<double>> directions;
                        List<List<int>> assignedIds;
                        MCSearch problem;

                        //debug output
                        if (Debug)
                        {
                            if (problemChanged)
                                Console.Write(String.Format(".{0}:{1}.",nofSteps, AbstractTreeNode.NodeCount));
                            else
                                Console.Write(String.Format(".{0}.", AbstractTreeNode.NodeCount));
                        }

                        limitStatus = 0;
                        softLimiter.Start();
                        limiter.Start();
                        solverTime.Start();
                        if (problemChanged | (AbstractTreeNode.NodeCount > 500000))   //node count limit to avoid memory issues
                            problem = new MCSearch(model as SimModel, (controller as SimController).BeliefSampler, new int[] { 10, 5, 20 }, lastProblem, fixedExplorationSteps);
                        else
                            problem = new MCSearch(lastProblem, model as SimModel, fixedExplorationSteps);
                        problem.Solve(samplePerStep, ref limitStatus, out assignedIds, out directions, ref levelsAssigned);
                        solverTime.Stop();

                        if (firstRun)
                        {   //after initial growth, set the fixed explotarion steps to all (-1) or one (default)
                            if ((solver == "mcssingle") || (solver == "mcsUCBsingle"))
                                fixedExplorationSteps = -1;
                            else
                                fixedExplorationSteps = 1;
                            limiter.Interval = timeStep * 100.0 * RealtimeMultiplier;
                            softLimiter.Interval = (int)((double)limiter.Interval * 0.9);
                            firstRun = false;
                            initSummary = "\n" + problem.PopulationSummary() + "\nInitial expansions per level\n" + MyExtensions.MyExtensionMethods.ToDelimitedString(levelsAssigned, "\t");
                            if (Debug)
                            {
                                Console.WriteLine();
                                Console.WriteLine(initSummary);
                                Console.WriteLine();
                            }
                        }

                        lastProblem = problem;
                        utilitiesPerStep.Add(problem.GetUtilities(new double[]{solverTime.ElapsedMilliseconds}));
                        problemChanged = (controller as SimController).SimulateOneStepWithExternalResults(directions, assignedIds);
                        computationTime.Stop();
                        //Console.WriteLine(nofSteps.ToString() + ": " + computationTime.ElapsedMilliseconds.ToString() + (problemChanged?" Change!":""));
                        SafeWrite(sModel.GetTimestepData(), matlabOut, ref emergencyOut, ref IOError);
                        nofSteps++;
                    }
                    SafeWrite("];\n\n", matlabOut, ref emergencyOut, ref IOError);
                    SafeWrite(sModel.GetSummaryData(), matlabOut, ref emergencyOut, ref IOError);
                    SafeWrite(MyExtensions.MyExtensionMethods.Convert2MFileVariable(utilitiesPerStep, "UtilsPerStep"), matlabOut, ref emergencyOut, ref IOError);
                    if (IOError)
                    {
                        int erCount = 0;
                        while ((erCount < 100) && (!TryWrite(emergencyOut, matlabOut)))
                            erCount++;
                    }
                    matlabOut.Close();
                }
                statusSummary = BuildStatusSummary(sampleNum, solutionMode, sModel, taskNumStart, batteryTime, computationTime, solverTime, nofSteps, statusSummary);
                statusSummary = String.Format("{0}\nOverall expansions per level\n{1}\t{2}\t{3}\t{4}", statusSummary, levelsAssigned[0], levelsAssigned[1], levelsAssigned[2], levelsAssigned[3]);
                statusSummary += initSummary;
                statusSummary += String.Format("\nFirst step utilities\n{0}\t{1}", utilitiesPerStep[0][0], utilitiesPerStep[0][1]);
            }
            else if ((solutionMode == 2) || (solutionMode >= 10)) //simple plan solution may be given int seconds of time
            {
                long solver_ms = (long)solutionMode * 1000;
                if (solutionMode == 2)
                    solver_ms = 0;
                computationTime.Start();
                (controller as SimController).SimualteSinglePlan(false, solver, CleanTmpFiles, solver_ms, String.Concat(OutputFolder, "Sim", stamp, ".m"), solverTime);
                computationTime.Stop();
                statusSummary += "Single run planning solution\n";
                statusSummary = String.Format("{0}Solver\t{1}", statusSummary, (controller as SimController).Solver);
                statusSummary = String.Format("{0}\nComputation time:\n\t{1}\nof which by solver:\n\t{2}", statusSummary, computationTime.ElapsedMilliseconds, solverTime.ElapsedMilliseconds);
            }
            else
            {
                using (TextWriter matlabOut = new StreamWriter(String.Concat(OutputFolder, "Sim", stamp, ".m"), false))
                {
                    string emergencyOut = "";
                    bool IOError = false;
                    SafeWrite("TimeStepData = [...\n", matlabOut, ref emergencyOut, ref IOError);
                    List<string> tmpFiles = new List<string>(0);
                    while (!sModel.IsCompleted())
                    {
                        computationTime.Start();
                        switch (solutionMode)
                        {
                            case 0: //use sampling
                                tmpFiles = (controller as SimController).SimulateOneStepBySampling(sampleNum, false, solverTime);
                                break;
                            case 1: //no sampling
                                tmpFiles = (controller as SimController).SimulateOneStepExisting(false, solverTime);
                                break;
                            case 3: //no sampling, moves to mean while idle
                                tmpFiles = (controller as SimController).SimulateOneStepToMean(false, solverTime);
                                break;
                            default://other
                                break;
                        }
                        computationTime.Stop();
                        SafeWrite(sModel.GetTimestepData(), matlabOut, ref emergencyOut, ref IOError);
                        nofSteps++;
                        CleanTmpFiles(tmpFiles);
                    }
                    SafeWrite("];\n\n", matlabOut, ref emergencyOut, ref IOError);
                    SafeWrite(sModel.GetSummaryData(), matlabOut, ref emergencyOut, ref IOError);
                    if (IOError)
                    {
                        int erCount = 0;
                        while ((erCount < 100) && (!TryWrite(emergencyOut, matlabOut)))
                            erCount++;
                    }
                    matlabOut.Close();
                }
                statusSummary = BuildStatusSummary(sampleNum, solutionMode, sModel, taskNumStart, batteryTime, computationTime, solverTime, nofSteps, statusSummary);
            }
            using (TextWriter sumOut = new StreamWriter(String.Concat(OutputFolder, "Summary", stamp, ".txt"), false))
            {
                sumOut.WriteLine(statusSummary);
                sumOut.Close();
            }
            return statusSummary;
        }



        void debugTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Process currentProcess = System.Diagnostics.Process.GetCurrentProcess();
            long totalBytesOfMemoryUsed = currentProcess.WorkingSet64;
            Console.WriteLine();
            Console.WriteLine(totalBytesOfMemoryUsed);
        }

        void limiter_Elapsed(object sender, ElapsedEventArgs e)
        {
            limitStatus = 2;
        }

        void softLimiter_Elapsed(object sender, ElapsedEventArgs e)
        {
            limitStatus++;
        }

        private string BuildStatusSummary(int sampleNum, int solutionMode, SimModel sModel, int[] taskNumStart, int[] batteryTime, Stopwatch computationTime, Stopwatch solverTime, int nofSteps, string statusSummary)
        {
            double simTime = (controller as SimController).TimeCounter;
            double utilHi = sModel.GetGainedUtility(0);
            double utilLo = sModel.GetGainedUtility(1);
            double distHi = sModel.GetFlightDistance(0);
            double distLo = sModel.GetFlightDistance(1);
            int[] taskNumEnd = new int[2] { sModel.GetGroundTruthNofTasks(0), sModel.GetGroundTruthNofTasks(1) };
            statusSummary = String.Format("{0}Solver\t{1}", statusSummary, (controller as SimController).Solver);
            statusSummary = String.Format("{0}\nSample/step\t{1}", statusSummary, (solutionMode == 0) ? sampleNum.ToString() : "No Sampling");
            statusSummary = String.Format("{0}\nTimestep\t{1}", statusSummary, SimController.timeStep);
            statusSummary = String.Format("{0}\nBattery time\n{1}\t{2}", statusSummary, batteryTime[0], batteryTime[1]);
            statusSummary = String.Format("{0}\nTime spent by UAVs:\n\t{1}", statusSummary, simTime);
            statusSummary = String.Format("{0}\nUilities:\n{1}\t{2}", statusSummary, utilHi, utilLo);
            statusSummary = String.Format("{0}\nDistances:\n{1}\t{2}", statusSummary, distHi, distLo);
            statusSummary = String.Format("{0}\nNumber of tasks:\nStart\n{1}\t{2}\nEnd\n{3}\t{4}", statusSummary, taskNumStart[0], taskNumStart[1], taskNumEnd[0], taskNumEnd[1]);
            statusSummary = String.Format("{0}\nComputation time:\n\t{1}\nof which by solver:\n\t{2}", statusSummary, computationTime.ElapsedMilliseconds, solverTime.ElapsedMilliseconds);
            statusSummary = String.Format("{0}\nAverage time per step:\n\t{1}\nof which by solver:\n\t{2}", statusSummary, computationTime.ElapsedMilliseconds / nofSteps, solverTime.ElapsedMilliseconds / nofSteps);
            statusSummary = String.Format("{0}\nAverage time per sample by solver:\n\t{1}", statusSummary, (solutionMode == 0) ? (solverTime.ElapsedMilliseconds / nofSteps / sampleNum).ToString() : "No Sampling");
            return statusSummary;
        }

        private bool CleanTmpFiles(List<string> tmpFiles)
        {
            foreach (string f in tmpFiles)
                File.Delete(f);
            return true;
        }

        public SolutionModel SolveDiscovery()
        {
            return (controller as SimController).SolveDiscovery(false);
        }

        public void Test(string p)
        {
            model.Test(p);
        }

        public string TestMCTSImprovement(string solver, int sampleNum, double timeStep, int ID, int solutionMode)
        {
            string statusSummary = "";
            SimModel sModel = model as SimModel;
            samplePerStep = sampleNum;
            SimController.timeStep = timeStep;
            (controller as SimController).Solver = solver;
            int[] taskNumStart = new int[2] { sModel.GetGroundTruthNofTasks(0), sModel.GetGroundTruthNofTasks(1) };
            int[] batteryTime = new int[2] { sModel.TravelTime[0], sModel.TravelTime[1] };

            Stopwatch computationTime = new Stopwatch();
            Stopwatch solverTime = new Stopwatch();
            List<double[]> utilitiesPerStep = new List<double[]>();
            string stamp = ID.ToString();
            using (TextWriter matlabOut = new StreamWriter(String.Concat(OutputFolder, "Sim", stamp, ".m"), false))
            {
                MCSearch.TimeStep = timeStep;
                if (solver == "UCBImprovement")
                    GroupSoltion.ExpansionPolicy = GroupSoltion.SolutionExpansionPolicyType.UCBOnly;
                //start with 0 fixed explotarion steps
                int fixedExplorationSteps = 0;
                MCSearch lastProblem = null;
                string emergencyOut = "";
                bool IOError = false;
                computationTime.Start();
                MCSearch problem;

                //Initialisation START
                int[] levelsAssigned = new int[4] { 0, 0, 0, 0 };
                solverTime.Start();
                problem = new MCSearch(model as SimModel, (controller as SimController).BeliefSampler, new int[] { 10, 5, 20 }, lastProblem, fixedExplorationSteps);
                solverTime.Stop();
                Console.WriteLine(solverTime.ElapsedMilliseconds);
                int? lu = null;
                int expandedLevel;
                int timeLimit = solutionMode * 1000;
                while (solverTime.ElapsedMilliseconds < timeLimit)
                {
                    solverTime.Start();
                    double expIncrease = problem.ExpandOnce(sampleNum, ref lu, out expandedLevel);
                    solverTime.Stop();
                    utilitiesPerStep.Add(problem.GetUtilities(new double[] { solverTime.ElapsedMilliseconds, expIncrease, (double)expandedLevel }));
                    if (utilitiesPerStep.Count > 1)
                    {
                        double diff = utilitiesPerStep.Last()[0] - utilitiesPerStep[utilitiesPerStep.Count - 2][0];
                        if (diff < 0)
                            Console.WriteLine(expandedLevel.ToString() + " " + diff.ToString());
                    }
                }
                computationTime.Stop();
                SafeWrite(MyExtensions.MyExtensionMethods.Convert2MFileVariable(utilitiesPerStep, "UtilsPerStep"), matlabOut, ref emergencyOut, ref IOError);
                if (IOError)
                {
                    int erCount = 0;
                    while ((erCount < 100) && (!TryWrite(emergencyOut, matlabOut)))
                        erCount++;
                }
                matlabOut.Close();
                statusSummary = BuildStatusSummary(sampleNum, solutionMode, sModel, taskNumStart, batteryTime, computationTime, solverTime, 1, statusSummary);
            }
            using (TextWriter sumOut = new StreamWriter(String.Concat(OutputFolder, "Summary", stamp, ".txt"), false))
            {
                sumOut.WriteLine(statusSummary);
                sumOut.Close();
            }
            return statusSummary;
        }

        public void TestDualRunTime(string solver, int sampleNum, double timeStep, int ID, int solutionMode)
        {
            SimModel sModel = model as SimModel;
            samplePerStep = sampleNum;
            SimController.timeStep = timeStep;
            (controller as SimController).Solver = solver;
            int[] taskNumStart = new int[2] { sModel.GetGroundTruthNofTasks(0), sModel.GetGroundTruthNofTasks(1) };
            int[] batteryTime = new int[2] { sModel.TravelTime[0], sModel.TravelTime[1] };

            Stopwatch computationTime = new Stopwatch();
            Stopwatch solverTime = new Stopwatch();
            Stopwatch solverBottomTime = new Stopwatch();
            int nofSteps = 0;
            string stamp = ID.ToString();
            using (TextWriter matlabOut = new StreamWriter(String.Concat(OutputFolder, "Sim", stamp, ".m"), false))
            {
                string emergencyOut = "";
                bool IOError = false;
                SafeWrite("TimingData = [...\n", matlabOut, ref emergencyOut, ref IOError);
                SimModel currentModel = sModel;
                int ntasksToRemove = 0;
                while (currentModel.SearchTaskCount > 0)
                {
                    currentModel = new SimModel(sModel);
                    currentModel.TestRemoveRandomSearchTasks(ntasksToRemove);
                    ntasksToRemove += solutionMode;

                    computationTime.Restart();
                    List<List<double>> directions;
                    List<List<int>> assignedIds;
                    DualProblem problem;
                    problem = new DualProblem(currentModel, (controller as SimController).BeliefSampler, null, true, 0);
                    problem.gradientSampler = (controller as SimController).GradientSampler;
                    solverTime.Restart();
                    problem.Solve(3, samplePerStep, out assignedIds, out directions, solverBottomTime);
                    solverTime.Stop();
                    (controller as SimController).SimulateOneStepWithExternalResults(directions, assignedIds);
                    computationTime.Stop();
                    string data = String.Format("{0} {1} {2} {3} {4};...\n", currentModel.SearchTaskCount, currentModel.RescueTaskCount + currentModel.UndiscoveredTaskCount,
                        solverTime.ElapsedMilliseconds, computationTime.ElapsedMilliseconds, solverBottomTime.ElapsedMilliseconds);
                    SafeWrite(data, matlabOut, ref emergencyOut, ref IOError);
                    nofSteps++;
                }
                SafeWrite("];\n\n", matlabOut, ref emergencyOut, ref IOError);
                if (IOError)
                {
                    int erCount = 0;
                    while ((erCount < 100) && (!TryWrite(emergencyOut, matlabOut)))
                        erCount++;
                }
                matlabOut.Close();
            }
        }

        public double[,] GetDistanceTableForGT(int level, out double battery)
        {
            SimModel gtModel = (model as SimModel).GetGroundTruthModel();
            battery = gtModel.TravelTime[level];
            return gtModel.AggregatedDistanceTableForLevel(level);
        }

        public void GetDataForGT(out double[][,] dist, out double[] batt, out List<int>[][] C)
        {
            SimModel gtModel = (model as SimModel).GetGroundTruthModel();
            batt = new double[gtModel.nLevel];
            dist = new double[gtModel.nLevel][,];
            C = new List<int>[gtModel.nLevel-1][];
            for (int i = 0; i < gtModel.nLevel; i++)
            {
                batt[i] = gtModel.TravelTime[i];
                dist[i] = gtModel.AggregatedDistanceTableForLevel(i);
                if (i > 0)
                    C[i - 1] = gtModel.GetContsraints(i - 1, i);
            }
        }

        public List<int>[] GetContsraints(int hiLevel, int loLevel)
        {
            return (model as SimModel).GetGroundTruthModel().GetContsraints(hiLevel, loLevel);
        }

        public void OpenGradientSampler(FileStream fileStream)
        {
            (controller as SimController).LoadGradientSampler(fileStream);
        }
    }
}
