﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AgentDraw
{
    public class Variable : IComparable
    {
        public readonly int level;
        public readonly int task;
        public readonly int reltask;
        public readonly int uav;
        public readonly int reluav;
        public readonly int slot;
        private int id;
        public static string GetIndexErrorMessage = "element not found in UAV or Task list";
        public static string MultiLevelErrorMessage = "Inter level connections are not allowed!";
        static List<Variable> allvars;
        public static List<Variable> Allvars
        {
            set
            {
                allvars = value;
                allvars.ForEach(delegate(Variable v) { v.setId(); });
            }
        }

        public Variable(int l, int t, int rt, int u, int ru, int s)
        {
            level = l;
            uav = u;
            reluav = ru;
            task = t;
            reltask = rt;
            slot = s;
            setId();
        }

        public Variable(int l, List<NodeList> UAVs, int ru, List<NodeList> Tasks, int rt, int s)
        {
            level = l;
            uav = getGlobalId(UAVs, ru, l);
            reluav = ru;
            task = getGlobalId(Tasks, rt, l);
            reltask = rt;
            slot = s;
            setId();
        }

        public Variable(List<NodeList> UAVs, NodeModel uav, List<NodeList> Tasks, NodeModel task, int slot)
        {
            int[] uavindex = GetIndexList(UAVs, uav);
            int[] taskindex = GetIndexList(Tasks, task);
            if (uavindex[0] != taskindex[0])
                throw new Exception(MultiLevelErrorMessage);
            this.level = uavindex[0];
            this.uav = uavindex[1];
            this.reluav = uavindex[2];
            this.task = taskindex[1];
            this.reltask = taskindex[2];
            this.slot = slot;
            setId();
        }

        internal int[] GetIndexList(List<NodeList> list, NodeModel element)
        {
            int[] index = new int[3];
            index[0] = 0;
            index[1] = 0;
            index[2] = 0;

            for (int i = 0; i < list.Count; i++)
            {
                int foundIndex = list[i].BinarySearch(element);
                if (foundIndex >= 0)
                {  //if found
                    index[0] = i;
                    index[1] += foundIndex;
                    index[2] = foundIndex;
                    return index;
                }
                else
                    index[1] += list[i].Count;
            }
            throw new Exception(GetIndexErrorMessage);
        }

        protected int getGlobalId(List<NodeList> list, int localId, int level)
        {
            int globalId = localId;
            for (int i = 0; i < level; i++)
                globalId += list[i].Count;
            return globalId;
        }
        protected void setId()
        {
            if (allvars == null)
                id = -1;
            else
                id = allvars.BinarySearch(this);
        }

        public int CompareTo(object obj)
        {
            Variable o = obj as Variable;
            int ret;
            ret = level - o.level;
            if (ret == 0)
            {
                ret = uav - o.uav;
                if (ret == 0)
                {
                    ret = task - o.task;
                    if (ret == 0)
                        ret = slot - o.slot;
                }
            }
            return ret;
        }

        override public string ToString()
        {
            if (id == -1)
                throw (new Exception("Can't perform Variable.ToString() if Variable.allvars is not set!"));
            else
                return id.ToString();
        }

        public string DetailsToString()
        {
            return String.Concat(this.level.ToString(), " ", this.uav.ToString(), " ", this.task.ToString(), " ", this.slot.ToString());
        }

        public static int CompareUST(Variable v1, Variable v2)
        {
            int ret;
            ret = v1.level.CompareTo(v2.level);
            if (ret == 0)
            {
                ret = v1.uav.CompareTo(v2.uav);
                if (ret == 0)
                {
                    ret = v1.slot.CompareTo(v2.slot);
                    if (ret == 0)
                        ret = v1.task.CompareTo(v2.task);
                }
            }
            return ret;
        }
    }
}
