﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AgentDraw
{
    public abstract partial class Node : UserControl
    {
        protected string idString;
        public string IdString { get { return idString; } }
        protected Brush color;
        protected AgentController controller;
        protected NodeModel model;
        public NodeModel Model { get { return model; } }
        public static int NodeSize = 8;

        protected Brush DefaultColor;
        protected Brush SecondaryColor;

        protected Form myParentForm;

        public int ReachRadius { get { return model.ReachRadius; } }
        public int DiscoveryRadius { get { return model.DiscoveryRadius; } }

        public Point Position
        {
            set
            {
                Size offset = new Size(NodeSize / 2, NodeSize / 2);
                this.Location = Point.Subtract(value, offset);
            }
            get
            {
                Size offset = new Size(NodeSize / 2, NodeSize / 2);
                return Point.Add(this.Location, offset);
            }
        }


        public Brush Color { get { return color; } }

        public bool IsSelected
        {
            get { return (color != DefaultColor); }
            set
            {
                if (value)
                    color = SecondaryColor;
                else
                    color = DefaultColor;
                Invalidate();
            }
        }

        public Node()
        {
            DefaultConstructor();
        }

        public Node(string idString, AgentController cont, NodeModel model, Form parent)
        {
            this.idString = idString;
            controller = cont;

            DefaultConstructor();

            this.model = model;
            myParentForm = parent;
            myParentForm.FormClosed += ParentForm_FormClosed;
        }

        private void DefaultConstructor()
        {
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Node_MouseClick);
            this.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Node_MouseClick);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Node_MouseDown);
            InitializeComponent();
            this.Size = new System.Drawing.Size(NodeSize, NodeSize);
        }

        protected void ParentForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Dispose();
        }

        private void Node_MouseClick(object sender, MouseEventArgs e)
        {
            if (controller != null)
                controller.MouseClickEventHandler(this, e);
        }

        private void Node_MouseDown(object sender, MouseEventArgs e)
        {
            if (controller != null)
                controller.MouseDragStart(this, e);
        }

        public void SetColor() {  }
        public void ResetColor() {  }

        public new void Dispose()
        {
            base.Dispose();
            model.RemoveView(this);
        }

    }
}
