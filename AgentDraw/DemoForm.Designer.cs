﻿namespace AgentDraw
{
    partial class DemoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DemoForm));
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxScenario = new System.Windows.Forms.ComboBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setPathToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enableInteractionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateLayoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fromStateOfTheArtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fromJointApproachToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBoxRunSelect = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxNSamples = new System.Windows.Forms.TextBox();
            this.buttonLoad = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxTimeStep = new System.Windows.Forms.TextBox();
            this.buttonPlay = new System.Windows.Forms.Button();
            this.buttonStep = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonRefreshBelief = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.radioButtonBgBelief = new System.Windows.Forms.RadioButton();
            this.radioButtonBgSatellite = new System.Windows.Forms.RadioButton();
            this.radioButtonBgGTIntensity = new System.Windows.Forms.RadioButton();
            this.grabFrameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Scenario";
            // 
            // comboBoxScenario
            // 
            this.comboBoxScenario.FormattingEnabled = true;
            this.comboBoxScenario.Items.AddRange(new object[] {
            "Haiti earthquake",
            "Ajka industrial spill"});
            this.comboBoxScenario.Location = new System.Drawing.Point(12, 40);
            this.comboBoxScenario.Name = "comboBoxScenario";
            this.comboBoxScenario.Size = new System.Drawing.Size(121, 21);
            this.comboBoxScenario.TabIndex = 1;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(289, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setPathToolStripMenuItem,
            this.enableInteractionToolStripMenuItem,
            this.updateLayoutToolStripMenuItem,
            this.grabFrameToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // setPathToolStripMenuItem
            // 
            this.setPathToolStripMenuItem.Name = "setPathToolStripMenuItem";
            this.setPathToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.setPathToolStripMenuItem.Text = "Set Path";
            this.setPathToolStripMenuItem.Click += new System.EventHandler(this.setPathToolStripMenuItem_Click);
            // 
            // enableInteractionToolStripMenuItem
            // 
            this.enableInteractionToolStripMenuItem.Name = "enableInteractionToolStripMenuItem";
            this.enableInteractionToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.enableInteractionToolStripMenuItem.Text = "Enable Interaction";
            this.enableInteractionToolStripMenuItem.Click += new System.EventHandler(this.enableInteractionToolStripMenuItem_Click);
            // 
            // updateLayoutToolStripMenuItem
            // 
            this.updateLayoutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fromStateOfTheArtToolStripMenuItem,
            this.fromJointApproachToolStripMenuItem});
            this.updateLayoutToolStripMenuItem.Name = "updateLayoutToolStripMenuItem";
            this.updateLayoutToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.updateLayoutToolStripMenuItem.Text = "Update Layout";
            // 
            // fromStateOfTheArtToolStripMenuItem
            // 
            this.fromStateOfTheArtToolStripMenuItem.Name = "fromStateOfTheArtToolStripMenuItem";
            this.fromStateOfTheArtToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.fromStateOfTheArtToolStripMenuItem.Text = "From State of The Art";
            this.fromStateOfTheArtToolStripMenuItem.Click += new System.EventHandler(this.fromStateOfTheArtToolStripMenuItem_Click);
            // 
            // fromJointApproachToolStripMenuItem
            // 
            this.fromJointApproachToolStripMenuItem.Name = "fromJointApproachToolStripMenuItem";
            this.fromJointApproachToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.fromJointApproachToolStripMenuItem.Text = "From Joint Planning";
            this.fromJointApproachToolStripMenuItem.Click += new System.EventHandler(this.fromJointApproachToolStripMenuItem_Click);
            // 
            // textBoxRunSelect
            // 
            this.textBoxRunSelect.Location = new System.Drawing.Point(140, 40);
            this.textBoxRunSelect.Name = "textBoxRunSelect";
            this.textBoxRunSelect.Size = new System.Drawing.Size(26, 20);
            this.textBoxRunSelect.TabIndex = 3;
            this.textBoxRunSelect.Leave += new System.EventHandler(this.textBoxRunSelect_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(172, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Samples";
            // 
            // textBoxNSamples
            // 
            this.textBoxNSamples.Location = new System.Drawing.Point(175, 41);
            this.textBoxNSamples.Name = "textBoxNSamples";
            this.textBoxNSamples.Size = new System.Drawing.Size(49, 20);
            this.textBoxNSamples.TabIndex = 5;
            this.textBoxNSamples.Leave += new System.EventHandler(this.textBoxNSamples_Leave);
            // 
            // buttonLoad
            // 
            this.buttonLoad.Location = new System.Drawing.Point(12, 67);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(269, 23);
            this.buttonLoad.TabIndex = 6;
            this.buttonLoad.Text = "Load";
            this.buttonLoad.UseVisualStyleBackColor = true;
            this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(226, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Time Step";
            // 
            // textBoxTimeStep
            // 
            this.textBoxTimeStep.Location = new System.Drawing.Point(231, 41);
            this.textBoxTimeStep.Name = "textBoxTimeStep";
            this.textBoxTimeStep.Size = new System.Drawing.Size(50, 20);
            this.textBoxTimeStep.TabIndex = 8;
            this.textBoxTimeStep.Leave += new System.EventHandler(this.textBoxTimeStep_Leave);
            // 
            // buttonPlay
            // 
            this.buttonPlay.Location = new System.Drawing.Point(12, 114);
            this.buttonPlay.Name = "buttonPlay";
            this.buttonPlay.Size = new System.Drawing.Size(80, 23);
            this.buttonPlay.TabIndex = 9;
            this.buttonPlay.Text = "Play";
            this.buttonPlay.UseVisualStyleBackColor = true;
            this.buttonPlay.Click += new System.EventHandler(this.buttonPlay_Click);
            // 
            // buttonStep
            // 
            this.buttonStep.Location = new System.Drawing.Point(98, 114);
            this.buttonStep.Name = "buttonStep";
            this.buttonStep.Size = new System.Drawing.Size(80, 23);
            this.buttonStep.TabIndex = 10;
            this.buttonStep.Text = "Step";
            this.buttonStep.UseVisualStyleBackColor = true;
            this.buttonStep.Click += new System.EventHandler(this.buttonStep_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.buttonRefreshBelief);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.radioButtonBgBelief);
            this.groupBox1.Controls.Add(this.radioButtonBgSatellite);
            this.groupBox1.Controls.Add(this.radioButtonBgGTIntensity);
            this.groupBox1.Location = new System.Drawing.Point(12, 144);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(265, 105);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Settings";
            // 
            // buttonRefreshBelief
            // 
            this.buttonRefreshBelief.Location = new System.Drawing.Point(102, 52);
            this.buttonRefreshBelief.Name = "buttonRefreshBelief";
            this.buttonRefreshBelief.Size = new System.Drawing.Size(75, 23);
            this.buttonRefreshBelief.TabIndex = 4;
            this.buttonRefreshBelief.Text = "Refresh";
            this.buttonRefreshBelief.UseVisualStyleBackColor = true;
            this.buttonRefreshBelief.Click += new System.EventHandler(this.buttonRefreshBelief_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Background";
            // 
            // radioButtonBgBelief
            // 
            this.radioButtonBgBelief.AutoSize = true;
            this.radioButtonBgBelief.Location = new System.Drawing.Point(6, 55);
            this.radioButtonBgBelief.Name = "radioButtonBgBelief";
            this.radioButtonBgBelief.Size = new System.Drawing.Size(90, 17);
            this.radioButtonBgBelief.TabIndex = 2;
            this.radioButtonBgBelief.Text = "Robots\' Belief";
            this.radioButtonBgBelief.UseVisualStyleBackColor = true;
            this.radioButtonBgBelief.CheckedChanged += new System.EventHandler(this.radioButtonBgBelief_CheckedChanged);
            // 
            // radioButtonBgSatellite
            // 
            this.radioButtonBgSatellite.AutoSize = true;
            this.radioButtonBgSatellite.Checked = true;
            this.radioButtonBgSatellite.Location = new System.Drawing.Point(6, 32);
            this.radioButtonBgSatellite.Name = "radioButtonBgSatellite";
            this.radioButtonBgSatellite.Size = new System.Drawing.Size(62, 17);
            this.radioButtonBgSatellite.TabIndex = 1;
            this.radioButtonBgSatellite.TabStop = true;
            this.radioButtonBgSatellite.Text = "Satellite";
            this.radioButtonBgSatellite.UseVisualStyleBackColor = true;
            this.radioButtonBgSatellite.CheckedChanged += new System.EventHandler(this.radioButtonBgSatellite_CheckedChanged);
            // 
            // radioButtonBgGTIntensity
            // 
            this.radioButtonBgGTIntensity.AutoSize = true;
            this.radioButtonBgGTIntensity.Location = new System.Drawing.Point(6, 78);
            this.radioButtonBgGTIntensity.Name = "radioButtonBgGTIntensity";
            this.radioButtonBgGTIntensity.Size = new System.Drawing.Size(104, 17);
            this.radioButtonBgGTIntensity.TabIndex = 0;
            this.radioButtonBgGTIntensity.Text = "Rescue Intensity";
            this.radioButtonBgGTIntensity.UseVisualStyleBackColor = true;
            this.radioButtonBgGTIntensity.CheckedChanged += new System.EventHandler(this.radioButtonBgGTIntensity_CheckedChanged);
            // 
            // grabFrameToolStripMenuItem
            // 
            this.grabFrameToolStripMenuItem.Name = "grabFrameToolStripMenuItem";
            this.grabFrameToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.grabFrameToolStripMenuItem.Text = "Grab Frame";
            this.grabFrameToolStripMenuItem.Click += new System.EventHandler(this.grabFrameToolStripMenuItem_Click);
            // 
            // DemoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(289, 261);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonStep);
            this.Controls.Add(this.buttonPlay);
            this.Controls.Add(this.textBoxTimeStep);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buttonLoad);
            this.Controls.Add(this.textBoxNSamples);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxRunSelect);
            this.Controls.Add(this.comboBoxScenario);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "DemoForm";
            this.Text = "ORCHID Demo";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DemoForm_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setPathToolStripMenuItem;
        private System.Windows.Forms.TextBox textBoxRunSelect;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxNSamples;
        private System.Windows.Forms.Button buttonLoad;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxTimeStep;
        private System.Windows.Forms.Button buttonPlay;
        private System.Windows.Forms.Button buttonStep;
        private System.Windows.Forms.ComboBox comboBoxScenario;
        private System.Windows.Forms.ToolStripMenuItem enableInteractionToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ToolStripMenuItem updateLayoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fromStateOfTheArtToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fromJointApproachToolStripMenuItem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton radioButtonBgBelief;
        private System.Windows.Forms.RadioButton radioButtonBgSatellite;
        private System.Windows.Forms.RadioButton radioButtonBgGTIntensity;
        private System.Windows.Forms.Button buttonRefreshBelief;
        private System.Windows.Forms.ToolStripMenuItem grabFrameToolStripMenuItem;
    }
}