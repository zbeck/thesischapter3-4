﻿namespace AgentDraw
{
    partial class SimFormMinimal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SimFormMinimal));
            this.progressBarRescue = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.progressBarSearch = new System.Windows.Forms.ProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            this.labelRecscueTime = new System.Windows.Forms.Label();
            this.placementView = new AgentDraw.PlacementView();
            this.SuspendLayout();
            // 
            // progressBarRescue
            // 
            this.progressBarRescue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarRescue.Location = new System.Drawing.Point(106, 288);
            this.progressBarRescue.Name = "progressBarRescue";
            this.progressBarRescue.Size = new System.Drawing.Size(296, 23);
            this.progressBarRescue.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 292);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Rescue Progress";
            // 
            // progressBarSearch
            // 
            this.progressBarSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarSearch.Location = new System.Drawing.Point(106, 259);
            this.progressBarSearch.Name = "progressBarSearch";
            this.progressBarSearch.Size = new System.Drawing.Size(296, 23);
            this.progressBarSearch.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 263);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Search Progress";
            // 
            // labelRecscueTime
            // 
            this.labelRecscueTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelRecscueTime.AutoSize = true;
            this.labelRecscueTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRecscueTime.Location = new System.Drawing.Point(12, 233);
            this.labelRecscueTime.Name = "labelRecscueTime";
            this.labelRecscueTime.Size = new System.Drawing.Size(189, 20);
            this.labelRecscueTime.TabIndex = 5;
            this.labelRecscueTime.Text = "Average Rescue Time:";
            // 
            // placementView
            // 
            this.placementView.Location = new System.Drawing.Point(12, 12);
            this.placementView.Name = "placementView";
            this.placementView.ShowActivations = true;
            this.placementView.ShowArea = false;
            this.placementView.ShowConnections = true;
            this.placementView.ShowNames = true;
            this.placementView.ShowRadius = false;
            this.placementView.Size = new System.Drawing.Size(390, 241);
            this.placementView.TabIndex = 0;
            // 
            // SimFormMinimal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(414, 314);
            this.Controls.Add(this.labelRecscueTime);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.progressBarSearch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.progressBarRescue);
            this.Controls.Add(this.placementView);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SimFormMinimal";
            this.Text = "SimFormMinimal";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PlacementView placementView;
        private System.Windows.Forms.ProgressBar progressBarRescue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ProgressBar progressBarSearch;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelRecscueTime;
    }
}