﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AgentDraw
{
    [Serializable]
    public class XmlModel : SimModel
    {
        public List<NodeModel> UndiscoveredTasks { get { return undiscoveredTasks; } }

        public List<NodeList> UAVs { get { return UAVNodes; } }
        public List<NodeList> Tasks { get { return TaskNodes; } }
        public List<RelationModel> Connected { get { return Connections; } }
        public List<RelationModel> Activated { get { return Activations; } }

        public double[] P { get { return p; } }
        public int[] Lambda { get { return lam; } }
        public double[] Epsilon { get { return epsilon; } }
        public int[] Degrade { get { return degrade; } }

        public XmlModel() { }

        public XmlModel(SimModel src) :
            base(src)
        {
        }

    }
}
