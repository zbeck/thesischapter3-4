﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace AgentDraw
{
    public interface IAgentForm
    {
        void ReSize(Size size);

        void SetTravelTime(int p, int level);

        void SetTravelTime(int[] p);

        void SetNofSlot(int p);

        void SetProgress(int[] p, double[] t);
    }
}
