﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace AgentDraw
{
    public class GeneratorProg : AgentProg
    {
        public Sampler sampler;

        public static string TmpFolder = String.Concat(AgentProg.WorkFolder, "tmp/");

        Bitmap densityMap;
        //sets the image as a background, and resizes the window
        Bitmap DensityMap
        {
            set
            {
                densityMap = value;
                form.ReSize(densityMap.Size);
                View.BackgroundImage = densityMap;
                View.Size = densityMap.Size;

            }
        }

        public int NofSamples = 100;

        public GeneratorProg(PlacementView pview, IAgentForm form) :
            base(pview, form)
        {
            model = new GeneratorModel(pview);
            controller = new GeneratorController(model, this);
            InitView();
        }

        internal void OpenBitmapAsDensityMap(System.IO.Stream fs)
        {
            DensityMap = new Bitmap(fs);
            sampler = new Sampler(densityMap);
            AskForNumberForm getEVal = new AskForNumberForm();
            getEVal.textBoxEVal.Text = sampler.ExpectedVal.ToString();
            getEVal.ShowDialog();
            try
            {
                double eval = Double.Parse(getEVal.textBoxEVal.Text);
                sampler.ExpectedVal = eval;
            }
            catch (FormatException ex) { Console.WriteLine(ex.ToString()); }
        }

        internal void Sample()
        {
            if (sampler == null)
                return;
            (model as GeneratorModel).Sample(sampler);
        }  

        internal void GenerateProblemOutput(System.IO.TextWriter tw)
        {
            (model as GeneratorModel).GenerateProblemOutput(tw);
        }

        internal void SolveSamples()
        {
            //clean TMP folder
            if (Directory.Exists(GeneratorProg.TmpFolder))
            {
                Directory.Delete(GeneratorProg.TmpFolder, true);
            }
            Directory.CreateDirectory(GeneratorProg.TmpFolder);
            using (TextWriter outFile = new StreamWriter(String.Concat(GeneratorProg.TmpFolder, "output.log")))
            {
                for (int s = 0; s < NofSamples; s++)
                {
                    outFile.Write("Generating sample ");
                    outFile.WriteLine(s);
                    Sample();
                    string dataFile = String.Concat(GeneratorProg.TmpFolder, "sample", s.ToString(), ".txt");
                    string CPLEXFile = String.Concat(GeneratorProg.TmpFolder, "cplex", s.ToString(), ".txt");
                    using (TextWriter tw = new StreamWriter(dataFile))
                    {
                        GenerateOutput(tw);
                    }
                    using (TextWriter tw = new StreamWriter(CPLEXFile))
                    {
                        GenerateProblemOutput(tw);
                    }
                    string batFile = String.Concat(GeneratorProg.TmpFolder, "run", s.ToString(), ".bat");
                    using (TextWriter batWriter = new StreamWriter(batFile))
                    {
                        batWriter.WriteLine(String.Concat("python ", GeneratorProg.TmpFolder, "../uav.py ", dataFile));
                        batWriter.WriteLine(String.Concat("python ", GeneratorProg.TmpFolder, "../oplgenbinary.py ", CPLEXFile));
                    }
                    System.Diagnostics.Process p = new System.Diagnostics.Process();
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.FileName = batFile;
                    p.Start();
                    string output = p.StandardOutput.ReadToEnd();
                    p.WaitForExit();
                    outFile.Write(output);
                    outFile.WriteLine();
                    outFile.WriteLine();
                }
            }
        }

        public void Clear()
        {
            GeneratorModel model = new GeneratorModel(View);
            Clear(model);
        }

        internal void OpenFile(Stream fs)
        {
            IFormatter formatter = new BinaryFormatter();
            GeneratorModel newModel = formatter.Deserialize(fs) as GeneratorModel;
            View.Clear();
            newModel.View = View;
            newModel.RefreshRelations();
            newModel.SelectLevel(0);    //only highes level tasks and UAVs selected
            controller.Model = newModel;
            model = newModel;
            form.SetNofSlot(model.NofSlots);
            form.SetTravelTime(model.TravelTime);
        }

        internal void RevertModel()
        {
            model.Revert();
        }

        internal void SolveAStar()
        {
            AgentController.SolveAStar(model, 1, true);
        }
    }
}
