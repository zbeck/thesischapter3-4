﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using MyExtensions;
using DiscoverySolver;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace AgentDraw
{
    public class SimController : AgentController
    {
        public Sampler BeliefSampler { get; private set; }
        public Sampler GradientSampler { get; private set; }
        Sampler BeliefSamplerResetState;
        public double BeliefExpectedTaskNumber { get { return BeliefSampler.ExpectedVal; } set { BeliefSampler.ExpectedVal = value; } }
        public string Solver = "Neighborhood.py";
        double timeCounter;
        public double TimeCounter { get { return timeCounter; } }
        public static double StopAngle = MyExtensionMethods.StopAngle;
//        public static double timeStep = 6;
        public static double timeStep = 60;
        public bool HasBitmap { get { return BeliefSampler != null; } }

        public SimController(AgentModel model, AgentProg program) :
            base(model, program)
        {
            timeCounter = 0;
            TmpFolder = "simtmp/";
        }

        internal void OpenBitmapAsDensityMap(Bitmap densityMap)
        {
            BeliefSampler = new Sampler(densityMap);
            AskForNumberForm getEVal = new AskForNumberForm("Expected number of tasks");
            getEVal.textBoxEVal.Text = BeliefSampler.ExpectedVal.ToString();
            getEVal.ShowDialog();
            try
            {
                double eval = Double.Parse(getEVal.textBoxEVal.Text);
                BeliefSampler.ExpectedVal = eval;
            }
            catch (FormatException ex) { Console.WriteLine(ex.ToString()); }
            int discoveryRadius = SimProg.DefaultDiscoveryRadius;
            getEVal = new AskForNumberForm("Radius of discovery");
            getEVal.textBoxEVal.Text = discoveryRadius.ToString();
            getEVal.ShowDialog();
            try
            {
                int eval = Int32.Parse(getEVal.textBoxEVal.Text);
                discoveryRadius = eval;
            }
            catch (FormatException ex) { Console.WriteLine(ex.ToString()); }
            (model as SimModel).InitGroundTruth(BeliefSampler, discoveryRadius);
            (program as SimProg).InitModelState = new SimModel(model as SimModel);
            SetBeliefResetState();
        }

        /// <summary>
        /// Opens density map is the model is externally loaded
        /// </summary>
        /// <param name="densityMap">Bitmap containing the intensity function of the spatial Poisson process</param>
        /// <param name="expectedTasks">expected number of tasks for sampling</param>
        internal void OpenBitmapAsDensityMap(Bitmap densityMap, double expectedTasks)
        {
            BeliefSampler = new Sampler(densityMap);
            BeliefSampler.ExpectedVal = expectedTasks;
        }

        /// <summary>
        /// Opens gradient map
        /// </summary>
        /// <param name="densityMap">Bitmap containing the gradient function (leading the agents at zero intensity)</param>
        internal void OpenBitmapAsGradient(Bitmap gradientMap)
        {
            GradientSampler = new Sampler(gradientMap);
        }

        internal void ResampleGroundTruth()
        {
            (model as SimModel).InitGroundTruth(BeliefSampler);
        }


        void SetBeliefResetState()
        {
            BeliefSamplerResetState = new Sampler(BeliefSampler);
        }

        public void SimualteSinglePlan(bool showSolution, string solver, Func<List<string>, bool> cleanFunc, long maxTime = 0, string outfile = "SimOpt.m", System.Diagnostics.Stopwatch solverTime = null)
        {
            Stopwatch totalSolverTime;
            if (solverTime != null)
                totalSolverTime = solverTime;
            else
                totalSolverTime = new Stopwatch();
            List<string> tmpFiles = new List<string>(2);
            SimModel gtModel = (model as SimModel).GetGroundTruthModel();
            int[] nofslots = gtModel.GetMaxSlots();

            SolutionModel bestSolution = null;
            string bestSolverOutput = "";
            double bestUtil = 0;
            while (totalSolverTime.ElapsedMilliseconds <= maxTime)
            {
                tmpFiles.Clear();
                totalSolverTime.Start();
                string output = Solve(gtModel, solver, TmpFolder, AgentProg.WorkFolder, TmpFolder, tmpFiles, nofslots);
                totalSolverTime.Stop();

                SolutionModel smodel;
                smodel = gtModel.ReadSolution(output, solver);
                if (smodel.Util > bestUtil)
                {
                    bestUtil = smodel.Util;
                    bestSolution = smodel;
                    bestSolverOutput = output;
                }
                cleanFunc(tmpFiles);
            }
            using (TextWriter matlabOut = new StreamWriter(outfile, false))
            {
                bestSolution.WriteOutput(matlabOut);
                matlabOut.Close();
            }
            if (showSolution)
            {
                gtModel.OpenSolutionModel(bestSolverOutput, solver);
            }
        }

        public List<string> SimulateOneStepToMean(bool showSolution, System.Diagnostics.Stopwatch solverSW = null)
        {
            List<string> tmpFiles = new List<string>(2);
            timeCounter += timeStep;

            List<List<double>> moveAngleList = new List<List<double>>();
            List<List<NodeModel>> assignedTaskList = new List<List<NodeModel>>();
            //copy current model
            GeneratorModel copyModel = new GeneratorModel(this.model);
            copyModel.ClearActivations();    //everything is discovered
            if (solverSW != null)
                solverSW.Start();
            string output = Solve(copyModel, Solver, TmpFolder, AgentProg.WorkFolder, TmpFolder, tmpFiles);
            if (solverSW != null)
                solverSW.Stop();

            //extract solution
            SolutionModel smodel;
            if (showSolution)
                smodel = copyModel.OpenSolutionModel(output, Solver);
            else
                smodel = copyModel.ReadSolution(output, Solver);
            moveAngleList = smodel.GetDirections(assignedTaskList, BeliefSampler);
            SimModel model = this.model as SimModel;
            //not the best
            model.RefreshUAVCoords();
            model.MoveUAVpos(moveAngleList, GetIndices(assignedTaskList), timeStep, timeCounter);
            model.RefreshUAVCoords();
            model.DoDiscovery(BeliefSampler, timeStep, timeCounter);
            return tmpFiles;
        }

        public List<string> SimulateOneStepExisting(bool showSolution, System.Diagnostics.Stopwatch solverSW = null)
        {
            List<string> tmpFiles = new List<string>(2);
            timeCounter += timeStep;

            List<List<double>> moveAngleList = new List<List<double>>();
            List<List<NodeModel>> assignedTaskList = new List<List<NodeModel>>();
            //copy current model
            GeneratorModel copyModel = new GeneratorModel(this.model);
            copyModel.ClearActivations();    //everything is discovered
            if (solverSW != null)
                solverSW.Start();
            string output = Solve(copyModel, Solver, TmpFolder, AgentProg.WorkFolder, TmpFolder, tmpFiles);
            if (solverSW != null)
                solverSW.Stop();

            //extract solution
            SolutionModel smodel;
            if (showSolution)
                smodel = copyModel.OpenSolutionModel(output, Solver);
            else
                smodel = copyModel.ReadSolution(output, Solver);
            moveAngleList = smodel.GetDirections(assignedTaskList);
            SimModel model = this.model as SimModel;
            //not the best
            model.RefreshUAVCoords();
            model.MoveUAVpos(moveAngleList, GetIndices(assignedTaskList), timeStep, timeCounter);
            model.RefreshUAVCoords();
            model.DoDiscovery(BeliefSampler, timeStep, timeCounter);
            return tmpFiles;
        }

        public List<string> SimulateOneStepBySampling(int sampleNum, bool showSolution, System.Diagnostics.Stopwatch solverSW = null)
        {
            List<string> tmpFiles = new List<string>(2);
            if (sampleNum < 1)
                return tmpFiles;

            timeCounter += timeStep;

            List<List<List<double>>> moveAngleList = new List<List<List<double>>>();
            List<List<List<NodeModel>>> assignedTaskList = new List<List<List<NodeModel>>>();
            for (int i = 0; i < sampleNum; i++)
            {
                //copy current model
                GeneratorModel sampledModel = new GeneratorModel(this.model);
                sampledModel.ClearActivations();    //everything is discovered
                //sample undiscovered tasks
                sampledModel.SampleAdditionalTasks(BeliefSampler); //this will assign activations only for the sampled tasks

                if (solverSW != null)
                    solverSW.Start();
                string output = Solve(sampledModel, Solver, TmpFolder, AgentProg.WorkFolder, TmpFolder, tmpFiles);
                if (solverSW != null)
                    solverSW.Stop();

                //extract solution
                SolutionModel smodel;
                if (showSolution)
                    smodel = sampledModel.OpenSolutionModel(output, Solver);
                else
                    smodel = sampledModel.ReadSolution(output, Solver);
                if (smodel != null)
                {
                    List<List<NodeModel>> currentAssignmentList = new List<List<NodeModel>>();
                    moveAngleList.Add(smodel.GetDirections(currentAssignmentList));
                    assignedTaskList.Add(currentAssignmentList);
                }
            }
            SimModel model = this.model as SimModel;
            //not the best
            model.RefreshUAVCoords();
            model.MoveUAVpos(FilterDirections(moveAngleList), FilterAssignments(assignedTaskList), timeStep, timeCounter);
            model.RefreshUAVCoords();
            model.DoDiscovery(BeliefSampler, timeStep, timeCounter);
            return tmpFiles;
        }

        public bool SimulateOneStepWithExternalResults(List<List<double>> moveAngleList, List<List<int>> assignedTaskIdList)
        {
            timeCounter += timeStep;
            SimModel model = this.model as SimModel;
            
            //not the best
            model.RefreshUAVCoords();
            bool problemChanged = model.MoveUAVpos(moveAngleList, assignedTaskIdList, timeStep, timeCounter);                
            model.RefreshUAVCoords();
            model.DoDiscovery(BeliefSampler, timeStep, timeCounter);
            return problemChanged;
        }

        private List<List<int>> FilterAssignments(List<List<List<NodeModel>>> assignedTaskList)
        {
            int nofSolutions = assignedTaskList.Count;
            int majority = nofSolutions / 2 + 1;
            List<List<int>> retval = new List<List<int>>();
            switch (nofSolutions)
            {
                case 0:
                    throw new Exception("No solution is given to Filter function!");
                case 1:
                    foreach (List<NodeModel> nlist in assignedTaskList[0])
                    {
                        List<int> currentList = new List<int>();
                        foreach (NodeModel node in nlist)
                        {
                            if (model.HasTask(node))
                                currentList.Add(node.ID);
                            else
                                currentList.Add(-1);
                        }
                        retval.Add(currentList);
                    }
                    return retval;
                default:    //simple averaging
                    Dictionary<int, int> assigned = new Dictionary<int, int>();
                    for (int a = 0; a < assignedTaskList[0].Count; a++)
                    {
                        List<int> currentList = new List<int>();
                        for (int b = 0; b < assignedTaskList[0][a].Count; b++)
                        {
                            assigned.Clear();
                            for (int i = 0; i < nofSolutions; i++)
                            {
                                NodeModel task = assignedTaskList[i][a][b];
                                if (model.HasTask(task))
                                {
                                    if (!assigned.ContainsKey(task.ID))
                                        assigned.Add(task.ID, 1);
                                    else
                                        assigned[task.ID]++;
                                }
                            }
                            if (assigned.Count == 0)
                            {
                                currentList.Add(-1);
                                continue;
                            }
                            KeyValuePair<int, int> maxAssigned = assigned.Aggregate((l, r) => l.Value > r.Value ? l : r);
                            if (maxAssigned.Value >= majority)
                                currentList.Add(maxAssigned.Key);
                            else
                                currentList.Add(-1);
                        }
                        retval.Add(currentList);
                    }
                    return retval;
            }                    
        }

        private List<List<int>> GetIndices(List<List<NodeModel>> assignedTaskList)
        {
            List<List<int>> retval = new List<List<int>>(assignedTaskList.Count);
            foreach (List<NodeModel> l in assignedTaskList)
            {
                List<int> currentList = new List<int>(l.Count);
                foreach (NodeModel n in l)
                {
                    if (n == null)
                        currentList.Add(-1);
                    else
                        currentList.Add(n.ID);
                }
                retval.Add(currentList);
            }
            return retval;
        }

        internal List<List<double>> FilterDirections(List<List<List<double>>> moveAngleList)
        {
            int nofSolutions = moveAngleList.Count;
            switch (nofSolutions)
            {
                case 0:
                    throw new Exception("No solution is given to Filter function!");
                case 1:
                    return moveAngleList[0];
                default:    //simple averaging
                    List<List<double>> retval = new List<List<double>>();
                    for (int a = 0; a < moveAngleList[0].Count; a++)
                    {
                        List<double> currentList = new List<double>();
                        for (int b = 0; b < moveAngleList[0][a].Count; b++)
                        {
                            double currentSin = 0.0;
                            double currentCos = 0.0;
                            for (int i = 0; i < nofSolutions; i++)
                            {
                                if (moveAngleList[i][a][b] != StopAngle)
                                {
                                    currentSin += Math.Sin(moveAngleList[i][a][b]);
                                    currentCos += Math.Cos(moveAngleList[i][a][b]);
                                }
                            }
                            if (currentSin == 0 && currentCos == 0)
                                currentList.Add(StopAngle);
                            else
                                currentList.Add(Math.Atan2(currentSin, currentCos));
                        }
                        retval.Add(currentList);
                    }
                    return retval;
            }
        }


        public override void MouseClickEventHandler(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            SimModel model = this.model as SimModel;
            if (sender is PlacementView)
            {
                switch (e.Button)
                {
                    case System.Windows.Forms.MouseButtons.Left:
                        model.AddDiscoveryTask(new NodeModel(e.Location));
                        break;
                    case System.Windows.Forms.MouseButtons.Right:
                        NodeModel newUav = new NodeModel(e.Location);
                        model.AddLoUAV(newUav);
                        break;
                }
            }
            else if (sender is UAV)
            {
                switch (e.Button)
                {
                    case System.Windows.Forms.MouseButtons.Right:
                        if (e.Clicks > 1)
                            model.RemoveNode(sender as UAV);
                        break;
                    case System.Windows.Forms.MouseButtons.Left:
                        model.RemoveNode(sender as UAV);
                        NodeModel newUav = new NodeModel((sender as UAV).Position);
                        model.AddHiUAV(newUav);
                        break;
                }
            }
            else if (sender is Task)
            {
                switch (e.Button)
                {
                    case System.Windows.Forms.MouseButtons.Right:
                        if (e.Clicks > 1)
                            model.RemoveNode(sender as Task);
                        break;
                }
            }
        }

        internal void RefreshBitmap(Bitmap bmp)
        {
            BeliefSampler.RefreshBitmap(bmp);
        }

        internal void ClearOutsideRectangles(List<Rectangle> list)
        {
            BeliefSampler.ClearOutsideRectangles(list);
            SetBeliefResetState();
        }

        public override void Reset()
        {
            timeCounter = 0;
            if (BeliefSamplerResetState != null)
                BeliefSampler = new Sampler(BeliefSamplerResetState);
        }

        internal void GenerateDiscoveryProblemOutput(TextWriter tw, SimModel simModel)
        {
            simModel.GenerateDiscoveryProblemOutput(tw, BeliefSampler);
        }

        internal SolutionModel SolveDiscovery(bool showSolution)
        {
            Stopwatch sw = new Stopwatch();
            //copy current model
            SimModel copyModel = new SimModel(this.model as SimModel);

            sw.Start();
            Solver solver = copyModel.GetDiscoverySolver(BeliefSampler);
            List<List<int>> output = solver.Solve();
            sw.Stop();

            string sol = String.Format("Exploration brute force, took {0:d}ms", sw.ElapsedMilliseconds);
            //extract solution
            SolutionModel smodel = copyModel.OpenHighLevelSolution(output, sol, showSolution);
            return smodel;
        }

        internal void SaveSampler(Stream fs)
        {
            IFormatter formatter = new BinaryFormatter();
            formatter.Serialize(fs, BeliefSampler);
        }

        public Sampler LoadBeliefSampler(Stream fs, bool setResetState = false)
        {
            IFormatter formatter = new BinaryFormatter();
            Sampler sam = formatter.Deserialize(fs) as Sampler;
            BeliefSampler = sam;
            if(setResetState)
                SetBeliefResetState();
            return sam;
        }

        internal object LoadGradientSampler(FileStream fileStream)
        {
            IFormatter formatter = new BinaryFormatter();
            Sampler sam = formatter.Deserialize(fileStream) as Sampler;
            GradientSampler = sam;
            return sam;
        }
    }
}
