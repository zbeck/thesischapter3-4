﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace AgentDraw
{
    [Serializable]
    class GeneratorModel : AgentModel
    {
        Size Size
        {
            get {
                if (views.Count != 0) return views[0].Size;
                else throw new NotImplementedException(); 
            }
        }

        public GeneratorModel(PlacementView view) :
            base(view)
        {
            UAVNodes.Add(new NodeList());   //high level UAVs
            UAVNodes.Add(new NodeList());   //low level UAVs
            TaskNodes.Add(new NodeList());  //high level Tasks
            TaskNodes.Add(new NodeList());  //low level Tasks
            //hiUAV - UAVNodes[0];
            //loUAV - UAVNodes[1];
            //discoveryTasks - TaskNodes[0];
            //sampledTasks - TaskNodes[1];
            //speed = new double[2] { 1.0, 1.0 };
            //this.p = new double[2] { DefaultP, DefaultP };
            //this.lam = new int[2] { DefaultLam, DefaultLam };
            //this.epsilon = new double[2] { DefaultEpsilon, DefaultEpsilon };
            //this.degrade = new int[1] { DefaultDegrade };
            InitDefault();
        }

        public GeneratorModel(AgentModel model):
            base(model)
        {
        }

        public void Sample(Sampler sampler)
        {
            ClearNodes(TaskNodes[1]);
            SampleAdditionalTasks(sampler);
        }

        public void SampleAdditionalTasks(Sampler sampler)
        {
            List<NodeModel> sample = SampleTasks(sampler);
            foreach (NodeModel task in sample)
                AddTask(task, 1);
            ShuffleTasks(1);
            RefreshLoHiConnections();
            ReAssignActivations(sample);
        }

        public void AddSampledTask(NodeModel task)
        {
            task.IsSelected = false;
            AddTask(task, 1);
            RefreshLoHiConnections();
            ReAssignActivations();
        }

        public void AddDiscoveryTask(NodeModel task)
        {
            task.IsSelected = true;
            AddTask(task, 0);
            RefreshLoHiConnections();
            ReAssignActivations();
        }

        public void AddLoUAV(NodeModel uav)
        {
            uav.IsSelected = false;
            AddUAV(uav, 1);
            ConnectUAV(uav, TaskNodes[1]);
        }

        internal void AddHiUAV(NodeModel uav)
        {
            uav.IsSelected = true;
            AddUAV(uav, 0);
            ConnectUAV(uav, TaskNodes[0]);
        }

        protected void ConnectUAV(NodeModel uav, NodeList tasks)
        {
            foreach (NodeModel task in tasks)
            {
                AddConnection(new RelationModel(task, uav));
            }
        }

        protected void RefreshLoHiConnections()
        {
            Connections.Clear();
            foreach (NodeModel uav in UAVNodes[0])
            {
                ConnectUAV(uav, TaskNodes[0]);
            }
            foreach (NodeModel uav in UAVNodes[1])
            {
                ConnectUAV(uav, TaskNodes[1]);
            }
        }

        public override void OnDeserialization(object sender)
        {
            base.OnDeserialization(sender);
            RefreshLoHiConnections();
        }

        internal void ClearActivations()
        {
            this.Activations.Clear();
        }
    }
}
