﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows.Forms;
using TreeSearchSolver;

namespace AgentDraw
{
    public class SimProg : AgentProg
    {
        protected Bitmap densityMap;
        //sets the image as a background, and resizes the window
        Bitmap DensityMap
        {
            set
            {
                densityMap = value;
                form.ReSize(densityMap.Size);
                View.BackgroundImage = densityMap;
                View.Size = densityMap.Size;

            }
        }
        protected SimModel initModelState;
        public SimModel InitModelState { set { initModelState = value; } }

        public SimModel Model
        {
            get
            {
                //return a copy of the model without the view set
                return new SimModel(model as SimModel);
            }
            set
            {
                //clear information from last run, because of model change
                lastProblem = null;
                //remove current nodes from view
                View.Clear();
                //set new view in MVC
                SimModel newModel = value;
                newModel.View = View;
                model = newModel;
                controller.Model = model;
                //enable interaction with the new model
                InitView();
            }
        }
        protected int samplePerStep;
        public int SamplePerStep
        {
            get { return samplePerStep; }
            set
            {
                if (value > 0)
                    samplePerStep = value;
            }
        }
        public bool ShowSolutions;
        public static int DefaultDiscoveryRadius = 10;
        public bool HasBitmap { get { return (controller as SimController).HasBitmap; } }
        public Timer playTimer;
        public int PlayInterval
        {
            set { if (value > 50) playTimer.Interval = value; }
            get { return playTimer.Interval; }
        }
        bool problemChanged = true;
        DualProblem lastProblem;

        public SimProg(PlacementView pview, IAgentForm form, bool interactionEnabled = true) :
            base(pview, form)
        {
            model = new SimModel(pview);
            controller = new SimController(model, this);
            samplePerStep = 100;
            ShowSolutions = false;
            if (interactionEnabled)
                InitView();

            playTimer = new Timer();
            playTimer.Interval = 1000;
            playTimer.Tick += playTimer_Tick;
        }

        void playTimer_Tick(object sender, EventArgs e)
        {
            if (!model.IsCompleted())
            {
                //(controller as SimController).SimulateOneStepBySampling(samplePerStep, ShowSolutions);
                List<List<double>> directions;
                List<List<int>> assignedIds;
                /*MCSearch problem;
                problem = new MCSearch(model as SimModel, (controller as SimController).BeliefSampler, new int[] { 20, 5, 20 });
                int[] levelsAssigned = new int[4];*/
                //problem.Solve(10, 100000, out assignedIds, out directions, ref levelsAssigned);
                DualProblem problem;
                if(problemChanged)
                    problem = new DualProblem(model as SimModel, (controller as SimController).BeliefSampler);
                else
                    problem = new DualProblem(model as SimModel, (controller as SimController).BeliefSampler, lastProblem);
                problem.gradientSampler = (controller as SimController).GradientSampler;
                problem.Solve(NofSlots, samplePerStep, out assignedIds, out directions);
                problemChanged = (controller as SimController).SimulateOneStepWithExternalResults(directions, assignedIds);
                lastProblem = problem;
                UpdateProgress();
            }
            else
            {
                playTimer.Stop();
                //MessageBox.Show("Simulation ended.", "Simulation end", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        internal void OpenBitmapAsDensityMap(System.IO.Stream fs, double? enTasks=null)
        {
            Bitmap density;
            if (enTasks == null)
            {
                DensityMap = new Bitmap(fs);
                density = densityMap;
                (controller as SimController).OpenBitmapAsDensityMap(density);
            }
            else
            {
                density = new Bitmap(fs);
                (controller as SimController).OpenBitmapAsDensityMap(density, (double)enTasks);
            }

            //clean TMP folder
            string tmpFolder = String.Concat(WorkFolder, (controller as SimController).TmpFolder);
            try
            {
                if (Directory.Exists(tmpFolder))
                {
                    Directory.Delete(tmpFolder, true);
                }
                Directory.CreateDirectory(tmpFolder);
            }
            catch (UnauthorizedAccessException e)
            {
                Console.WriteLine(e);
            }
        }

        internal void OpenBitmapAsBackground(System.IO.Stream fsbg)
        {
            DensityMap = new Bitmap(fsbg);
        }

        internal void OpenBitmapAsGradient(System.IO.Stream fs)
        {
            Bitmap gradient = new Bitmap(fs);
            (controller as SimController).OpenBitmapAsGradient(gradient);
        }

        internal void OpenFile(System.IO.Stream fs)
        {
            IFormatter formatter = new BinaryFormatter();
            SimModel newModel = formatter.Deserialize(fs) as SimModel;
            View.Clear();
            newModel.View = View;
            newModel.RefreshRelations();
            controller.Model = newModel;
            model = newModel;
            initModelState = new SimModel(newModel);
            form.SetNofSlot(model.NofSlots);
            form.SetTravelTime(model.TravelTime);
            problemChanged = true;
        }

        internal void Step()
        {
            if (!playTimer.Enabled)
                playTimer_Tick(this, null);
        }

        public void RefreshBitmap()
        {
            (controller as SimController).RefreshBitmap(densityMap);
            View.Invalidate();
        }

        internal void ClearUndiscoveredArea()
        {
            (controller as SimController).ClearOutsideRectangles(model.GetDiscoveryRectangles());
            RefreshBitmap();
        }

        internal void Play()
        {
            playTimer.Start();
        }

        internal void Stop()
        {
            playTimer.Stop();
        }

        internal void Reset()
        {
            this.model = new SimModel(this.initModelState);
            this.model.View = View;
            problemChanged = true;
        }

        internal virtual void GenerateDiscoveryProblemOutput(TextWriter tw)
        {
            (controller as SimController).GenerateDiscoveryProblemOutput(tw, model as SimModel);
            tw.Close();
        }

        internal string RunSimulationNoUI(string solver, int sampleNum, int timeStep, ProgressBar progressBar, bool useSampling)
        {
            SimModel copyModel = new SimModel(this.model as SimModel);
            //Console.WriteLine((this.model as SimModel).GetHashCode());
            //SimController copyController = new SimController(copyModel, this);
            SimController.timeStep = timeStep;
            controller.Model = copyModel;
            (controller as SimController).Solver = solver;
            int[] taskNumStart = new int[2] { copyModel.GetGroundTruthNofTasks(0), copyModel.GetGroundTruthNofTasks(1) };
            int[] batteryTime = new int[2] { copyModel.TravelTime[0], copyModel.TravelTime[1] };

            progressBar.Maximum = copyModel.TravelTime.Max();

            Stopwatch computationTime = new Stopwatch();
            Stopwatch solverTime = new Stopwatch();
            int nofSteps = 0;
            string stamp = String.Concat(DateTime.Now.ToString("yyyy_MM_dd_hh_mm_ss"), solver.Replace('>', '_').Replace(';', '_').Replace(' ', '_').Replace('.', '_'), useSampling ? "_Sampled" : "_NoSample");
            using (TextWriter matlabOut = new StreamWriter(String.Concat(AgentProg.WorkFolder, "Sim", stamp, ".m"), false))
            {
                matlabOut.WriteLine("TimeStepData = [...");
                while (!copyModel.IsCompleted())
                {
                    computationTime.Start();
                    if (useSampling)
                        (controller as SimController).SimulateOneStepBySampling(sampleNum, false, solverTime);
                    else
                        (controller as SimController).SimulateOneStepExisting(false, solverTime);
                    computationTime.Stop();
                    progressBar.Value = progressBar.Maximum - copyModel.TravelTime.Max();
                    matlabOut.Write(copyModel.GetTimestepData());
                    nofSteps++;
                }
                matlabOut.WriteLine("];\n");
                matlabOut.Write(copyModel.GetSummaryData());
                matlabOut.Close();
            }
            double simTime = (controller as SimController).TimeCounter;
            double utilHi = copyModel.GetGainedUtility(0);
            double utilLo = copyModel.GetGainedUtility(1);
            double distHi = copyModel.GetFlightDistance(0);
            double distLo = copyModel.GetFlightDistance(1);
            int[] taskNumEnd = new int[2] { copyModel.GetGroundTruthNofTasks(0), copyModel.GetGroundTruthNofTasks(1) };
            controller.Model = this.model;
            progressBar.Value = 0;
            string statusSummary = "";
            statusSummary = String.Format("{0}Solver\t{1}", statusSummary, (controller as SimController).Solver);
            statusSummary = String.Format("{0}\nSample/step\t{1}", statusSummary, useSampling ? sampleNum.ToString() : "No Sampling");
            statusSummary = String.Format("{0}\nTimestep\t{1}", statusSummary, SimController.timeStep);
            statusSummary = String.Format("{0}\nBattery time\n{1}\t{2}", statusSummary, batteryTime[0], batteryTime[1]);
            statusSummary = String.Format("{0}\nTime spent by UAVs:\n\t{1}", statusSummary, simTime);
            statusSummary = String.Format("{0}\nUilities:\n{1}\t{2}", statusSummary, utilHi, utilLo);
            statusSummary = String.Format("{0}\nDistances:\n{1}\t{2}", statusSummary, distHi, distLo);
            statusSummary = String.Format("{0}\nNumber of tasks:\nStart\n{1}\t{2}\nEnd\n{3}\t{4}", statusSummary, taskNumStart[0], taskNumStart[1], taskNumEnd[0], taskNumEnd[1]);
            statusSummary = String.Format("{0}\nComputation time:\n\t{1}\nof which by solver:\n\t{2}", statusSummary, computationTime.ElapsedMilliseconds, solverTime.ElapsedMilliseconds);
            statusSummary = String.Format("{0}\nAverage time per step:\n\t{1}\nof which by solver:\n\t{2}", statusSummary, computationTime.ElapsedMilliseconds / nofSteps, solverTime.ElapsedMilliseconds / nofSteps);
            statusSummary = String.Format("{0}\nAverage time per sample by solver:\n\t{1}", statusSummary, useSampling ? (solverTime.ElapsedMilliseconds / nofSteps / sampleNum).ToString() : "No Sampling");

            using (TextWriter sumOut = new StreamWriter(String.Concat(AgentProg.WorkFolder, "Summary", stamp, ".txt"), false))
            {
                sumOut.WriteLine(statusSummary);
                sumOut.Close();
            }
            return statusSummary;
        }

        internal void SolveDiscovery()
        {
            (controller as SimController).SolveDiscovery(true);
        }

        internal void SaveSampler(Stream fs)
        {
            (controller as SimController).SaveSampler(fs);
        }

        internal void SaveNewModel(string filename)
        {
            (controller as SimController).ResampleGroundTruth();
            using (Stream fs = new FileStream(filename, FileMode.Create))
            {
                SaveFile(fs);
            }
        }

        internal void ResetSpeed()
        {
            model.ResetSpeed();
        }

        internal void SolveGroundTruth()
        {
            Func<List<string>, bool> noFunc = delegate(List<string> l) {return true;};
            Stopwatch sw = new Stopwatch();
            Stopwatch solverTime = new Stopwatch();
            sw.Start();
            //(controller as SimController).SimualteSinglePlan(false, "oplgeninteger2.py>dat;intmodel_real.mod", noFunc);
            //(controller as SimController).SimualteSinglePlan(true, "NeighborhoodShuffle.py", noFunc, 30000, "SimOpt.m", solverTime);
            (controller as SimController).SimualteSinglePlan(true, "greedyAgents.py", noFunc, 0, "SimGrdy.m", solverTime);
            sw.Stop();
            Console.WriteLine(String.Format("run time: {0}\nsolver time: {1}", sw.ElapsedMilliseconds, solverTime.ElapsedMilliseconds));
        }

        internal void setDefaultParameters()
        {
            model.SetDefaultParams();
        }

        internal void ShowPoint(Point point)
        {
            View.ShowPoint(point);
        }
        
        public void OpenBeliefSampler(System.IO.Stream fs)
        {
            (controller as SimController).LoadBeliefSampler(fs);
        }

        internal void AddDiscoveryGrid(Point topleft, Size size, int p)
        {
            for (int x = 0; x < size.Width; x++)
                for (int y = 0; y < size.Height; y++)
                {
                    Size offset = new Size(p * x + p / 2, p * y + p / 2);
                    (model as SimModel).AddDiscoveryTask(new NodeModel(Point.Add(topleft, offset)));
                }
        }

        internal void RemoveEmptyDiscoveries()
        {
            (model as SimModel).RemoveEmptyDiscoveries((controller as SimController).BeliefSampler);
        }

        private int GtRescueCount;
        /// <summary>
        /// Sets the limit for the progress bars.
        /// Search progress: #executed search tasks + #discovered rescue tasks
        /// Rescue progress: #executed rescue tasks
        /// Search maximum: #search tasks + #undiscovered rescue tasks
        /// Rescue maximum: #undiscovered rescue tasks
        /// </summary>
        /// <returns>[search progress bar maximum, rescue progress bar maximum]</returns>
        internal int[] GetProgressSize()
        {
            GtRescueCount = (model as SimModel).UndiscoveredTaskCount;
            return new int[] { (model as SimModel).SearchTaskCount + GtRescueCount, GtRescueCount };
        }

        /// <summary>
        /// Sets the current value of the progress bars on the form.
        /// Search progress: #executed search tasks + #discovered rescue tasks
        /// Rescue progress: #executed rescue tasks
        /// </summary>
        private void UpdateProgress()
        {
            int[] tasksDone = new int[(model as SimModel).ExecutedTaskCount.Length];
            Array.Copy((model as SimModel).ExecutedTaskCount, tasksDone, tasksDone.Length);
            tasksDone[0] += GtRescueCount - (model as SimModel).UndiscoveredTaskCount;
            
            form.SetProgress(tasksDone, (model as SimModel).AvgExecutionTime);
        }
    }
}
