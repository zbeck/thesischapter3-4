﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using System.Runtime.Serialization;

namespace AgentDraw
{
    public delegate void ChangedEventHandler(object sender);

    [Serializable]
    public class NodeModel : IComparable, IDeserializationCallback
    {
        public int ID { get { return this.id; } }

        static int nextID = 0;
        public static int NextID { set { if (value >= nextID) nextID = value; } }
        Point position;
        bool isSelected;
        public int ReachRadius;
        public int DiscoveryRadius;

        public event ChangedEventHandler Changed;
        private int id;

        [NonSerialized]
        private List<Node> views;
        public IEnumerable<Node> Views { get { return views; } }

        public static string ViewTypeErrorMessage = "Wrong view type specified!";

        public NodeModel()
        {
            DefaultConstructor();
        }

        public NodeModel(Point pos, bool isSelected = false)
        {
            this.isSelected = isSelected;
            position = pos;
            DefaultConstructor();
        }

        private void DefaultConstructor()
        {
            id = nextID++;
            views = new List<Node>();
            Changed = RefreshViews;

            ReachRadius = 0;
            DiscoveryRadius = 0;
        }

        /// <summary>
        /// Copy constructor that doesn't copy the view
        /// </summary>
        /// <param name="source">source node</param>
        public NodeModel(NodeModel source)
        {
            id = source.id;
            position = source.position;
            isSelected = source.isSelected;
            views = new List<Node>();
            Changed = RefreshViews;

            ReachRadius = source.ReachRadius;
            DiscoveryRadius = source.DiscoveryRadius;
        }

        public Point Position
        {
            get { return position; }
            set
            {
                if (position != value)
                {
                    position = value;
                    Changed(this);
                }
            }
        }

        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                if (isSelected != value)
                {
                    isSelected = value;
                    Changed(this);
                }
            }
        }

        public override bool Equals(object obj)
        {
            //Console.WriteLine("NodeModel.Equals called.");
            NodeModel o = obj as NodeModel;
            if (id != o.ID)
                return false;
            if (!position.Equals(o.position))
                return false;
            return true;
        }

        public override int GetHashCode()
        {
            int result = 57 + id.GetHashCode();
            result *= 401;
            result += position.GetHashCode();
            return result;
        }

        public int CompareTo(object obj)
        {
            NodeModel other = obj as NodeModel;
            return this.ID.CompareTo(other.ID);
        }

        public void RemoveView(Node node)
        {
            views.Remove(node);
        }

        public Node AddNewView(AgentController controller, Type type, Form parentForm)
        {
            if (type.Equals(typeof(Task)))
            {
                Task newtask = new Task(id, controller, this, parentForm);
                views.Add(newtask);
                Changed(this);
                return newtask;
            }
            if (type.Equals(typeof(UAV)))
            {
                UAV newuav = new UAV(id, controller, this, parentForm);
                views.Add(newuav);
                Changed(this);
                return newuav;
            }
            throw new Exception(ViewTypeErrorMessage);
        }

        protected void RefreshViews(object sender)
        {
            foreach (Node view in views)
            {
                view.IsSelected = isSelected;
                view.Position = position;
            }
        }

        public void SetReachRadius(int radius)
        {
            ReachRadius = radius;
        }

        public void SetDiscoveryRadius(int radius)
        {
            DiscoveryRadius = radius;
        }

        public void OnDeserialization(object sender)
        {
            views = new List<Node>();
            Changed = RefreshViews;
        }

        public static void ResetIDCounter()
        {
            nextID = 0;
        }

        internal Rectangle GetDiscoveryArea()
        {
            Size radius = new Size(DiscoveryRadius, DiscoveryRadius);
            Point center = Position;
            return new Rectangle(Point.Subtract(center, radius), radius + radius);
        }
    }
}
