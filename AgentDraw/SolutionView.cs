﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AgentDraw
{
    public class SolutionView: PlacementView
    {
        static Pen PathColor = new Pen(System.Drawing.Color.FromArgb(232, 0, 46));      //Malefactor color (quite red)
        static Pen PathArrowColor = new Pen(System.Drawing.Color.FromArgb(232, 0, 46)); //Malefactor color (quite red)
        protected List<RelationModel> paths;
        public List<RelationModel> Paths { set { paths = value; } }

        public SolutionView()
            : base()
        {
            paths = new List<RelationModel>();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            foreach (RelationModel r in paths)
            {
                Draw(e.Graphics, r, PathColor, PathArrowColor);
            }
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // SolutionContainer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Name = "SolutionContainer";
            this.ResumeLayout(false);

        }

    }
}
