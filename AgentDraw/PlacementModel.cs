﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace AgentDraw
{
    [Serializable]
    public class PlacementModel : AgentModel
    {
        public PlacementModel(PlacementView view) :
            base(view)
        {
            UAVNodes.Add(new NodeList());
            TaskNodes.Add(new NodeList());
            speed = new double[1] { 1.0 };

            InitDefault();
        }

        /// <summary>
        /// Copy constructor
        /// !!! WARNING !!! Doesn't set view
        /// </summary>
        /// <param name="source">copy source</param>
        public PlacementModel(AgentModel source) :
            base(source)
        {
            InitDefault();
        }

        public bool UnconnectUAV(UAV uav)
        {
            int removed = Connections.RemoveAll(c => uav.Model.Equals(c.To));
            if (removed > 0)
                RefreshRelations();
            return (removed > 0);
        }

        internal void ConnectUAV(NodeModel uav)
        {
            foreach (NodeModel t in TaskNodes[0])
            {
                if (t.IsSelected)
                    AddConnection(new RelationModel(t, uav));
            }
            RefreshRelations();
        }

        internal void ActivateFrom(Task task)
        {
            foreach (NodeModel t in TaskNodes[0])
            {
                if (t.IsSelected)
                {
                    AddActivation(new RelationModel(task.Model, t));
                    t.IsSelected = false;
                }
            }
            RefreshRelations();
        }
    }
}
