﻿namespace AgentDraw
{
    partial class AskForTwoNumbersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxEval2 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBoxEval2
            // 
            this.textBoxEval2.Location = new System.Drawing.Point(13, 64);
            this.textBoxEval2.Name = "textBoxEval2";
            this.textBoxEval2.Size = new System.Drawing.Size(260, 20);
            this.textBoxEval2.TabIndex = 3;
            this.textBoxEval2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxEval2_KeyPress);
            // 
            // AskFroTwoNumbersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(284, 133);
            this.Controls.Add(this.textBoxEval2);
            this.Name = "AskFroTwoNumbersForm";
            this.Controls.SetChildIndex(this.textBoxEVal, 0);
            this.Controls.SetChildIndex(this.textBoxEval2, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.TextBox textBoxEval2;

    }
}
