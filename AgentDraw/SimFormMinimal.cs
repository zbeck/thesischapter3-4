﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AgentDraw
{
    public partial class SimFormMinimal : Form, IAgentForm
    {
        Size FormAndViewSizeDiff = new Size(40, 112);
        public SimProg Program;
        public SimFormMinimal(bool interaction = false)
        {
            InitializeComponent();
            placementView.ShowArea = false;
            placementView.ShowConnections = false;
            placementView.ShowRadius = false;
            placementView.ShowActivations = false;
            placementView.ShowNames = false;
            this.Program = new SimProg(placementView, this, interaction);
        }

        public void ReSize(Size size)
        {
            Size = Size.Add(size, FormAndViewSizeDiff);
        }

        public void SetTravelTime(int p, int level)
        {
            Program.SetTravelTime(p, level);
        }

        public void SetTravelTime(int[] p)
        {
            for (int i = 0; i < p.Length; i++)
                SetTravelTime(p[i], i);
        }

        public void SetNofSlot(int p)
        {
            Program.NofSlots = p;
        }

        public void InitProgress()
        {
            int[] maxprogress = this.Program.GetProgressSize();
            progressBarSearch.Minimum = 0;
            progressBarSearch.Maximum = maxprogress[0];
            progressBarRescue.Minimum = 0;
            progressBarRescue.Maximum = maxprogress[1];
        }

        public void SetProgress(int[] p, double[] rescueTime)
        {
            progressBarSearch.Value = p[0];
            progressBarRescue.Value = p[1];
            if (Double.IsNaN(rescueTime[1]))
                labelRecscueTime.Text = "Average Rescue Time: -";
            else
                labelRecscueTime.Text = "Average Rescue Time: " + (rescueTime[1] / 10).ToString("F0") + " (" + p[1].ToString() + " tasks)";
        }

        public Bitmap GrabFrame()
        {
            Size imgsize = Size;
            Point imgloc = new Point(0,0);
            Bitmap frame = new Bitmap(imgsize.Width, imgsize.Height);
            this.DrawToBitmap(frame, new Rectangle(imgloc, imgsize));
            Point locationOnForm = new Point(labelRecscueTime.Location.X, labelRecscueTime.Location.Y);
            locationOnForm.Offset(8, 31);
            labelRecscueTime.DrawToBitmap(frame, new Rectangle(locationOnForm, labelRecscueTime.Size));
            return frame;
        }
    }
}
