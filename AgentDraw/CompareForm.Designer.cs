﻿namespace AgentDraw
{
    partial class CompareForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listViewCompare = new System.Windows.Forms.ListView();
            this.columnHeaderID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderRatioLow = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderRatioHi = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderRatioFirst = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderDiscoveryTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderExecutionTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderTimeSum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.textBoxSolver = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxSpeed1 = new System.Windows.Forms.TextBox();
            this.textBoxSpeed0 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonSolve = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.progressBarSolve = new System.Windows.Forms.ProgressBar();
            this.columnHeaderLowUtil = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderUtil = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // listViewCompare
            // 
            this.listViewCompare.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewCompare.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderID,
            this.columnHeaderRatioLow,
            this.columnHeaderRatioHi,
            this.columnHeaderRatioFirst,
            this.columnHeaderDiscoveryTime,
            this.columnHeaderExecutionTime,
            this.columnHeaderTimeSum,
            this.columnHeaderLowUtil,
            this.columnHeaderUtil});
            this.listViewCompare.Location = new System.Drawing.Point(12, 12);
            this.listViewCompare.Name = "listViewCompare";
            this.listViewCompare.Size = new System.Drawing.Size(722, 510);
            this.listViewCompare.TabIndex = 0;
            this.listViewCompare.UseCompatibleStateImageBehavior = false;
            this.listViewCompare.View = System.Windows.Forms.View.Details;
            this.listViewCompare.SelectedIndexChanged += new System.EventHandler(this.listViewCompare_SelectedIndexChanged);
            // 
            // columnHeaderID
            // 
            this.columnHeaderID.Text = "ID";
            this.columnHeaderID.Width = 165;
            // 
            // columnHeaderRatioLow
            // 
            this.columnHeaderRatioLow.Text = "C Rate Lo";
            // 
            // columnHeaderRatioHi
            // 
            this.columnHeaderRatioHi.Text = "C Rate Hi";
            // 
            // columnHeaderRatioFirst
            // 
            this.columnHeaderRatioFirst.Text = "C Rate 1st";
            this.columnHeaderRatioFirst.Width = 65;
            // 
            // columnHeaderDiscoveryTime
            // 
            this.columnHeaderDiscoveryTime.Text = "Time discovery";
            this.columnHeaderDiscoveryTime.Width = 85;
            // 
            // columnHeaderExecutionTime
            // 
            this.columnHeaderExecutionTime.Text = "Time execution";
            this.columnHeaderExecutionTime.Width = 89;
            // 
            // columnHeaderTimeSum
            // 
            this.columnHeaderTimeSum.Text = "Time score";
            this.columnHeaderTimeSum.Width = 69;
            // 
            // textBoxSolver
            // 
            this.textBoxSolver.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSolver.Location = new System.Drawing.Point(740, 29);
            this.textBoxSolver.Name = "textBoxSolver";
            this.textBoxSolver.Size = new System.Drawing.Size(157, 20);
            this.textBoxSolver.TabIndex = 1;
            this.textBoxSolver.Text = "oplgeninteger2.py>dat;intmodel_real.mod";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(741, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Solver:";
            // 
            // textBoxSpeed1
            // 
            this.textBoxSpeed1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSpeed1.Location = new System.Drawing.Point(740, 89);
            this.textBoxSpeed1.Name = "textBoxSpeed1";
            this.textBoxSpeed1.Size = new System.Drawing.Size(70, 20);
            this.textBoxSpeed1.TabIndex = 3;
            this.textBoxSpeed1.Text = "1";
            // 
            // textBoxSpeed0
            // 
            this.textBoxSpeed0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSpeed0.Location = new System.Drawing.Point(827, 89);
            this.textBoxSpeed0.Name = "textBoxSpeed0";
            this.textBoxSpeed0.Size = new System.Drawing.Size(70, 20);
            this.textBoxSpeed0.TabIndex = 4;
            this.textBoxSpeed0.Text = "1";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(737, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Speeds:";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(737, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Low level";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(824, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "High level";
            // 
            // buttonSolve
            // 
            this.buttonSolve.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSolve.Location = new System.Drawing.Point(740, 126);
            this.buttonSolve.Name = "buttonSolve";
            this.buttonSolve.Size = new System.Drawing.Size(157, 23);
            this.buttonSolve.TabIndex = 8;
            this.buttonSolve.Text = "Solve";
            this.buttonSolve.UseVisualStyleBackColor = true;
            this.buttonSolve.Click += new System.EventHandler(this.buttonSolve_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(778, 195);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Test";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // progressBarSolve
            // 
            this.progressBarSolve.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarSolve.Location = new System.Drawing.Point(740, 499);
            this.progressBarSolve.Name = "progressBarSolve";
            this.progressBarSolve.Size = new System.Drawing.Size(157, 23);
            this.progressBarSolve.TabIndex = 10;
            this.progressBarSolve.Visible = false;
            this.progressBarSolve.Click += new System.EventHandler(this.progressBarSolve_Click);
            // 
            // columnHeaderLowUtil
            // 
            this.columnHeaderLowUtil.Text = "Util (low)";
            // 
            // columnHeaderUtil
            // 
            this.columnHeaderUtil.Text = "Utility";
            // 
            // CompareForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(909, 534);
            this.Controls.Add(this.progressBarSolve);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonSolve);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxSpeed0);
            this.Controls.Add(this.textBoxSpeed1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxSolver);
            this.Controls.Add(this.listViewCompare);
            this.Name = "CompareForm";
            this.Text = "CompareForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listViewCompare;
        private System.Windows.Forms.ColumnHeader columnHeaderID;
        private System.Windows.Forms.ColumnHeader columnHeaderRatioLow;
        private System.Windows.Forms.ColumnHeader columnHeaderRatioHi;
        private System.Windows.Forms.ColumnHeader columnHeaderRatioFirst;
        private System.Windows.Forms.ColumnHeader columnHeaderDiscoveryTime;
        private System.Windows.Forms.ColumnHeader columnHeaderExecutionTime;
        private System.Windows.Forms.ColumnHeader columnHeaderTimeSum;
        private System.Windows.Forms.TextBox textBoxSolver;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxSpeed1;
        private System.Windows.Forms.TextBox textBoxSpeed0;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonSolve;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ProgressBar progressBarSolve;
        private System.Windows.Forms.ColumnHeader columnHeaderLowUtil;
        private System.Windows.Forms.ColumnHeader columnHeaderUtil;
    }
}