﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AgentDraw
{
    public partial class CompareForm : Form
    {
        CompareProg program;
        double[] speed;
        public CompareForm()
        {
            program = new CompareProg();
            InitializeComponent();
            RefreshCompare();
            speed = program.Speed;
            textBoxSpeed0.Text = speed[0].ToString();
            textBoxSpeed1.Text = speed[1].ToString();
        }

        public void RefreshCompare()
        {
            listViewCompare.Items.Clear();
            listViewCompare.Groups.Clear();
            foreach (ListViewGroup g in program.ListViewGroups)
                listViewCompare.Groups.Add(g);
            foreach (ListViewItem c in program.ListViewItems)
                listViewCompare.Items.Add(c);
        }

        private void listViewCompare_SelectedIndexChanged(object sender, EventArgs e)
        {
            program.ChangeSelected(listViewCompare.SelectedIndices);
        }

        private void buttonSolve_Click(object sender, EventArgs e)
        {
            string solver = textBoxSolver.Text;
            bool p0 = Double.TryParse(textBoxSpeed1.Text, out speed[0]);
            bool p1 = Double.TryParse(textBoxSpeed0.Text, out speed[1]);

            if(!p0 || !p1)
                if(MessageBox.Show(String.Concat("Speed parse error.\r\nUsing: ", speed[0].ToString(), ", ", speed[1].ToString()), "Speed error", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.Cancel)
                    return;

            listViewCompare.SelectedIndices.Clear();
            program.Solve(solver, speed, progressBarSolve);
            RefreshCompare();
            progressBarSolve.Visible = false;
            progressBarSolve.Value = 0;
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            program.CloseWindows();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to overwrite the caompare problems??", "ARE YOU SURE?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                program.Test();
        }

        private void progressBarSolve_Click(object sender, EventArgs e)
        {
            RefreshCompare();
        }
    }
}
