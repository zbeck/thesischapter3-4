﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AgentDraw
{
    public partial class SimForm : Form, IAgentForm
    {
        Size FormAndViewSizeDiff = new Size(146, 78);
        SimProg Program;

        public SimForm()
        {
            InitializeComponent();

            this.Program = new SimProg(placementView, this);
            placementView.ShowArea = true;

            //Initalize values
            Program.NofSlots = 3;
            sampleNumTextBox.Text = Program.SamplePerStep.ToString();
            nofSlotsBox.Text = Program.NofSlots.ToString();
            checkBoxConnections_CheckedChanged(this, null);
            checkBoxActivation_CheckedChanged(this, null);
            checkBoxName_CheckedChanged(this, null);
            checkBoxSolutions_CheckedChanged(this, null);
        }

        public void ReSize(Size size)
        {
            Size = Size.Add(size, FormAndViewSizeDiff);
        }

        public void SetTravelTime(int p, int level)
        {
            Program.SetTravelTime(p, level);
        }

        public void SetTravelTime(int[] p)
        {
            for (int i = 0; i < p.Length; i++)
                SetTravelTime(p[i], i);
        }

        public void SetNofSlot(int p)
        {
            Program.NofSlots = p;
        }

        private void sampleNumTextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                Program.SamplePerStep = Int32.Parse(sampleNumTextBox.Text);
            }
            catch (FormatException ex) { ex.ToString(); }
            finally
            {
                sampleNumTextBox.Text = Program.SamplePerStep.ToString();
            }
        }

        private void sampleNumTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                sampleNumTextBox_Leave(sender, e);
            }
        }

        private void nofSlotsBox_Leave(object sender, EventArgs e)
        {
            try
            {
                SetNofSlot(Int32.Parse(nofSlotsBox.Text));
            }
            catch (FormatException ex) { ex.ToString(); }
            finally
            {
                nofSlotsBox.Text = Program.NofSlots.ToString();
            }
        }

        private void nofSlotsBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                nofSlotsBox_Leave(sender, e);
            }
        }

        private void checkBoxConnections_CheckedChanged(object sender, EventArgs e)
        {
            placementView.ShowConnections = checkBoxConnections.Checked;
            placementView.Invalidate();
        }

        private void checkBoxActivation_CheckedChanged(object sender, EventArgs e)
        {
            placementView.ShowActivations = checkBoxActivation.Checked;
            placementView.Invalidate();
        }

        private void checkBoxName_CheckedChanged(object sender, EventArgs e)
        {
            placementView.ShowNames = checkBoxName.Checked;
            placementView.Invalidate();
        }

        private void checkBoxSolutions_CheckedChanged(object sender, EventArgs e)
        {
            Program.ShowSolutions = checkBoxSolutions.Checked;
        }

        private void checkBoxDiscovery_CheckedChanged(object sender, EventArgs e)
        {
            placementView.ShowArea = checkBoxDiscovery.Checked;
        }

        private void checkBoxRadius_CheckedChanged(object sender, EventArgs e)
        {
            placementView.ShowRadius = checkBoxRadius.Checked;
        }

        private void trackBarDistance_Scroll(object sender, EventArgs e)
        {
            SetTravelTime(trackBarDistance0.Value, 0);
            checkBoxRadius.Checked = true;
        }

        private void trackBarDistance1_Scroll(object sender, EventArgs e)
        {
            SetTravelTime(trackBarDistance1.Value, 1);
            checkBoxRadius.Checked = true;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "Multi-level UAV-Task simulation|*.mus";
            saveDialog.Title = "Save simulation";
            saveDialog.RestoreDirectory = true;
            saveDialog.InitialDirectory = "../../";
            saveDialog.ShowDialog();

            if (saveDialog.FileName != "")
            {
                using (Stream fs = saveDialog.OpenFile())
                {
                    Program.SaveFile(fs);
                }
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Multi-level UAV-Task simulation|*.mus";
            openDialog.Title = "Open simulation";
            openDialog.RestoreDirectory = true;
            openDialog.InitialDirectory = "../../";
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                using (Stream fs = openDialog.OpenFile())
                {
                    Program.OpenFile(fs);
                }
            }
        }

        private void openSamplerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "2D spatial Poisson process sampler|*.sam";
            openDialog.Title = "Open Sampler";
            openDialog.RestoreDirectory = true;
            openDialog.InitialDirectory = "../../";
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                using (Stream fs = openDialog.OpenFile())
                {
                    (Program as SimProg).OpenBeliefSampler(fs);
                }
            }
        }

        private void loadBitmapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Any Image Format|*.png;*.jpg;*.jpeg;*.gif|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|JPEG Files (*.jpeg)|*.jpeg|GIF Files (*.gif)|*.gif";
            openDialog.Title = "Open Density Map";
            openDialog.RestoreDirectory = true;
            openDialog.InitialDirectory = "../../";
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                Stream fs = openDialog.OpenFile();
                Program.OpenBitmapAsDensityMap(fs);
            }
        }

        private void runToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //load bitmap if nothing is loaded
            if (!Program.HasBitmap)
            {
                runToolStripMenuItem.HideDropDown();
                loadBitmapToolStripMenuItem_Click(this, null);
            }
        }

        private void stepToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.Step();
        }

        private void refreshBitmapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.RefreshBitmap();
        }

        private void playToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.Play();
        }

        private void stopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.Stop();
        }

        private void setSpeedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AskForNumberForm getSpeed = new AskForNumberForm("Simulation Speed");
            getSpeed.textBoxEVal.Text = Program.PlayInterval.ToString();
            getSpeed.ShowDialog();
            try
            {
                int eval = Int32.Parse(getSpeed.textBoxEVal.Text);
                Program.PlayInterval = eval;
            }
            catch (FormatException ex) { Console.WriteLine(ex.ToString()); }
        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.Reset();
        }

        private void setUAVSpeedsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.SpeedsDialog();
        }

        private void solveWithoutUIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SimRunnerForm newModalWindow = new SimRunnerForm(Program);
            newModalWindow.ShowDialog();
        }

        private void clearNondiscoveredToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.ClearUndiscoveredArea();
        }

        private void discoveryProblemOutputToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "CPLEX data file|*.dat";
            saveDialog.Title = "Save Discovery problem";
            saveDialog.RestoreDirectory = true;
            saveDialog.InitialDirectory = "../../";
            saveDialog.ShowDialog();

            if (saveDialog.FileName != "")
            {
                using (Stream fs = saveDialog.OpenFile())
                {
                    Program.GenerateDiscoveryProblemOutput(new StreamWriter(fs));
                }
            }
        }

        private void solveDiscoveryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.SolveDiscovery();
        }

        private void saveXMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "XML file|*.xml";
            saveDialog.Title = "Save model";
            saveDialog.RestoreDirectory = true;
            saveDialog.InitialDirectory = "../../";
            saveDialog.ShowDialog();

            if (saveDialog.FileName != "")
            {
                using (Stream fs = saveDialog.OpenFile())
                {
                    Program.SaveToXmlFile(fs);
                }
            }
        }

        private void saveBitmapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "2D spatial Poisson process sampler|*.sam";
            saveDialog.Title = "Save Sampler";
            saveDialog.RestoreDirectory = true;
            saveDialog.InitialDirectory = "../../";
            saveDialog.ShowDialog();

            if (saveDialog.FileName != "")
            {
                using (Stream fs = saveDialog.OpenFile())
                {
                    Program.SaveSampler(fs);
                }
            }
        }

        private void batchSaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AskForNumberForm getNum = new AskForNumberForm("Number of samples");
            getNum.textBoxEVal.Text = "100";
            int nSamples = 0;
            getNum.ShowDialog();
            try
            {
                nSamples = Int32.Parse(getNum.textBoxEVal.Text);
            }
            catch (FormatException ex) { Console.WriteLine(ex.ToString()); }
            
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "Multi-level UAV-Task simulation|*.mus";
            saveDialog.Title = "Save model batch";
            saveDialog.RestoreDirectory = true;
            saveDialog.InitialDirectory = "../../";
            saveDialog.ShowDialog();

            if (saveDialog.FileName != "")
            {
                int extension_cut = saveDialog.FileName.LastIndexOf('.');
                string baseName = saveDialog.FileName.Substring(0, extension_cut);
                string extension = saveDialog.FileName.Substring(extension_cut);
                
                for(int i=0; i<nSamples; i++){
                    Program.SaveNewModel(String.Concat(baseName, i.ToString("D3"), extension));
                }
            }
        }

        private void resetSpeedsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.ResetSpeed();
        }

        private void solveGroundTruthToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.SolveGroundTruth();
        }

        private void setDefaultParametersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.setDefaultParameters();
        }

        private void addDiscoveryGridToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AskForNumberForm formSpacing = new AskForNumberForm("Grid Sapcing");
            AskForTwoNumbersForm formSize = new AskForTwoNumbersForm("Grid Size");
            AskForTwoNumbersForm formPos = new AskForTwoNumbersForm("Grid Top Left Corner Position");
            formSpacing.ShowDialog();
            formSize.ShowDialog();
            formPos.ShowDialog();
            if ((formSpacing.DialogResult != System.Windows.Forms.DialogResult.OK) || (formSize.DialogResult != System.Windows.Forms.DialogResult.OK) || (formPos.DialogResult != System.Windows.Forms.DialogResult.OK))
                return;
            Program.AddDiscoveryGrid(new Point(Int32.Parse(formPos.textBoxEVal.Text), Int32.Parse(formPos.textBoxEval2.Text)), new Size(Int32.Parse(formSize.textBoxEVal.Text), Int32.Parse(formSize.textBoxEval2.Text)), Int32.Parse(formSpacing.textBoxEVal.Text));
        }

        private void removeEmptyDiscoveriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.RemoveEmptyDiscoveries();
        }


        public void SetProgress(int[] p, double[] t)
        {
            throw new NotImplementedException();
        }
    }
}
