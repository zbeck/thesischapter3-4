﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Globalization;
using MyExtensions;
using System.Diagnostics;

namespace AgentDraw
{
    public partial class MainForm : Form
    {
        PlacementForm editor;
        SamplerForm sampler;
        SimForm simulator;
        CompareForm comparer;
        SolutionExplorer solutionExplorer;
        DemoForm demo;

        public MainForm()
        {
            InitializeComponent();
            if(Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator != ".")
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-UK");
            //testButton_Click(this, null);
        }

        private void buttonEditor_Click(object sender, EventArgs e)
        {
            if ((editor == null) || (!editor.Visible))
            {
                editor = new PlacementForm();
                editor.Show();
            }
            else
                editor.BringToFront();
        }

        private void buttonSampler_Click(object sender, EventArgs e)
        {
            if ((sampler == null) || (!sampler.Visible))
            {
                sampler = new SamplerForm();
                sampler.Show();
            }
            else
                sampler.BringToFront();
        }
        
        private void testButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Multi-level UAV-Task simulation|*.mus";
            openDialog.Title = "Open problems";
            openDialog.RestoreDirectory = true;
            openDialog.InitialDirectory = "../../";
            openDialog.Multiselect = true;
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                foreach (String file in openDialog.FileNames)
                {
                    using (FileStream fs = new FileStream(file, FileMode.Open))
                    {
                        System.Runtime.Serialization.IFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                        SimModel newModel = formatter.Deserialize(fs) as SimModel;
                        /*
                         * Scaling the model
                         */
                        double scaler = 0.5;
                        SimModel scaledModel = new SimModel(newModel);
                        scaledModel.Scale(scaler);
                        string newName = file.Replace("\\model", "\\scaledmodel");
                        using (FileStream fsout = new FileStream(newName, FileMode.Create))
                        {
                            formatter.Serialize(fsout, scaledModel);
                        }

                        /*
                         * Getting ground truth models
                         */
                        /*
                        SimModel gtModel = newModel.GetGroundTruthModel();
                        string newDirName = Path.GetDirectoryName(file)+"_t";
                        if (!Directory.Exists(newDirName))
                            Directory.CreateDirectory(newDirName);
                        string newFileName = Path.Combine(newDirName, Path.ChangeExtension(Path.GetFileName(file), "txt"));
                        using (TextWriter tw = new StreamWriter(newFileName))
                        {
                            gtModel.GenerateProblemOutput(tw, gtModel.GetMaxSlots());
                        }*/

                    }
                }
            }
            /*Stopwatch computationTime = new Stopwatch();
            NodeModel[] uavs = { new NodeModel(new Point(365, 246)), new NodeModel(new Point(430, 63)) };
            NodeModel[] allTasks = { new NodeModel(new Point(205, 107)), new NodeModel(new Point(257, 81)), new NodeModel(new Point(315, 71)),
                                    new NodeModel(new Point(321, 73)), new NodeModel(new Point(360, 90)), new NodeModel(new Point(370, 119)),
                                    new NodeModel(new Point(370, 187)), new NodeModel(new Point(271, 217)), new NodeModel(new Point(228, 208)),
                                    new NodeModel(new Point(226, 186)), new NodeModel(new Point(232, 144)), new NodeModel(new Point(282, 143)) };
            int nTasks = 12;
            NodeModel[] tasks = new NodeModel[nTasks];
            Array.Copy(allTasks, tasks, nTasks);
            //AStarSolver.TeamPath<NodeModel>  best = EvaluateSolver(computationTime, uavs, tasks, AStarSolver.TeamEstimator<NodeModel>.DumbOverestimate);
            //ValidateEstimate(best, AStarSolver.TeamEstimator<NodeModel>.EnhancedOverestimate);
            AStarSolver.TeamPath<NodeModel> best = EvaluateSolver(computationTime, uavs, tasks, AStarSolver.TeamEstimator<NodeModel>.PreciseOverestimate);
            //EvaluateSolver(computationTime, uavs, tasks, AStarSolver.TeamEstimator<NodeModel>.MediumOverestimate);
            //EvaluateSolver(computationTime, uavs, tasks, AStarSolver.TeamEstimator<NodeModel>.ParallelOverestimate);
            MessageBox.Show(best.TotalUtil.ToString());
            this.Close();*/
        }

        /*private void ValidateEstimate(AStarSolver.TeamPath<NodeModel> best, Func<AStarSolver.TeamPath<NodeModel>, double> func)
        {
            int nAgents = best.Agents.Length;
            for (int agent = 0; agent < nAgents; agent++)
            {
                AStarSolver.TeamPath<NodeModel> current;
                while (best.StepBack(agent, out current))
                {
                    best = current;
                    double estimate = func(best);
                    Console.WriteLine(String.Format("{2}:\t{0}\t{1}", best.TotalUtil, best.TotalUtil+estimate, agent));
                }
            }
        }

        private static AStarSolver.TeamPath<NodeModel> EvaluateSolver(Stopwatch computationTime, NodeModel[] uavs, NodeModel[] tasks, Func<AStarSolver.TeamPath<NodeModel>, double> estimate)
        {
            //AStarSolver.TeamPath<NodeModel>.P = 3000;
            //AStarSolver.TeamEstimator<NodeModel>.EstimateCompare = 0;
            computationTime.Start();
            AStarSolver.TeamPath<NodeModel> best = AStarSolver.Program.FindPath<NodeModel>(uavs, 500 * 1.5, tasks, AgentModel.Distance, estimate);
            computationTime.Stop();
            foreach (AStarSolver.Agent a in best.Agents)
            {
                foreach (int i in a.Path)
                {
                    if (i >= tasks.Length)
                        Console.Write(String.Format("UAV{0} ", i - tasks.Length));
                    else
                    {
                        NodeModel n = tasks[i];
                        Console.Write(String.Format("({2})[{0}, {1}] ", n.Position.X, n.Position.Y, i));
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine(String.Format("Collected Utility: {0}", best.TotalUtil));
            Console.WriteLine(String.Format("Elapsed time[ms]: {0}", computationTime.Elapsed.TotalMilliseconds));
            Console.WriteLine(String.Format("Number of states visited: {0}", AStarSolver.TeamPath<NodeModel>.seenStates));
            //Console.WriteLine(String.Format("Number of cases when new estimate is lower: {0}", AStarSolver.TeamEstimator<NodeModel>.EstimateCompare));
            return best;
        }*/

        private void ChangingArgument(List<int> arglist)
        {
            arglist.Clear();
            arglist.Add(5);
            arglist.Add(6);
            arglist.Add(8);
        }

        private void buttonSimulator_Click(object sender, EventArgs e)
        {
            if ((simulator == null) || (!simulator.Visible))
            {
                simulator = new SimForm();
                simulator.Show();
            }
            else
                simulator.BringToFront();
        }

        private void buttonCompare_Click(object sender, EventArgs e)
        {
            if ((comparer == null) || (!comparer.Visible))
            {
                comparer = new CompareForm();
                comparer.Show();
            }
            else
                comparer.BringToFront();
        }

        private void buttonSolutionExplorer_Click(object sender, EventArgs e)
        {
            if ((solutionExplorer == null) || (!solutionExplorer.Visible))
            {
                solutionExplorer = new SolutionExplorer();
                solutionExplorer.Show();
            }
            else
                solutionExplorer.BringToFront();
        }

        private void buttonDemo_Click(object sender, EventArgs e)
        {
            if ((demo == null) || (!demo.Visible))
            {
                demo = new DemoForm();
                demo.Show();
            }
            else
                demo.BringToFront();
        }
    }
}
