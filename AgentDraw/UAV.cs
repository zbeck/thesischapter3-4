﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AgentDraw
{
    public class UAV : Node
    {
        public UAV(int id, AgentController cont, NodeModel model, Form parent)
            : base(String.Concat("U", id.ToString()), cont, model, parent)
        {
            DefaultColor = new SolidBrush(System.Drawing.Color.FromArgb(255, 187, 46)); //honey glow color (yellowish)
            //SecondaryColor = new SolidBrush(System.Drawing.Color.FromArgb(232, 0, 46)); //Malefactor color (quite red)
            SecondaryColor = new SolidBrush(System.Drawing.Color.FromArgb(18, 12, 0)); //Earth color (quite black)
            IsSelected = false;
        }

        public UAV(string id, AgentController cont, NodeModel model, Form parent)
            : base(id, cont, model, parent)
        {
            DefaultColor = Brushes.Green;
            SecondaryColor = Brushes.Red;
            IsSelected = false;
        }

        protected override void  OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            Rectangle rect = new Rectangle(0, 0, NodeSize, NodeSize);
            e.Graphics.FillEllipse(color, rect);
        }
    }
}
