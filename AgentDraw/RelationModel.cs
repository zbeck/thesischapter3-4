﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AgentDraw
{
    [Serializable]
    public class RelationModel
    {
        NodeModel from;
        NodeModel to;

        public NodeModel From { get { return from; } }
        public NodeModel To { get { return to; } }

        public RelationModel() { }

        public RelationModel(NodeModel from, NodeModel to)
        {
            this.from = from;
            this.to = to;
        }
    }
}
