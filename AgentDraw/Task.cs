﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AgentDraw
{
    public class Task : Node
    {
        public Task(int id, AgentController cont, NodeModel model, Form parent)
            : base(String.Concat("T", id.ToString()), cont, model, parent)
        {
            DefaultColor = new SolidBrush(System.Drawing.Color.FromArgb(0, 111, 47));   //Worry color (greenish-blueish)
            SecondaryColor = new SolidBrush(System.Drawing.Color.FromArgb(232, 0, 46)); //Malefactor color (quite red)
            IsSelected = false;
        }

        public Task(string id, AgentController cont, NodeModel model, Form parent)
            : base(id, cont, model, parent)
        {
            DefaultColor = Brushes.Blue;
            SecondaryColor = Brushes.Red;
            IsSelected = false;
        }

        protected override void  OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            Rectangle rect = new Rectangle(0, 0, NodeSize, NodeSize);
            e.Graphics.FillRectangle(color, rect);
        }
    }
}
