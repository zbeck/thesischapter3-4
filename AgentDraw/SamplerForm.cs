﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace AgentDraw
{
    public partial class SamplerForm : Form, IAgentForm
    {
        Size FormAndViewSizeDiff = new Size(41, 92);
        public GeneratorProg Program;

        public SamplerForm()
        {
            InitializeComponent();

            this.Program = new GeneratorProg(placementView, this);

            FormAndViewSizeDiff = Size.Subtract(this.Size, placementView.Size);

            placementView.ShowNames = false;
            Program.NofSlots = 1;
            nofSlotsBox.Text = "1";
            toolStripMenuItem2.Text = Program.NofSamples.ToString();
        }

        void SetStatusLabel()
        {
            statusLabel.Text = String.Concat("Expected task number: ", Program.sampler.ExpectedVal);
            Invalidate();
        }

        private void openBitmapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Any Image Format|*.png;*.jpg;*.jpeg;*.gif|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|JPEG Files (*.jpeg)|*.jpeg|GIF Files (*.gif)|*.gif";
            openDialog.Title = "Open Density Map";
            openDialog.RestoreDirectory = true;
            openDialog.InitialDirectory = "../../";
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                Stream fs = openDialog.OpenFile();
                Program.OpenBitmapAsDensityMap(fs);
                SetStatusLabel();
            }
        }

        private void sampleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.Sample();
        }

        private void checkBoxConnections_CheckedChanged(object sender, EventArgs e)
        {
            placementView.ShowConnections = checkBoxConnections.Checked;
        }

        private void checkBoxActivation_CheckedChanged(object sender, EventArgs e)
        {
            placementView.ShowActivations = checkBoxActivation.Checked;
        }

        private void checkBoxRadius_CheckedChanged(object sender, EventArgs e)
        {
            placementView.ShowRadius = checkBoxRadius.Checked;
        }

        private void nofSlotsBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Program.NofSlots = Int32.Parse(nofSlotsBox.Text);
            }
            catch (FormatException ex) { ex.ToString(); }
            finally
            {
                nofSlotsLabel.Text = Program.NofSlots.ToString();
            }
        }

        private void trackBarRadius_Scroll(object sender, EventArgs e)
        {
            Program.SetTravelTime(trackBarRadius0.Value, 0);
            labelRadius.Text = trackBarRadius0.Value.ToString();
            checkBoxRadius.Checked = true;
        }

        private void trackBarRadius1_Scroll(object sender, EventArgs e)
        {
            Program.SetTravelTime(trackBarRadius1.Value, 1);
            labelRadius.Text = trackBarRadius1.Value.ToString();
            checkBoxRadius.Checked = true;
        }

        private void checkBoxName_CheckedChanged(object sender, EventArgs e)
        {
            placementView.ShowNames = checkBoxName.Checked;
            placementView.Invalidate();
        }


        public void SetTravelTime(int p, int level)
        {
            Program.SetTravelTime(p, level);
            labelRadius.Text = p.ToString();
            trackBarRadius0.Value = p;
        }

        public void SetTravelTime(int[] p)
        {
            for (int i = 0; i < p.Length; i++)
                SetTravelTime(p[i], i);
        }

        public void SetNofSlot(int p)
        {
            Program.NofSlots = p;
            nofSlotsLabel.Text = Program.NofSlots.ToString();
        }

        public void ReSize(Size size)
        {
            Size = Size.Add(size, FormAndViewSizeDiff);
        }

        private void runToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Program.SolveSamples();
        }

        private void setNumberToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AskForNumberForm getEVal = new AskForNumberForm();
            getEVal.textBoxEVal.Text = Program.NofSamples.ToString();
            getEVal.ShowDialog();
            try
            {
                int eval = Int32.Parse(getEVal.textBoxEVal.Text);
                Program.NofSamples = eval;
                toolStripMenuItem2.Text = Program.NofSamples.ToString();
            }
            catch (FormatException ex) { Console.WriteLine(ex.ToString()); }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "Multi-level UAV-Task graph|*.mug";
            saveDialog.Title = "Save model";
            saveDialog.RestoreDirectory = true;
            saveDialog.InitialDirectory = "../../";
            saveDialog.ShowDialog();

            if (saveDialog.FileName != "")
            {
                using (Stream fs = saveDialog.OpenFile())
                {
                    Program.SaveFile(fs);
                }
            }
        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.Clear();
        }

        private void problemDescriptionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (TextWriter tw = new StreamWriter(fileNameTextBox.Text))
            {
                Program.GenerateProblemOutput(tw);
            }
        }

        private void factorGraphDescriptionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (TextWriter tw = new StreamWriter(fileNameTextBox.Text))
            {
                Program.GenerateOutput(tw);
            }
        }

        private void modelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Multi-level UAV-Task graph|*.mug";
            openDialog.Title = "Open graph";
            openDialog.RestoreDirectory = true;
            openDialog.InitialDirectory = "../../";
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                using (Stream fs = openDialog.OpenFile())
                {

                    Program.OpenFile(fs);
                }
            }
        }

        private void newSolutionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Title = "Open solution file";
            openDialog.RestoreDirectory = true;
            openDialog.InitialDirectory = "../../";
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                StreamReader sr = new StreamReader(openDialog.OpenFile());
                Program.OpenNewSolution(sr, openDialog.FileName);
                sr.Close();
            }
        }

        private void oldSolutionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Title = "Open solution file";
            openDialog.RestoreDirectory = true;
            openDialog.InitialDirectory = "../../";
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                StreamReader sr = new StreamReader(openDialog.OpenFile());
                Program.OpenSolution(sr, openDialog.FileName);
                sr.Close();
            }
        }

        private void revertStructureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.RevertModel();
            saveToolStripMenuItem_Click(this, null);
        }

        private void setTravelSpeedsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.SpeedsDialog();
        }

        private void solveLowLevelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.SolveAStar();
        }


        public void SetProgress(int[] p, double[] t)
        {
            throw new NotImplementedException();
        }
    }
}
