﻿namespace AgentDraw
{
    partial class Solution
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.solutionContainer = new AgentDraw.SolutionView();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.Location = new System.Drawing.Point(654, 12);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(162, 473);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            // 
            // solutionContainer
            // 
            this.solutionContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.solutionContainer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.solutionContainer.Location = new System.Drawing.Point(12, 12);
            this.solutionContainer.Name = "solutionContainer";
            this.solutionContainer.ShowActivations = true;
            this.solutionContainer.ShowArea = false;
            this.solutionContainer.ShowConnections = true;
            this.solutionContainer.ShowNames = true;
            this.solutionContainer.ShowRadius = false;
            this.solutionContainer.Size = new System.Drawing.Size(636, 473);
            this.solutionContainer.TabIndex = 0;
            // 
            // Solution
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 497);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.solutionContainer);
            this.Name = "Solution";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Solution";
            this.ResumeLayout(false);

        }

        #endregion

        protected SolutionView solutionContainer;
        private System.Windows.Forms.RichTextBox richTextBox1;



    }
}