﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AgentDraw
{
    public partial class AskForNumberForm : Form
    {
        public AskForNumberForm()
        {
            InitializeComponent();
        }

        public AskForNumberForm(string title)
        {
            InitializeComponent();
            Text = title;
            label1.Text = String.Concat("Please enter the ", title.ToLower());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBoxEVal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
                this.Close();
        }

        public new void ShowDialog()
        {
            base.ShowDialog();
            textBoxEVal.Focus();
            textBoxEVal.SelectAll();
        }
    }
}
