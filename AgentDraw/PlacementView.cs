﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MyExtensions;

namespace AgentDraw
{
    public partial class PlacementView : UserControl
    {
        static Pen ActivationColor = new Pen(System.Drawing.Color.FromArgb(25, 73, 0));     //stark color (dark grass-greenish)
        static Pen ConnectionColor = new Pen(System.Drawing.Color.FromArgb(180, 152, 0));   //Basement Blues color (dark golden)
        static Pen RadiusColor = new Pen(System.Drawing.Color.FromArgb(255, 187, 46));      //honey glow color (yellowish)
        static Brush AreaColor = new SolidBrush(System.Drawing.Color.FromArgb(127, 255, 187, 46));      //honey glow color (yellowish)

        protected NodeViewList<UAV> UAVs;
        protected NodeViewList<Task> Tasks;
        protected List<RelationModel> connections;
        public List<RelationModel> Connections
        {
            set { connections = value; }
        }
        protected List<RelationModel> activations;
        public List<RelationModel> Activations
        {
            set { activations = value; }
        }

        protected AgentController controller;
        internal AgentController Controller { set { controller = value; } }
        protected RelationModel marker;

        bool showConnections = true;
        bool showActivations = true;
        bool showNames = true;
        bool showRadius = false;
        bool showArea = false;
        public bool ShowConnections
        {
            set { showConnections = value; Invalidate(); }
            get { return showConnections; }
        }
        public bool ShowActivations
        {
            set { showActivations = value; Invalidate(); }
            get { return showActivations; }
        }
        public bool ShowNames
        {
            set { showNames = value; Invalidate(); }
            get { return showNames; }
        }
        public bool ShowRadius
        {
            set { showRadius = value; Invalidate(); }
            get { return showRadius; }
        }
        public bool ShowArea
        {
            set { showArea = value; Invalidate(); }
            get { return showArea; }
        }
        
        public PlacementView()
        {
            UAVs = new NodeViewList<UAV>();
            Tasks = new NodeViewList<Task>();
            connections = new List<RelationModel>();
            activations = new List<RelationModel>();

            UAVs.Changed += new ChangedEventHandler(Update);
            Tasks.Changed += new ChangedEventHandler(Update);

            InitializeComponent();
            DoubleBuffered = true;
        }
        
        protected virtual void Update(object sender)
        {
            Invalidate();
        }

        protected void Draw(Graphics g, RelationModel rel, Pen line, Pen mark = null){
            if (line != null)
            {
                g.DrawLine(line, rel.From.Position, rel.To.Position);
            }
            if (mark != null)
            {
                double x = rel.From.Position.X - rel.To.Position.X;
                double y = rel.From.Position.Y - rel.To.Position.Y;
                double len = Math.Sqrt(x * x + y * y);
                if (len == 0)
                    return;
                x *= Node.NodeSize / len;
                y *= Node.NodeSize / len;
                double angle = Math.PI / 4;
                double s = Math.Sin(angle);
                double c = Math.Cos(angle);
                double xrotp = c * x - s * y;
                double yrotp = s * x + c * y;
                double xrotm = c * x + s * y;
                double yrotm = -s * x + c * y;
                Size offsetp = new Size((int)xrotp, (int)yrotp);
                Size offsetm = new Size((int)xrotm, (int)yrotm);
                Point endPoint = rel.To.Position;
                endPoint = Point.Add(endPoint, new Size((int)x, (int)y));
                Point[] arrowHead = { Point.Add(endPoint, offsetp), endPoint, Point.Add(endPoint, offsetm) };
                g.DrawLines(mark, arrowHead);
            }
        }

        protected void DrawName(Graphics g, Node n)
        {
            Size offset = new Size(Node.NodeSize, 0);
            if (n != null)
            {
                g.DrawString(n.IdString, SystemFonts.DefaultFont, n.Color, Point.Add(n.Location, offset));
            }
        }

        protected void DrawRadius(Graphics g, Node n)
        {
            Size radius = new Size(n.ReachRadius, n.ReachRadius);
            Point center = n.Position;
            g.DrawEllipse(RadiusColor, new Rectangle(Point.Subtract(center, radius), radius + radius));
        }

        protected void DrawArea(Graphics g, Node n)
        {
            if (n.DiscoveryRadius == 0)
                return;
            g.FillRectangle(AreaColor, n.Model.GetDiscoveryArea());
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            if (showConnections)
                foreach (RelationModel r in connections)
                {
                    Draw(e.Graphics, r, ConnectionColor);
                }
            if (showActivations)
                foreach (RelationModel r in activations)
                {
                    Draw(e.Graphics, r, ActivationColor, ActivationColor);
                }
            if (showNames)
            {
                foreach (UAV n in UAVs)
                {
                    DrawName(e.Graphics, n);
                }
                foreach (Task n in Tasks)
                {
                    DrawName(e.Graphics, n);
                }
            }
            if (marker != null)
                Draw(e.Graphics, marker, ConnectionColor, ConnectionColor);
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            base.OnPaintBackground(e);

            if (showRadius)
            {
                foreach (UAV n in UAVs)
                    DrawRadius(e.Graphics, n);
            }
            if (showArea)
            {
                foreach (UAV n in UAVs)
                    if (n.IsSelected)
                        DrawArea(e.Graphics, n);
                foreach (Task n in Tasks)
                    if (n.IsSelected)
                        DrawArea(e.Graphics, n);
            }
        }

        internal void AddNode(NodeModel node, Type type)
        {
            Node toAdd = node.AddNewView(controller, type, ParentForm);
            Controls.Add(toAdd);
            if (type.Equals(typeof(UAV)))
                UAVs.Add(toAdd as UAV);
            else if (type.Equals(typeof(Task)))
                Tasks.Add(toAdd as Task);
            else
                throw new Exception(NodeModel.ViewTypeErrorMessage);
        }

        internal void Clear()
        {
            ClearRelations();
            ClearNodes();
        }

        internal void ClearNodes()
        {
            foreach (UAV n in UAVs)
            {
                Controls.Remove(n);
                n.Dispose();    //to remove connection from corresponding moodel
            }
            foreach (Task n in Tasks)
            {
                Controls.Remove(n);
                n.Dispose();    //to remove connection from corresponding moodel
            }
            UAVs.Clear();
            Tasks.Clear();
            Invalidate();
        }

        internal void ClearRelations()
        {
            connections.Clear();
            activations.Clear();
        }

        private void PlacementView_MouseClick(object sender, MouseEventArgs e)
        {
            if (controller != null)
                controller.MouseClickEventHandler(this, e);
        }

        private void PlacementView_MouseMove(object sender, MouseEventArgs e)
        {
            if ((controller != null) && (e.Button == System.Windows.Forms.MouseButtons.Middle))
                controller.MouseDragMove(e);
        }

        private void PlacementView_MouseUp(object sender, MouseEventArgs e)
        {
            if (controller != null)
                controller.MouseDragEnd(e);
        }

        internal void RemoveNode(NodeModel node)
        {
            foreach (Node nodeview in node.Views)
            {
                if (nodeview is UAV)
                    UAVs.Remove(nodeview as UAV);
                if (nodeview is Task)
                    Tasks.Remove(nodeview as Task);
                Controls.Remove(nodeview);
            }
            Invalidate();
        }

        internal void ShowPoint(Point point)
        {
            Size arrowDiff = new Size(30, 30);
            marker = new RelationModel(new NodeModel(Point.Subtract(point, arrowDiff)), new NodeModel(point));
            Invalidate();
        }
    }
}
