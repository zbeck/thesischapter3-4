﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Drawing;
using System.Runtime.Serialization;
using MyExtensions;
using AStarSolver;
using System.Diagnostics;

namespace AgentDraw
{
    [Serializable]
    abstract public class AgentModel : IDeserializationCallback
    {
        protected List<NodeList> UAVNodes;
        protected List<NodeList> TaskNodes;
        protected List<RelationModel> Connections;
        protected List<RelationModel> Activations;
        /// <summary>
        /// platforms:
        /// Fixed wing: weControl CARD CH
        /// http://www.wecontrol.ch/Fixed-Wings-UAV/card-ch.html
        ///     speed: 90 km/h = 25 m/s (max cruise speed 100 km/h)
        ///     flight time: 90 min
        ///     
        /// Rotary wing: Event 38 Aphex Hexacopter
        /// https://www.event38.com/ProductDetails.asp?ProductCode=AHX-2
        ///     max wind speed: 13.4 m/s => max speed 10 m/s
        ///     flight time: 11 min
        /// 
        /// Default travel time: 10 min = 600 sec
        /// Extended travel time: 6 hours = 21600 sec
        /// </summary>
        protected static double[] DSpeeds = { 2.5, 1};
        public static double PixelsPerMeter
        {
            get { return pixelsPerMeter; }
            set
            {
                pixelsPerMeter = value;
                DefaultSpeed[0] = DSpeeds[0] * PixelsPerMeter;
                DefaultSpeed[1] = DSpeeds[1] * PixelsPerMeter;
            }
        }
        public static double pixelsPerMeter = 1;    //for Haiti
        //public static double pixelsPerMeter = 0.045148;    //for Ajka
        
        protected static double DefaultP = 432000; //expected lifetime: 12 hours
        protected static int DefaultLam = 1;
        protected static double DefaultEpsilon = 1;
        protected static int DefaultDegrade = 5;
        //protected static int DefaultTravelTime = 6000;  //time step 0.1 s
        protected static int DefaultTravelTime = 216000;  //time step 0.1 s
        protected static double[] DefaultSpeed = { 2.5 * PixelsPerMeter, 1.0 * PixelsPerMeter };
        public static double[] TaskTime = { 0, 300 };  //0s to transfer and process aeiral imagery, 30s to execute a helicopter task

        [NonSerialized]
        protected List<PlacementView> views;
        internal virtual PlacementView View
        {
            set
            {
                if (value == null)
                    return;
                views.Add(value);
                foreach (NodeList list in UAVNodes)
                    foreach (NodeModel node in list)
                        value.AddNode(node, typeof(UAV));
                foreach (NodeList list in TaskNodes)
                    foreach (NodeModel node in list)
                        value.AddNode(node, typeof(Task));
                RefreshRelations();
            }
        }
        public int nLevel { get { return UAVNodes.Count; } }
        public int UAVCount
        {
            get
            {
                int retval = 0;
                foreach (NodeList l in UAVNodes)
                    retval += l.Count;
                return retval;
            }
        }
        public int TaskCount
        {
            get
            {
                int retval = 0;
                foreach (NodeList l in TaskNodes)
                    retval += l.Count;
                return retval;
            }
        }


        protected int nofSlots;
        public int NofSlots
        {
            get { return nofSlots; }
            set { /*if (value < 1) nofSlots = 1; else*/ nofSlots = Math.Min(value, MaxSlots); }
        }
        public static int MaxSlots = 10;

        protected int[] travelTime;
        public int[] TravelTime
        {
            get { return travelTime; }
            set
            {
                if (value.Length == UAVNodes.Count)
                    travelTime = value;
                else
                    throw new Exception("Incorrect TravelTime count!");
                RefreshRadius();
                UpdateView(this);
            }
        }
        protected double[] speed;
        public double[] Speed
        {
            get
            {
                return speed;
            }
            set
            {
                if (value.Length == UAVNodes.Count)
                    speed = value;
                else
                    throw new Exception("Incorrect Speed count!");
                RefreshRadius();
                UpdateView(this);
            }
        }
        protected double[] p;
        protected int[] lam;
        protected double[] epsilon;
        protected int[] degrade;

        public AgentModel()
        {
            DefaultConstructor();
        }

        public AgentModel(PlacementView view)
        {
            DefaultConstructor();
            this.View = view;
        }

        private void DefaultConstructor()
        {
            UAVNodes = new List<NodeList>();
            TaskNodes = new List<NodeList>();
            Connections = new List<RelationModel>();
            Activations = new List<RelationModel>();
            this.views = new List<PlacementView>();
        }

        /// <summary>
        /// Copy constructor
        /// !!! WARNING !!! Doesn't set view
        /// </summary>
        /// <param name="source">copy source</param>
        public AgentModel(AgentModel source)
        {
            UAVNodes = new List<NodeList>();
            foreach (NodeList l in source.UAVNodes)
            {
                UAVNodes.Add(new NodeList(l));  //copies every node without views
            }
            TaskNodes = new List<NodeList>();
            foreach (NodeList l in source.TaskNodes)
            {
                TaskNodes.Add(new NodeList(l));  //copies every node without views
            }
            InitDefault();

            Connections = new List<RelationModel>();
            Connections.AddRange(source.Connections);
            Activations = new List<RelationModel>();
            Activations.AddRange(source.Activations);
            nofSlots = source.nofSlots;

            this.travelTime = new int[source.travelTime.Length];
            Array.Copy(source.travelTime, this.travelTime, source.travelTime.Length);

            if (source.speed != null)
            {
                this.speed = new double[source.speed.Length];
                Array.Copy(source.speed, this.speed, source.speed.Length);
            }
            if (source.p != null)
            {
                this.p = new double[source.p.Length];
                Array.Copy(source.p, this.p, source.p.Length);
            }
            if (source.lam != null)
            {
                this.lam = new int[source.lam.Length];
                Array.Copy(source.lam, this.lam, source.lam.Length);
            }
            if (source.epsilon != null)
            {
                this.epsilon = new double[source.epsilon.Length];
                Array.Copy(source.epsilon, this.epsilon, source.epsilon.Length);
            }
            if (source.degrade != null)
            {
                this.degrade = new int[source.degrade.Length];
                Array.Copy(source.degrade, this.degrade, source.degrade.Length);
            }

            this.views = new List<PlacementView>();
        }

        internal void SetDefaultParams()
        {
            int level = UAVNodes.Count;
            this.p = new double[level];
            this.lam = new int[level];
            this.epsilon = new double[level];
            this.degrade = new int[level - 1];
            this.travelTime = new int[level];
            this.speed = new double[level];
            for (int i = 0; i < level; i++)
            {
                this.p[i] = DefaultP;
                this.lam[i] = DefaultLam;
                this.epsilon[i] = DefaultEpsilon;
                if (i != 0)
                    this.degrade[i - 1] = DefaultDegrade;
                this.speed[i] = DefaultSpeed[i];
                SetTravelTime(DefaultTravelTime, i);
            }
        }

        protected void InitLists()
        {
            foreach (NodeList l in UAVNodes)
            {
                l.Changed += new ChangedEventHandler(UpdateView);
            }
            foreach (NodeList l in TaskNodes)
            {
                l.Changed += new ChangedEventHandler(UpdateView);
            }
        }

        protected void InitDefault()
        {
            InitLists();
            SetDefaultParams();
        }

        protected virtual void UpdateView(object sender)
        {
            foreach (PlacementView view in views)
                view.Invalidate();
        }


        public void AddUAV(NodeModel uav, int groupIndex = 0)
        {
            UAVNodes[groupIndex].Add(uav);
            foreach (PlacementView view in views)
                view.AddNode(uav, typeof(UAV));
        }

        public void AddUAVs(List<NodeModel> uavs, int groupIndex = 0)
        {
            UAVNodes[groupIndex].AddRange(uavs);
            foreach (PlacementView view in views)
                foreach (NodeModel uav in uavs)
                    view.AddNode(uav, typeof(UAV));
        }

        public void AddTask(NodeModel task, int groupIndex = 0)
        {
            TaskNodes[groupIndex].Add(task);
            foreach (PlacementView view in views)
                view.AddNode(task, typeof(Task));
        }

        public void ShuffleTasks(int groupIndex)
        {
            MyExtensions.MyExtensionMethods.Shuffle<NodeModel>(TaskNodes[groupIndex]);
        }

        public void AddConnection(RelationModel r)
        {
            Connections.Add(r);
            RefreshRelations();
        }

        public void AddActivation(RelationModel r)
        {
            if (r.From.Equals(r.To))
                return;
            Activations.Add(r);
            RefreshRelations();
        }

        public void RefreshRelations()
        {
            foreach (PlacementView view in views)
            {
                view.Connections = this.Connections;
                view.Activations = this.Activations;
                view.Invalidate();
            }
        }

        protected NodeModel FindNode(List<NodeList> list, Point pos)
        {
            NodeModel found = null;
            foreach (NodeList l in list)
            {
                found = l.Find(u => u.Position == pos);
                if (found != null)
                    break;
            }
            return found;
        }

        public bool HasTask(NodeModel task)
        {
            if (task == null)
                return false;
            foreach (NodeList l in TaskNodes)
            {
                if (l.Find(t => t.ID == task.ID) != null)
                    return true;
            }
            return false;
        }

        public virtual NodeModel RemoveNode(UAV uav)
        {
            NodeModel found = uav.Model;
            Connections.RemoveAll(c => c.To == found);
            foreach (NodeList list in UAVNodes)
                list.Remove(found);
            foreach (PlacementView view in views)
                view.RemoveNode(found);
            RefreshRelations();
            return found;
        }

        public virtual NodeModel RemoveNode(Task task)
        {
            NodeModel found = task.Model;
            Connections.RemoveAll(c => c.From == found);
            Activations.RemoveAll(a => a.From == found);
            Activations.RemoveAll(a => a.To == found);
            foreach (NodeList l in TaskNodes)
                l.Remove(found);
            foreach (PlacementView view in views)
                view.RemoveNode(found);
            RefreshRelations();
            return found;
        }

        public virtual NodeModel RemoveNode(NodeModel node)
        {
            Connections.RemoveAll(c => c.From == node);
            Connections.RemoveAll(c => c.To == node);
            Activations.RemoveAll(a => a.From == node);
            Activations.RemoveAll(a => a.To == node);
            foreach (NodeList l in TaskNodes)
                l.Remove(node);
            foreach (NodeList l in UAVNodes)
                l.Remove(node);
            foreach (PlacementView view in views)
                view.RemoveNode(node);
            RefreshRelations();
            return node;
        }

        public virtual NodeModel FindById(List<NodeList> list, int id)
        {
            NodeModel found = null;
            foreach (NodeList l in list)
            {
                found = l.Find(n => n.ID == id);
                if (found != null)
                    break;
            }
            return found;
        }

        public void ClearNodes(NodeList targetList)
        {
            foreach (NodeModel node in targetList)
            {
                Connections.RemoveAll(c => c.From == node);
                Activations.RemoveAll(a => a.From == node);
                Activations.RemoveAll(a => a.To == node);
                foreach (PlacementView view in views)
                    view.RemoveNode(node);
            }
            targetList.Clear();
        }

        internal void UpdateLocation(Point oldPos, Point newPos)
        {
            NodeModel found = FindNode(UAVNodes, oldPos);
            if (found == null)
            {
                found = FindNode(TaskNodes, oldPos);
            }
            if (found == null)
                return;

            found.Position = newPos;
        }

        internal void ToggleSelection(Node target)
        {
            target.Model.IsSelected = !target.Model.IsSelected;
        }

        internal void ClearSelected()
        {
            foreach (NodeList tl in TaskNodes) foreach (NodeModel n in tl)
                    n.IsSelected = false;
        }

        protected void ReAssignActivations()
        {
            Activations.Clear();
            foreach (NodeModel lo in TaskNodes[1])
            {
                foreach (NodeModel hi in TaskNodes[0])
                {
                    if (IsDiscoveredBy(hi, lo))
                        AddActivation(new RelationModel(hi, lo));
                }
            }
            RefreshRelations();
        }

        protected void ReAssignActivations(List<NodeModel> sampled)
        {
            Activations.Clear();
            foreach (NodeModel lo in sampled)
            {
                foreach (NodeModel hi in TaskNodes[0])
                {
                    if (IsDiscoveredBy(hi, lo))
                        AddActivation(new RelationModel(hi, lo));
                }
            }
            RefreshRelations();
        }

        /// <summary>
        /// Lists the variables connected to an "T" node assicoated to tasks
        /// </summary>
        /// <param name="t">Task</param>
        /// <returns>list of variables connected in the factor graph</returns>
        public List<Variable> GetTNodeConnections(NodeModel t)
        {
            List<Variable> list = new List<Variable>();
            foreach (RelationModel c in Connections.FindAll(c => t.Equals(c.From)))
            {
                for (int s = 0; s < nofSlots; s++)
                    list.Add(new Variable(UAVNodes, c.To, TaskNodes, t, s));
            }
            return list;
        }

        /// <summary>
        /// Lists the variables connected to an "F" node associated to UAV timeslots
        /// </summary>
        /// <param name="uav">UAV</param>
        /// <param name="slot">slot number</param>
        /// <returns>two lists: 0-variables fo own timeslot, 1-varibles for next timeslot</returns>
        public List<Variable>[] GetFNodeConnections(NodeModel uav, int slot)
        {
            List<Variable>[] list = new List<Variable>[2];
            list[0] = new List<Variable>();
            list[1] = new List<Variable>();

            if (slot >= nofSlots)
                return list;


            foreach (RelationModel c in Connections.FindAll(c => uav.Equals(c.To)))
            {
                list[0].Add(new Variable(UAVNodes, uav, TaskNodes, c.From, slot));
                if (slot < (nofSlots - 1))
                    list[1].Add(new Variable(UAVNodes, uav, TaskNodes, c.From, slot + 1));
            }
            return list;
        }


        /// <summary>
        /// Lists the variables for a type "U" node of the factor graph associated to every variable, containing utilities
        /// </summary>
        /// <param name="uav">UAV</param>
        /// <param name="t">Task</param>
        /// <param name="slot">slot number</param>
        /// <returns>three lists: 0-own variable, 1-previous timeslot, 2-activators</returns>
        public List<Variable>[] GetUNodeConnections(Variable v)
        {
            List<Variable>[] list = new List<Variable>[3];
            list[0] = new List<Variable>();
            list[1] = new List<Variable>();
            list[2] = new List<Variable>();
            RelationModel thisActivation = null;

            //own variable
            list[0].Add(v);

            //previous timeslot
            if (v.slot > 0)
            {
                NodeModel uav = UAVNodes[v.level][v.reluav];
                foreach (RelationModel c in Connections.FindAll(c => uav.Equals(c.To)))
                {
                    list[1].Add(new Variable(UAVNodes, uav, TaskNodes, c.From, v.slot - 1));
                }
            }

            //find activation
            NodeModel t = TaskNodes[v.level][v.reltask];
            thisActivation = Activations.Find(a => t.Equals(a.To));
            if (thisActivation != null)
            {
                foreach (RelationModel c in Connections.FindAll(c => thisActivation.From.Equals(c.From)))
                {
                    for (int s = 0; s < nofSlots; s++)
                        list[2].Add(new Variable(UAVNodes, c.To, TaskNodes, thisActivation.From, s));
                }
            }

            return list;
        }

        protected static bool IsDiscoveredBy(NodeModel discoverer, NodeModel victim)
        {
            return discoverer.GetDiscoveryArea().Contains(victim.Position);
        }

        public static double Distance(NodeModel a, NodeModel b)
        {
            return Math.Sqrt((a.Position.X - b.Position.X) * (a.Position.X - b.Position.X) + (a.Position.Y - b.Position.Y) * (a.Position.Y - b.Position.Y));
        }

        protected static void PrintDistanceTable(System.IO.TextWriter tw, NodeList rows, NodeList cols, double speed = 1.0)
        {
            foreach (NodeModel r in rows)
            {
                foreach (NodeModel c in cols)
                {
                    tw.Write(Distance(r, c) / speed);
                    tw.Write(" ");
                }
                tw.WriteLine();
            }
        }

        protected static double[,] GetDistanceArray(NodeList rows, NodeList cols, double speed = 1.0)
        {
            double[,] retval = new double[rows.Count, cols.Count];
            for (int i = 0; i < rows.Count; i++)
            {
                for (int j = 0; j < cols.Count; j++)
                {
                    retval[i, j] = Distance(rows[i], cols[j]) / speed;
                }
            }
            return retval;
        }

        protected static void PrintDistanceMatrix(System.IO.TextWriter tw, NodeList rows, NodeList cols,
            double speed = 1.0, double multiplier = 100, bool extraCol = false, bool extraRow = false, double extraColValue = 0, double extraRowValue = 1000)
        {
            tw.Write('[');
            bool s2 = false;
            foreach (NodeModel r in rows)
            {
                if (s2)
                    tw.Write(", ");
                s2 = true;
                tw.Write('[');
                bool started = false;
                foreach (NodeModel c in cols)
                {
                    if (started)
                        tw.Write(", ");
                    started = true;
                    tw.Write(Math.Round(Distance(r, c) / speed * multiplier));
                }
                if (extraCol)
                    WriteExtraCol(tw, cols, multiplier, extraColValue);
                tw.Write(']');
            }
            if (extraRow)
            {
                tw.Write('[');
                for (int i = 0; i < cols.Count; i++)
                {
                    if (i > 0)
                        tw.Write(", ");
                    tw.Write(Math.Round(extraRowValue * multiplier));
                }
                if (extraCol)
                    WriteExtraCol(tw, cols, multiplier, extraColValue);
                tw.Write(']');
            }
            tw.Write(']');
        }

        private static void WriteExtraCol(System.IO.TextWriter tw, NodeList cols, double multiplier, double extraColValue)
        {
            if (cols.Count > 0)
                tw.Write(", ");
            tw.Write(Math.Round(extraColValue * multiplier));
        }

        protected static void PrintDistanceTable(System.IO.TextWriter tw, List<NodeList> rows, List<NodeList> cols, int[] subtract = null)
        {
            if (subtract == null)
            {
                subtract = new int[rows.Count];
                for (int i = 0; i < rows.Count; i++)
                    subtract[i] = 0;
            }
            for (int i = 0; i < rows.Count; i++) foreach (NodeModel r in rows[i])
                {
                    foreach (NodeList clist in cols) foreach (NodeModel c in clist)
                        {
                            tw.Write(Distance(r, c) - subtract[i]);
                            tw.Write(" ");
                        }
                    tw.WriteLine();
                }
        }

        /// <summary>
        /// writes the Variable array elements in one line
        /// </summary>
        /// <param name="l"></param>
        /// <returns></returns>
        protected String toPrint(List<Variable> l)
        {
            return String.Concat(MyExtensionMethods.ToDelimitedString<Variable>(l, " "), "\r\n");
        }
        /// <summary>
        /// writes the Variable list array elements in separate lines
        /// </summary>
        /// <param name="la"></param>
        /// <returns></returns>
        protected String toPrint(List<Variable>[] la)
        {
            string ret = "";
            foreach (List<Variable> l in la)
                ret = String.Concat(ret, toPrint(l));
            return ret;
        }

        internal List<Variable> GenerateAllVariables(List<RelationModel> Connections)
        {
            List<Variable> allVars = new List<Variable>();
            UAVNodes.ForEach(delegate(NodeList l) { l.Sort(); });
            TaskNodes.ForEach(delegate(NodeList l) { l.Sort(); });

            foreach (RelationModel c in Connections)
            {
                for (int i = 0; i < nofSlots; i++)
                    allVars.Add(new Variable(UAVNodes, c.To, TaskNodes, c.From, i));
            }
            allVars.Sort();
            return allVars;
        }

        /// <summary>
        /// Legacy output generator
        /// </summary>
        /// <param name="tw">the stream to write into</param>
        public void GenerateMaxSumOutput(System.IO.TextWriter tw)
        {
            //generate all Variables
            List<Variable> allVars = GenerateAllVariables(Connections);
            Variable.Allvars = allVars; //to be able to use ToString()

            ///////////////// VARIABLES ///////////////////////////////

            //number of variables
            tw.WriteLine(allVars.Count.ToString());

            //descriptions for variables
            foreach (Variable v in allVars)
            {
                tw.Write(v.ToString());
                tw.Write(" ");
                tw.WriteLine(v.DetailsToString());
            }

            ///////////////// DISTANCES ///////////////////////////////

            //number of tasks                
            tw.WriteLine(TaskCount.ToString());
            //number of uavs
            tw.WriteLine(UAVCount.ToString());
            PrintDistanceTable(tw, TaskNodes, TaskNodes);
            PrintDistanceTable(tw, UAVNodes, TaskNodes, travelTime);

            ///////////////// U NODES ///////////////////////////////

            //number of units
            tw.WriteLine(allVars.Count.ToString());

            //U nodes by variables
            foreach (Variable v in allVars)
            {
                tw.Write(toPrint(GetUNodeConnections(v)));
            }

            ///////////////// T NODES ///////////////////////////////

            //number of units
            tw.WriteLine(TaskCount.ToString());
            foreach (NodeList tlist in TaskNodes)
                foreach (NodeModel t in tlist)
                {
                    //print variables for every task
                    tw.Write(toPrint(GetTNodeConnections(t)));
                }

            ///////////////// F NODES ///////////////////////////////

            //number of units
            tw.WriteLine((UAVCount * nofSlots).ToString());
            foreach (NodeList ulist in UAVNodes)
                foreach (NodeModel uav in ulist)
                {
                    for (int i = 0; i < nofSlots; i++)
                    {
                        //print variables for every uav timeslot
                        tw.Write(toPrint(GetFNodeConnections(uav, i)));
                    }
                }
        }

        public virtual void GenerateProblemOutput(System.IO.TextWriter tw, int[] nofslots = null)
        {
            if (nofslots == null)
                nofslots = new int[0];
            WriteCounts(tw, nofslots);
            WriteEnableMatrix(tw);
            WriteDistances(tw);
            WriteParams(tw);
        }

        protected void WriteParams(System.IO.TextWriter tw)
        {
            //battery
            for (int i = 0; i < speed.Length; i++)
            {
                tw.Write(TravelTime[i]);
                tw.Write(' ');
            }
            tw.WriteLine();

            //P: max utility
            foreach (double val in this.p)
            {
                tw.Write(val);
                tw.Write(' ');
            }
            tw.WriteLine();

            //lambda: utility decrease by time
            foreach (int val in this.lam)
            {
                tw.Write(val);
                tw.Write(' ');
            }
            tw.WriteLine();

            //epsilon: utility decrease by later timeslot
            foreach (double val in this.epsilon)
            {
                tw.Write(val);
                tw.Write(' ');
            }
            tw.WriteLine();

            //degrade: penalty amount for waiting for discovery (penalty = (degrade-lambda)*waittime)
            foreach (int val in this.degrade)
            {
                tw.Write(val);
                tw.Write(' ');
            }
            tw.WriteLine();
        }

        protected void WriteDistances(System.IO.TextWriter tw)
        {
            //travel time task-task
            for (int i = 0; i < TaskNodes.Count; i++)
                PrintDistanceTable(tw, TaskNodes[i], TaskNodes[i], speed[i]);

            //travel time agent-task
            for (int i = 0; i < TaskNodes.Count; i++)
                PrintDistanceTable(tw, UAVNodes[i], TaskNodes[i], speed[i]);
        }

        protected void WriteEnableMatrix(System.IO.TextWriter tw)
        {
            //high tasks: rows
            for (int i = 0; i < (TaskNodes.Count - 1); i++)
            {
                foreach (NodeModel th in TaskNodes[i])
                {
                    //low tasks: colums
                    foreach (NodeModel tl in TaskNodes[i + 1])
                    {
                        RelationModel act = Activations.Find(a => th.ID.Equals(a.From.ID) && tl.ID.Equals(a.To.ID));
                        tw.Write((act == null) ? 0 : 1);
                        tw.Write(' ');
                    }
                    tw.WriteLine();
                }
            }
        }

        protected void WriteCounts(System.IO.TextWriter tw, int[] nofslots)
        {
            //nof levels
            tw.WriteLine(UAVNodes.Count);

            //nof agents
            foreach (NodeList l in UAVNodes)
            {
                tw.Write(l.Count);
                tw.Write(' ');
            }
            tw.WriteLine();

            //nof tasks
            foreach (NodeList l in TaskNodes)
            {
                tw.Write(l.Count);
                tw.Write(' ');
            }
            tw.WriteLine();

            //timeslots
            for(int i=0; i<TaskNodes.Count; i++)
            {
                if (nofslots.Length <= i)
                    tw.Write(this.NofSlots);
                else
                    tw.Write(nofslots[i]);
                tw.Write(' ');
            }
            tw.WriteLine();
        }

        internal SolutionModel OpenSolution(List<int> values, string solver)
        {
            Solution s = new Solution(this, values, solver);
            s.Show();
            return s.Model as SolutionModel;
        }

        internal SolutionModel OpenSolutionModel(string p, string solver)
        {
            Solution s = OpenSolution(p, solver);
            if (s == null)
                return null;
            return s.Model as SolutionModel;
        }

        internal Solution OpenSolution(string p, string solver)
        {
            var solutionParsed = NodeList.ParseNewSolution(p, UAVNodes);
            if (solutionParsed == null)
            {
                return null;
            }
            Solution s = new Solution(this, solutionParsed, solver);
            s.Show();
            return s;
        }

        internal SolutionModel OpenHighLevelSolution(List<List<int>> allocation, string solver, bool openInWindow)
        {
            List<List<List<int>>> completeSolution = new List<List<List<int>>>(UAVNodes.Count);
            completeSolution.Add(allocation);
            for (int i = 1; i < UAVNodes.Count; i++)
            {
                List<List<int>> currentLevel = new List<List<int>>(UAVNodes[i].Count);
                for (int j = 0; j < UAVNodes[i].Count; j++)
                    currentLevel.Add(new List<int>(0));
                completeSolution.Add(currentLevel);
            }
            if (openInWindow)
            {
                Solution s = new Solution(this, completeSolution, solver);
                s.Show();
                return s.Model as SolutionModel;
            }
            else
                return new SolutionModel(this, completeSolution, solver);
        }

        internal SolutionModel ReadSolution(string p, string solver)
        {
            var solutionParsed = NodeList.ParseNewSolution(p, UAVNodes);
            if (solutionParsed == null)
            {
                return null;
            }
            return new SolutionModel(this, solutionParsed , solver);
        }

        public virtual void OnDeserialization(object sender)
        {
            views = new List<PlacementView>();
            InitLists();
        }

        public void SelectLevel(int level)
        {
            for (int i = 0; i < UAVNodes.Count; i++)
                foreach (NodeModel m in UAVNodes[i])
                    m.IsSelected = (i == level);
            for (int i = 0; i < TaskNodes.Count; i++)
                foreach (NodeModel m in TaskNodes[i])
                    m.IsSelected = (i == level);
        }

        internal bool IsCompleted()
        {
            if (IsBatteryOver())
                return true;
            if (UAVCount == 0)
                return true;
            if (TaskCount != 0)
                return false;
            else
                return true;
        }

        private bool IsBatteryOver()
        {
            foreach (NodeList l in UAVNodes)
                foreach (NodeModel n in l)
                    if (n.ReachRadius > 0)
                        return false;
            return true;
        }

        internal void SetDiscovery(int radius, int level, Type type)
        {
            if (type == typeof(UAV))
                foreach (NodeModel n in UAVNodes[level])
                    n.SetDiscoveryRadius(radius);
            else if (type == typeof(Task))
                foreach (NodeModel n in TaskNodes[level])
                    n.SetDiscoveryRadius(radius);
        }

        internal void SetTravelTime(int tt, int level)
        {
            if ((level < 0) || (level >= travelTime.Length))
                return;
            travelTime[level] = Math.Max(tt, 0);
            RefreshRadius();
        }

        protected void RefreshRadius()
        {
            for (int i = 0; i < UAVNodes.Count; i++)
            {
                foreach (NodeModel n in UAVNodes[i])
                {
                    n.SetReachRadius((int)(travelTime[i] * speed[i]));
                }
            }
        }

        internal void Revert()
        {
            UAVNodes.Reverse();
            TaskNodes.Reverse();
        }

        internal void RevertActivations()
        {
            for (int i = 0; i < Activations.Count; i++)
            {
                RelationModel newa = new RelationModel(Activations[i].To, Activations[i].From);
                Activations[i] = newa;
            }
        }

        public static double CalculateUtility(AgentModel m, int level, double executiontime, int slot)
        {
            double util = m.p[level] - (m.lam[level] * executiontime) * Math.Pow(m.epsilon[level], slot);
            util /= m.p[m.p.Length-1];
            return util;
        }

        internal List<Rectangle> GetDiscoveryRectangles()
        {
            List<Rectangle> retval = new List<Rectangle>(TaskNodes[0].Count);
            foreach (NodeModel n in TaskNodes[0])
                retval.Add(n.GetDiscoveryArea());
            return retval;
        }

        internal List<List<List<int>>> CompleteSolutionForLevelAStar(int level, double battery = -1, Stopwatch computationTime = null)
        {

            List<List<List<int>>> completeSolution = new List<List<List<int>>>(UAVNodes.Count);
            for (int lev = 0; lev < UAVNodes.Count; lev++)
            {
                List<List<int>> currentLevel;
                if (lev == level)
                    currentLevel = SolveLevelAStar(level, battery, computationTime);
                else
                {
                    currentLevel = new List<List<int>>(UAVNodes[lev].Count);
                    for (int j = 0; j < UAVNodes[lev].Count; j++)
                        currentLevel.Add(new List<int>(0));
                }
                completeSolution.Add(currentLevel);
            }
            return completeSolution;
        }

        internal List<List<int>> SolveLevelAStar(int level, double battery, Stopwatch computationTime)
        {
            TeamPath<NodeModel>.P = p[level];
            //TODO should use ReachRadius properly
            if (battery < 0)
                battery = UAVNodes[level][0].ReachRadius;
            if (computationTime != null)
                computationTime.Start();
            AStarSolver.TeamPath<NodeModel> best = AStarSolver.Program.FindPath<NodeModel>(UAVNodes[level], battery, TaskNodes[level], AgentModel.Distance, TeamEstimator<NodeModel>.BestEstimate);
            if (computationTime != null)
                computationTime.Stop();

            List<List<int>> currentLevel = new List<List<int>>(best.Agents.Length);
            foreach (Agent a in best.Agents)
            {
                List<int> agentPath = new List<int>();
                foreach (int task in a.Path)
                    agentPath.Insert(0, task);  //reverse order
                agentPath.RemoveAt(0);  //removes the UAV element
                currentLevel.Add(agentPath);
            }
            return currentLevel;
        }

        internal void Test(string p)
        {
            var solution = NodeList.ParseNewSolution(p, UAVNodes);
            Console.WriteLine(solution);
        }

        internal void ResetSpeed()
        {
            Array.Copy(DefaultSpeed, speed, speed.Length);
        }

        public List<NodeModel> SampleTasks(Sampler sampler)
        {
            List<NodeModel> nodes = new List<NodeModel>();
            List<Point> samplePoints = sampler.SampleLocations();
            foreach (Point loc in samplePoints)
                nodes.Add(new NodeModel(loc));
            return nodes;
        }
    }
}
