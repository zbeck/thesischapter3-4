﻿namespace AgentDraw
{
    partial class SimRunnerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxSolver = new System.Windows.Forms.TextBox();
            this.labelSolver = new System.Windows.Forms.Label();
            this.numericUpDownSampleNum = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.buttonRun = new System.Windows.Forms.Button();
            this.numericUpDownTimeStep = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.richTextBoxResult = new System.Windows.Forms.RichTextBox();
            this.checkBoxSampling = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSampleNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTimeStep)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxSolver
            // 
            this.textBoxSolver.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSolver.Location = new System.Drawing.Point(12, 29);
            this.textBoxSolver.Name = "textBoxSolver";
            this.textBoxSolver.Size = new System.Drawing.Size(405, 20);
            this.textBoxSolver.TabIndex = 0;
            this.textBoxSolver.Text = "greedyAgents.py";
            this.textBoxSolver.TextChanged += new System.EventHandler(this.textBoxSolver_TextChanged);
            // 
            // labelSolver
            // 
            this.labelSolver.AutoSize = true;
            this.labelSolver.Location = new System.Drawing.Point(13, 13);
            this.labelSolver.Name = "labelSolver";
            this.labelSolver.Size = new System.Drawing.Size(37, 13);
            this.labelSolver.TabIndex = 1;
            this.labelSolver.Text = "Solver";
            // 
            // numericUpDownSampleNum
            // 
            this.numericUpDownSampleNum.Location = new System.Drawing.Point(12, 68);
            this.numericUpDownSampleNum.Name = "numericUpDownSampleNum";
            this.numericUpDownSampleNum.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownSampleNum.TabIndex = 2;
            this.numericUpDownSampleNum.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Samples per step";
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Location = new System.Drawing.Point(12, 95);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(405, 23);
            this.progressBar1.TabIndex = 4;
            // 
            // buttonRun
            // 
            this.buttonRun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRun.Location = new System.Drawing.Point(291, 66);
            this.buttonRun.Name = "buttonRun";
            this.buttonRun.Size = new System.Drawing.Size(127, 23);
            this.buttonRun.TabIndex = 5;
            this.buttonRun.Text = "Run";
            this.buttonRun.UseVisualStyleBackColor = true;
            this.buttonRun.Click += new System.EventHandler(this.buttonRun_Click);
            // 
            // numericUpDownTimeStep
            // 
            this.numericUpDownTimeStep.Location = new System.Drawing.Point(138, 68);
            this.numericUpDownTimeStep.Name = "numericUpDownTimeStep";
            this.numericUpDownTimeStep.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownTimeStep.TabIndex = 6;
            this.numericUpDownTimeStep.Value = new decimal(new int[] {
            6,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(135, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Time step size";
            // 
            // richTextBoxResult
            // 
            this.richTextBoxResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxResult.Location = new System.Drawing.Point(12, 124);
            this.richTextBoxResult.Name = "richTextBoxResult";
            this.richTextBoxResult.ReadOnly = true;
            this.richTextBoxResult.Size = new System.Drawing.Size(406, 465);
            this.richTextBoxResult.TabIndex = 8;
            this.richTextBoxResult.Text = "";
            // 
            // checkBoxSampling
            // 
            this.checkBoxSampling.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxSampling.AutoSize = true;
            this.checkBoxSampling.Checked = true;
            this.checkBoxSampling.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSampling.Location = new System.Drawing.Point(326, 12);
            this.checkBoxSampling.Name = "checkBoxSampling";
            this.checkBoxSampling.Size = new System.Drawing.Size(91, 17);
            this.checkBoxSampling.TabIndex = 9;
            this.checkBoxSampling.Text = "Use Sampling";
            this.checkBoxSampling.UseVisualStyleBackColor = true;
            this.checkBoxSampling.CheckedChanged += new System.EventHandler(this.checkBoxSampling_CheckedChanged);
            // 
            // SimRunnerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(430, 601);
            this.Controls.Add(this.checkBoxSampling);
            this.Controls.Add(this.richTextBoxResult);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.numericUpDownTimeStep);
            this.Controls.Add(this.buttonRun);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.numericUpDownSampleNum);
            this.Controls.Add(this.labelSolver);
            this.Controls.Add(this.textBoxSolver);
            this.Name = "SimRunnerForm";
            this.Text = "SimRunnerForm";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSampleNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTimeStep)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxSolver;
        private System.Windows.Forms.Label labelSolver;
        private System.Windows.Forms.NumericUpDown numericUpDownSampleNum;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button buttonRun;
        private System.Windows.Forms.NumericUpDown numericUpDownTimeStep;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox richTextBoxResult;
        private System.Windows.Forms.CheckBox checkBoxSampling;
    }
}