﻿namespace AgentDraw
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.buttonEditor = new System.Windows.Forms.Button();
            this.buttonSampler = new System.Windows.Forms.Button();
            this.testButton = new System.Windows.Forms.Button();
            this.buttonSimulator = new System.Windows.Forms.Button();
            this.buttonCompare = new System.Windows.Forms.Button();
            this.buttonSolutionExplorer = new System.Windows.Forms.Button();
            this.buttonDemo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonEditor
            // 
            this.buttonEditor.Location = new System.Drawing.Point(12, 12);
            this.buttonEditor.Name = "buttonEditor";
            this.buttonEditor.Size = new System.Drawing.Size(130, 70);
            this.buttonEditor.TabIndex = 0;
            this.buttonEditor.Text = "Open Editor";
            this.buttonEditor.UseVisualStyleBackColor = true;
            this.buttonEditor.Click += new System.EventHandler(this.buttonEditor_Click);
            // 
            // buttonSampler
            // 
            this.buttonSampler.Location = new System.Drawing.Point(12, 88);
            this.buttonSampler.Name = "buttonSampler";
            this.buttonSampler.Size = new System.Drawing.Size(130, 70);
            this.buttonSampler.TabIndex = 1;
            this.buttonSampler.Text = "Open Sampler";
            this.buttonSampler.UseVisualStyleBackColor = true;
            this.buttonSampler.Click += new System.EventHandler(this.buttonSampler_Click);
            // 
            // testButton
            // 
            this.testButton.Location = new System.Drawing.Point(231, 108);
            this.testButton.Name = "testButton";
            this.testButton.Size = new System.Drawing.Size(75, 23);
            this.testButton.TabIndex = 2;
            this.testButton.Text = "Test";
            this.testButton.UseVisualStyleBackColor = true;
            this.testButton.Click += new System.EventHandler(this.testButton_Click);
            // 
            // buttonSimulator
            // 
            this.buttonSimulator.Location = new System.Drawing.Point(12, 164);
            this.buttonSimulator.Name = "buttonSimulator";
            this.buttonSimulator.Size = new System.Drawing.Size(130, 70);
            this.buttonSimulator.TabIndex = 3;
            this.buttonSimulator.Text = "Open Simulator";
            this.buttonSimulator.UseVisualStyleBackColor = true;
            this.buttonSimulator.Click += new System.EventHandler(this.buttonSimulator_Click);
            // 
            // buttonCompare
            // 
            this.buttonCompare.Location = new System.Drawing.Point(12, 240);
            this.buttonCompare.Name = "buttonCompare";
            this.buttonCompare.Size = new System.Drawing.Size(130, 70);
            this.buttonCompare.TabIndex = 4;
            this.buttonCompare.Text = "Open Comparsion";
            this.buttonCompare.UseVisualStyleBackColor = true;
            this.buttonCompare.Click += new System.EventHandler(this.buttonCompare_Click);
            // 
            // buttonSolutionExplorer
            // 
            this.buttonSolutionExplorer.Location = new System.Drawing.Point(176, 240);
            this.buttonSolutionExplorer.Name = "buttonSolutionExplorer";
            this.buttonSolutionExplorer.Size = new System.Drawing.Size(130, 70);
            this.buttonSolutionExplorer.TabIndex = 5;
            this.buttonSolutionExplorer.Text = "Open Solution Explorer";
            this.buttonSolutionExplorer.UseVisualStyleBackColor = true;
            this.buttonSolutionExplorer.Click += new System.EventHandler(this.buttonSolutionExplorer_Click);
            // 
            // buttonDemo
            // 
            this.buttonDemo.Location = new System.Drawing.Point(176, 13);
            this.buttonDemo.Name = "buttonDemo";
            this.buttonDemo.Size = new System.Drawing.Size(130, 69);
            this.buttonDemo.TabIndex = 6;
            this.buttonDemo.Text = "Demo";
            this.buttonDemo.UseVisualStyleBackColor = true;
            this.buttonDemo.Click += new System.EventHandler(this.buttonDemo_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(354, 333);
            this.Controls.Add(this.buttonDemo);
            this.Controls.Add(this.buttonSolutionExplorer);
            this.Controls.Add(this.buttonCompare);
            this.Controls.Add(this.buttonSimulator);
            this.Controls.Add(this.testButton);
            this.Controls.Add(this.buttonSampler);
            this.Controls.Add(this.buttonEditor);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Robot Collaboration Framework";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonEditor;
        private System.Windows.Forms.Button buttonSampler;
        private System.Windows.Forms.Button testButton;
        protected internal System.Windows.Forms.Button buttonSimulator;
        protected internal System.Windows.Forms.Button buttonCompare;
        protected internal System.Windows.Forms.Button buttonSolutionExplorer;
        private System.Windows.Forms.Button buttonDemo;
    }
}