﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using MyExtensions;
using DiscoverySolver;

namespace AgentDraw
{
    [Serializable]
    public class SimModel : AgentModel, TreeSearchSolver.IExtractableModel
    {
        public enum WaitType { DelayedDiscovery, WaitAtSpot };

        protected static WaitType[] WaitMode = { WaitType.DelayedDiscovery, WaitType.WaitAtSpot };

        protected List<NodeModel> undiscoveredTasks;
        protected List<NodeModel> executedTasks;
        public int UndiscoveredTaskCount { get { return undiscoveredTasks.Count; } }
        public int SearchTaskCount { get { return TaskNodes[0].Count; } }
        public int RescueTaskCount { get { return TaskNodes[1].Count; } }
        private int[] executedCount;
        public double[] AvgExecutionTime {
            get
            {
                double[] t = new double[executedTimeSum.Length];
                Array.Copy(executedTimeSum, t, t.Length);
                for(int i=0;i<t.Length;i++)
                    t[i] /= executedCount[i];
                return t;
            }
        }
        private double[] executedTimeSum;
        public int[] ExecutedTaskCount { get { return executedCount; } }
        protected int discoveryRadius;
        public int DiscoveryRadius
        {
            get { return discoveryRadius; }
            set
            {
                if (value < 0)
                    return;
                discoveryRadius = value;
                //refresh discovery area for high UAVs and high tasks (level 0)
                if (ShowDiscoveryForUAV)
                    SetDiscovery(discoveryRadius, 0, typeof(UAV));
                if (ShowDiscoveryForTask)
                    SetDiscovery(discoveryRadius, 0, typeof(Task));
            }
        }
        static bool ShowDiscoveryForUAV = false;
        static bool ShowDiscoveryForTask = true;
        public static bool DiscoverContinously = false;

        [NonSerialized]
        Dictionary<int, double> xCoord, yCoord, flightDistance, gainedUtility, stayTime; //UAVs
        [NonSerialized]
        Dictionary<int, double> discoveryTime, executionTime; //tasks
        [NonSerialized]
        List<Pair<NodeModel, double>> ToDiscover;

        public SimModel()
        {
            DefaultConstructor();
        }

        public SimModel(PlacementView view) :
            base(view)
        {
            DefaultConstructor();
        }

        private void DefaultConstructor()
        {
            undiscoveredTasks = new List<NodeModel>();
            executedTasks = new List<NodeModel>();
            UAVNodes.Add(new NodeList());
            UAVNodes.Add(new NodeList());
            TaskNodes.Add(new NodeList());
            TaskNodes.Add(new NodeList());
            //hiUAV - UAVNodes[0];
            //loUAV - UAVNodes[1];
            //discoveryTasks - TaskNodes[0];
            //sampledTasks - TaskNodes[1];
            speed = new double[2] { 1.0, 1.0 };
            InitEmptyVars();
            InitDefault();
        }

        /// <summary>
        /// Copy constructor without setting view
        /// </summary>
        /// <param name="source">source class</param>
        public SimModel(SimModel source) :
            base(source)
        {
            undiscoveredTasks = new List<NodeModel>();
            foreach (NodeModel m in source.undiscoveredTasks)
                undiscoveredTasks.Add(new NodeModel(m));
            executedTasks = new List<NodeModel>();
            foreach (NodeModel m in source.executedTasks)
                executedTasks.Add(new NodeModel(m));
            InitEmptyVars();
            RefreshUAVCoords();

            DiscoveryRadius = source.discoveryRadius;
        }

        public SimModel GetGroundTruthModel()
        {
            SimModel gtModel = new SimModel(this);
            foreach (NodeModel n in gtModel.undiscoveredTasks)
            {
                gtModel.AddTask(n, 1);
            }
            gtModel.undiscoveredTasks.Clear();
            gtModel.RefreshLoHiConnections();
            gtModel.ReAssignActivations();
            return gtModel;
        }

        public int[] GetMaxSlots()
        {
            int[] MaxSlots = new int[2] { TaskNodes[0].Count * 2 / 3, (int)(TaskNodes[1].Count / (UAVNodes[1].Count-1)) };
            return MaxSlots;
        }

        void InitEmptyVars()
        {
            xCoord = new Dictionary<int, double>();
            yCoord = new Dictionary<int, double>();
            flightDistance = new Dictionary<int, double>();
            gainedUtility = new Dictionary<int, double>();
            stayTime = new Dictionary<int, double>();
            ToDiscover = new List<Pair<NodeModel, double>>();
            InitTaskVariables();
        }

        void InitTaskVariables()
        {
            discoveryTime = new Dictionary<int, double>();
            executionTime = new Dictionary<int, double>();
            executedTasks = new List<NodeModel>();
            foreach (NodeList l in TaskNodes)
                foreach (NodeModel n in l)
                {
                    discoveryTime.Add(n.ID, 0);
                    executionTime.Add(n.ID, - 1);
                }
            foreach (NodeModel n in undiscoveredTasks)
            {
                discoveryTime.Add(n.ID, -1);
                executionTime.Add(n.ID, -1);
            }
            executedCount = new int[] { 0, 0 };
            executedTimeSum = new double[] { 0.0, 0.0 };
        }

        public void InitGroundTruth(Sampler GTSampler, int discoveryRadius = -1)
        {
            TaskNodes[1].Clear();
            undiscoveredTasks = SampleTasks(GTSampler);
            if (discoveryRadius > 0)
                DiscoveryRadius = discoveryRadius;
            InitTaskVariables();
        }

        internal void RefreshUAVCoords()
        {
            foreach(NodeList list in UAVNodes)
                foreach(NodeModel uav in list){
                    if (xCoord.ContainsKey(uav.ID) == false)
                    {
                        xCoord.Add(uav.ID, (double)uav.Position.X);
                        yCoord.Add(uav.ID, (double)uav.Position.Y);
                        flightDistance.Add(uav.ID, 0);
                        gainedUtility.Add(uav.ID, 0);
                        stayTime.Add(uav.ID, 0);
                    }
                    else{
                        Point newPos = new Point((int)xCoord[uav.ID], (int)yCoord[uav.ID]);
                        uav.Position = newPos;
                    }
                }
        }

        protected IEnumerable<NodeModel> allNodes()
        {
            foreach (NodeList l in UAVNodes)
                foreach (NodeModel n in l)
                    yield return n;
            foreach (NodeList l in TaskNodes)
                foreach (NodeModel n in l)
                    yield return n;
            foreach (NodeModel n in undiscoveredTasks)
                yield return n;
            foreach (NodeModel n in executedTasks)
                yield return n;
        }

        public void DoDiscovery(Sampler sampler, double step, double currentTime)
        {
            int changed = 0;
            if (DiscoverContinously)
                foreach (NodeModel uav in UAVNodes[0])
                    changed += DoDiscovery(uav, sampler, currentTime);
            else
            {
                foreach (var tUav in ToDiscover)
                {
                    if (tUav.Item2 <= step)
                    {
                        tUav.Item2 = 0;
                        changed += DoDiscovery(tUav.Item1, sampler, currentTime);
                    }
                    else
                        tUav.Item2 -= step;
                }
                ToDiscover.RemoveAll(p => p.Item2 == 0);
            }
            if (changed > 0)
                RefreshLoHiConnections();
        }

        protected int DoDiscovery(NodeModel uav, Sampler sampler, double time)
        {
            Rectangle area = new Rectangle(uav.Position.X - discoveryRadius, uav.Position.Y - discoveryRadius, 2 * discoveryRadius, 2 * discoveryRadius);
            List<NodeModel> tmpList = undiscoveredTasks.FindAll(t => area.Contains(t.Position));
            sampler.ClearRectangle(uav.Position, discoveryRadius);
            if (tmpList.Count == 0)
                return 0;
            foreach (NodeModel task in tmpList)
            {
                discoveryTime[task.ID] = time;
                AddTask(task, 1);
            }
            undiscoveredTasks.RemoveAll(t => area.Contains(t.Position));
            return tmpList.Count;
        }

        /// <summary>
        /// Moves the UAVs according to the solutions
        /// </summary>
        /// <param name="angle">solution directions</param>
        /// <param name="assignment">solution task ID assignment</param>
        /// <param name="step">timestep</param>
        /// <param name="currentTime">time for utility calculation</param>
        /// <returns>if any task has been executed</returns>
        public bool MoveUAVpos(List<List<double>> angle, List<List<int>> assignment, double step, double currentTime)
        {
            bool problemChange = false;
            for (int l = 0; l < UAVNodes.Count; l++)
                for (int a = 0; a < UAVNodes[l].Count; a++)
                {
                    //waiting time check
                    int uav = UAVNodes[l][a].ID;
                    double currentStep = step;
                    if (stayTime[uav] >= 0) //if has to wait
                    {
                        currentStep -= stayTime[uav];
                        stayTime[uav] = Math.Max(stayTime[uav] - step, 0);
                    }
                    if (currentStep <= 0.0)
                        continue;   //if only waits, don't move

                    //retrieve assigned task
                    NodeModel assigned;
                    if (assignment[l][a] == -1)
                        assigned = null;
                    else
                        assigned = FindById(TaskNodes, assignment[l][a]);

                    //move UAV
                    if (assigned != null)   //if there is an existing task assigned
                    {
                        double mul = currentStep * speed[l] / AgentModel.Distance(UAVNodes[l][a], assigned);
                        if (mul >= 1.0)    //target reached in this timestep
                        {
                            flightDistance[uav] += AgentModel.Distance(UAVNodes[l][a], assigned);
                            xCoord[uav] = assigned.Position.X;
                            yCoord[uav] = assigned.Position.Y;
                            ExecuteTask(UAVNodes[l][a], assigned, l, currentTime);
                            problemChange = true;
                        }
                        else    //target is farther
                        {
                            flightDistance[UAVNodes[l][a].ID] += currentStep * speed[l];
                            xCoord[UAVNodes[l][a].ID] += (assigned.Position.X - UAVNodes[l][a].Position.X) * mul;
                            yCoord[UAVNodes[l][a].ID] += (assigned.Position.Y - UAVNodes[l][a].Position.Y) * mul;
                        }
                    }
                    else if (angle[l][a] != SimController.StopAngle)    //if there is a particular direction to go for
                    {
                        flightDistance[UAVNodes[l][a].ID] += currentStep * speed[l];
                        xCoord[UAVNodes[l][a].ID] += Math.Cos(angle[l][a]) * currentStep * speed[l];
                        yCoord[UAVNodes[l][a].ID] += Math.Sin(angle[l][a]) * currentStep * speed[l];
                    }
                }

            //decrease battery
            for (int i = 0; i < TravelTime.Length; i++)
                SetTravelTime((int)Math.Round(TravelTime[i] - step), i);
            return problemChange;
        }

        private void ExecuteTask(NodeModel uav, NodeModel task, int level, double time)
        {
            gainedUtility[uav.ID] += CalculateUtility(this, level, time + TaskTime[level], 0);
            executionTime[task.ID] = time + TaskTime[level];
            stayTime[uav.ID] = (WaitMode[level] == WaitType.DelayedDiscovery) ? 0 : TaskTime[level];
            RemoveNode(task);
            executedTasks.Add(task);
            executedCount[level]++;
            executedTimeSum[level] += time;
            if((!DiscoverContinously) && (level == 0))
                ToDiscover.Add(new Pair<NodeModel, double>(task, TaskTime[level]));
        }

        public void AddLoUAV(NodeModel uav)
        {
            uav.IsSelected = false;
            AddUAV(uav, 1);
            ConnectUAV(uav, TaskNodes[1]);
        }

        internal void AddHiUAV(NodeModel uav)
        {
            uav.IsSelected = true;
            AddUAV(uav, 0);
            ConnectUAV(uav, TaskNodes[0]);
            if (ShowDiscoveryForUAV)
                SetDiscovery(discoveryRadius, 0, typeof(UAV));
        }

        protected void ConnectUAV(NodeModel uav, NodeList tasks)
        {
            foreach (NodeModel task in tasks)
            {
                AddConnection(new RelationModel(task, uav));
            }
        }

        protected void RefreshLoHiConnections()
        {
            Connections.Clear();
            for(int i=0; i< UAVNodes.Count;i++)
                foreach (NodeModel uav in UAVNodes[i])
                {
                    ConnectUAV(uav, TaskNodes[i]);
                }
        }

        public void AddDiscoveryTask(NodeModel task)
        {
            task.IsSelected = true;
            AddTask(task, 0);
            RefreshLoHiConnections();
            ReAssignActivations();
            if (ShowDiscoveryForTask)
                SetDiscovery(discoveryRadius, 0, typeof(Task));
            InitTaskVariables();
        }

        protected override void UpdateView(object sender)
        {
            //RefreshUAVCoords();
            base.UpdateView(sender);
        }

        internal double GetGainedUtility(int level = -1)
        {
            double sum = 0;
            if (level < 0)
                for (int i = 0; i < UAVNodes.Count; i++)
                    sum += GetGainedUtility(i);
            else
                foreach (NodeModel n in UAVNodes[level])
                    sum += gainedUtility[n.ID];
            return sum;
        }

        internal double GetFlightDistance(int level = -1)
        {
            double sum = 0;
            if (level < 0)
                for (int i = 0; i < UAVNodes.Count; i++)
                    sum += GetFlightDistance(i);
            else
                foreach (NodeModel n in UAVNodes[level])
                    sum += flightDistance[n.ID];
            return sum;
        }

        internal int GetGroundTruthNofTasks(int level = -1)
        {
            int sum = 0;
            if (level < 0)
                for (int i = 0; i < TaskNodes.Count; i++)
                    sum += GetGroundTruthNofTasks(i);
            else
            {
                sum += TaskNodes[level].Count;
                if (level == 1)
                    sum += undiscoveredTasks.Count;
            }
            return sum;
        }

        internal string GetTimestepData()
        {
            string stout = "";
            foreach (NodeList l in UAVNodes)
                foreach (NodeModel n in l)
                {
                    stout = String.Format("{0}{1} {2} {3} ", stout, xCoord[n.ID], -yCoord[n.ID], gainedUtility[n.ID]);
                }
            stout = String.Format("{0};...\n", stout);
            return stout;
        }

        internal string GetSummaryData()
        {
            string stout = "TaskTime=[...\n";
            foreach (NodeList l in TaskNodes)
                foreach (NodeModel n in l)
                {
                    stout = GetSummaryData(stout, n);
                }
            foreach (NodeModel n in undiscoveredTasks)
                stout = GetSummaryData(stout, n);
            foreach (NodeModel n in executedTasks)
                stout = GetSummaryData(stout, n);
            stout = String.Format("{0}];\n\n", stout);
            return stout;
        }

        private string GetSummaryData(string stout, NodeModel n)
        {
            stout = String.Format("{0}{1} {2} {3} {4};...\n", stout, n.Position.X, -n.Position.Y, discoveryTime[n.ID], executionTime[n.ID]);
            return stout;
        }

        /// <summary>
        /// Calculates the aggregated distance table in the following structure:
        ///        | tasks
        /// -------+-------
        /// tasks  |  
        /// agents |  
        /// DANGER!! this is designed to be used with the ground truth solution, so it does not use the stayTime structure
        /// </summary>
        /// <param name="level">the agent level of which the table is generated</param>
        /// <returns>the distance table</returns>
        public double[,] AggregatedDistanceTableForLevel(int level)
        {
            var tasks = TaskNodes[level];
            var agents = UAVNodes[level];
            double[,] table = new double[tasks.Count + agents.Count, tasks.Count];
            for (int i = 0; i < tasks.Count; i++)
                for (int j = 0; j < tasks.Count; j++)
                {
                    table[i, j] = Distance(tasks[i], tasks[j]) / speed[level] + ((WaitMode[level] == WaitType.DelayedDiscovery) ? 0 : TaskTime[level]);
                }
            for(int i=0; i<(agents.Count); i++)
                for (int j = 0; j < tasks.Count; j++)
                {
                    table[tasks.Count + i, j] = Distance(agents[i], tasks[j]) / speed[level] + TaskTime[level];
                }
            return table;
        }

        protected static void PrintDistanceTableWithWait(System.IO.TextWriter tw, NodeList rows, NodeList cols, double speed = 1.0, double taskTime = 0.0, Dictionary<int, double> extraTimePerRow = null)
        {
            foreach (NodeModel r in rows)
            {
                double extra;
                if (extraTimePerRow == null)
                    extra = 0;
                else
                    extra = extraTimePerRow[r.ID];
                foreach (NodeModel c in cols)
                {
                    tw.Write(Distance(r, c) / speed + taskTime + extra);
                    tw.Write(" ");
                }
                tw.WriteLine();
            }
        }

        public override void GenerateProblemOutput(TextWriter tw, int[] nofslots = null)
        {
            if (nofslots == null)
                nofslots = new int[0];
            WriteCounts(tw, nofslots);
            WriteEnableMatrix(tw);

            //travel time task-task
            for (int i = 0; i < TaskNodes.Count; i++)
                PrintDistanceTableWithWait(tw, TaskNodes[i], TaskNodes[i], speed[i], (WaitMode[i] == WaitType.DelayedDiscovery)?0:TaskTime[i]);

            //travel time agent-task
            for (int i = 0; i < TaskNodes.Count; i++)
                PrintDistanceTableWithWait(tw, UAVNodes[i], TaskNodes[i], speed[i], TaskTime[i], stayTime);
            
            WriteParams(tw);
        }

        public void GenerateDiscoveryProblemOutput(TextWriter tw, Sampler distribution)
        {
            double multiplier = 100;

            //nof agents
            tw.WriteLine(String.Format("nofAgents = {0};", UAVNodes[0].Count));

            //nof tasks
            tw.WriteLine(String.Format("nofTasks = {0};", TaskNodes[0].Count));

            //travel time task-task
            tw.Write("TravelTT = ");
            PrintDistanceMatrix(tw, TaskNodes[0], TaskNodes[0], speed[0], multiplier, true, true, 0, TravelTime[0] * speed[0] + 1);
            tw.WriteLine(';');

            //travel time agent-task
            tw.Write("TravelAT = ");
            PrintDistanceMatrix(tw, UAVNodes[0], TaskNodes[0], speed[0], multiplier, true, false);
            tw.WriteLine(';');

            //expected discovery single
            /*tw.Write("EDiscoverySingle = [");
            List<Rectangle> l = new List<Rectangle>(1);
            bool started = false;
            foreach (NodeModel t in TaskNodes[0])
            {
                if (started)
                {
                    l.Clear();
                    tw.Write(", ");
                }
                started = true;
                l.Add(MyExtensionMethods.GetDiscoveryArea(t));
                tw.Write(distribution.GetExpectedValueInIntersection(l));
            }
            tw.WriteLine("];");*/

            //expected discovery intersection
            tw.Write("EDiscovery = [");
            List<Rectangle> l = new List<Rectangle>(2);
            bool s2 = false;
            foreach (NodeModel t1 in TaskNodes[0])
            {
                if (s2)
                    tw.Write(", ");
                s2 = true;
                tw.Write('[');
                bool started = false;
                foreach (NodeModel t2 in TaskNodes[0])
                {
                    l.Clear();
                    l.Add(t1.GetDiscoveryArea());
                    l.Add(t2.GetDiscoveryArea());
                    if (started)
                        tw.Write(", ");
                    started = true;
                    tw.Write(distribution.GetExpectedValueInIntersection(l));
                }
                tw.Write(']');
            }
            tw.WriteLine("];");

            //battery
            double battery = Math.Round(TravelTime[0] * multiplier);
            tw.WriteLine(String.Format("battery = {0};", battery));

            //P: max utility
            tw.WriteLine(String.Format("P = {0};", p[0]));

            //lambda: utility decrease by time
            tw.WriteLine(String.Format("lam = {0};", lam[0]));

            //epsilon: utility degrade by later timeslot
            tw.WriteLine(String.Format("epsilon = {0};", epsilon[0]));
        }

        internal Solver GetDiscoverySolver(Sampler distribution)
        {
            double[,] tt = GetDistanceArray(TaskNodes[0], TaskNodes[0], speed[0]);
            double[,] at = GetDistanceArray(UAVNodes[0], TaskNodes[0], speed[0]);
            double[,] ed = new double[TaskNodes[0].Count,TaskNodes[0].Count];
            List<Rectangle> l = new List<Rectangle>(2);
            for(int i=0; i<TaskNodes[0].Count; i++)
            {
                for(int j=0; j<TaskNodes[0].Count; j++)
                {
                    l.Clear();
                    l.Add(TaskNodes[0][i].GetDiscoveryArea());
                    l.Add(TaskNodes[0][j].GetDiscoveryArea());
                    ed[i,j] = distribution.GetExpectedValueInIntersection(l);
                }
            }
            Solver retval = new global::DiscoverySolver.Solver(UAVNodes[0].Count, TaskNodes[0].Count,
                tt, at, ed, TravelTime[0], p[0], (double)lam[0], epsilon[0]);
            return retval;
        }

        public override void OnDeserialization(object sender)
        {
            base.OnDeserialization(sender);
            InitEmptyVars();
            InitTaskVariables();
            RefreshUAVCoords();
            RefreshRadius();
            //Console.WriteLine(String.Format("deserialized: {0}", this.ToString()));
            InitNodeID();
        }

        private void InitNodeID()
        {
            int maxID = 0;
            foreach (NodeModel n in this.allNodes())
                if (n.ID > maxID) maxID = n.ID;
            NodeModel.NextID = maxID + 1;
        }

        public override int GetHashCode()
        {
            int hash;
            using (MemoryStream fs = new MemoryStream())
            {
                IFormatter formatter = new BinaryFormatter();
                formatter.Serialize(fs, this);
                hash = fs.ToArray().GetHashCode();
            }
            return hash;
        }

        internal List<int>[] GetContsraints(int hiLevel, int loLevel)
        {
            List<int>[] C = new List<int>[TaskNodes[loLevel].Count];
            for (int i = 0; i < TaskNodes[loLevel].Count; i++)
            {
                C[i] = new List<int>();
                NodeModel task = TaskNodes[loLevel][i];
                foreach (var a in Activations)
                {
                    if (a.To == task)
                        C[i].Add(TaskNodes[hiLevel].IndexOf(a.From));
                }
            }
            return C;
        }

        internal void RemoveEmptyDiscoveries(Sampler sampler)
        {
            List<Rectangle> r = new List<Rectangle>(1);
            List<NodeModel> toDelete = new List<NodeModel>();
            foreach (NodeModel task in TaskNodes[0])
            {
                r.Clear();
                r.Add(task.GetDiscoveryArea());
                if (sampler.GetExpectedValueInIntersection(r) == 0.0)
                    toDelete.Add(task);
            }
            foreach(NodeModel t in toDelete)
                RemoveNode(t);
        }

        public void ExtractFixData(out Dictionary<PointF, int>[] tasks, out double[] speed, out double[] radius, out double[] taskTime)
        {
            tasks = new Dictionary<PointF, int>[nLevel];
            speed = new double[nLevel];
            radius = new double[nLevel];
            taskTime = TaskTime;
            for (int i = 0; i < nLevel; i++)
            {
                speed[i] = this.speed[i];
                radius[i] = discoveryRadius;
                tasks[i] = new Dictionary<PointF, int>();
                for (int j = 0; j < TaskNodes[i].Count; j++)
                {
                    PointF pos = TaskNodes[i][j].Position;
                    while (tasks[i].ContainsKey(pos))
                        pos.X += 0.0017f;
                    tasks[i].Add(pos, TaskNodes[i][j].ID);
                }
            }
        }

        public void ExtractCurrentData(out Dictionary<int, PointF>[] agents, out double[] battery, out Dictionary<int, double> stayTime)
        {
            agents = new Dictionary<int, PointF>[nLevel];
            battery = new double[nLevel];
            stayTime = this.stayTime;
            for (int i = 0; i < nLevel; i++)
            {
                speed[i] = this.speed[i];
                battery[i] = TravelTime[i];
                agents[i] = new Dictionary<int, PointF>();
                for (int j = 0; j < UAVNodes[i].Count; j++)
                    agents[i].Add(UAVNodes[i][j].ID, UAVNodes[i][j].Position);
            }
        }

        public void Scale(double scaler)
        {
            Random rnd = new Random();
            HashSet<Point> posiitons = new HashSet<Point>();
            foreach (var node in allNodes())
            {
                Point scaledLoc = new Point((int)(node.Position.X * scaler), (int)(node.Position.Y * scaler));
                while (!posiitons.Add(scaledLoc))
                {
                    scaledLoc.Offset(rnd.Next(-2, 2), rnd.Next(-2, 2));
                }
                node.Position = scaledLoc;
            }
            for (int i = 0; i < speed.Length; i++)
                speed[i] *= scaler;
            bool sdt = ShowDiscoveryForTask;
            bool sdu = ShowDiscoveryForUAV;
            DiscoveryRadius = (int)Math.Ceiling(DiscoveryRadius * scaler);
        }

        public void TestRemoveRandomSearchTasks(int count)
        {
            for (int i = 0; i < count; i++)
            {
                Random rnd = new Random();
                int chosenTask = rnd.Next(SearchTaskCount);
                if (SearchTaskCount>0)
                    RemoveNode(TaskNodes[0][chosenTask]);
            }
        }
    }
}
