﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AgentDraw
{
    public partial class SolutionExplorer : Form, IAgentForm
    {
        Size FormAndViewSizeDiff = new Size(41, 236);
        public GeneratorProg Program;
        List<Solution> solutions;

        public SolutionExplorer()
        {
            InitializeComponent();

            solutions = new List<Solution>();
            this.Program = new GeneratorProg(placementView, this);
        }

        private void loadProblemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Multi-level UAV-Task graph|*.mug";
            openDialog.Title = "Open graph";
            openDialog.RestoreDirectory = true;
            openDialog.InitialDirectory = "../../";
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                using (Stream fs = openDialog.OpenFile())
                {
                    Program.OpenFile(fs);
                }
            }
        }

        public void ReSize(Size size)
        {
            Size = Size.Add(size, FormAndViewSizeDiff);
        }

        public void SetTravelTime(int p, int level)
        {
            Program.SetTravelTime(p, level);
        }

        public void SetTravelTime(int[] p)
        {
            for (int i = 0; i < p.Length; i++)
                SetTravelTime(p[i], i);
        }

        public void SetNofSlot(int p)
        {
            Program.NofSlots = p;
        }

        private List<Point> clearSolutions(){
            List<Point> locations = new List<Point>();
            foreach(Solution sol in solutions){
                locations.Add(sol.Location);
                sol.Close();
            }
            solutions.Clear();
            return locations;
        }

        private void refreshSolutionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            refreshSolutions();
        }

        internal void refreshSolutions()
        {
            List<Point> locations = clearSolutions();
            int line = 0;
            foreach (string s in textBoxSolutions.Lines)
            {
                Solution sol = Program.OpenNewSolution(s, "Line " + line.ToString());
                if (sol != null)
                {
                    try
                    {
                        sol.Location = locations[solutions.Count];
                    }
                    catch { }
                    solutions.Add(sol);
                }
                line++;
            }
            this.BringToFront();
        }

        private void textBoxSolutions_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Return))
                refreshSolutions();
        }


        public void SetProgress(int[] p, double[] t)
        {
            throw new NotImplementedException();
        }
    }
}
