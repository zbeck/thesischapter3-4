﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AgentDraw
{
    public partial class PlacementForm : Form, IAgentForm
    {
        public PlacementProg program;

        public PlacementForm()
        {
            InitializeComponent();
            this.program = new PlacementProg(placementView, this);

            placementView.ShowConnections = true;
            placementView.ShowActivations = true;
            placementView.ShowNames = true;
            placementView.ShowRadius = false;
            program.NofSlots = 1;
            nofSlotsBox.Text = "1";
        }

        private void checkBoxActivation_CheckedChanged(object sender, EventArgs e)
        {
            placementView.ShowActivations = checkBoxActivation.Checked;
            placementView.Invalidate();
        }

        private void checkBoxConnections_CheckedChanged(object sender, EventArgs e)
        {
            placementView.ShowConnections = checkBoxConnections.Checked;
            placementView.Invalidate();
        }

        private void checkBoxRadius_CheckedChanged(object sender, EventArgs e)
        {
            placementView.ShowRadius = checkBoxRadius.Checked;
            placementView.Invalidate();
        }

        private void nofSlotsBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                program.NofSlots = Int32.Parse(nofSlotsBox.Text);
            }
            catch (FormatException ex) { ex.ToString(); }
            finally
            {
                nofSlotsLabel.Text = program.NofSlots.ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (TextWriter tw = new StreamWriter(fileNameTextBox.Text))
            {
                program.GenerateOutput(tw);
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "Multi-level UAV-Task graph|*.mut";
            saveDialog.Title = "Save graph";
            saveDialog.RestoreDirectory = true;
            saveDialog.InitialDirectory = "../../";
            saveDialog.ShowDialog();

            if (saveDialog.FileName != "")
            {
                using (Stream fs = saveDialog.OpenFile())
                {
                    program.SaveFile(fs);
                }
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Multi-level UAV-Task graph|*.mut";
            openDialog.Title = "Open graph";
            openDialog.RestoreDirectory = true;
            openDialog.InitialDirectory = "../../";
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                using (Stream fs = openDialog.OpenFile())
                {
                    program.OpenFile(fs);
                }
            }
        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            program.Clear();
        }

        private void loadSolutionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Title = "Open solution file";
            openDialog.RestoreDirectory = true;
            openDialog.InitialDirectory = "../../";
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                StreamReader sr = new StreamReader(openDialog.OpenFile());
                program.OpenSolution(sr, openDialog.FileName);
                sr.Close();
            }

        }

        private void trackBarDistance_Scroll(object sender, EventArgs e)
        {
            program.SetTravelTime(trackBarDistance.Value, 0);
            checkBoxRadius.Checked = true;
        }

        public void SetNofSlot(int p)
        {
            nofSlotsLabel.Text = program.NofSlots.ToString();
            nofSlotsBox.Text = program.NofSlots.ToString();
        }

        public void SetTravelTime(int p, int l)
        {
            trackBarDistance.Value = p;
        }

        public void SetTravelTime(int[] p)
        {
            SetTravelTime(p[0], 0);
        }

        public void ReSize(Size size)
        {
            throw new NotImplementedException();
        }


        public void SetProgress(int[] p, double[] t)
        {
            throw new NotImplementedException();
        }
    }
}
