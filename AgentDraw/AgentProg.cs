﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using MyExtensions;

namespace AgentDraw
{
    public abstract class AgentProg
    {
        public PlacementView View;
        protected AgentModel model;
        protected AgentController controller;
        protected IAgentForm form;
        public static string WorkFolder = "../../../TestResults/";

        public int NofSlots
        {
            get { if (model == null) return 0; return model.NofSlots; }
            set { if (model != null) model.NofSlots = value; }
        }

        public AgentProg(PlacementView pview, IAgentForm form)
        {
            this.form = form;
            View = pview;
        }

        protected void InitView()
        {
            if (View != null)
                View.Controller = controller;
        }

        public void SetCapture()
        {
            if (View != null)
                View.Capture = true;
        }

        public void StopCapture()
        {
            if (View != null)
                View.Capture = false;
        }

        internal virtual void GenerateOutput(TextWriter tw)
        {
            model.GenerateProblemOutput(tw);
        }

        internal void SaveFile(System.IO.Stream fs)
        {
            IFormatter formatter = new BinaryFormatter();
            formatter.Serialize(fs, model);
        }

        internal void SaveToXmlFile(System.IO.Stream fs)
        {
            XmlModel copyModel = new XmlModel(model as SimModel);
            System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(copyModel.GetType());
            x.Serialize(fs, copyModel);
        }

        internal void SetTravelTime(int p, int level)
        {
            model.SetTravelTime(p, level);
            if (View != null)
                View.Invalidate();
        }

        internal void OpenSolution(System.IO.StreamReader sr, string solver)
        {
            foreach (List<int> values in MyExtensionMethods.ParseOldSolution(sr.ReadToEnd()))
                model.OpenSolution(values, solver);
        }

        internal void OpenNewSolution(System.IO.StreamReader sr, string solver)
        {
            model.OpenSolutionModel(sr.ReadToEnd(), solver);
        }

        internal Solution OpenNewSolution(string solution, string solver)
        {
            return model.OpenSolution(solution, solver);
        }

        internal void Clear(AgentModel model)
        {
            if (View != null)
                View.Clear();
            controller.Model = model;
            NodeModel.ResetIDCounter();
        }

        internal static bool ValidSolver(string solver)
        {
            foreach (string slv in solver.Split(';'))
            {
                string script = slv.Split(new char[] { ' ', '>' })[0];
                if (!File.Exists(String.Concat(WorkFolder, script)))
                    return false;
            }
            return true;
        }

        internal void SpeedsDialog()
        {
            AskForTwoNumbersForm dialog = new AskForTwoNumbersForm("UAV speeds");

            dialog.textBoxEVal.Text = model.Speed[0].ToString();
            dialog.textBoxEval2.Text = model.Speed[1].ToString();
            dialog.ShowDialog();
            try
            {
                double[] sp = new double[2];
                sp[0] = Double.Parse(dialog.textBoxEVal.Text);
                sp[1] = Double.Parse(dialog.textBoxEval2.Text);
                model.Speed = sp;
            }
            catch (FormatException ex) { Console.WriteLine(ex.ToString()); }
        }
    }
}
