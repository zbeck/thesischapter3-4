%% preprocessing

TimeStep = 30;

color = 'k';
sampled = 0;

if(sampled == 0)
    primary = [color ':'];
    secondary = [color '-.'];
else
    primary = color;
    secondary = [color '--'];
end
stepdataW = size(TimeStepData, 2);
stepdataL = size(TimeStepData, 1);

myColours = {[232/255, 0, 46/255], [1, 187/255, 46/255], [180/255, 152/255, 0],...
    [0, 111/255, 47/255], [25/255, 73/255, 0]};

%% tasks
%taskPoints = TaskTime(:, 1:3);
maxTime = 2*stepdataL;
undone = ~(TaskTime(:,4)+1);
search = ~TaskTime(:,3);
nresq = sum(~search);
taskTimes = (TaskTime(:,4)+undone)./TimeStep + undone*maxTime;
[~, sridx] = sort(search);

resqTasks = [TaskTime(sridx(1:nresq), [1 2]) taskTimes(sridx(1:nresq))];
[~, rtidx] = sort(resqTasks(:,3));
resqTasks = resqTasks(rtidx, :);

searchTasks = [TaskTime(sridx((nresq+1):end), [1 2]) taskTimes(sridx((nresq+1):end))];
[~, stidx] = sort(searchTasks(:,3));
searchTasks = searchTasks(stidx, :);
%% number of active UAVs
distances = (TimeStepData(2:stepdataL, 1:3:stepdataW)-TimeStepData(1:(stepdataL-1), 1:3:stepdataW)).^2 +...
    (TimeStepData(2:stepdataL, 2:3:stepdataW)-TimeStepData(1:(stepdataL-1), 2:3:stepdataW)).^2;
NofActiveUAV = sum(logical(distances), 2);
%% paths
%figure(1)
SxData = [1 4];
SyData = [2 5];
RxData = 7:3:stepdataW;
RyData = 8:3:stepdataW;
AllxData = [SxData RxData];
AllyData = [SyData RyData];

%plot(TimeStepData(45:length(TimeStepData), 7:3:stepdataW), TimeStepData(45:length(TimeStepData), 8:3:stepdataW))
plot(searchTasks(:, 1), searchTasks(:, 2), 'rx', 'LineWidth',2, 'MarkerSize', 20,'MarkerEdgeColor',myColours{1}, 'MarkerFaceColor', myColours{1})
hold on
%plot(TimeStepData(:, RxData), TimeStepData(:, RyData), 'LineWidth', 2)
plot([TimeStepData(1, SxData); TimeStepData(1, SxData)], [TimeStepData(1, SyData); TimeStepData(1, SyData)], 'LineWidth', 2, 'Marker', '*', 'MarkerSize', 20)
%plot(resqTasks(:, 1), resqTasks(:, 2), 'rx', 'LineWidth',2, 'MarkerSize', 20,'MarkerEdgeColor',myColours{1}, 'MarkerFaceColor', myColours{1})
hold off
axis equal
%plot(TimeStepData(:, 1), TimeStepData(:, 2))
%% utilities
% figure(2)
% subplot(6, 1, 1:5 )
% hold on
% plot(sum(TimeStepData(:, 3:3:stepdataW), 2), primary)
% subplot(6, 1, 6)
% hold on
% ylim([0, stepdataW/3])
% plot(NofActiveUAV, primary)
% figure(3)
% hold on
% plot(sum(TimeStepData(:, 9:3:stepdataW), 2), primary)
%% number of available tasks
% endTime = max(TaskTime(:,4));
% nofTasks = zeros(endTime, 1);
% for i = 1:size(TaskTime, 1)
%     if(TaskTime(i, 3) ~= 0)
%         if(TaskTime(i,4) ~= -1)
%             for j = round(TaskTime(i,3)):round(TaskTime(i,4))
%                 nofTasks(round(j)) = nofTasks(round(j))+1;
%             end
%         elseif(TaskTime(i,3) ~= -1)
%             for j = round(TaskTime(i,3)):round(endTime)
%                 nofTasks(j) = nofTasks(j)+1;
%             end
%         end
%     end
% end
% calcTasks = min(nofTasks, 12*ones(endTime, 1));
% figure(4)
% %hold off
% plot(0:(endTime-1),nofTasks)
% hold on
% plot(0:(endTime-1),calcTasks, 'g')

%% estimate calculation time
% CalculationTime = 0;
% time = [0.07*ones(1,9) 0.29 7.11 44.19];
% current = 0;
% for i=1:endTime
%     if(nofTasks(i) ~= current)
%         if(nofTasks(i) > 0)
%             CalculationTime = CalculationTime+time(calcTasks(i));
%         end
%         current = nofTasks(i);
%     end
% end
% CalculationTime/60
