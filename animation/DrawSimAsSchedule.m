%% load data
simPath = 'd:\VisualStudio\AgentDraw\IRIDIS\relationTest\run07P4';
origPath = cd(simPath);
eval('Sim0');

cd(origPath)
%% preprocessing
TimeStep = 10;
stepdataW = size(TimeStepData, 2);
stepdataL = size(TimeStepData, 1);
searchUAVs = 1:2;
resqUAVs = 3:(size(TimeStepData, 2)/3);
allUAVs = [searchUAVs resqUAVs];
colormapSize = round(stepdataL/3);

myColours = {[232/255, 0, 46/255], [1, 187/255, 46/255], [180/255, 152/255, 0],...
    [0, 111/255, 47/255], [25/255, 73/255, 0]};

UAVColors=zeros([length(allUAVs), 3]);
for uavID = searchUAVs
    UAVColors(uavID, :) = myColours{uavID*2};
end
for uavID = resqUAVs
    UAVColors(uavID, :) = myColours{3};
end

%% tasks
maxTime = 2*stepdataL;
undone = ~(TaskTime(:,4)+1);
search = ~TaskTime(:,3);
nresq = sum(~search);
taskTimes = (TaskTime(:,4)+undone)./TimeStep + undone*maxTime;
[~, sridx] = sort(search);

resqTasks = [TaskTime(sridx(1:nresq), [1 2]) taskTimes(sridx(1:nresq))];
[~, rtidx] = sort(resqTasks(:,3));
resqTasks = resqTasks(rtidx, :);

searchTasks = [TaskTime(sridx((nresq+1):end), [1 2]) taskTimes(sridx((nresq+1):end))];
[~, stidx] = sort(searchTasks(:,3));
searchTasks = searchTasks(stidx, :);
%% figure
hf=figure();
clf
hold on
% for task=1:nresq
%     rh(task) = plot(resqTasks(task, 1), resqTasks(task, 2), 'gx', 'MarkerSize', 10, 'LineWidth', 2);
% end
for task=1:size(searchTasks,1)
    sh(task) = plot(searchTasks(task, 1), searchTasks(task, 2), 'ro', 'MarkerSize', 10, 'LineWidth', 2);
end
axRange = axis();
axRange = [ min(axRange(1), min(min(TimeStepData(:, 1:3:stepdataW)))),...
            max(axRange(2), max(max(TimeStepData(:, 1:3:stepdataW)))),... 
            min(axRange(3), min(min(TimeStepData(:, 2:3:stepdataW)))),...
            max(axRange(4), max(max(TimeStepData(:, 2:3:stepdataW))))];
axis(axRange);
axis equal
for uav = searchUAVs
    lineBegin = 1;
    plot(TimeStepData(lineBegin, uav*3-2), TimeStepData(lineBegin, uav*3-1), 'bs', 'MarkerSize', 10, 'LineWidth', 2);
    running = true;
    while running
        lineEnd = lineBegin;
        while TimeStepData(lineBegin, uav*3) == TimeStepData(lineEnd, uav*3)
            if lineEnd >= size(TimeStepData, 1)
                running = false;
                break;
            end
            lineEnd = lineEnd+1;
        end
        if(running)
            line(TimeStepData([lineBegin lineEnd], uav*3-2), TimeStepData([lineBegin lineEnd], uav*3-1),...
                'Color', UAVColors(uav, :), 'LineWidth', 2)
            lineBegin = lineEnd;
        end
    end
end
hold off