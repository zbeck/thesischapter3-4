%% preprocessing
TimeStep = 10;
color = 'k';
sampled = 0;

if(sampled == 0)
    primary = [color ':'];
    secondary = [color '-.'];
else
    primary = color;
    secondary = [color '--'];
end
stepdataW = size(TimeStepData, 2);
stepdataL = size(TimeStepData, 1);
%% tasks
maxTime = 2*stepdataL;
undone = ~(TaskTime(:,4)+1);
search = ~TaskTime(:,3);
nresq = sum(~search);
taskTimes = (TaskTime(:,4)+undone)./TimeStep + undone*maxTime;
[~, sridx] = sort(search);

resqTasks = [TaskTime(sridx(1:nresq), [1 2]) taskTimes(sridx(1:nresq))];
[~, rtidx] = sort(resqTasks(:,3));
resqTasks = resqTasks(rtidx, :);

searchTasks = [TaskTime(sridx((nresq+1):end), [1 2]) taskTimes(sridx((nresq+1):end))];
[~, stidx] = sort(searchTasks(:,3));
searchTasks = searchTasks(stidx, :);
%% number of active UAVs
distances = (TimeStepData(2:stepdataL, 1:3:stepdataW)-TimeStepData(1:(stepdataL-1), 1:3:stepdataW)).^2 +...
    (TimeStepData(2:stepdataL, 2:3:stepdataW)-TimeStepData(1:(stepdataL-1), 2:3:stepdataW)).^2;
NofActiveUAV = sum(logical(distances), 2);
%% paths
hf=figure();
clf
hold on
for task=1:nresq
    rh(task) = plot(resqTasks(task, 1), resqTasks(task, 2), 'gx', 'MarkerSize', 5);
end
for task=1:size(searchTasks,1)
    sh(task) = plot(searchTasks(task, 1), searchTasks(task, 2), 'ro', 'MarkerSize', 5);
end
axRange = axis();
axRange = [ min(axRange(1), min(min(TimeStepData(:, 1:3:stepdataW)))),...
            max(axRange(2), max(max(TimeStepData(:, 1:3:stepdataW)))),... 
            min(axRange(3), min(min(TimeStepData(:, 2:3:stepdataW)))),...
            max(axRange(4), max(max(TimeStepData(:, 2:3:stepdataW))))];
axis(axRange);
axis equal
scounter = 1;
rcounter = 1;
waitforbuttonpress;
for row = 2:stepdataL
    while (scounter <= size(searchTasks,1)) && (searchTasks(scounter,3) < row)
        delete(sh(scounter));
        scounter = scounter + 1;
    end
    while (rcounter <= size(resqTasks,1)) && (resqTasks(rcounter,3) < row)
        delete(rh(rcounter));
        rcounter = rcounter + 1;
    end
    line(TimeStepData(row-1:row, 1:3:stepdataW), TimeStepData(row-1:row, 2:3:stepdataW))
    drawnow
    pause(0.1)
    k = get(hf, 'CurrentKey');
    if strcmp(k, 'space')
        waitforbuttonpress;
    elseif strcmp(k, 'escape')
        break
    end
end
hold off
%plot(TimeStepData(:, 1), TimeStepData(:, 2))
%% utilities
% figure(2)
% subplot(6, 1, 1:5 )
% hold on
% plot(sum(TimeStepData(:, 3:3:stepdataW), 2), primary)
% subplot(6, 1, 6)
% hold on
% ylim([0, stepdataW/3])
% plot(NofActiveUAV, primary)
% figure(3)
% hold on
% plot(sum(TimeStepData(:, 9:3:stepdataW), 2), primary)
%% number of available tasks
% endTime = max(TaskTime(:,4));
% nofTasks = zeros(endTime, 1);
% for i = 1:size(TaskTime, 1)
%     if(TaskTime(i, 3) ~= 0)
%         if(TaskTime(i,4) ~= -1)
%             for j = round(TaskTime(i,3)):round(TaskTime(i,4))
%                 nofTasks(round(j)) = nofTasks(round(j))+1;
%             end
%         elseif(TaskTime(i,3) ~= -1)
%             for j = round(TaskTime(i,3)):round(endTime)
%                 nofTasks(j) = nofTasks(j)+1;
%             end
%         end
%     end
% end
% calcTasks = min(nofTasks, 12*ones(endTime, 1));
% figure(4)
% %hold off
% plot(0:(endTime-1),nofTasks)
% hold on
% plot(0:(endTime-1),calcTasks, 'g')

%% estimate calculation time
% CalculationTime = 0;
% time = [0.07*ones(1,9) 0.29 7.11 44.19];
% current = 0;
% for i=1:endTime
%     if(nofTasks(i) ~= current)
%         if(nofTasks(i) > 0)
%             CalculationTime = CalculationTime+time(calcTasks(i));
%         end
%         current = nofTasks(i);
%     end
% end
% CalculationTime/60
