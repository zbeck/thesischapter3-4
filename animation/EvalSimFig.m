function EvalSimFig(sampled, fignum, TimeStepData, TaskTime)
%% preprocessing
color = 'k';

if(sampled == 0)
    primary = [color ':'];
    secondary = [color '-.'];
else
    primary = color;
    secondary = [color '--'];
end
stepdataW = size(TimeStepData, 2);
stepdataL = size(TimeStepData, 1);
%% tasks
taskPoints = TaskTime(:, 1:3);
%% number of active UAVs
% distances = (TimeStepData(2:stepdataL, 1:3:stepdataW)-TimeStepData(1:(stepdataL-1), 1:3:stepdataW)).^2 +...
%     (TimeStepData(2:stepdataL, 2:3:stepdataW)-TimeStepData(1:(stepdataL-1), 2:3:stepdataW)).^2;
% NofActiveUAV = sum(logical(distances), 2);
%% paths
%figure(1)

%plot(TimeStepData(45:length(TimeStepData), 7:3:stepdataW), TimeStepData(45:length(TimeStepData), 8:3:stepdataW))
plot(TimeStepData(:, 1:3:stepdataW), TimeStepData(:, 2:3:stepdataW))
hold on
plot(taskPoints(:, 1), taskPoints(:, 2), 'kX', 'MarkerSize', 5)
hold off
axis equal
%plot(TimeStepData(:, 1), TimeStepData(:, 2))
%% utilities
figure(fignum)
subplot(211)
hold on
plot(sum(TimeStepData(:, 3:3:stepdataW), 2), primary)
subplot(212)
hold on
plot(TimeStepData(:, 3:3:stepdataW)+repmat(3:3:stepdataW, size(TimeStepData,1),1))
plot(sum(TimeStepData(:, 9:3:stepdataW), 2), primary)

%% number of available tasks
% endTime = max(TaskTime(:,4));
% nofTasks = zeros(endTime, 1);
% for i = 1:size(TaskTime, 1)
%     if(TaskTime(i, 3) ~= 0)
%         if(TaskTime(i,4) ~= -1)
%             for j = round(TaskTime(i,3)):round(TaskTime(i,4))
%                 nofTasks(round(j)) = nofTasks(round(j))+1;
%             end
%         elseif(TaskTime(i,3) ~= -1)
%             for j = round(TaskTime(i,3)):round(endTime)
%                 nofTasks(j) = nofTasks(j)+1;
%             end
%         end
%     end
% end
% calcTasks = min(nofTasks, 12*ones(endTime, 1));
% figure(4)
% %hold off
% plot(0:(endTime-1),nofTasks)
% hold on
% plot(0:(endTime-1),calcTasks, 'g')

%% estimate calculation time
% CalculationTime = 0;
% time = [0.07*ones(1,9) 0.29 7.11 44.19];
% current = 0;
% for i=1:endTime
%     if(nofTasks(i) ~= current)
%         if(nofTasks(i) > 0)
%             CalculationTime = CalculationTime+time(calcTasks(i));
%         end
%         current = nofTasks(i);
%     end
% end
% CalculationTime/60

end