%% preprocessing
TimeStep = 10;
stepdataW = size(TimeStepData, 2);
stepdataL = size(TimeStepData, 1);
searchUAVs = 1:2;
resqUAVs = 3:(size(TimeStepData, 2)/3);
allUAVs = [searchUAVs resqUAVs];
colormapSize = round(stepdataL/3);
UAVColors=zeros([length(allUAVs), colormapSize, 3]);
for uavID = searchUAVs
    UAVColors(uavID, :, :) = colormap(winter(colormapSize));
end
for uavID = resqUAVs
    UAVColors(uavID, :, :) = colormap(copper(colormapSize));
end

%% tasks
maxTime = 2*stepdataL;
undone = ~(TaskTime(:,4)+1);
search = ~TaskTime(:,3);
nresq = sum(~search);
taskTimes = (TaskTime(:,4)+undone)./TimeStep + undone*maxTime;
[~, sridx] = sort(search);

resqTasks = [TaskTime(sridx(1:nresq), [1 2]) taskTimes(sridx(1:nresq))];
[~, rtidx] = sort(resqTasks(:,3));
resqTasks = resqTasks(rtidx, :);

searchTasks = [TaskTime(sridx((nresq+1):end), [1 2]) taskTimes(sridx((nresq+1):end))];
[~, stidx] = sort(searchTasks(:,3));
searchTasks = searchTasks(stidx, :);
%% paths
hf=figure();
clf
hold on
for task=1:nresq
    rh(task) = plot(resqTasks(task, 1), resqTasks(task, 2), 'gx', 'MarkerSize', 5);
end
for task=1:size(searchTasks,1)
    sh(task) = plot(searchTasks(task, 1), searchTasks(task, 2), 'ro', 'MarkerSize', 5);
end
axRange = axis();
axRange = [ min(axRange(1), min(min(TimeStepData(:, 1:3:stepdataW)))),...
            max(axRange(2), max(max(TimeStepData(:, 1:3:stepdataW)))),... 
            min(axRange(3), min(min(TimeStepData(:, 2:3:stepdataW)))),...
            max(axRange(4), max(max(TimeStepData(:, 2:3:stepdataW))))];
axis(axRange);
axis equal
scounter = 1;
rcounter = 1;
writerObj = VideoWriter('simVideo.avi');
writerObj.FrameRate = 10;
open(writerObj);
for row = 2:stepdataL
    while (scounter <= size(searchTasks,1)) && (searchTasks(scounter,3) < row)
        delete(sh(scounter));
        scounter = scounter + 1;
    end
    while (rcounter <= size(resqTasks,1)) && (resqTasks(rcounter,3) < row)
        delete(rh(rcounter));
        rcounter = rcounter + 1;
    end
    for uavID = allUAVs
        line(TimeStepData(row-1:row, uavID*3-2), TimeStepData(row-1:row, uavID*3-1),...
            'Color', squeeze(UAVColors(uavID, rem(row, colormapSize)+1, :)))
    end
    writeVideo(writerObj,getframe);
end
hold off
close(writerObj);