﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCTS_MRTA
{
    public class Allocation
    {
        const short gap = -1;
        public static IUtilityCalculator UCalc;
        readonly short[] alloc;
        readonly double utility;

        public Allocation(int nTasks, int nAgents)
        {
            //Empty allocation
            alloc = new short[nTasks + nAgents];
            for (int i = 0; i < nTasks + nAgents; i++)
                alloc[i] = gap;
            utility = 0.0;
        }

        public Allocation(Allocation parent, short taskid, int agentid)
        {
            alloc = parent.Schedule(taskid, agentid);
            utility = parent.utility + UCalc.GetUtilityForLastTask(GetSchedule(agentid), agentid);
        }

        public short[] GetSchedule(int agentid)
        {
            int i, start = -1, end = 0;
            for (i = 0; i < alloc.Length; i++)
            {
                if ((agentid == 0) && (start < 0))
                    start = i;
                if (alloc[i] == gap)
                    agentid--;
                if (agentid == -1)
                {
                    end = i;
                    break;
                }
            }
            short[] sch = new short[end - start];
            Array.Copy(alloc, start, sch, 0, end - start);
            return sch;
        }

        private short[] Schedule(short taskid, int agentid)
        {
            short[] nalloc = new short[alloc.Length];
            int insertpos = 0;
            for (int i = 0; i < alloc.Length; i++)
            {
                if (alloc[i] == gap)
                    agentid--;
                if (agentid == -1)
                {
                    insertpos = i;
                    break;
                }
            }
            Array.Copy(alloc, nalloc, insertpos);   //segment before insertion
            nalloc[insertpos] = taskid;
            Array.Copy(alloc, insertpos, nalloc, insertpos + 1, alloc.Length - insertpos - 1);
            return nalloc;
        }

        public void GetAllocatedTasks(ref bool[] taskScheduled)
        {
            for (int i = 1; i < alloc.Length; i++)
            {
                if (alloc[i] >= 0)
                    taskScheduled[alloc[i]] = true;
            }
        }

        public override int GetHashCode()
        {
            int hc = alloc[0] + 57;
            for (int i = 1; i < alloc.Length; i++)
            {
                hc = unchecked(hc * 401 + alloc[i]);
            }
            return hc;
        }

        public override string ToString()
        {
            int nAllocated = alloc.Count(e => e >= 0);
            return String.Format("Alloc{2}[{0}:u{1}]", nAllocated, utility, System.Convert.ToBase64String(BitConverter.GetBytes(GetHashCode())));
        }

        public string ToShortString()
        {
            return "<" + System.Convert.ToBase64String(BitConverter.GetBytes(GetHashCode())).Replace("==", "") + ">";
        }
    }
}
