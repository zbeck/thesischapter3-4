﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCTS_MRTA
{
    class Program
    {
        static void Main(string[] args)
        {
            //example from compareProblem_ObviousDiscovery_UAV-Share.txt low level problem
            double[,] dist = {{0, 16, 33.3333333333333, 88.0025252162939, 94.800375057861, 138.057958843379},
                            {16, 0, 18.5232586525997, 89.5643778395059, 92.7529574251469, 138.520756567382},
                            {33.3333333333333, 18.5232586525997, 0, 102.668831146015, 102.703456611742, 149.970367443409},
                            {88.0025252162939, 89.5643778395059, 102.668831146015, 0, 21.1869981094276, 50.2173055607106},
                            {94.800375057861, 92.7529574251469, 102.703456611742, 21.1869981094276, 0, 48.0740170061865},
                            {138.057958843379, 138.520756567382, 149.970367443409, 50.2173055607106, 48.0740170061865, 0},
                            {66.3257952165755, 79.3585394130826, 97.8752039872999, 68.4332602694853, 87.4960316560446, 110.805134257298},
                            {89.1839547103501, 76.0116950066092, 71.2179752590594, 87.0989220497144, 72.4706837279738, 116.300377375904}};

            //double[,] dist = 
            ParseDatFile(@"d:\VisualStudio\AgentDraw\MCTS_MRTA\TestProblems\Haiti11_discovery.dat");
            /*AllocationTree AT = new AllocationTree(dist);
            AT.Test();*/
            Console.ReadKey();
        }

        private static double[,] ParseDatFile(string p)
        {
            char[] universalTrimmer = {'\t', ' ', ';', ']', '['};
            int nAgents = 0, nTasks = 0;
            double[,] dist;
            List<double> ttValues = new List<double>();
            List<double> atValues = new List<double>();
            string[] lines = System.IO.File.ReadAllLines(p);
            for (int i = 0; i < 2/*lines.Length*/; i++)
            {
                string[] words = lines[i].Split('=');
                switch (words[0].Trim())
                {
                    case "nofAgents":
                        nAgents = Int32.Parse(words[1].Trim(universalTrimmer));
                        break;
                    case "nofTasks":
                        nTasks = Int32.Parse(words[1].Trim(universalTrimmer));
                        break;
                    case "TravelTT":
                        string[] ttfields = words[1].Split(',');
                        foreach (string field in ttfields)
                            ttValues.Add(Double.Parse(field.Trim(universalTrimmer)));
                        break;
                    case "TravelAT":
                        string[] atfields = words[1].Split(',');
                        foreach (string field in atfields)
                            atValues.Add(Double.Parse(field.Trim(universalTrimmer)));
                        break;
                }
                dist = new double[nTasks + nAgents, nTasks];
                using (var ttenum = ttValues.GetEnumerator())
                {
                    for (int x = 0; x < nTasks; x++)
                    {
                        for (int y = 0; y < nTasks; y++)
                        {
                            dist[x, y] = ttenum.Current;
                            ttenum.MoveNext();
                        }
                    }
                }
                using (var atenum = atValues.GetEnumerator())
                {
                    for (int x = nTasks; x < nTasks+nAgents; x++)
                    {
                        for (int y = 0; y < nTasks; y++)
                        {
                            dist[x, y] = atenum.Current;
                            atenum.MoveNext();
                        }
                    }
                }
            }
            return null;
        }
    }
}
