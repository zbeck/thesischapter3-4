﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCTS_MRTA
{
    public interface IUtilityCalculator
    {
        double GetUtilityForLastTask(short[] schedule, int agentid);
    }
}
