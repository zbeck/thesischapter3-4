﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyExtensions;

namespace MCTS_MRTA
{
    public class AllocationTreeNode
    {
        private static int nAgents=1;
        public readonly Allocation data;
        private List<AllocationTreeNode> parents;
        private List<AllocationTreeNode> children;
        //!TODO! have children sorted
        public AllocationTreeNode BestChild
        {
            get { return children[0]; }
        }

        public AllocationTreeNode(int nTasks, int NAgents)
        {
            nAgents = NAgents;
            parents = new List<AllocationTreeNode>(0);
            children = new List<AllocationTreeNode>();
            data = new Allocation(nTasks, NAgents);
        }

        public AllocationTreeNode(Allocation alloc, AllocationTreeNode parent)
        {
            parents = new List<AllocationTreeNode>(nAgents);
            children = new List<AllocationTreeNode>();
            parents.Add(parent);
            data = alloc;
        }

        public AllocationTreeNode AddChild(int taskid, int agentid)
        {
            AllocationTreeNode tn = new AllocationTreeNode(new Allocation(data, (short)taskid, agentid), this);
            children.Add(tn);
            return tn;
        }

        public override int GetHashCode()
        {
            return data.GetHashCode();
        }

        public override string ToString()
        {
            return String.Format("{0}par{1}, ch{2}", data, parents.ToDelimitedString(n => n.data.ToShortString(), ""), children.ToDelimitedString(n => n.data.ToShortString(), ""));
        }


        public string PrettyPrint(int p)
        {
            string output = new String(' ', p);
            output += ToString() + "\n";
            foreach (var n in children)
                output += n.PrettyPrint(p + 1);
            return output;
        }
    }
}
