﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCTS_MRTA
{
    public class AllocationTree: IUtilityCalculator
    {
        //!TODO! battery check is not implemented


        //!TODO! copied from TreeStructure for now, fix dependency structure and have it in one place!!
        public static double U0 = 1;
        //public static double lambda = -1.0 / 432000.0;
        public static double lambda = -1.0 / 8000.0;    //makes a larger difference of utilities when the simulation only goes for 6000 time
        //END TODO

        AllocationTreeNode root;
        double[,] distTable;
        //!TODO! not in use yet
        double[] activationTime;
        public int NAgents, NTasks;
        Random rnd;

        public AllocationTree(double[,] dist)
        {
            rnd = new Random();
            NTasks = dist.GetLength(1);
            NAgents = dist.GetLength(0) - NTasks;
            if (NAgents <= 0)
                throw new Exception("Wrong distance table format in AllocationTree constructor!");
            distTable = dist;
            Allocation.UCalc = this;
            root = new AllocationTreeNode(NTasks, NAgents);
            ApplyDefaultPolicy(root);
        }

        private void ApplyDefaultPolicy(AllocationTreeNode node)
        {
            //!TODO! This could be enhanced by native calculation at Allocation by checking the alloc array
            short[][] allocs = new short[NAgents][];
            int[] agentLocation = new int[NAgents];
            double[] agentTime = new double[NAgents];
            bool[] taskScheduled = new bool[NTasks];
            for (int a = 0; a < NAgents; a++)
            {
                allocs[a] = node.data.GetSchedule(a);
                if (allocs[a].Length > 0)
                    agentLocation[a] = allocs[a][allocs[a].Length - 1];
                else
                    agentLocation[a] = NTasks + a;
                foreach (var t in allocs[a])
                    taskScheduled[t] = true;
                agentTime[a] = GetTimeOfLastTask(allocs[a], a);
            }
            //END TODO
            AllocationTreeNode currentNode = node;
            while (!Array.TrueForAll(taskScheduled, a => a))
            {
                double mintime = Double.PositiveInfinity;
                int bestTask = -1;
                int bestAgent = -1;
                for (int a = 0; a < NAgents; a++)
                {
                    for (int t = 0; t < NTasks; t++)
                    {
                        if (!taskScheduled[t])
                        {
                            double currentT = agentTime[a] + distTable[agentLocation[a], t];
                            if (currentT < mintime)
                            {
                                mintime = currentT;
                                bestTask = t;
                                bestAgent = a;
                            }
                        }
                    }
                }
                if (Double.IsPositiveInfinity(mintime))
                    break;
                //we only keep track of the locations and times of agents but not their allocations
                agentLocation[bestAgent] = bestTask;
                agentTime[bestAgent] = mintime;
                taskScheduled[bestTask] = true;
                currentNode = currentNode.AddChild(bestTask, bestAgent);
            }
        }

        private void Expand(AllocationTreeNode node)
        {
            bool[] taskScheduled = new bool[NTasks];
            node.data.GetAllocatedTasks(ref taskScheduled);
            int taskid = MyExtensions.MyExtensionMethods.GetRandomIndex(taskScheduled, rnd);
            int agentid = rnd.Next(NAgents);
            AllocationTreeNode newNode = node.AddChild(taskid, agentid);
            ApplyDefaultPolicy(newNode);
        }

        private double GetTimeOfLastTask(short[] schedule, int agentid)
        {
            if (schedule.Length < 1)
                return 0.0;
            double t = distTable[NTasks + agentid, schedule[0]];
            for (int i = 1; i < schedule.Length; i++)
            {
                t += distTable[i - 1, i];
            }
            return t;
        }

        double IUtilityCalculator.GetUtilityForLastTask(short[] schedule, int agentid)
        {
            if (schedule.Length < 1)
                return 0.0;
            double t = GetTimeOfLastTask(schedule, agentid);
            return Math.Max(0.0, U0 + t * lambda);
        }

        public override string ToString()
        {
            string os = "Allocation Tree:\n\n";
            os += root.PrettyPrint(0);
            return os;
        }

        public void Test()
        {
            Console.WriteLine(this);
            Expand(root.BestChild);

            Console.WriteLine(this);
            Expand(root);

            Console.WriteLine(this);
        }
    }
}
